USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  UserDefinedFunction [dbo].[fnAssignedWorkQueueByMapGrid]    Script Date: 4/22/2019 2:18:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fnAssignedWorkQueueByMapGrid](@SearchString varchar(200) = '')


RETURNS @WorkQueue TABLE
(
	[MapGrid] varchar(200) NULL
	,[AssignedUser] varchar(200)     
	,[ComplianceStart] date NULL        
	,[ComplianceEnd] date NULL          
	,[InspectionAttemptcounter] int NULL 
	,[SectionFlag] int NULL             
	,[AssignedWorkOrderCount] int NULL  
	,[AssignedCount] varchar(10) NULL 
	--,[AssignedCount2] varchar(10) NULL  
	,[Percent Completed] float NULL     
	,[Total] int NULL                   
	,[Remaining] int NULL 
			--- added on 12/27
		,InspectionType varchar(50)
		,BillingCode varchar(50)
		-----              
	,[UserID] int NULL                  
	,[TechName] varchar(200) NULL 
	,[InProgressFlag] bit NULL      
	,[OfficeName] varchar(50)
)

AS




/*

	Testing
	Select * From fnAssignedWorkQueueByMapGrid('')
	Select * From fnAssignedWorkQueueByMapGrid('Jose')
	Select * From fnAssignedWorkQueueByMapGrid('tech')
	Select * From fnAssignedWorkQueueByMapGrid('Bar')
	Select * From fnAssignedWorkQueueByMapGrid('110-08-3')


*/
BEGIN




	If COALESCE(@SearchString, '') = ''
	BEGIN
	
		Insert Into @WorkQueue
		(
			[MapGrid]  
			,[AssignedUser]      
			,[ComplianceStart]       
			,[ComplianceEnd]         
			,[InspectionAttemptcounter]
			,[SectionFlag]            
			,[AssignedWorkOrderCount] 
			,[AssignedCount]  
			--,[AssignedCount2] 
			,[Percent Completed]    
			,[Total]                  
			,[Remaining]
			 --- added on 12/27
		,InspectionType
		,Billingcode
		-----  
			,[InProgressFlag]
			,[OfficeName]
	    
		)
		
		select 
		MapGrid
		, AssignedUser
		, ComplianceStart
		, ComplianceEnd
		, Sum(InspectionAttemptcounter) [InspectionAttemptcounter]
		, SectionFlag
		, Sum(AssignedWorkOrderCount) [AssignedWorkOrderCount]
		, AssignedCount
		, [Percent Completed]
		, Total
		, Remaining
		--- added on 12/27
		,InspectionType
		,Billingcode
		-----
		, CASE WHEN Sum(TotalInProgress) > 0 THEN 1 ELSE 0 END [InProgressFlag]
		,OfficeName
		from
		(
		SELECT 
					wo.MapGrid
					,CASE WHEN 
						  (
						   select count(distinct AssignedUserID ) from 
						   tworkOrder wo2, tworkQueue Wq2
						   where wo2.Mapgrid = WO.Mapgrid 
						   and	WO2.BillingCode	= WO.BillingCode
					       and	WO2.InspectionType = WO.InspectionType
					       and	WO2.OfficeName = wo.OfficeName
						   and wo2.ID = wq2.WorkOrderID
						   and wq2.WorkQueueStatus in (100,101)
						   and wo2.completedflag=0
						   ) > 1 THEN  'MANY' ELSE UserFirstName + ' ' +  UserLastName + ' (' + UserName + ')'  END [AssignedUser]
					--,CASE WHEN count(distinct AssignedUserID ) = 1 THEN UserFirstName + ' ' +  UserLastName + ' (' + UserName + ')' ELSE 'MANY' END --AssignedCount
					,Min(ComplianceStart) [ComplianceStart]
					,Max(ComplianceEnd) [ComplianceEnd]
					,sum(InspectionAttemptCounter) as  InspectionAttemptcounter 
	
					,CASE WHEN 
						  (
						   select count(*) from [dbo].tWorkOrder WO1
						   where WO1.Mapgrid = WO.Mapgrid 
						   and WO1.SectionNumber is not NULL
						   ) > 0 THEN 1 ELSE 0 END 
						   as SectionFlag
					,count(distinct WQ.WorkOrderID) as AssignedWorkOrderCount
					,count(distinct InProgress.WorkOrderID) as TotalInProgress
					,CASE WHEN 
						  (
						   select count(distinct AssignedUserID ) from 
						   tworkOrder wo2, tworkQueue Wq2
						   where wo2.Mapgrid = WO.Mapgrid 
						      and	WO2.BillingCode	= WO.BillingCode
					       and	WO2.InspectionType = WO.InspectionType
					       and	WO2.OfficeName = wo.OfficeName
						   and wo2.ID = wq2.WorkOrderID
						   and wq2.WorkQueueStatus in (100,101)
							  and wo2.completedflag=0
						   ) > 1 THEN  'MANY' ELSE '1' END [AssignedCount]
					,mgp.[Percent Completed]
					,mgp.Total [Total]
					,mgp.Remaining [Remaining]
					,WO.BillingCode
					,WO.InspectionType
					,wo.OfficeName
					
				FROM (Select * From [dbo].[tWorkOrder] where Completedflag = 0 ) WO 
					 join (select * from [dbo].tWorkQueue where WorkQueueStatus in (100,101))  WQ on WQ.Workorderid = WO.ID
					Left join (select * from [UserTb] ) U on WQ.AssignedUserID = U.Userid 
					left join [dbo].[vMapGridPercentage_ITBCON] mgp on wo.MapGrid = mgp.MapGrid  
					and WO.BillingCode =  mgp.Billingcode 
					and WO.InspectionType = mgp.InspectionType 
					and wo.OfficeName = mgp.officename
					Left Join (Select * from tWorkQueue where WorkQueueStatus = 101) InProgress on wo.ID = InProgress.WorkOrderID
				group by wo.Mapgrid --, WQ.WorkOrderID 
					, UserFirstName + ' ' +  UserLastName + ' (' + UserName + ')' 
					, mgp.[Percent Completed]
					, mgp.Total

					, mgp.Remaining
					,WO.BillingCode
					,WO.InspectionType
					,wo.OfficeName) Source
		Group By MapGrid
		, AssignedUser
		, ComplianceStart
		, ComplianceEnd
		--, InspectionAttemptcounter
		, SectionFlag
		--, Sum(AssignedWorkOrderCount) [AssignedWorkOrderCount]
		, AssignedCount
		, [Percent Completed]
		, Total
		, Remaining
	   --- added on 12/27
		,InspectionType
		,Billingcode
		,OfficeName
		-----
		--, CASE WHEN TotalInProgress > 0 THEN 1 ELSE 0 END
		Order by MapGrid


	END
	ELSE
	BEGIN

		set @SearchString = '%' +  @SearchString + '%'

		
		Insert Into @WorkQueue
		(
			[MapGrid]
			,[AssignedUser]        
			,[ComplianceStart]       
			,[ComplianceEnd]         
			,[InspectionAttemptcounter]
			,[SectionFlag]            
			,[AssignedWorkOrderCount] 
			,[AssignedCount]   
			,[Percent Completed]    
			,[Total]                  
			,[Remaining]
					--- added on 12/27
		,InspectionType
		,Billingcode
		----- 
			,[InProgressFlag]
			,OfficeName 
			
		)
		select 
			MapGrid
			, AssignedUser
			, ComplianceStart
			, ComplianceEnd
			, Sum(InspectionAttemptcounter) [InspectionAttemptcounter]
			, SectionFlag
			, Sum(AssignedWorkOrderCount) [AssignedWorkOrderCount]
			, AssignedCount
			, [Percent Completed]
			, Total
			, Remaining
		--- added on 12/27
		,InspectionType
		,Billingcode
		-----
			, CASE WHEN Sum(TotalInProgress) > 0 THEN 1 ELSE 0 END [InProgressFlag]
		,OfficeName
		
		From
		(SELECT wo.MapGrid
		,CASE WHEN 
				  (
				   select count(distinct AssignedUserID ) from 
				   tworkOrder wo2, tworkQueue Wq2
				   where wo2.Mapgrid = WO.Mapgrid
				   and	WO2.BillingCode	= WO.BillingCode 
				   	 and	WO2.InspectionType = WO.InspectionType
					 and	WO2.OfficeName = wo.OfficeName
					and wo2.ID = wq2.WorkOrderID
				   --and wo2.ID = wq2.WorkOrderID
				   and wq2.WorkQueueStatus in (100,101)
					  and wo2.completedflag=0
				   ) > 1 THEN  'MANY' ELSE UserFirstName + ' ' +  UserLastName + ' (' + UserName + ')'  END [AssignedUser]
			,Min(ComplianceStart) [ComplianceStart]
			,Max(ComplianceEnd) [ComplianceEnd]
			,sum(InspectionAttemptCounter) as  InspectionAttemptcounter 
			,CASE WHEN 
				(
				select count(*) from [dbo].tWorkOrder WO1
				where WO1.Mapgrid = WO.Mapgrid 
				and WO1.SectionNumber is not NULL
				) > 0 THEN 1 ELSE 0 END 
				as SectionFlag
			,count(distinct WQ.WorkOrderID) as AssignedWorkOrderCount
			,count(distinct InProgress.WorkOrderID) as TotalInProgress
			,CASE WHEN 
				  (
				   select count(distinct AssignedUserID ) from 
				   tworkOrder wo2, tworkQueue Wq2
				   where wo2.Mapgrid = WO.Mapgrid 
				      and	WO2.BillingCode	= WO.BillingCode
					       and	WO2.InspectionType = WO.InspectionType
					       and	WO2.OfficeName = wo.OfficeName
				   and wo2.ID = wq2.WorkOrderID
				   and wq2.WorkQueueStatus in (100,101)
					  and wo2.completedflag=0
				   ) > 1 THEN  'MANY' ELSE '1' END AssignedCount
			,mgp.[Percent Completed]
			,mgp.Total [Total]
			,mgp.Remaining [Remaining]
					,WO.BillingCode
					,WO.InspectionType
					,WO.OfficeName
			--,u.UserID
			--,UserFirstName + ' ' +  UserLastName + ' (' + UserName + ')' 
		FROM (Select * From [dbo].[tWorkOrder]  where Completedflag = 0 ) WO 
			 join (select * from [dbo].tWorkQueue where WorkQueueStatus in (100,101))  WQ on WQ.Workorderid = WO.ID 
			Left join (select * from [UserTb] ) U on WQ.AssignedUserID = U.Userid 
			left join [dbo].[vMapGridPercentage_ITBCON] mgp on wo.MapGrid = mgp.MapGrid 
			and WO.BillingCode = mgp.Billingcode 
			and WO.InspectionType = mgp.InspectionType and WO.OfficeName = mgp.officename

			Left Join (Select * from tWorkQueue where WorkQueueStatus = 101) InProgress on wo.ID = InProgress.WorkOrderID
		Where u.UserFirstName like @SearchString
			or u.UserLastName  like @SearchString 
			or u.UserName  like @SearchString 
			or wo.MapGrid  like @SearchString 
			--- new line added below
			or WO.officename like @searchstring
			or WO.BillingCode like @searchstring
			or WO.InspectionType like @searchstring
				
			or Cast(wo.ComplianceStart as varchar(10)) like @SearchString 
			or Cast(wo.ComplianceEnd as varchar(10)) like @SearchString 
		group by wo.Mapgrid		
			,mgp.[Percent Completed]
			, mgp.Total
			, mgp.Remaining
			,WO.BillingCode
			,WO.InspectionType
			,wo.OfficeName
			, u.UserID
			, UserFirstName + ' ' +  UserLastName + ' (' + UserName + ')' ) Source
		Group By MapGrid
		, AssignedUser
		, ComplianceStart
		, ComplianceEnd
		--, InspectionAttemptcounter
		, SectionFlag
		--, Sum(AssignedWorkOrderCount) [AssignedWorkOrderCount]
		, AssignedCount
		, [Percent Completed]
		, Total
		, Remaining
		--- added on 12/27
		,InspectionType
		,Billingcode
		,OfficeName
		-----
		Order by MapGrid

	END

RETURN

END
















GO


