USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  UserDefinedFunction [dbo].[fnAssignedWorkQueueByMapGridCount]    Script Date: 4/22/2019 2:18:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fnAssignedWorkQueueByMapGridCount](@SearchString varchar(200) = '')


RETURNS @WorkQueueCount TABLE
(
	WorkQueueCount int
)

AS




/*

	Testing
	Select * From fnAssignedWorkQueueByMapGridCount('')
	Select * From fnAssignedWorkQueueByMapGridCount('JSILMON')
	Select * From fnAssignedWorkQueueByMapGridCount('tech')
	Select * From fnAssignedWorkQueueByMapGridCount('Bar')
	Select * From fnAssignedWorkQueueByMapGridCount('110-08-3')


*/
BEGIN




	If COALESCE(@SearchString, '') = ''
	BEGIN
	
		Insert Into @WorkQueueCount
		(
			WorkQueueCount
	    
		)
		
		Select Count(*)
		From
		(
		
		select 
		MapGrid
		, AssignedUser
		, ComplianceStart
		, ComplianceEnd
		, Sum(InspectionAttemptcounter) [InspectionAttemptcounter]
		, SectionFlag
		, Sum(AssignedWorkOrderCount) [AssignedWorkOrderCount]
		, AssignedCount
		, [Percent Completed]
		, Total
		, Remaining
		--- added on 12/27
		,InspectionType
		,Billingcode
		-----
		, CASE WHEN Sum(TotalInProgress) > 0 THEN 1 ELSE 0 END [InProgressFlag]
		,OfficeName
		from
		(
		SELECT 
					wo.MapGrid
					,0 [AssignedUser]
					--,CASE WHEN count(distinct AssignedUserID ) = 1 THEN UserFirstName + ' ' +  UserLastName + ' (' + UserName + ')' ELSE 'MANY' END --AssignedCount
					,Min(ComplianceStart) [ComplianceStart]
					,Max(ComplianceEnd) [ComplianceEnd]
					,sum(InspectionAttemptCounter) as  InspectionAttemptcounter 
	
					,0 SectionFlag
					,count(distinct WQ.WorkOrderID) as AssignedWorkOrderCount
					,count(distinct InProgress.WorkOrderID) as TotalInProgress
					,0 [AssignedCount]
					,mgp.[Percent Completed]
					,mgp.Total [Total]
					,mgp.Remaining [Remaining]
					,WO.BillingCode
					,WO.InspectionType
					,wo.OfficeName
					
				FROM (Select * From [dbo].[tWorkOrder] where Completedflag = 0 ) WO 
					 join (select * from [dbo].tWorkQueue where WorkQueueStatus in (100,101))  WQ on WQ.Workorderid = WO.ID
					Left join (select * from [UserTb] ) U on WQ.AssignedUserID = U.Userid 
					left join [dbo].[vMapGridPercentage_ITBCON] mgp on wo.MapGrid = mgp.MapGrid  
					and WO.BillingCode =  mgp.Billingcode 
					and WO.InspectionType = mgp.InspectionType 
					and wo.OfficeName = mgp.officename
					Left Join (Select * from tWorkQueue where WorkQueueStatus = 101) InProgress on wo.ID = InProgress.WorkOrderID
				group by wo.Mapgrid --, WQ.WorkOrderID 
					, UserFirstName + ' ' +  UserLastName + ' (' + UserName + ')' 
					, mgp.[Percent Completed]
					, mgp.Total

					, mgp.Remaining
					,WO.BillingCode
					,WO.InspectionType
					,wo.OfficeName) Source
		Group By MapGrid
		, AssignedUser
		, ComplianceStart
		, ComplianceEnd
		--, InspectionAttemptcounter
		, SectionFlag
		--, Sum(AssignedWorkOrderCount) [AssignedWorkOrderCount]
		, AssignedCount
		, [Percent Completed]
		, Total
		, Remaining
	   --- added on 12/27
		,InspectionType
		,Billingcode
		,OfficeName
		) Source


	END
	ELSE
	BEGIN

		set @SearchString = '%' +  @SearchString + '%'

		
		Insert Into @WorkQueueCount
		(
			WorkQueueCount
	    
		)
		
		Select Count(*)
		From
		(
		select 
			MapGrid
			, AssignedUser
			, ComplianceStart
			, ComplianceEnd
			, Sum(InspectionAttemptcounter) [InspectionAttemptcounter]
			, SectionFlag
			, Sum(AssignedWorkOrderCount) [AssignedWorkOrderCount]
			, AssignedCount
			, [Percent Completed]
			, Total
			, Remaining
		--- added on 12/27
		,InspectionType
		,Billingcode
		-----
			, CASE WHEN Sum(TotalInProgress) > 0 THEN 1 ELSE 0 END [InProgressFlag]
		,OfficeName
		
		From
		(SELECT wo.MapGrid
		,0 [AssignedUser]
			,Min(ComplianceStart) [ComplianceStart]
			,Max(ComplianceEnd) [ComplianceEnd]
			,sum(InspectionAttemptCounter) as  InspectionAttemptcounter 
			,0 SectionFlag
			,count(distinct WQ.WorkOrderID) as AssignedWorkOrderCount
			,count(distinct InProgress.WorkOrderID) as TotalInProgress
			,0 AssignedCount
			,mgp.[Percent Completed]
			,mgp.Total [Total]
			,mgp.Remaining [Remaining]
					,WO.BillingCode
					,WO.InspectionType
					,WO.OfficeName
			--,u.UserID
			--,UserFirstName + ' ' +  UserLastName + ' (' + UserName + ')' 
		FROM (Select * From [dbo].[tWorkOrder]  where Completedflag = 0 ) WO 
			 join (select * from [dbo].tWorkQueue where WorkQueueStatus in (100,101))  WQ on WQ.Workorderid = WO.ID 
			Left join (select * from [UserTb] ) U on WQ.AssignedUserID = U.Userid 
			left join [dbo].[vMapGridPercentage_ITBCON] mgp on wo.MapGrid = mgp.MapGrid 
			and WO.BillingCode = mgp.Billingcode 
			and WO.InspectionType = mgp.InspectionType and WO.OfficeName = mgp.officename

			Left Join (Select * from tWorkQueue where WorkQueueStatus = 101) InProgress on wo.ID = InProgress.WorkOrderID
		Where u.UserFirstName like @SearchString
			or u.UserLastName  like @SearchString 
			or u.UserName  like @SearchString 
			or wo.MapGrid  like @SearchString 
			--- new line added below
			or WO.officename like @searchstring
			or WO.BillingCode like @searchstring
			or WO.InspectionType like @searchstring
				
			or Cast(wo.ComplianceStart as varchar(10)) like @SearchString 
			or Cast(wo.ComplianceEnd as varchar(10)) like @SearchString 
		group by wo.Mapgrid		
			,mgp.[Percent Completed]
			, mgp.Total
			, mgp.Remaining
			,WO.BillingCode
			,WO.InspectionType
			,wo.OfficeName
			, u.UserID
			, UserFirstName + ' ' +  UserLastName + ' (' + UserName + ')' ) Source
		Group By MapGrid
		, AssignedUser
		, ComplianceStart
		, ComplianceEnd
		--, InspectionAttemptcounter
		, SectionFlag
		--, Sum(AssignedWorkOrderCount) [AssignedWorkOrderCount]
		, AssignedCount
		, [Percent Completed]
		, Total
		, Remaining
		--- added on 12/27
		,InspectionType
		,Billingcode
		,OfficeName
		) Source

	END

RETURN

END






GO


