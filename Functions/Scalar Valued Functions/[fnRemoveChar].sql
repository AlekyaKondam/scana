USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  UserDefinedFunction [dbo].[fnRemoveChar]    Script Date: 4/22/2019 2:20:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnRemoveChar] 
( 
       @Text		varchar(max)

)
RETURNS varchar(max) AS

/*****************************************************************************************************************
	NAME:		[fnRemoveChar]
	AUTHOR:		Gary Wheeler
	CREATED:	20170607

	**** REVISION HISTORY ***
	Developer		Date		    Description
	Gary Wheeler	20170607		Re Build


	**** TEST BLOCK ***

	
	

******************************************************************************************************************/

BEGIN

	Declare @MaxChar int = 127
		,@MinChar int = 32
		,@CurrentChar int
		,@CurrentLoc int = 1
		,@Length int = LEN(@Text) + 1

    --DECLARE @Characters TABLE(NonPrintable char(1) NOT NULL)
	


	
	WHILE @CurrentLoc < @Length
	BEGIN

		Set @CurrentChar = ascii(SUBSTRING(@Text, @CurrentLoc, 1))

		If @CurrentChar < @MinChar or @CurrentChar > @MaxChar
		BEGIN

			IF @CurrentChar = 13
			BEGIN

				Select @Text = REPLACE(@Text, @CurrentChar, ' ')

			END
			ELSE
			BEGIN

				Select @Text = REPLACE(@Text, @CurrentChar, '')

			END

		END

		Set @CurrentLoc = @CurrentLoc + 1

	END
		
	RETURN LTRIM(RTRIM(@Text))

END


GO


