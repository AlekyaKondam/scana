USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  UserDefinedFunction [dbo].[fnIsWithinDate]    Script Date: 4/22/2019 2:20:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE FUNCTION [dbo].[fnIsWithinDate] 
( 
       @InspectionDate		Date
      ,@ComplianceDate		Date
      ,@NumberOfDay			int
      
)
--RETURNS FLOAT(18) AS
RETURNS Bit AS


/*****************************************************************************************************************
	NAME:		[fnIsWithingDate]
	AUTHOR:		Gary Wheeler
	CREATED:	10/9/2017

	**** PURPOSE ****
	Return a True of False if the Number of Days Difference between Inspection Date and Compliance Date is Grater Than Equal To (>=) the Number of Days
	And Inspection Date is less than (<) the Compliance Date
	
	
	**** REVISION HISTORY ***
	Developer		Date		    Description
	


	**** TEST BLOCK ***

	Declare
		@InspectionDate		Date
		,@ComplianceDate		Date
		,@NumberOfDay			int


	SET @InspectionDate = '9/1/2017'
	SET @ComplianceDate = '9/15/2017'
	Set @NumberOfDay = 10

	SELECT [dbo].[fnIsWithinDate](@InspectionDate, @ComplianceDate, @NumberOfDay )

******************************************************************************************************************/

BEGIN
    
	Declare @ReturnVal bit

	If (@InspectionDate < @ComplianceDate) and DateDiff(d, @InspectionDate, @ComplianceDate) <= @NumberOfDay
	BEGIN
	
		Set @ReturnVal = 1
		

	END
	ELSE
	BEGIN

		Set @ReturnVal = 0
	
	END

		
	RETURN @ReturnVal

END





GO


