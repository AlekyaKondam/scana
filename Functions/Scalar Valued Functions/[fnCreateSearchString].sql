USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  UserDefinedFunction [dbo].[fnCreateSearchString]    Script Date: 4/22/2019 2:19:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fnCreateSearchString] 
( 
       @MapGrid varchar(50)
	  ,@SectionNum varchar(50)
	  ,@Type varchar(50)
	  ,@InspectionType varchar(50)
	  ,@BillingCode varchar(50)
      
)

RETURNS varchar(4000) AS


BEGIN

	Declare @ReturnVal varchar(4000)

	IF @Type = 'ID'
	BEGIN

		select @ReturnVal = COALESCE(@ReturnVal + ',', '') + Cast(s.UserID as varchar(10))
		From
		(SELECT distinct Userid 
				   FROM (select * from tworkqueue where  WorkQueueStatus <> 102) wq2
				   join Usertb u2 on u2.Userid = wq2.AssignedUserID
				   Join (select * from tWorkOrder where completedflag = 0) wo2 on wo2.id = wq2.WorkOrderID
				  WHERE 
				   wo2.MapGrid = @MapGrid
				   and ISNULL(wq2.sectionnumber, 0) = isNULL(@SectionNum, 0)
				   and wo2.InspectionType = @InspectionType
				   and wo2.BillingCode = @BillingCode) s
    
	END

	IF @Type = 'NAME'
	BEGIN

		

		select @ReturnVal = COALESCE(@ReturnVal + ',', '') + s.UserFirstName + ' ' + s.UserLastName + ' (' + s.UserName + ')'
		From
		(SELECT distinct UserFirstName, UserLastName, UserName
				   FROM (select * from tworkqueue where  WorkQueueStatus <> 102) wq2
				   join Usertb u2 on u2.Userid = wq2.AssignedUserID
				   Join (select * from tWorkOrder where completedflag = 0) wo2 on wo2.id = wq2.WorkOrderID
				  WHERE 
				   wo2.MapGrid = @MapGrid
				   and ISNULL(wq2.sectionnumber, 0) = isNULL(@SectionNum, 0)
				   and wo2.InspectionType = @InspectionType
				   and wo2.BillingCode = @BillingCode) s
    
	END

		
	RETURN @ReturnVal

END










GO


