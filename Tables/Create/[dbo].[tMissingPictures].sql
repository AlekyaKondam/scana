USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[tMissingPictures]    Script Date: 4/17/2019 4:00:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tMissingPictures](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateDiscovered] [datetime] NULL,
	[PhotoFileName] [varchar](500) NULL,
	[Tech] [varchar](50) NULL,
	[DateTaken] [date] NULL,
 CONSTRAINT [PK_tMissingPictures] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tMissingPictures] ADD  CONSTRAINT [DF_tMissingPictures_DateDiscovered]  DEFAULT (getdate()) FOR [DateDiscovered]
GO


