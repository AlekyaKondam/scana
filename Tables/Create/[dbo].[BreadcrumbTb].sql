
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BreadcrumbTb](
	[BreadcrumbID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[BreadcrumbUID] [varchar](100) NOT NULL,
	[BreadcrumbActivityUID] [varchar](100) NULL,
	[BreadcrumbSourceID] [varchar](100) NULL,
	[BreadcrumbCreatedUserUID] [varchar](100) NOT NULL,
	[BreadcrumbSrcDTLT] [datetime] NOT NULL,
	[BreadcrumbSrvDTLTOffset] [datetimeoffset](7) NULL,
	[BreadcrumbSrvDTLT] [datetime] NULL CONSTRAINT [DF_BreadcrumbTb_BreadcrumbSrvDTLT]  DEFAULT (getdate()),
	[BreadcrumbGPSType] [varchar](100) NULL,
	[BreadcrumbGPSSentence] [varchar](400) NULL,
	[BreadcrumbLatitude] [float] NULL,
	[BreadcrumbLongitude] [float] NULL,
	[BreadcrumbShape] [geography] NULL,
	[BreadcrumbActivityType] [varchar](50) NULL,
	[BreadcrumbWorkQueueFilter] [varchar](200) NULL,
	[BreadcrumbBatteryLevel] [float] NULL,
	[BreadcrumbGPSTime] [datetime] NULL,
	[BreadcrumbSpeed] [float] NULL,
	[BreadcrumbHeading] [varchar](50) NULL,
	[BreadcrumbGPSAccuracy] [varchar](50) NULL,
	[BreadcrumbSatellites] [int] NULL,
	[BreadcrumbAltitude] [float] NULL,
	[BreadcrumbTrackingGroupID] [int] NULL,
	[BreadcrumbMapPlat] [varchar](50) NULL,
	[BreadcrumbArchiveFlag] [bit] NULL,
	[BreadcrumbComments] [varchar](500) NULL,
	[BreadcrumbCreatedDate] [datetime] NULL,
	[BreadcrumbDeviceID] [varchar](50) NULL,
	[OrderProcessed] [bit] NULL CONSTRAINT [DF_BreadcrumbTb_OrderProcessed]  DEFAULT ((0)),
	[IsStationary] [bit] NULL CONSTRAINT [DF_BreadcrumbTb_IsStationary_1]  DEFAULT ((0)),
	[PaceOfTravel] [decimal](10, 5) NULL CONSTRAINT [DF_BreadcrumbTb_PaceOfTravel_1]  DEFAULT ((0)),
	[IsDistanceBased] [bit] NULL,
	[DistanceTraveled] [decimal](18, 10) NULL CONSTRAINT [DF_BreadcrumbTb_DistanceTraveled]  DEFAULT ((0)),
	[OriginBreadcrumbLatitude] [float] NULL,
	[OriginBreadcrumbLongitude] [float] NULL,
	[SpeedAttribute] [varchar](50) NULL,
 CONSTRAINT [PK_BreadcrumbTb] PRIMARY KEY CLUSTERED 
(
	[BreadcrumbID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


