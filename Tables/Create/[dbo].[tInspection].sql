
/****** Object:  Table [dbo].[tInspection]    Script Date: 4/17/2019 3:59:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tInspection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InspectionTabletID] [varchar](100) NULL,
	[ActivityID] [int] NULL,
	[WorkQueueID] [int] NULL,
	[WorkQueueStatus] [int] NULL,
	[MapGrid] [varchar](50) NULL,
	[IsAdHocFlag] [bit] NULL CONSTRAINT [DF_tInspection_IsAdHocFlag]  DEFAULT ((0)),
	[IsInGridFlag] [bit] NULL CONSTRAINT [DF_tInspection_IsInGridFlag]  DEFAULT ((0)),
	[IsCGEFlag] [bit] NULL CONSTRAINT [DF_tInspection_IsCGEFlag]  DEFAULT ((0)),
	[IsAOCFlag] [bit] NULL CONSTRAINT [DF_tInspection_IsAOCFlag]  DEFAULT ((0)),
	[IsIndicationFlag] [bit] NULL CONSTRAINT [DF_tInspection_IsIndicationFlag]  DEFAULT ((0)),
	[IsPipelineFlag] [bit] NULL CONSTRAINT [DF_tInspection_IsPipelineFlag]  DEFAULT ((0)),
	[AGLeakCounter] [int] NULL CONSTRAINT [DF_tInspection_AGLeakCounter]  DEFAULT ((0)),
	[BGLeakCounter] [int] NULL CONSTRAINT [DF_tInspection_BGLeakCounter]  DEFAULT ((0)),
	[Grade1Counter] [int] NULL CONSTRAINT [DF_tInspection_Grade1Counter]  DEFAULT ((0)),
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[AssetID] [int] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[GPSType] [varchar](100) NULL,
	[GPSSentence] [varchar](max) NULL,
	[GPSTime] [varchar](50) NULL,
	[FixQuality] [float] NULL,
	[NumberOfSatellites] [int] NULL,
	[HDOP] [float] NULL,
	[AltitudeMetersAboveMeanSeaLevel] [float] NULL,
	[HeightOfGeoid] [float] NULL,
	[TimeSecondsSinceLastDGPS] [float] NULL,
	[ChecksumData] [varchar](50) NULL,
	[Bearing] [float] NULL,
	[Speed] [float] NULL,
	[NumberOfGPSAttempts] [int] NULL,
	[SrvDTLT] [datetime] NULL CONSTRAINT [DF_tInspection_SrvDTLT]  DEFAULT (getdate()),
	[SrvDTLTOffset] [datetimeoffset](7) NULL CONSTRAINT [DF_tInspection_SrvDTLTOffset]  DEFAULT (sysdatetimeoffset()),
	[IsWorkOrderUpdated] [bit] NULL,
	[Photo1Path] [varchar](500) NULL,
	[PipelineType] [varchar](200) NULL,
	[Comments] [varchar](500) NULL,
	[HardToLocateFlag] [bit] NULL,
	[MeterNumberPhoto1Path] [varchar](500) NULL,
	[MeterNumberPhoto2Path] [varchar](500) NULL,
	[MeterNumberPhoto3Path] [varchar](500) NULL,
	[TaskOutActivityUID] [varchar](200) NULL,
 CONSTRAINT [PK_tInspection] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tInspection]  WITH CHECK ADD  CONSTRAINT [FK_tInspection_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserTb] ([UserID])
GO

ALTER TABLE [dbo].[tInspection] CHECK CONSTRAINT [FK_tInspection_CreatedBy]
GO


