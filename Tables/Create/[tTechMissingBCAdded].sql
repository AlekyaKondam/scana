USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[tTechMissingBCAdded]    Script Date: 4/22/2019 2:08:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tTechMissingBCAdded](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MissingBCDate] [date] NULL,
	[UserUID] [varchar](50) NULL,
	[DeviceID] [varchar](50) NULL,
	[ActivityCount] [int] NULL,
	[BCCount] [int] NULL,
	[TotalInserted] [int] NULL,
	[CompareType] [varchar](50) NULL,
	[InsertedDateTime] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tTechMissingBCAdded] ADD  CONSTRAINT [DF_tTechMissingBCAdded_TotalInserted]  DEFAULT ((0)) FOR [TotalInserted]
GO

ALTER TABLE [dbo].[tTechMissingBCAdded] ADD  CONSTRAINT [DF_tTechMissingBCAdded_InsertedDateTime]  DEFAULT (getdate()) FOR [InsertedDateTime]
GO


