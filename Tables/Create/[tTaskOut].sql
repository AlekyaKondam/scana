USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[tTaskOut]    Script Date: 4/22/2019 2:08:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tTaskOut](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NOT NULL,
	[AboveGroundLeakCount] [int] NULL CONSTRAINT [DF_tTaskOut_AboveGroundLeakCount]  DEFAULT ((0)),
	[BelowGroundLeakCount] [int] NULL CONSTRAINT [DF_tTaskOut_BelowGroundLeakCount]  DEFAULT ((0)),
	[ServicesCount] [int] NULL CONSTRAINT [DF_tTaskOut_ServicesCount]  DEFAULT ((0)),
	[FeetOfMain] [int] NULL CONSTRAINT [DF_tTaskOut_FeetOfMain]  DEFAULT ((0)),
	[CreatedUserID] [int] NOT NULL,
	[SrcDTLT] [datetime] NULL,
	[SrvDTLT] [datetime] NULL CONSTRAINT [DF_tTaskOut_SrvDTLT]  DEFAULT (getdate()),
	[SrvDTLTOffSet] [datetimeoffset](7) NULL CONSTRAINT [DF_tTaskOut_SrvDTLTOffSet]  DEFAULT (sysdatetimeoffset()),
	[MapGrid] [varchar](50) NULL,
	[StartDTLT] [datetime] NULL,
	[EndDTLT] [datetime] NULL,
	[DeletedFlag] [bit] NULL CONSTRAINT [DF_tTaskOut_DeletedFlag]  DEFAULT ((0)),
	[Comments] [varchar](500) NULL,
	[TotalMapTime] [float] NULL,
	[FeetOfTransmission] [int] NULL,
	[FeetOfHighPressure] [int] NULL,
	[TaskOutUID] [varchar](100) NULL,
 CONSTRAINT [PK_tTaskOut] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tTaskOut]  WITH CHECK ADD  CONSTRAINT [FK_tTaskOut_CreatedID] FOREIGN KEY([CreatedUserID])
REFERENCES [dbo].[UserTb] ([UserID])
GO

ALTER TABLE [dbo].[tTaskOut] CHECK CONSTRAINT [FK_tTaskOut_CreatedID]
GO


