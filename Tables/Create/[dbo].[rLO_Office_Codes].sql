/****** Object:  Table [dbo].[rLO_Office_Codes]    Script Date: 4/17/2019 3:56:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[rLO_Office_Codes](
	[tOfficeID] [int] NOT NULL,
	[ClientID] [int] NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[ModifiedUserID] [int] NOT NULL,
	[CreateDTLT] [datetime] NOT NULL,
	[ModifiedDTLT] [datetime] NULL,
	[InactiveDTLT] [datetime] NULL,
	[COMPANY_NO] [varchar](200) NULL,
	[Division_Name] [varchar](200) NULL,
	[LO_OFFICE] [varchar](200) NULL,
	[LO_Office_Name] [varchar](200) NULL,
	[LO_Office_Rollup] [varchar](200) NULL,
	[LO_Office_Region] [varchar](200) NULL,
	[LO_Billing_Office] [varchar](200) NULL,
	[LO_Billing_Office_Rollup] [varchar](200) NULL,
	[LO_Billing_Office_PO] [varchar](200) NULL,
	[Survey_LO_Office] [varchar](200) NULL,
	[Survey_LO_Office_Name] [varchar](200) NULL,
	[LO_Office_Seq] [int] NOT NULL,
	[LO_Office_Region_Seq] [int] NOT NULL,
	[LO_Office_Rollup_Seq] [int] NOT NULL,
	[Status] [varchar](20) NOT NULL,
	[Revision] [int] NULL,
	[ActiveFlag] [bit] NULL,
	[Comments] [varchar](200) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


