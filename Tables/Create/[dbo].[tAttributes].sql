
/****** Object:  Table [dbo].[tAttributes]    Script Date: 4/17/2019 3:58:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tAttributes](
	[ClientWorkOrderID] [varchar](200) NOT NULL,
	[RowIndex] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Value] [varchar](500) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


