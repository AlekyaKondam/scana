/****** Object:  Table [dbo].[tNotification]    Script Date: 4/17/2019 4:00:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tNotification](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationType] [int] NULL,
	[SrvDTLT] [datetime] NULL,
	[UserID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tNotification] ADD  CONSTRAINT [DF_tNotification_SrvDTLT]  DEFAULT (getdate()) FOR [SrvDTLT]
GO

ALTER TABLE [dbo].[tNotification]  WITH CHECK ADD  CONSTRAINT [FK_tNotification_NotificationType] FOREIGN KEY([NotificationType])
REFERENCES [dbo].[rStatusLookup] ([StatusCode])
GO

ALTER TABLE [dbo].[tNotification] CHECK CONSTRAINT [FK_tNotification_NotificationType]
GO

ALTER TABLE [dbo].[tNotification]  WITH CHECK ADD  CONSTRAINT [FK_tNotification_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserTb] ([UserID])
GO

ALTER TABLE [dbo].[tNotification] CHECK CONSTRAINT [FK_tNotification_UserID]
GO


