/****** Object:  Table [dbo].[rReport]    Script Date: 4/17/2019 3:56:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[rReport](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ProjectID] [int] NULL,
	[CreatedUserID] [varchar](100) NULL,
	[ModifiedUserID] [varchar](100) NULL,
	[CreateDTLT] [datetime] NULL,
	[ModifiedDTLT] [datetime] NULL,
	[InactiveDTLT] [datetime] NULL,
	[Comments] [varchar](2000) NULL,
	[Revision] [int] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[ReportDisplayName] [varchar](100) NULL,
	[ReportDate] [date] NULL,
	[ReportInactiveDate] [date] NULL,
	[ReportType] [varchar](200) NULL,
	[ReportSPName] [varchar](200) NULL,
	[ReportDescription] [varchar](2000) NULL,
	[ReportSortSeq] [int] NULL,
	[Parm] [varchar](500) NULL,
	[ParmInspectorFlag] [bit] NULL,
	[ParmDropDownFlag] [bit] NULL,
	[ParmDateOverrideFlag] [bit] NULL,
	[ParmBetweenDateFlag] [bit] NULL,
	[ParmDateFlag] [bit] NULL,
	[ExportFlag] [bit] NULL,
	[ParmProjectFlag] [bit] NULL CONSTRAINT [DF_rReport_ParmProjectFlag]  DEFAULT ((0))
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


