
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ActivityTb](
	[ActivityID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ActivityUID] [varchar](100) NULL,
	[ActivityProjectID] [int] NULL,
	[ActivitySourceID] [varchar](100) NULL,
	[ActivityCreatedUserUID] [varchar](100) NULL,
	[ActivityModifiedUserUID] [varchar](100) NULL,
	[ActivitySrvDTLT] [datetime] NULL CONSTRAINT [DF_ActivityTb_[ActivitySrvDTLT_1]  DEFAULT (getdate()),
	[ActivitySrvDTLTOffset] [datetimeoffset](7) NULL,
	[ActivitySrcDTLT] [datetime] NULL,
	[ActivityGPSType] [varchar](100) NULL,
	[ActivityGPSSentence] [varchar](400) NULL,
	[ActivityLatitude] [float] NULL,
	[ActivityLongitude] [float] NULL,
	[ActivityShape] [geography] NULL,
	[ActivityComments] [varchar](500) NULL,
	[ActivityType] [varchar](200) NULL,
	[ActivityBatteryLevel] [float] NULL,
	[ActivityStartTime] [datetime] NULL,
	[ActivityEndTime] [datetime] NULL,
	[ActivityElapsedSec] [int] NULL,
	[ActivityTitle] [varchar](100) NULL,
	[ActivityBillingCode] [varchar](50) NULL,
	[ActivityCode] [int] NULL,
	[ActivityPayCode] [int] NULL,
	[ActivityArchiveFlag] [bit] NULL,
	[ActivityCreateDate] [datetime] NULL,
	[ActivityModifiedDate] [datetime] NULL,
	[ActivityRevisionComments] [varchar](500) NULL,
	[ActivityGPSSource] [varchar](20) NULL,
	[ActivityGPSTime] [varchar](20) NULL,
	[ActivityFixQuality] [float] NULL,
	[ActivityNumberOfSatellites] [int] NULL,
	[ActivityHDOP] [float] NULL,
	[ActivityAltitudemetersAboveMeanSeaLevel] [float] NULL,
	[ActivityHeightofGeoid] [float] NULL,
	[ActivityTimeSecondsSinceLastDGPS] [float] NULL,
	[ActivityChecksumData] [varchar](10) NULL,
	[ActivityBearing] [float] NULL,
	[ActivitySpeed] [float] NULL,
	[ActivityGPSStatus] [varchar](20) NULL,
	[ActivityNumberOfGPSAttempts] [int] NULL,
	[ActivityAppVersion] [varchar](50) NULL,
	[ActivityAppVersionName] [varchar](50) NULL,
	[srvUTCDatetime] [datetime] NULL CONSTRAINT [DF_ActivityTb_srvUTCDatetime_1]  DEFAULT (getutcdate()),
	[ActivityMapGrid] [varchar](100) NULL,
	[ActivityPhoneNumber] [varchar](200) NULL,
	[ActivityIsManualEndTime] [bit] NULL,
 CONSTRAINT [PK_ActivityTb] PRIMARY KEY CLUSTERED 
(
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


