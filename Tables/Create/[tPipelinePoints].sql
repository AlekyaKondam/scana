USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[tPipelinePoints]    Script Date: 4/22/2019 2:07:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tPipelinePoints](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WorkOrderID] [int] NULL,
	[Shape] [geography] NULL,
	[MapGrid] [varchar](10) NULL,
	[Segment_ID] [varchar](50) NULL,
	[LineID] [varchar](50) NULL,
	[VerticeID] [varchar](50) NULL,
	[Latitude] [numeric](38, 8) NULL,
	[Longitude] [numeric](38, 8) NULL,
	[BreadCrumbUID] [varchar](200) NULL,
	[Distance] [float] NULL,
	[Verified] [bit] NULL,
 CONSTRAINT [PK__t_Gasmai__678E1764304A00C9] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tPipelinePoints] ADD  CONSTRAINT [DF_t_Gasmains_Vertices_BreadCrumbUID]  DEFAULT ('') FOR [BreadCrumbUID]
GO

ALTER TABLE [dbo].[tPipelinePoints] ADD  CONSTRAINT [DF_t_Gasmains_Vertices_Distance]  DEFAULT ((0)) FOR [Distance]
GO

ALTER TABLE [dbo].[tPipelinePoints] ADD  CONSTRAINT [DF_t_Gasmains_Vertices_Verified]  DEFAULT ((0)) FOR [Verified]
GO

ALTER TABLE [dbo].[tPipelinePoints]  WITH CHECK ADD  CONSTRAINT [FK_tPipelinePoints_tWorkOrder] FOREIGN KEY([WorkOrderID])
REFERENCES [dbo].[tWorkOrder] ([ID])
GO

ALTER TABLE [dbo].[tPipelinePoints] CHECK CONSTRAINT [FK_tPipelinePoints_tWorkOrder]
GO


