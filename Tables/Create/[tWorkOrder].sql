USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[tWorkOrder]    Script Date: 4/22/2019 2:09:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tWorkOrder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientWorkOrderID] [varchar](200) NULL,
	[CreatedBy] [int] NULL,
	[ModifiedBy] [int] NULL,
	[CreatedDateTime] [datetime] NULL CONSTRAINT [DF_tWorkOrder_CreatedDateTime]  DEFAULT (getdate()),
	[ModifiedDateTime] [datetime] NULL,
	[InspectionType] [varchar](50) NULL,
	[HouseNumber] [varchar](50) NULL,
	[Street] [varchar](100) NULL,
	[AptSuite] [varchar](50) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](50) NULL,
	[Zip] [varchar](50) NULL,
	[MeterNumber] [varchar](50) NULL,
	[MeterLocationDesc] [varchar](200) NULL,
	[LocationType] [varchar](50) NULL,
	[LocationLatitude] [float] NULL,
	[LocationLongitude] [float] NULL,
	[MapGrid] [varchar](50) NULL,
	[ComplianceStart] [date] NULL,
	[ComplianceEnd] [date] NULL,
	[MapLatitudeBegin] [float] NULL,
	[MapLongitudeBegin] [float] NULL,
	[MapLatitudeEnd] [float] NULL,
	[MapLongitudeEnd] [float] NULL,
	[AccountNumber] [varchar](50) NULL,
	[AccountName] [varchar](100) NULL,
	[AccountTelephoneNumber] [varchar](50) NULL,
	[Comments] [varchar](500) NULL,
	[CompletedFlag] [bit] NULL CONSTRAINT [DF_tWorkOrder_CompletedFlag]  DEFAULT ((0)),
	[CompletedDate] [datetime] NULL,
	[InspectionAttemptCounter] [int] NULL CONSTRAINT [DF_tWorkOrder_InspectionAttemptCounter]  DEFAULT ((0)),
	[SequenceNumber] [varchar](50) NULL CONSTRAINT [DF_tWorkOrder_SequenceNumber]  DEFAULT ('1'),
	[SectionNumber] [varchar](50) NULL CONSTRAINT [DF_tWorkOrder_SectionNumber]  DEFAULT ('1'),
	[Shape] [geography] NULL,
	[EventIndicator] [int] NULL,
	[OrderProcessed] [bit] NULL CONSTRAINT [DF_tWorkOrder_OrderProcessed]  DEFAULT ((0)),
	[Frequency] [varchar](50) NULL,
	[Division] [varchar](50) NULL,
	[BillingCode] [varchar](50) NULL,
	[Rollup] [varchar](200) NULL,
	[MeterLocation] [varchar](25) NULL,
	[PipelineFootage] [float] NULL,
	[SpecialInstructions] [varchar](250) NULL,
	[OfficeName] [varchar](100) NULL,
	[RollUpOfficeName] [varchar](100) NULL,
	[BillingOfficeName] [varchar](100) NULL,
	[ComplianceFlag] [bit] NULL,
	[OverrideFlag] [bit] NULL,
	[ComplianceReason] [varchar](max) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tWorkOrder]  WITH CHECK ADD  CONSTRAINT [FK_tWorkOrder_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserTb] ([UserID])
GO

ALTER TABLE [dbo].[tWorkOrder] CHECK CONSTRAINT [FK_tWorkOrder_CreatedBy]
GO

ALTER TABLE [dbo].[tWorkOrder]  WITH CHECK ADD  CONSTRAINT [FK_tWorkOrder_EventIndicator] FOREIGN KEY([EventIndicator])
REFERENCES [dbo].[rStatusLookup] ([StatusCode])
GO

ALTER TABLE [dbo].[tWorkOrder] CHECK CONSTRAINT [FK_tWorkOrder_EventIndicator]
GO

ALTER TABLE [dbo].[tWorkOrder]  WITH CHECK ADD  CONSTRAINT [FK_tWorkOrder_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[UserTb] ([UserID])
GO

ALTER TABLE [dbo].[tWorkOrder] CHECK CONSTRAINT [FK_tWorkOrder_ModifiedBy]
GO


