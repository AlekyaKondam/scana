USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[xReportNameEmailList]    Script Date: 4/22/2019 2:10:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[xReportNameEmailList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmailID] [int] NULL,
	[ReportID] [int] NULL,
	[EmailType] [varchar](10) NULL,
	[ActiveFlag] [bit] NULL,
	[SortOrder] [int] NULL,
 CONSTRAINT [PK_xReportNameEmailList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


