USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[tWorkQueue]    Script Date: 4/22/2019 2:09:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tWorkQueue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WorkOrderID] [int] NULL,
	[AssignedUserID] [int] NULL,
	[WorkQueueStatus] [int] NULL CONSTRAINT [DF_tWorkQueur_WorkQueueStatus]  DEFAULT ((100)),
	[SectionNumber] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[tAssetID] [int] NULL,
	[TaskedOut] [bit] NULL CONSTRAINT [DF_tWorkQueue_TaskedOut]  DEFAULT ((0)),
	[ScheduledDispatchDate] [datetime] NULL,
 CONSTRAINT [PK_tWorkQueue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tWorkQueue]  WITH CHECK ADD  CONSTRAINT [FK_tWorkQueue_AssignedID] FOREIGN KEY([AssignedUserID])
REFERENCES [dbo].[UserTb] ([UserID])
GO

ALTER TABLE [dbo].[tWorkQueue] CHECK CONSTRAINT [FK_tWorkQueue_AssignedID]
GO

ALTER TABLE [dbo].[tWorkQueue]  WITH CHECK ADD  CONSTRAINT [FK_tWorkQueue_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserTb] ([UserID])
GO

ALTER TABLE [dbo].[tWorkQueue] CHECK CONSTRAINT [FK_tWorkQueue_CreatedBy]
GO

ALTER TABLE [dbo].[tWorkQueue]  WITH CHECK ADD  CONSTRAINT [FK_tWorkQueue_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[UserTb] ([UserID])
GO

ALTER TABLE [dbo].[tWorkQueue] CHECK CONSTRAINT [FK_tWorkQueue_ModifiedBy]
GO

ALTER TABLE [dbo].[tWorkQueue]  WITH CHECK ADD  CONSTRAINT [FK_tWorkQueue_WorkQueueStatus] FOREIGN KEY([WorkQueueStatus])
REFERENCES [dbo].[rStatusLookup] ([StatusCode])
GO

ALTER TABLE [dbo].[tWorkQueue] CHECK CONSTRAINT [FK_tWorkQueue_WorkQueueStatus]
GO


