USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[tWorkOrderIDLookup]    Script Date: 4/22/2019 2:09:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tWorkOrderIDLookup](
	[InternalWorkOrderID] [varchar](200) NOT NULL,
	[ClientWorkOrderID] [varchar](200) NOT NULL,
	[LookupKey] [varchar](100) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


