USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[tWebJSONDataInsertError]    Script Date: 4/22/2019 2:09:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tWebJSONDataInsertError](
	[tJSONDataInsertErrorID] [int] IDENTITY(1,1) NOT NULL,
	[SvrDTLT] [datetime] NULL CONSTRAINT [DF_twebJSONDataInsertError_SvrDTLT]  DEFAULT (getdate()),
	[SvrDTLT_Offset] [datetimeoffset](7) NULL CONSTRAINT [DF_webJSONDataInsertError_SvrDTLT_Offset]  DEFAULT (sysdatetimeoffset()),
	[InsertedData] [varchar](max) NULL,
	[ErrorNumber] [int] NULL,
	[ErrorMessage] [varchar](max) NULL,
	[InsertedData2] [varchar](max) NULL,
	[InsertedData3] [varchar](max) NULL,
	[InsertedData4] [varchar](max) NULL,
	[InsertedData5] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


