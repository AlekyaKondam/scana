
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[rDropDown](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NULL,
	[CreatedUserID] [varchar](100) NULL,
	[ModifiedUserID] [int] NULL,
	[CreatedDTLT] [datetime] NULL CONSTRAINT [DF_rDropDown_CreatedDTLT]  DEFAULT (getdate()),
	[ModifiedDTLT] [datetime] NULL,
	[Comments] [varchar](500) NULL,
	[DropDownType] [varchar](50) NULL,
	[FilterName] [varchar](50) NULL,
	[SortSeq] [int] NULL CONSTRAINT [DF_rDropDown_SortSeq]  DEFAULT ((0)),
	[FieldValue] [varchar](200) NULL,
	[FieldDescription] [varchar](500) NULL,
	[ConversionValue] [varchar](200) NULL,
	[FieldDisplay] [varchar](200) NULL,
 CONSTRAINT [PK_rDropDown] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Moble, Web, Both' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rDropDown', @level2type=N'COLUMN',@level2name=N'DropDownType'
GO


