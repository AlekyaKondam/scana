USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[tTabletJSONDataInsertError]    Script Date: 4/22/2019 2:08:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tTabletJSONDataInsertError](
	[tJSONDataInsertErrorID] [int] IDENTITY(1,1) NOT NULL,
	[SvrDTLT] [datetime] NULL CONSTRAINT [DF_tTabletJSONDataInsertError_SvrDTLT]  DEFAULT (getdate()),
	[SvrDTLT_Offset] [datetimeoffset](3) NULL CONSTRAINT [DF_tTabletJSONDataInsertError_SvrDTLT_Offset]  DEFAULT (sysdatetimeoffset()),
	[InsertedData] [varchar](max) NULL,
	[ErrorNumber] [int] NULL,
	[ErrorMessage] [varchar](max) NULL,
	[InsertedData2] [varchar](max) NULL,
	[InsertedData3] [varchar](max) NULL,
	[InsertedData4] [varchar](max) NULL,
	[InsertedData5] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


