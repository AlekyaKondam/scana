USE [vCAT_SCANA_GIS_DEV]
GO
/****** Object:  Table [dbo].[ActivityTb]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActivityTb](
	[ActivityID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ActivityUID] [varchar](100) NULL,
	[ActivityProjectID] [int] NULL,
	[ActivitySourceID] [varchar](100) NULL,
	[ActivityCreatedUserUID] [varchar](100) NULL,
	[ActivityModifiedUserUID] [varchar](100) NULL,
	[ActivitySrvDTLT] [datetime] NULL CONSTRAINT [DF_ActivityTb_[ActivitySrvDTLT_1]  DEFAULT (getdate()),
	[ActivitySrvDTLTOffset] [datetimeoffset](7) NULL,
	[ActivitySrcDTLT] [datetime] NULL,
	[ActivityGPSType] [varchar](100) NULL,
	[ActivityGPSSentence] [varchar](400) NULL,
	[ActivityLatitude] [float] NULL,
	[ActivityLongitude] [float] NULL,
	[ActivityShape] [geography] NULL,
	[ActivityComments] [varchar](500) NULL,
	[ActivityType] [varchar](200) NULL,
	[ActivityBatteryLevel] [float] NULL,
	[ActivityStartTime] [datetime] NULL,
	[ActivityEndTime] [datetime] NULL,
	[ActivityElapsedSec] [int] NULL,
	[ActivityTitle] [varchar](100) NULL,
	[ActivityBillingCode] [varchar](50) NULL,
	[ActivityCode] [int] NULL,
	[ActivityPayCode] [int] NULL,
	[ActivityArchiveFlag] [bit] NULL,
	[ActivityCreateDate] [datetime] NULL,
	[ActivityModifiedDate] [datetime] NULL,
	[ActivityRevisionComments] [varchar](500) NULL,
	[ActivityGPSSource] [varchar](20) NULL,
	[ActivityGPSTime] [varchar](20) NULL,
	[ActivityFixQuality] [float] NULL,
	[ActivityNumberOfSatellites] [int] NULL,
	[ActivityHDOP] [float] NULL,
	[ActivityAltitudemetersAboveMeanSeaLevel] [float] NULL,
	[ActivityHeightofGeoid] [float] NULL,
	[ActivityTimeSecondsSinceLastDGPS] [float] NULL,
	[ActivityChecksumData] [varchar](10) NULL,
	[ActivityBearing] [float] NULL,
	[ActivitySpeed] [float] NULL,
	[ActivityGPSStatus] [varchar](20) NULL,
	[ActivityNumberOfGPSAttempts] [int] NULL,
	[ActivityAppVersion] [varchar](50) NULL,
	[ActivityAppVersionName] [varchar](50) NULL,
	[srvUTCDatetime] [datetime] NULL CONSTRAINT [DF_ActivityTb_srvUTCDatetime_1]  DEFAULT (getutcdate()),
	[ActivityMapGrid] [varchar](100) NULL,
	[ActivityPhoneNumber] [varchar](200) NULL,
	[ActivityIsManualEndTime] [bit] NULL,
 CONSTRAINT [PK_ActivityTb] PRIMARY KEY CLUSTERED 
(
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[auth_assignment]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[auth_assignment](
	[item_name] [varchar](64) NOT NULL,
	[user_id] [varchar](64) NOT NULL,
	[created_at] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[item_name] ASC,
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[auth_item]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[auth_item](
	[name] [varchar](64) NOT NULL,
	[type] [int] NOT NULL,
	[description] [text] NULL,
	[rule_name] [varchar](64) NULL,
	[data] [text] NULL,
	[created_at] [int] NULL,
	[updated_at] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[auth_item_child]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[auth_item_child](
	[parent] [varchar](64) NOT NULL,
	[child] [varchar](64) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[parent] ASC,
	[child] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[auth_rule]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[auth_rule](
	[name] [varchar](64) NOT NULL,
	[data] [text] NULL,
	[created_at] [int] NULL,
	[updated_at] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BreadcrumbTb]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BreadcrumbTb](
	[BreadcrumbID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[BreadcrumbUID] [varchar](100) NOT NULL,
	[BreadcrumbActivityUID] [varchar](100) NULL,
	[BreadcrumbSourceID] [varchar](100) NULL,
	[BreadcrumbCreatedUserUID] [varchar](100) NOT NULL,
	[BreadcrumbSrcDTLT] [datetime] NOT NULL,
	[BreadcrumbSrvDTLTOffset] [datetimeoffset](7) NULL,
	[BreadcrumbSrvDTLT] [datetime] NULL CONSTRAINT [DF_BreadcrumbTb_BreadcrumbSrvDTLT]  DEFAULT (getdate()),
	[BreadcrumbGPSType] [varchar](100) NULL,
	[BreadcrumbGPSSentence] [varchar](400) NULL,
	[BreadcrumbLatitude] [float] NULL,
	[BreadcrumbLongitude] [float] NULL,
	[BreadcrumbShape] [geography] NULL,
	[BreadcrumbActivityType] [varchar](50) NULL,
	[BreadcrumbWorkQueueFilter] [varchar](200) NULL,
	[BreadcrumbBatteryLevel] [float] NULL,
	[BreadcrumbGPSTime] [datetime] NULL,
	[BreadcrumbSpeed] [float] NULL,
	[BreadcrumbHeading] [varchar](50) NULL,
	[BreadcrumbGPSAccuracy] [varchar](50) NULL,
	[BreadcrumbSatellites] [int] NULL,
	[BreadcrumbAltitude] [float] NULL,
	[BreadcrumbTrackingGroupID] [int] NULL,
	[BreadcrumbMapPlat] [varchar](50) NULL,
	[BreadcrumbArchiveFlag] [bit] NULL,
	[BreadcrumbComments] [varchar](500) NULL,
	[BreadcrumbCreatedDate] [datetime] NULL,
	[BreadcrumbDeviceID] [varchar](50) NULL,
	[OrderProcessed] [bit] NULL CONSTRAINT [DF_BreadcrumbTb_OrderProcessed]  DEFAULT ((0)),
	[IsStationary] [bit] NULL CONSTRAINT [DF_BreadcrumbTb_IsStationary_1]  DEFAULT ((0)),
	[PaceOfTravel] [decimal](10, 5) NULL CONSTRAINT [DF_BreadcrumbTb_PaceOfTravel_1]  DEFAULT ((0)),
	[IsDistanceBased] [bit] NULL,
	[DistanceTraveled] [decimal](18, 10) NULL CONSTRAINT [DF_BreadcrumbTb_DistanceTraveled]  DEFAULT ((0)),
	[OriginBreadcrumbLatitude] [float] NULL,
	[OriginBreadcrumbLongitude] [float] NULL,
	[SpeedAttribute] [varchar](50) NULL,
 CONSTRAINT [PK_BreadcrumbTb] PRIMARY KEY CLUSTERED 
(
	[BreadcrumbID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rDropDown]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rDropDown](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NULL,
	[CreatedUserID] [varchar](100) NULL,
	[ModifiedUserID] [int] NULL,
	[CreatedDTLT] [datetime] NULL CONSTRAINT [DF_rDropDown_CreatedDTLT]  DEFAULT (getdate()),
	[ModifiedDTLT] [datetime] NULL,
	[Comments] [varchar](500) NULL,
	[DropDownType] [varchar](50) NULL,
	[FilterName] [varchar](50) NULL,
	[SortSeq] [int] NULL CONSTRAINT [DF_rDropDown_SortSeq]  DEFAULT ((0)),
	[FieldValue] [varchar](200) NULL,
	[FieldDescription] [varchar](500) NULL,
	[ConversionValue] [varchar](200) NULL,
	[FieldDisplay] [varchar](200) NULL,
 CONSTRAINT [PK_rDropDown] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rEmailList]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rEmailList](
	[emaillistID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[DateAdded] [datetime] NULL,
	[email] [varchar](200) NULL,
	[ActiveFlag] [bit] NULL,
 CONSTRAINT [PK_rEmailList] PRIMARY KEY CLUSTERED 
(
	[emaillistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rEmailReportName]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rEmailReportName](
	[rReportNameID] [int] IDENTITY(1,1) NOT NULL,
	[ReportName] [varchar](200) NULL,
	[ActiveFlag] [bit] NULL CONSTRAINT [DF_rEmailReportName_ActiveFlag]  DEFAULT ((1)),
 CONSTRAINT [PK_rEmailReportName] PRIMARY KEY CLUSTERED 
(
	[rReportNameID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rLO_Office_Codes]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rLO_Office_Codes](
	[tOfficeID] [int] NOT NULL,
	[ClientID] [int] NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[ModifiedUserID] [int] NOT NULL,
	[CreateDTLT] [datetime] NOT NULL,
	[ModifiedDTLT] [datetime] NULL,
	[InactiveDTLT] [datetime] NULL,
	[COMPANY_NO] [varchar](200) NULL,
	[Division_Name] [varchar](200) NULL,
	[LO_OFFICE] [varchar](200) NULL,
	[LO_Office_Name] [varchar](200) NULL,
	[LO_Office_Rollup] [varchar](200) NULL,
	[LO_Office_Region] [varchar](200) NULL,
	[LO_Billing_Office] [varchar](200) NULL,
	[LO_Billing_Office_Rollup] [varchar](200) NULL,
	[LO_Billing_Office_PO] [varchar](200) NULL,
	[Survey_LO_Office] [varchar](200) NULL,
	[Survey_LO_Office_Name] [varchar](200) NULL,
	[LO_Office_Seq] [int] NOT NULL,
	[LO_Office_Region_Seq] [int] NOT NULL,
	[LO_Office_Rollup_Seq] [int] NOT NULL,
	[Status] [varchar](20) NOT NULL,
	[Revision] [int] NULL,
	[ActiveFlag] [bit] NULL,
	[Comments] [varchar](200) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rReport]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rReport](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ProjectID] [int] NULL,
	[CreatedUserID] [varchar](100) NULL,
	[ModifiedUserID] [varchar](100) NULL,
	[CreateDTLT] [datetime] NULL,
	[ModifiedDTLT] [datetime] NULL,
	[InactiveDTLT] [datetime] NULL,
	[Comments] [varchar](2000) NULL,
	[Revision] [int] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[ReportDisplayName] [varchar](100) NULL,
	[ReportDate] [date] NULL,
	[ReportInactiveDate] [date] NULL,
	[ReportType] [varchar](200) NULL,
	[ReportSPName] [varchar](200) NULL,
	[ReportDescription] [varchar](2000) NULL,
	[ReportSortSeq] [int] NULL,
	[Parm] [varchar](500) NULL,
	[ParmInspectorFlag] [bit] NULL,
	[ParmDropDownFlag] [bit] NULL,
	[ParmDateOverrideFlag] [bit] NULL,
	[ParmBetweenDateFlag] [bit] NULL,
	[ParmDateFlag] [bit] NULL,
	[ExportFlag] [bit] NULL,
	[ParmProjectFlag] [bit] NULL CONSTRAINT [DF_rReport_ParmProjectFlag]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[rStatusLookup]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rStatusLookup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [varchar](100) NULL,
	[ModifiedBy] [varchar](100) NULL,
	[CreatedDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
	[StatusType] [varchar](50) NULL,
	[StatusCode] [int] NULL,
	[StatusDescription] [varchar](200) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tAsset]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tAsset](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AssetTabletID] [varchar](100) NULL,
	[InspectionID] [int] NULL,
	[MapGrid] [varchar](50) NULL,
	[CreatedUserID] [int] NULL,
	[HouseNo] [varchar](50) NULL,
	[Street] [varchar](100) NULL,
	[Apt] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[ReverseGeoLocationString] [varchar](max) NULL,
	[MeterID] [varchar](50) NULL,
	[PipelineType] [varchar](50) NULL,
	[Grade1ReleaseReasonType] [varchar](50) NULL,
	[Grade1ReleaseDateTime] [datetime] NULL,
	[Comments] [varchar](500) NULL,
	[Photo1Path] [varchar](max) NULL,
	[SrcDTLT] [datetime] NULL,
	[SrvDTLT] [datetime] NULL CONSTRAINT [DF_tAsset_SrvDTLT]  DEFAULT (getdate()),
	[SrvDTLTOffset] [datetimeoffset](7) NULL CONSTRAINT [DF_tAsset_SrvDTLTOffset]  DEFAULT (sysdatetimeoffset()),
	[Inspection] [varchar](50) NULL,
	[AOCs] [int] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[GPSType] [varchar](50) NULL,
	[GPSSentence] [varchar](max) NULL,
	[GPSTime] [varchar](50) NULL,
	[FixQuality] [float] NULL,
	[NumberOfSatellites] [int] NULL,
	[HDOP] [float] NULL,
	[AltitudeMetersAboveMeanSeaLevel] [float] NULL,
	[HeightOfGeoid] [float] NULL,
	[TimeSecondsSinceLastDGPS] [float] NULL,
	[CheckSumData] [varchar](50) NULL,
	[Bearing] [float] NULL,
	[Speed] [float] NULL,
	[NumberOfGPSAttempts] [int] NULL,
	[Zip] [varchar](50) NULL,
	[MeterLocation] [varchar](25) NULL,
 CONSTRAINT [PK_tAsset] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tAttributes]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tAttributes](
	[ClientWorkOrderID] [varchar](200) NOT NULL,
	[RowIndex] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Value] [varchar](500) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tCalibration]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tCalibration](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NULL,
	[EquipmentSourceID] [varchar](200) NULL,
	[CreatedUserID] [int] NULL,
	[SrcDTLT] [datetime] NULL,
	[SrvDTLT] [datetime] NULL CONSTRAINT [DF_tCalibration_SrvDTLT]  DEFAULT (getdate()),
	[SrvDTLTOffset] [datetimeoffset](7) NULL CONSTRAINT [DF_tCalibration_SrvDTLTOffset]  DEFAULT (sysdatetimeoffset()),
	[EquipmentType] [varchar](50) NULL,
	[SerialNumber] [varchar](50) NULL,
	[CalibrationVerification] [bit] NULL CONSTRAINT [DF_tCalibration_CalibrationVerification]  DEFAULT ((0)),
	[CalibrationLevel] [float] NULL,
	[Comments] [varchar](500) NULL,
	[DeletedFlag] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_tCalibration] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tEvent]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tEvent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventTabletID] [varchar](100) NULL,
	[EventType] [int] NULL,
	[InspectionTabletID] [varchar](100) NULL,
	[SrcDTLT] [datetime] NULL,
	[SrvDTLT] [datetime] NULL CONSTRAINT [DF_tEvent_SrvDTLT]  DEFAULT (getdate()),
	[SrvDTLTOffset] [datetimeoffset](7) NULL CONSTRAINT [DF_tEvent_SrvDTLTOffset]  DEFAULT (sysdatetimeoffset()),
	[CreatedByUserID] [int] NULL,
	[LocationID] [varchar](50) NULL,
	[LocationAddress] [varchar](200) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[MapGrid] [varchar](50) NULL,
	[Photo1Path] [varchar](500) NULL,
	[Photo2Path] [varchar](500) NULL,
	[Photo3Path] [varchar](500) NULL,
	[AOCReason] [varchar](200) NULL,
	[CGEReason] [varchar](200) NULL,
	[LeakNumber] [varchar](200) NULL,
	[LeakGrade] [varchar](100) NULL,
	[LeakAboveOrBelow] [varchar](200) NULL,
	[DetectedByEquipment] [varchar](200) NULL,
	[EquipmentSerialNumber] [varchar](50) NULL,
	[Comments] [varchar](500) NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[GPSType] [varchar](50) NULL,
	[GPSSentence] [varchar](max) NULL,
	[GPSTime] [varchar](50) NULL,
	[FixQuality] [float] NULL,
	[NumberOfSatellites] [int] NULL,
	[HDOP] [float] NULL,
	[AltitudeMetersAboveMeanSeaLevel] [float] NULL,
	[HeightOfGeoid] [float] NULL,
	[TimeSecondsSinceLastDGPS] [float] NULL,
	[ChecksumData] [varchar](50) NULL,
	[Bearing] [float] NULL,
	[Speed] [float] NULL,
	[NumberOfGPSAttempts] [int] NULL,
	[InspectionID] [int] NOT NULL,
	[DeletedFlag] [bit] NULL CONSTRAINT [DF_tEvent_DeletedFlag]  DEFAULT ((0)),
	[SplashGuardNeeded] [bit] NULL,
	[SplashGuardInstalled] [bit] NULL,
	[TracerWireMissing] [bit] NULL,
	[TamperSealNotPresent] [bit] NULL,
	[EnergyDiversionPresent] [bit] NULL,
	[ACGrade] [varchar](100) NULL,
	[RiserPostBad] [bit] NULL,
	[RecommendToRetireInActiveService] [bit] NULL,
	[FacilitiesNeedToBeProtected] [bit] NULL,
	[Other] [bit] NULL,
	[AOCFlag] [bit] NULL,
	[AccessIssues] [varchar](100) NULL,
	[CGE] [varchar](25) NULL,
	[LeakRepaired] [bit] NULL,
	[LeakFoundMainOrService] [varchar](20) NULL,
	[BadDogPresent] [bit] NULL,
	[NIFReason] [varchar](100) NULL,
	[PipelineAOCIssues] [varchar](100) NULL,
	[EnergyDiversionPhoto1Path] [varchar](500) NULL,
	[EnergyDiversionPhoto2Path] [varchar](500) NULL,
	[EnergyDiversionPhoto3Path] [varchar](500) NULL,
 CONSTRAINT [PK_tEvent] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tInspection]    Script Date: 4/24/2019 9:25:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tInspection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InspectionTabletID] [varchar](100) NULL,
	[ActivityID] [int] NULL,
	[WorkQueueID] [int] NULL,
	[WorkQueueStatus] [int] NULL,
	[MapGrid] [varchar](50) NULL,
	[IsAdHocFlag] [bit] NULL CONSTRAINT [DF_tInspection_IsAdHocFlag]  DEFAULT ((0)),
	[IsInGridFlag] [bit] NULL CONSTRAINT [DF_tInspection_IsInGridFlag]  DEFAULT ((0)),
	[IsCGEFlag] [bit] NULL CONSTRAINT [DF_tInspection_IsCGEFlag]  DEFAULT ((0)),
	[IsAOCFlag] [bit] NULL CONSTRAINT [DF_tInspection_IsAOCFlag]  DEFAULT ((0)),
	[IsIndicationFlag] [bit] NULL CONSTRAINT [DF_tInspection_IsIndicationFlag]  DEFAULT ((0)),
	[IsPipelineFlag] [bit] NULL CONSTRAINT [DF_tInspection_IsPipelineFlag]  DEFAULT ((0)),
	[AGLeakCounter] [int] NULL CONSTRAINT [DF_tInspection_AGLeakCounter]  DEFAULT ((0)),
	[BGLeakCounter] [int] NULL CONSTRAINT [DF_tInspection_BGLeakCounter]  DEFAULT ((0)),
	[Grade1Counter] [int] NULL CONSTRAINT [DF_tInspection_Grade1Counter]  DEFAULT ((0)),
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[AssetID] [int] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[GPSType] [varchar](100) NULL,
	[GPSSentence] [varchar](max) NULL,
	[GPSTime] [varchar](50) NULL,
	[FixQuality] [float] NULL,
	[NumberOfSatellites] [int] NULL,
	[HDOP] [float] NULL,
	[AltitudeMetersAboveMeanSeaLevel] [float] NULL,
	[HeightOfGeoid] [float] NULL,
	[TimeSecondsSinceLastDGPS] [float] NULL,
	[ChecksumData] [varchar](50) NULL,
	[Bearing] [float] NULL,
	[Speed] [float] NULL,
	[NumberOfGPSAttempts] [int] NULL,
	[SrvDTLT] [datetime] NULL CONSTRAINT [DF_tInspection_SrvDTLT]  DEFAULT (getdate()),
	[SrvDTLTOffset] [datetimeoffset](7) NULL CONSTRAINT [DF_tInspection_SrvDTLTOffset]  DEFAULT (sysdatetimeoffset()),
	[IsWorkOrderUpdated] [bit] NULL,
	[Photo1Path] [varchar](500) NULL,
	[PipelineType] [varchar](200) NULL,
	[Comments] [varchar](500) NULL,
	[HardToLocateFlag] [bit] NULL,
	[MeterNumberPhoto1Path] [varchar](500) NULL,
	[MeterNumberPhoto2Path] [varchar](500) NULL,
	[MeterNumberPhoto3Path] [varchar](500) NULL,
	[TaskOutActivityUID] [varchar](200) NULL,
 CONSTRAINT [PK_tInspection] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tMissingPictures]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tMissingPictures](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DateDiscovered] [datetime] NULL,
	[PhotoFileName] [varchar](500) NULL,
	[Tech] [varchar](50) NULL,
	[DateTaken] [date] NULL,
 CONSTRAINT [PK_tMissingPictures] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tNotification]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tNotification](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationType] [int] NULL,
	[SrvDTLT] [datetime] NULL,
	[UserID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tPipelinePoints]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tPipelinePoints](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WorkOrderID] [int] NULL,
	[Shape] [geography] NULL,
	[MapGrid] [varchar](10) NULL,
	[Segment_ID] [varchar](50) NULL,
	[LineID] [varchar](50) NULL,
	[VerticeID] [varchar](50) NULL,
	[Latitude] [numeric](38, 8) NULL,
	[Longitude] [numeric](38, 8) NULL,
	[BreadCrumbUID] [varchar](200) NULL,
	[Distance] [float] NULL,
	[Verified] [bit] NULL,
 CONSTRAINT [PK__t_Gasmai__678E1764304A00C9] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tSPInsertedData]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tSPInsertedData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[svrDTLT] [datetime] NULL CONSTRAINT [DF_tSPInsertedData_svrDTLT]  DEFAULT (getdate()),
	[SPName] [varchar](50) NULL,
	[ParmData] [varchar](max) NULL,
	[AffectedItems] [varchar](max) NULL,
 CONSTRAINT [PK_tSPInsertedData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tTabletDataInsertArchive]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tTabletDataInsertArchive](
	[TabletDataInsertArchiveID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CreatedUserUID] [varchar](100) NULL,
	[SvrDTLT] [datetime] NULL CONSTRAINT [DF_t_TabletDataInsertArchive_SvrDTLT]  DEFAULT (getdate()),
	[SvrDTLTOffset] [datetimeoffset](7) NULL CONSTRAINT [DF_t_TabletDataInsertArchive_SvrDTLTOffset]  DEFAULT (sysdatetimeoffset()),
	[TransactionType] [varchar](100) NULL,
	[InsertedData] [varchar](max) NULL,
 CONSTRAINT [PK_t_TabletDataInsertArchive] PRIMARY KEY CLUSTERED 
(
	[TabletDataInsertArchiveID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tTabletDataInsertBreadcrumbArchive]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tTabletDataInsertBreadcrumbArchive](
	[tTabletDataInsertArchiveBreadcrumbID] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [int] NULL,
	[UserUID] [varchar](100) NULL,
	[SvrDTLT] [datetime] NULL CONSTRAINT [DFtTabletDataInsertArchiveBC_SvrDTLT]  DEFAULT (getdate()),
	[SvrDTLT_Offset] [datetimeoffset](7) NULL CONSTRAINT [DFtTabletDataInsertArchiveBC_SvrDTLT_Offset]  DEFAULT (sysdatetimeoffset()),
	[TransactionType] [varchar](50) NULL,
	[InsertedData] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tTabletJSONDataInsertError]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tTabletJSONDataInsertError](
	[tJSONDataInsertErrorID] [int] IDENTITY(1,1) NOT NULL,
	[SvrDTLT] [datetime] NULL CONSTRAINT [DF_tTabletJSONDataInsertError_SvrDTLT]  DEFAULT (getdate()),
	[SvrDTLT_Offset] [datetimeoffset](3) NULL CONSTRAINT [DF_tTabletJSONDataInsertError_SvrDTLT_Offset]  DEFAULT (sysdatetimeoffset()),
	[InsertedData] [varchar](max) NULL,
	[ErrorNumber] [int] NULL,
	[ErrorMessage] [varchar](max) NULL,
	[InsertedData2] [varchar](max) NULL,
	[InsertedData3] [varchar](max) NULL,
	[InsertedData4] [varchar](max) NULL,
	[InsertedData5] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tTaskOut]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tTaskOut](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NOT NULL,
	[AboveGroundLeakCount] [int] NULL CONSTRAINT [DF_tTaskOut_AboveGroundLeakCount]  DEFAULT ((0)),
	[BelowGroundLeakCount] [int] NULL CONSTRAINT [DF_tTaskOut_BelowGroundLeakCount]  DEFAULT ((0)),
	[ServicesCount] [int] NULL CONSTRAINT [DF_tTaskOut_ServicesCount]  DEFAULT ((0)),
	[FeetOfMain] [int] NULL CONSTRAINT [DF_tTaskOut_FeetOfMain]  DEFAULT ((0)),
	[CreatedUserID] [int] NOT NULL,
	[SrcDTLT] [datetime] NULL,
	[SrvDTLT] [datetime] NULL CONSTRAINT [DF_tTaskOut_SrvDTLT]  DEFAULT (getdate()),
	[SrvDTLTOffSet] [datetimeoffset](7) NULL CONSTRAINT [DF_tTaskOut_SrvDTLTOffSet]  DEFAULT (sysdatetimeoffset()),
	[MapGrid] [varchar](50) NULL,
	[StartDTLT] [datetime] NULL,
	[EndDTLT] [datetime] NULL,
	[DeletedFlag] [bit] NULL CONSTRAINT [DF_tTaskOut_DeletedFlag]  DEFAULT ((0)),
	[Comments] [varchar](500) NULL,
	[TotalMapTime] [float] NULL,
	[FeetOfTransmission] [int] NULL,
	[FeetOfHighPressure] [int] NULL,
	[TaskOutUID] [varchar](100) NULL,
 CONSTRAINT [PK_tTaskOut] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tTechMissingBCAdded]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tTechMissingBCAdded](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MissingBCDate] [date] NULL,
	[UserUID] [varchar](50) NULL,
	[DeviceID] [varchar](50) NULL,
	[ActivityCount] [int] NULL,
	[BCCount] [int] NULL,
	[TotalInserted] [int] NULL,
	[CompareType] [varchar](50) NULL,
	[InsertedDateTime] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tWebDataInsertArchive]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tWebDataInsertArchive](
	[WebDataInsertArchiveID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CreatedUserUID] [varchar](100) NULL,
	[SvrDTLT] [datetime] NULL CONSTRAINT [DF_t_WebDataInsertArchive_SvrDTLT]  DEFAULT (getdate()),
	[SvrDTLTOffset] [datetimeoffset](7) NULL CONSTRAINT [DF_t_WebDataInsertArchive_SvrDTLTOffset]  DEFAULT (sysdatetimeoffset()),
	[TransactionType] [varchar](100) NULL,
	[InsertedData] [varchar](max) NULL,
 CONSTRAINT [PK_t_WebDataInsertArchive] PRIMARY KEY CLUSTERED 
(
	[WebDataInsertArchiveID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tWebJSONDataInsertError]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tWebJSONDataInsertError](
	[tJSONDataInsertErrorID] [int] IDENTITY(1,1) NOT NULL,
	[SvrDTLT] [datetime] NULL CONSTRAINT [DF_twebJSONDataInsertError_SvrDTLT]  DEFAULT (getdate()),
	[SvrDTLT_Offset] [datetimeoffset](7) NULL CONSTRAINT [DF_webJSONDataInsertError_SvrDTLT_Offset]  DEFAULT (sysdatetimeoffset()),
	[InsertedData] [varchar](max) NULL,
	[ErrorNumber] [int] NULL,
	[ErrorMessage] [varchar](max) NULL,
	[InsertedData2] [varchar](max) NULL,
	[InsertedData3] [varchar](max) NULL,
	[InsertedData4] [varchar](max) NULL,
	[InsertedData5] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tWorkOrder]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tWorkOrder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientWorkOrderID] [varchar](200) NULL,
	[CreatedBy] [int] NULL,
	[ModifiedBy] [int] NULL,
	[CreatedDateTime] [datetime] NULL CONSTRAINT [DF_tWorkOrder_CreatedDateTime]  DEFAULT (getdate()),
	[ModifiedDateTime] [datetime] NULL,
	[InspectionType] [varchar](50) NULL,
	[HouseNumber] [varchar](50) NULL,
	[Street] [varchar](100) NULL,
	[AptSuite] [varchar](50) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](50) NULL,
	[Zip] [varchar](50) NULL,
	[MeterNumber] [varchar](50) NULL,
	[MeterLocationDesc] [varchar](200) NULL,
	[LocationType] [varchar](50) NULL,
	[LocationLatitude] [float] NULL,
	[LocationLongitude] [float] NULL,
	[MapGrid] [varchar](50) NULL,
	[ComplianceStart] [date] NULL,
	[ComplianceEnd] [date] NULL,
	[MapLatitudeBegin] [float] NULL,
	[MapLongitudeBegin] [float] NULL,
	[MapLatitudeEnd] [float] NULL,
	[MapLongitudeEnd] [float] NULL,
	[AccountNumber] [varchar](50) NULL,
	[AccountName] [varchar](100) NULL,
	[AccountTelephoneNumber] [varchar](50) NULL,
	[Comments] [varchar](500) NULL,
	[CompletedFlag] [bit] NULL CONSTRAINT [DF_tWorkOrder_CompletedFlag]  DEFAULT ((0)),
	[CompletedDate] [datetime] NULL,
	[InspectionAttemptCounter] [int] NULL CONSTRAINT [DF_tWorkOrder_InspectionAttemptCounter]  DEFAULT ((0)),
	[SequenceNumber] [varchar](50) NULL CONSTRAINT [DF_tWorkOrder_SequenceNumber]  DEFAULT ('1'),
	[SectionNumber] [varchar](50) NULL CONSTRAINT [DF_tWorkOrder_SectionNumber]  DEFAULT ('1'),
	[Shape] [geography] NULL,
	[EventIndicator] [int] NULL,
	[OrderProcessed] [bit] NULL CONSTRAINT [DF_tWorkOrder_OrderProcessed]  DEFAULT ((0)),
	[Frequency] [varchar](50) NULL,
	[Division] [varchar](50) NULL,
	[BillingCode] [varchar](50) NULL,
	[Rollup] [varchar](200) NULL,
	[MeterLocation] [varchar](25) NULL,
	[PipelineFootage] [float] NULL,
	[SpecialInstructions] [varchar](250) NULL,
	[OfficeName] [varchar](100) NULL,
	[RollUpOfficeName] [varchar](100) NULL,
	[BillingOfficeName] [varchar](100) NULL,
	[ComplianceFlag] [bit] NULL,
	[OverrideFlag] [bit] NULL,
	[ComplianceReason] [varchar](max) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tWorkOrder2019]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tWorkOrder2019](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientWorkOrderID] [varchar](200) NULL,
	[CreatedBy] [int] NULL,
	[ModifiedBy] [int] NULL,
	[CreatedDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
	[InspectionType] [varchar](50) NULL,
	[HouseNumber] [varchar](50) NULL,
	[Street] [varchar](100) NULL,
	[AptSuite] [varchar](50) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](50) NULL,
	[Zip] [varchar](50) NULL,
	[MeterNumber] [varchar](50) NULL,
	[MeterLocationDesc] [varchar](200) NULL,
	[LocationType] [varchar](50) NULL,
	[LocationLatitude] [float] NULL,
	[LocationLongitude] [float] NULL,
	[MapGrid] [varchar](50) NULL,
	[ComplianceStart] [date] NULL,
	[ComplianceEnd] [date] NULL,
	[MapLatitudeBegin] [float] NULL,
	[MapLongitudeBegin] [float] NULL,
	[MapLatitudeEnd] [float] NULL,
	[MapLongitudeEnd] [float] NULL,
	[AccountNumber] [varchar](50) NULL,
	[AccountName] [varchar](100) NULL,
	[AccountTelephoneNumber] [varchar](50) NULL,
	[Comments] [varchar](500) NULL,
	[CompletedFlag] [bit] NULL,
	[CompletedDate] [datetime] NULL,
	[InspectionAttemptCounter] [int] NULL,
	[SequenceNumber] [varchar](50) NULL,
	[SectionNumber] [varchar](50) NULL,
	[Shape] [geography] NULL,
	[EventIndicator] [int] NULL,
	[OrderProcessed] [bit] NULL,
	[Frequency] [varchar](50) NULL,
	[Division] [varchar](50) NULL,
	[BillingCode] [varchar](50) NULL,
	[Rollup] [varchar](200) NULL,
	[MeterLocation] [varchar](25) NULL,
	[PipelineFootage] [float] NULL,
	[SpecialInstructions] [varchar](250) NULL,
	[OfficeName] [varchar](100) NULL,
	[RollUpOfficeName] [varchar](100) NULL,
	[BillingOfficeName] [varchar](100) NULL,
PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tWorkOrderIDLookup]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tWorkOrderIDLookup](
	[InternalWorkOrderID] [varchar](200) NOT NULL,
	[ClientWorkOrderID] [varchar](200) NOT NULL,
	[LookupKey] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tWorkQueue]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tWorkQueue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WorkOrderID] [int] NULL,
	[AssignedUserID] [int] NULL,
	[WorkQueueStatus] [int] NULL CONSTRAINT [DF_tWorkQueur_WorkQueueStatus]  DEFAULT ((100)),
	[SectionNumber] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[tAssetID] [int] NULL,
	[TaskedOut] [bit] NULL CONSTRAINT [DF_tWorkQueue_TaskedOut]  DEFAULT ((0)),
	[ScheduledDispatchDate] [datetime] NULL,
 CONSTRAINT [PK_tWorkQueue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserTb]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserTb](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserUID] [varchar](100) NULL,
	[ProjectID] [int] NULL,
	[UserCreatedUID] [varchar](100) NULL,
	[UserModifiedUID] [varchar](100) NULL,
	[UserCreatedDate] [datetime] NULL,
	[UserModifiedDate] [datetime] NULL,
	[UserInactiveDTLT] [datetime] NULL,
	[UserComments] [varchar](2000) NULL,
	[UserRevision] [int] NULL,
	[UserActiveFlag] [bit] NULL CONSTRAINT [DF_UserTb_UserActiveFlag]  DEFAULT ((1)),
	[UserInActiveFlag] [bit] NULL,
	[UserLoginID] [varchar](50) NULL,
	[UserFirstName] [varchar](50) NULL,
	[UserLastName] [varchar](50) NULL,
	[UserLANID] [varchar](20) NULL,
	[UserPassword] [varchar](75) NULL,
	[UserEmployeeType] [varchar](50) NULL,
	[UserCompanyName] [varchar](200) NULL,
	[UserCompanyPhone] [varchar](20) NULL,
	[UserSupervisorUserUID] [varchar](100) NULL,
	[UserName] [varchar](100) NULL,
	[UserAppRoleType] [varchar](50) NULL,
	[UserPhone] [varchar](20) NULL,
	[UserCreatedDTLTOffset] [datetimeoffset](7) NULL,
	[UserModifiedDTLTOffset] [datetimeoffset](7) NULL,
	[UserInactiveDTLTOffset] [datetimeoffset](7) NULL,
	[UserArchiveFlag] [bit] NULL,
	[HomeWorkCenterUID] [varchar](100) NULL,
	[TestUser] [bit] NULL CONSTRAINT [DF_UserTb_TestUser]  DEFAULT ((0)),
 CONSTRAINT [PK_UserTb] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserTbTest]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserTbTest](
	[UserID] [int] NOT NULL,
	[UserUID] [varchar](100) NULL,
	[ProjectID] [int] NULL,
	[UserCreatedUID] [varchar](100) NULL,
	[UserModifiedUID] [varchar](100) NULL,
	[UserCreatedDate] [datetime] NULL,
	[UserModifiedDate] [datetime] NULL,
	[UserInactiveDTLT] [datetime] NULL,
	[UserComments] [varchar](2000) NULL,
	[UserRevision] [int] NULL,
	[UserActiveFlag] [bit] NULL CONSTRAINT [DF_UserTbTest_UserActiveFlag]  DEFAULT ((1)),
	[UserInActiveFlag] [bit] NULL,
	[UserLoginID] [varchar](50) NULL,
	[UserFirstName] [varchar](50) NULL,
	[UserLastName] [varchar](50) NULL,
	[UserLANID] [varchar](20) NULL,
	[UserPassword] [varchar](75) NULL,
	[UserEmployeeType] [varchar](50) NULL,
	[UserCompanyName] [varchar](200) NULL,
	[UserCompanyPhone] [varchar](20) NULL,
	[UserSupervisorUserUID] [varchar](100) NULL,
	[UserName] [varchar](100) NULL,
	[UserAppRoleType] [varchar](50) NULL,
	[UserPhone] [varchar](20) NULL,
	[UserCreatedDTLTOffset] [datetimeoffset](7) NULL,
	[UserModifiedDTLTOffset] [datetimeoffset](7) NULL,
	[UserInactiveDTLTOffset] [datetimeoffset](7) NULL,
	[UserArchiveFlag] [bit] NULL,
	[HomeWorkCenterUID] [varchar](100) NULL,
	[TestUser] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[xReportNameEmailList]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[xReportNameEmailList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmailID] [int] NULL,
	[ReportID] [int] NULL,
	[EmailType] [varchar](10) NULL,
	[ActiveFlag] [bit] NULL,
	[SortOrder] [int] NULL,
 CONSTRAINT [PK_xReportNameEmailList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ZtAssetBackup20190319]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZtAssetBackup20190319](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AssetTabletID] [varchar](100) NULL,
	[InspectionID] [int] NULL,
	[MapGrid] [varchar](50) NULL,
	[CreatedUserID] [int] NULL,
	[HouseNo] [varchar](50) NULL,
	[Street] [varchar](100) NULL,
	[Apt] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[ReverseGeoLocationString] [varchar](max) NULL,
	[MeterID] [varchar](50) NULL,
	[PipelineType] [varchar](50) NULL,
	[Grade1ReleaseReasonType] [varchar](50) NULL,
	[Grade1ReleaseDateTime] [datetime] NULL,
	[Comments] [varchar](500) NULL,
	[Photo1Path] [varchar](max) NULL,
	[SrcDTLT] [datetime] NULL,
	[SrvDTLT] [datetime] NULL,
	[SrvDTLTOffset] [datetimeoffset](7) NULL,
	[Inspection] [varchar](50) NULL,
	[AOCs] [int] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[GPSType] [varchar](50) NULL,
	[GPSSentence] [varchar](max) NULL,
	[GPSTime] [varchar](50) NULL,
	[FixQuality] [float] NULL,
	[NumberOfSatellites] [int] NULL,
	[HDOP] [float] NULL,
	[AltitudeMetersAboveMeanSeaLevel] [float] NULL,
	[HeightOfGeoid] [float] NULL,
	[TimeSecondsSinceLastDGPS] [float] NULL,
	[CheckSumData] [varchar](50) NULL,
	[Bearing] [float] NULL,
	[Speed] [float] NULL,
	[NumberOfGPSAttempts] [int] NULL,
	[Zip] [varchar](50) NULL,
	[MeterLocation] [varchar](25) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ZtCalibrationBackup]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZtCalibrationBackup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NULL,
	[EquipmentSourceID] [varchar](200) NULL,
	[CreatedUserID] [int] NULL,
	[SrcDTLT] [datetime] NULL,
	[SrvDTLT] [datetime] NULL,
	[SrvDTLTOffset] [datetimeoffset](7) NULL,
	[EquipmentType] [varchar](50) NULL,
	[SerialNumber] [varchar](50) NULL,
	[CalibrationVerification] [bit] NULL,
	[CalibrationLevel] [float] NULL,
	[Comments] [varchar](500) NULL,
	[DeletedFlag] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ZtEventBackup]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZtEventBackup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventTabletID] [varchar](100) NULL,
	[EventType] [int] NULL,
	[InspectionTabletID] [varchar](100) NULL,
	[SrcDTLT] [datetime] NULL,
	[SrvDTLT] [datetime] NULL,
	[SrvDTLTOffset] [datetimeoffset](7) NULL,
	[CreatedByUserID] [int] NULL,
	[LocationID] [varchar](50) NULL,
	[LocationAddress] [varchar](200) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[MapGrid] [varchar](50) NULL,
	[Photo1Path] [varchar](500) NULL,
	[Photo2Path] [varchar](500) NULL,
	[Photo3Path] [varchar](500) NULL,
	[AOCReason] [varchar](200) NULL,
	[CGEReason] [varchar](200) NULL,
	[LeakNumber] [varchar](200) NULL,
	[LeakGrade] [varchar](100) NULL,
	[LeakAboveOrBelow] [varchar](200) NULL,
	[DetectedByEquipment] [varchar](200) NULL,
	[EquipmentSerialNumber] [varchar](50) NULL,
	[Comments] [varchar](500) NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[GPSType] [varchar](50) NULL,
	[GPSSentence] [varchar](max) NULL,
	[GPSTime] [varchar](50) NULL,
	[FixQuality] [float] NULL,
	[NumberOfSatellites] [int] NULL,
	[HDOP] [float] NULL,
	[AltitudeMetersAboveMeanSeaLevel] [float] NULL,
	[HeightOfGeoid] [float] NULL,
	[TimeSecondsSinceLastDGPS] [float] NULL,
	[ChecksumData] [varchar](50) NULL,
	[Bearing] [float] NULL,
	[Speed] [float] NULL,
	[NumberOfGPSAttempts] [int] NULL,
	[InspectionID] [int] NOT NULL,
	[DeletedFlag] [bit] NULL,
	[SplashGuardNeeded] [bit] NULL,
	[SplashGuardInstalled] [bit] NULL,
	[TracerWireMissing] [bit] NULL,
	[TamperSealNotPresent] [bit] NULL,
	[EnergyDiversionPresent] [bit] NULL,
	[ACGrade] [varchar](100) NULL,
	[RiserPostBad] [bit] NULL,
	[RecommendToRetireInActiveService] [bit] NULL,
	[FacilitiesNeedToBeProtected] [bit] NULL,
	[Other] [bit] NULL,
	[AOCFlag] [bit] NULL,
	[AccessIssues] [varchar](100) NULL,
	[CGE] [varchar](25) NULL,
	[LeakRepaired] [bit] NULL,
	[LeakFoundMainOrService] [varchar](20) NULL,
	[BadDogPresent] [bit] NULL,
	[NIFReason] [varchar](100) NULL,
	[PipelineAOCIssues] [varchar](100) NULL,
	[EnergyDiversionPhoto1Path] [varchar](500) NULL,
	[EnergyDiversionPhoto2Path] [varchar](500) NULL,
	[EnergyDiversionPhoto3Path] [varchar](500) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ZtWorkOrderbkup04042019]    Script Date: 4/24/2019 9:25:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZtWorkOrderbkup04042019](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientWorkOrderID] [varchar](200) NULL,
	[CreatedBy] [int] NULL,
	[ModifiedBy] [int] NULL,
	[CreatedDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
	[InspectionType] [varchar](50) NULL,
	[HouseNumber] [varchar](50) NULL,
	[Street] [varchar](100) NULL,
	[AptSuite] [varchar](50) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](50) NULL,
	[Zip] [varchar](50) NULL,
	[MeterNumber] [varchar](50) NULL,
	[MeterLocationDesc] [varchar](200) NULL,
	[LocationType] [varchar](50) NULL,
	[LocationLatitude] [float] NULL,
	[LocationLongitude] [float] NULL,
	[MapGrid] [varchar](50) NULL,
	[ComplianceStart] [date] NULL,
	[ComplianceEnd] [date] NULL,
	[MapLatitudeBegin] [float] NULL,
	[MapLongitudeBegin] [float] NULL,
	[MapLatitudeEnd] [float] NULL,
	[MapLongitudeEnd] [float] NULL,
	[AccountNumber] [varchar](50) NULL,
	[AccountName] [varchar](100) NULL,
	[AccountTelephoneNumber] [varchar](50) NULL,
	[Comments] [varchar](500) NULL,
	[CompletedFlag] [bit] NULL,
	[CompletedDate] [datetime] NULL,
	[InspectionAttemptCounter] [int] NULL,
	[SequenceNumber] [varchar](50) NULL,
	[SectionNumber] [varchar](50) NULL,
	[Shape] [geography] NULL,
	[EventIndicator] [int] NULL,
	[OrderProcessed] [bit] NULL,
	[Frequency] [varchar](50) NULL,
	[Division] [varchar](50) NULL,
	[BillingCode] [varchar](50) NULL,
	[Rollup] [varchar](200) NULL,
	[MeterLocation] [varchar](25) NULL,
	[PipelineFootage] [float] NULL,
	[SpecialInstructions] [varchar](250) NULL,
	[OfficeName] [varchar](100) NULL,
	[RollUpOfficeName] [varchar](100) NULL,
	[BillingOfficeName] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tMissingPictures] ADD  CONSTRAINT [DF_tMissingPictures_DateDiscovered]  DEFAULT (getdate()) FOR [DateDiscovered]
GO
ALTER TABLE [dbo].[tNotification] ADD  CONSTRAINT [DF_tNotification_SrvDTLT]  DEFAULT (getdate()) FOR [SrvDTLT]
GO
ALTER TABLE [dbo].[tPipelinePoints] ADD  CONSTRAINT [DF_t_Gasmains_Vertices_BreadCrumbUID]  DEFAULT ('') FOR [BreadCrumbUID]
GO
ALTER TABLE [dbo].[tPipelinePoints] ADD  CONSTRAINT [DF_t_Gasmains_Vertices_Distance]  DEFAULT ((0)) FOR [Distance]
GO
ALTER TABLE [dbo].[tPipelinePoints] ADD  CONSTRAINT [DF_t_Gasmains_Vertices_Verified]  DEFAULT ((0)) FOR [Verified]
GO
ALTER TABLE [dbo].[tTechMissingBCAdded] ADD  CONSTRAINT [DF_tTechMissingBCAdded_TotalInserted]  DEFAULT ((0)) FOR [TotalInserted]
GO
ALTER TABLE [dbo].[tTechMissingBCAdded] ADD  CONSTRAINT [DF_tTechMissingBCAdded_InsertedDateTime]  DEFAULT (getdate()) FOR [InsertedDateTime]
GO
ALTER TABLE [dbo].[auth_item]  WITH CHECK ADD FOREIGN KEY([rule_name])
REFERENCES [dbo].[auth_rule] ([name])
GO
ALTER TABLE [dbo].[auth_item_child]  WITH CHECK ADD FOREIGN KEY([child])
REFERENCES [dbo].[auth_item] ([name])
GO
ALTER TABLE [dbo].[auth_item_child]  WITH CHECK ADD FOREIGN KEY([parent])
REFERENCES [dbo].[auth_item] ([name])
GO
ALTER TABLE [dbo].[tAsset]  WITH CHECK ADD  CONSTRAINT [FK_tAsset_CreatedBy] FOREIGN KEY([CreatedUserID])
REFERENCES [dbo].[UserTb] ([UserID])
GO
ALTER TABLE [dbo].[tAsset] CHECK CONSTRAINT [FK_tAsset_CreatedBy]
GO
ALTER TABLE [dbo].[tCalibration]  WITH CHECK ADD  CONSTRAINT [FK_tCalibration_CreatedBy] FOREIGN KEY([CreatedUserID])
REFERENCES [dbo].[UserTb] ([UserID])
GO
ALTER TABLE [dbo].[tCalibration] CHECK CONSTRAINT [FK_tCalibration_CreatedBy]
GO
ALTER TABLE [dbo].[tEvent]  WITH CHECK ADD  CONSTRAINT [FK_tEvent_CreatedBy] FOREIGN KEY([CreatedByUserID])
REFERENCES [dbo].[UserTb] ([UserID])
GO
ALTER TABLE [dbo].[tEvent] CHECK CONSTRAINT [FK_tEvent_CreatedBy]
GO
ALTER TABLE [dbo].[tEvent]  WITH CHECK ADD  CONSTRAINT [FK_tEvent_EventType] FOREIGN KEY([EventType])
REFERENCES [dbo].[rStatusLookup] ([StatusCode])
GO
ALTER TABLE [dbo].[tEvent] CHECK CONSTRAINT [FK_tEvent_EventType]
GO
ALTER TABLE [dbo].[tInspection]  WITH CHECK ADD  CONSTRAINT [FK_tInspection_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserTb] ([UserID])
GO
ALTER TABLE [dbo].[tInspection] CHECK CONSTRAINT [FK_tInspection_CreatedBy]
GO
ALTER TABLE [dbo].[tNotification]  WITH CHECK ADD  CONSTRAINT [FK_tNotification_NotificationType] FOREIGN KEY([NotificationType])
REFERENCES [dbo].[rStatusLookup] ([StatusCode])
GO
ALTER TABLE [dbo].[tNotification] CHECK CONSTRAINT [FK_tNotification_NotificationType]
GO
ALTER TABLE [dbo].[tNotification]  WITH CHECK ADD  CONSTRAINT [FK_tNotification_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserTb] ([UserID])
GO
ALTER TABLE [dbo].[tNotification] CHECK CONSTRAINT [FK_tNotification_UserID]
GO
ALTER TABLE [dbo].[tPipelinePoints]  WITH CHECK ADD  CONSTRAINT [FK_tPipelinePoints_tWorkOrder] FOREIGN KEY([WorkOrderID])
REFERENCES [dbo].[tWorkOrder] ([ID])
GO
ALTER TABLE [dbo].[tPipelinePoints] CHECK CONSTRAINT [FK_tPipelinePoints_tWorkOrder]
GO
ALTER TABLE [dbo].[tTaskOut]  WITH CHECK ADD  CONSTRAINT [FK_tTaskOut_CreatedID] FOREIGN KEY([CreatedUserID])
REFERENCES [dbo].[UserTb] ([UserID])
GO
ALTER TABLE [dbo].[tTaskOut] CHECK CONSTRAINT [FK_tTaskOut_CreatedID]
GO
ALTER TABLE [dbo].[tWorkOrder]  WITH CHECK ADD  CONSTRAINT [FK_tWorkOrder_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserTb] ([UserID])
GO
ALTER TABLE [dbo].[tWorkOrder] CHECK CONSTRAINT [FK_tWorkOrder_CreatedBy]
GO
ALTER TABLE [dbo].[tWorkOrder]  WITH CHECK ADD  CONSTRAINT [FK_tWorkOrder_EventIndicator] FOREIGN KEY([EventIndicator])
REFERENCES [dbo].[rStatusLookup] ([StatusCode])
GO
ALTER TABLE [dbo].[tWorkOrder] CHECK CONSTRAINT [FK_tWorkOrder_EventIndicator]
GO
ALTER TABLE [dbo].[tWorkOrder]  WITH CHECK ADD  CONSTRAINT [FK_tWorkOrder_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[UserTb] ([UserID])
GO
ALTER TABLE [dbo].[tWorkOrder] CHECK CONSTRAINT [FK_tWorkOrder_ModifiedBy]
GO
ALTER TABLE [dbo].[tWorkQueue]  WITH CHECK ADD  CONSTRAINT [FK_tWorkQueue_AssignedID] FOREIGN KEY([AssignedUserID])
REFERENCES [dbo].[UserTb] ([UserID])
GO
ALTER TABLE [dbo].[tWorkQueue] CHECK CONSTRAINT [FK_tWorkQueue_AssignedID]
GO
ALTER TABLE [dbo].[tWorkQueue]  WITH CHECK ADD  CONSTRAINT [FK_tWorkQueue_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserTb] ([UserID])
GO
ALTER TABLE [dbo].[tWorkQueue] CHECK CONSTRAINT [FK_tWorkQueue_CreatedBy]
GO
ALTER TABLE [dbo].[tWorkQueue]  WITH CHECK ADD  CONSTRAINT [FK_tWorkQueue_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[UserTb] ([UserID])
GO
ALTER TABLE [dbo].[tWorkQueue] CHECK CONSTRAINT [FK_tWorkQueue_ModifiedBy]
GO
ALTER TABLE [dbo].[tWorkQueue]  WITH CHECK ADD  CONSTRAINT [FK_tWorkQueue_WorkQueueStatus] FOREIGN KEY([WorkQueueStatus])
REFERENCES [dbo].[rStatusLookup] ([StatusCode])
GO
ALTER TABLE [dbo].[tWorkQueue] CHECK CONSTRAINT [FK_tWorkQueue_WorkQueueStatus]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Moble, Web, Both' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rDropDown', @level2type=N'COLUMN',@level2name=N'DropDownType'
GO
