/****** Object:  Table [dbo].[tCalibration]    Script Date: 4/17/2019 3:58:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tCalibration](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NULL,
	[EquipmentSourceID] [varchar](200) NULL,
	[CreatedUserID] [int] NULL,
	[SrcDTLT] [datetime] NULL,
	[SrvDTLT] [datetime] NULL CONSTRAINT [DF_tCalibration_SrvDTLT]  DEFAULT (getdate()),
	[SrvDTLTOffset] [datetimeoffset](7) NULL CONSTRAINT [DF_tCalibration_SrvDTLTOffset]  DEFAULT (sysdatetimeoffset()),
	[EquipmentType] [varchar](50) NULL,
	[SerialNumber] [varchar](50) NULL,
	[CalibrationVerification] [bit] NULL CONSTRAINT [DF_tCalibration_CalibrationVerification]  DEFAULT ((0)),
	[CalibrationLevel] [float] NULL,
	[Comments] [varchar](500) NULL,
	[DeletedFlag] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_tCalibration] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tCalibration]  WITH CHECK ADD  CONSTRAINT [FK_tCalibration_CreatedBy] FOREIGN KEY([CreatedUserID])
REFERENCES [dbo].[UserTb] ([UserID])
GO

ALTER TABLE [dbo].[tCalibration] CHECK CONSTRAINT [FK_tCalibration_CreatedBy]
GO


