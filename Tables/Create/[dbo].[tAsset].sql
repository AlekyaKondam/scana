/****** Object:  Table [dbo].[tAsset]    Script Date: 4/17/2019 3:57:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tAsset](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AssetTabletID] [varchar](100) NULL,
	[InspectionID] [int] NULL,
	[MapGrid] [varchar](50) NULL,
	[CreatedUserID] [int] NULL,
	[HouseNo] [varchar](50) NULL,
	[Street] [varchar](100) NULL,
	[Apt] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[ReverseGeoLocationString] [varchar](max) NULL,
	[MeterID] [varchar](50) NULL,
	[PipelineType] [varchar](50) NULL,
	[Grade1ReleaseReasonType] [varchar](50) NULL,
	[Grade1ReleaseDateTime] [datetime] NULL,
	[Comments] [varchar](500) NULL,
	[Photo1Path] [varchar](max) NULL,
	[SrcDTLT] [datetime] NULL,
	[SrvDTLT] [datetime] NULL CONSTRAINT [DF_tAsset_SrvDTLT]  DEFAULT (getdate()),
	[SrvDTLTOffset] [datetimeoffset](7) NULL CONSTRAINT [DF_tAsset_SrvDTLTOffset]  DEFAULT (sysdatetimeoffset()),
	[Inspection] [varchar](50) NULL,
	[AOCs] [int] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[GPSType] [varchar](50) NULL,
	[GPSSentence] [varchar](max) NULL,
	[GPSTime] [varchar](50) NULL,
	[FixQuality] [float] NULL,
	[NumberOfSatellites] [int] NULL,
	[HDOP] [float] NULL,
	[AltitudeMetersAboveMeanSeaLevel] [float] NULL,
	[HeightOfGeoid] [float] NULL,
	[TimeSecondsSinceLastDGPS] [float] NULL,
	[CheckSumData] [varchar](50) NULL,
	[Bearing] [float] NULL,
	[Speed] [float] NULL,
	[NumberOfGPSAttempts] [int] NULL,
	[Zip] [varchar](50) NULL,
	[MeterLocation] [varchar](25) NULL,
 CONSTRAINT [PK_tAsset] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tAsset]  WITH CHECK ADD  CONSTRAINT [FK_tAsset_CreatedBy] FOREIGN KEY([CreatedUserID])
REFERENCES [dbo].[UserTb] ([UserID])
GO

ALTER TABLE [dbo].[tAsset] CHECK CONSTRAINT [FK_tAsset_CreatedBy]
GO


