/****** Object:  Table [dbo].[rStatusLookup]    Script Date: 4/17/2019 3:57:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[rStatusLookup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [varchar](100) NULL,
	[ModifiedBy] [varchar](100) NULL,
	[CreatedDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
	[StatusType] [varchar](50) NULL,
	[StatusCode] [int] NULL,
	[StatusDescription] [varchar](200) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


