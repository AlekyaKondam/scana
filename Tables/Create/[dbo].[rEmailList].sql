
/****** Object:  Table [dbo].[rEmailList]    Script Date: 4/17/2019 3:55:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[rEmailList](
	[emaillistID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[DateAdded] [datetime] NULL,
	[email] [varchar](200) NULL,
	[ActiveFlag] [bit] NULL,
 CONSTRAINT [PK_rEmailList] PRIMARY KEY CLUSTERED 
(
	[emaillistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


