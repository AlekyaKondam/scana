USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[tTabletDataInsertArchive]    Script Date: 4/22/2019 2:08:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tTabletDataInsertArchive](
	[TabletDataInsertArchiveID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CreatedUserUID] [varchar](100) NULL,
	[SvrDTLT] [datetime] NULL CONSTRAINT [DF_t_TabletDataInsertArchive_SvrDTLT]  DEFAULT (getdate()),
	[SvrDTLTOffset] [datetimeoffset](7) NULL CONSTRAINT [DF_t_TabletDataInsertArchive_SvrDTLTOffset]  DEFAULT (sysdatetimeoffset()),
	[TransactionType] [varchar](100) NULL,
	[InsertedData] [varchar](max) NULL,
 CONSTRAINT [PK_t_TabletDataInsertArchive] PRIMARY KEY CLUSTERED 
(
	[TabletDataInsertArchiveID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


