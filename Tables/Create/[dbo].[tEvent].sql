/****** Object:  Table [dbo].[tEvent]    Script Date: 4/17/2019 3:59:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tEvent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventTabletID] [varchar](100) NULL,
	[EventType] [int] NULL,
	[InspectionTabletID] [varchar](100) NULL,
	[SrcDTLT] [datetime] NULL,
	[SrvDTLT] [datetime] NULL CONSTRAINT [DF_tEvent_SrvDTLT]  DEFAULT (getdate()),
	[SrvDTLTOffset] [datetimeoffset](7) NULL CONSTRAINT [DF_tEvent_SrvDTLTOffset]  DEFAULT (sysdatetimeoffset()),
	[CreatedByUserID] [int] NULL,
	[LocationID] [varchar](50) NULL,
	[LocationAddress] [varchar](200) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[MapGrid] [varchar](50) NULL,
	[Photo1Path] [varchar](500) NULL,
	[Photo2Path] [varchar](500) NULL,
	[Photo3Path] [varchar](500) NULL,
	[AOCReason] [varchar](200) NULL,
	[CGEReason] [varchar](200) NULL,
	[LeakNumber] [varchar](200) NULL,
	[LeakGrade] [varchar](100) NULL,
	[LeakAboveOrBelow] [varchar](200) NULL,
	[DetectedByEquipment] [varchar](200) NULL,
	[EquipmentSerialNumber] [varchar](50) NULL,
	[Comments] [varchar](500) NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[GPSType] [varchar](50) NULL,
	[GPSSentence] [varchar](max) NULL,
	[GPSTime] [varchar](50) NULL,
	[FixQuality] [float] NULL,
	[NumberOfSatellites] [int] NULL,
	[HDOP] [float] NULL,
	[AltitudeMetersAboveMeanSeaLevel] [float] NULL,
	[HeightOfGeoid] [float] NULL,
	[TimeSecondsSinceLastDGPS] [float] NULL,
	[ChecksumData] [varchar](50) NULL,
	[Bearing] [float] NULL,
	[Speed] [float] NULL,
	[NumberOfGPSAttempts] [int] NULL,
	[InspectionID] [int] NOT NULL,
	[DeletedFlag] [bit] NULL CONSTRAINT [DF_tEvent_DeletedFlag]  DEFAULT ((0)),
	[SplashGuardNeeded] [bit] NULL,
	[SplashGuardInstalled] [bit] NULL,
	[TracerWireMissing] [bit] NULL,
	[TamperSealNotPresent] [bit] NULL,
	[EnergyDiversionPresent] [bit] NULL,
	[ACGrade] [varchar](100) NULL,
	[RiserPostBad] [bit] NULL,
	[RecommendToRetireInActiveService] [bit] NULL,
	[FacilitiesNeedToBeProtected] [bit] NULL,
	[Other] [bit] NULL,
	[AOCFlag] [bit] NULL,
	[AccessIssues] [varchar](100) NULL,
	[CGE] [varchar](25) NULL,
	[LeakRepaired] [bit] NULL,
	[LeakFoundMainOrService] [varchar](20) NULL,
	[BadDogPresent] [bit] NULL,
	[NIFReason] [varchar](100) NULL,
	[PipelineAOCIssues] [varchar](100) NULL,
	[EnergyDiversionPhoto1Path] [varchar](500) NULL,
	[EnergyDiversionPhoto2Path] [varchar](500) NULL,
	[EnergyDiversionPhoto3Path] [varchar](500) NULL,
 CONSTRAINT [PK_tEvent] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tEvent]  WITH CHECK ADD  CONSTRAINT [FK_tEvent_CreatedBy] FOREIGN KEY([CreatedByUserID])
REFERENCES [dbo].[UserTb] ([UserID])
GO

ALTER TABLE [dbo].[tEvent] CHECK CONSTRAINT [FK_tEvent_CreatedBy]
GO

ALTER TABLE [dbo].[tEvent]  WITH CHECK ADD  CONSTRAINT [FK_tEvent_EventType] FOREIGN KEY([EventType])
REFERENCES [dbo].[rStatusLookup] ([StatusCode])
GO

ALTER TABLE [dbo].[tEvent] CHECK CONSTRAINT [FK_tEvent_EventType]
GO


