USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[UserTb]    Script Date: 4/22/2019 2:10:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[UserTb](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserUID] [varchar](100) NULL,
	[ProjectID] [int] NULL,
	[UserCreatedUID] [varchar](100) NULL,
	[UserModifiedUID] [varchar](100) NULL,
	[UserCreatedDate] [datetime] NULL,
	[UserModifiedDate] [datetime] NULL,
	[UserInactiveDTLT] [datetime] NULL,
	[UserComments] [varchar](2000) NULL,
	[UserRevision] [int] NULL,
	[UserActiveFlag] [bit] NULL CONSTRAINT [DF_UserTb_UserActiveFlag]  DEFAULT ((1)),
	[UserInActiveFlag] [bit] NULL,
	[UserLoginID] [varchar](50) NULL,
	[UserFirstName] [varchar](50) NULL,
	[UserLastName] [varchar](50) NULL,
	[UserLANID] [varchar](20) NULL,
	[UserPassword] [varchar](75) NULL,
	[UserEmployeeType] [varchar](50) NULL,
	[UserCompanyName] [varchar](200) NULL,
	[UserCompanyPhone] [varchar](20) NULL,
	[UserSupervisorUserUID] [varchar](100) NULL,
	[UserName] [varchar](100) NULL,
	[UserAppRoleType] [varchar](50) NULL,
	[UserPhone] [varchar](20) NULL,
	[UserCreatedDTLTOffset] [datetimeoffset](7) NULL,
	[UserModifiedDTLTOffset] [datetimeoffset](7) NULL,
	[UserInactiveDTLTOffset] [datetimeoffset](7) NULL,
	[UserArchiveFlag] [bit] NULL,
	[HomeWorkCenterUID] [varchar](100) NULL,
	[TestUser] [bit] NULL CONSTRAINT [DF_UserTb_TestUser]  DEFAULT ((0)),
 CONSTRAINT [PK_UserTb] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


