USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  Table [dbo].[tTabletDataInsertBreadcrumbArchive]    Script Date: 4/22/2019 2:08:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tTabletDataInsertBreadcrumbArchive](
	[tTabletDataInsertArchiveBreadcrumbID] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [int] NULL,
	[UserUID] [varchar](100) NULL,
	[SvrDTLT] [datetime] NULL CONSTRAINT [DFtTabletDataInsertArchiveBC_SvrDTLT]  DEFAULT (getdate()),
	[SvrDTLT_Offset] [datetimeoffset](7) NULL CONSTRAINT [DFtTabletDataInsertArchiveBC_SvrDTLT_Offset]  DEFAULT (sysdatetimeoffset()),
	[TransactionType] [varchar](50) NULL,
	[InsertedData] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

