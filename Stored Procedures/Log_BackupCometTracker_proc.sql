USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[Log_BackupCometTracker_proc]    Script Date: 4/23/2019 1:20:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*****************************************************************************************************/
/*****************************************************************************************************/

/*  	Procedure Name:		Log_BackupCometTracker_proc											     */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		07/14/2016																 */
/*		Create Reason:		Log Job  															     */
/*		Parameter(1):		User ID integer															 */
/*		Utilized By:		Database Administrator	- SQL Agent Maintenance Job						 */
/*		Version :			1 .. Creation  expect version 2 to archive flagged records				 */
/*																									 */
/*		Status:				Enabled 																 */
/*-- example				exec BackUp	
     Modified By :		Alekya Kondam
     Modified Date:		03/29/2019	
     Modifications made : added Exceptional handling 															 */
/*																									 */
/******************************************************************************************************/
/******************************************************************************************************/

CREATE PROCEDURE [dbo].[Log_BackupCometTracker_proc]
AS
BEGIN TRY

  INSERT INTO CometTracker.dbo.SCJobLogTb
  EXEC msdb.dbo.sp_help_jobsteplog @job_name = N'BackupCometTracker'
END TRY
BEGIN CATCH
  THROW;
END CATCH



GO


