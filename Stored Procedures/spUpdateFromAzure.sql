USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateFromAzure]    Script Date: 4/24/2019 9:12:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE proc [dbo].[spUpdateFromAzure] 
as

Declare @CurrentDB varchar(100) = DB_NAME()
	,@URL varchar(100)
	,@YorkURL varchar(100)
	,@ScanaURL varchar(100)

select @URL =
CASE
 WHEN @CurrentDB = 'CometTracker' THEN 'scctdev'  
 WHEN @CurrentDB = 'CometTracker_Stage' THEN 'scctstage' 
 WHEN @CurrentDB = 'AZCometTracker_PROD' THEN 'scct'
 ELSE NULL

END

select @YorkURL =
CASE
 WHEN @CurrentDB = 'CometTracker' THEN 'yorktdev'  
 WHEN @CurrentDB = 'CometTracker_Stage' THEN 'yorkstage' 
 WHEN @CurrentDB = 'AZCometTracker_PROD' THEN 'york'
 ELSE NULL

END

select @ScanaURL =
CASE
 WHEN @CurrentDB = 'CometTracker' THEN 'scanadev'  
 WHEN @CurrentDB = 'CometTracker_Stage' THEN 'scanastage' 
 WHEN @CurrentDB = 'AZCometTracker_PROD' THEN 'azurescana'
 ELSE NULL

END

-- First we will add any new data


	--New ProjectType (Done)
BEGIN TRY

		Insert Into refProjectType
		(
			ProjectTypeName, ProjectTypeFullName, ProjectTypeReferenceID, ProjectTypeCreatedDate, ProjectTypeModifiedDate
		)
		select CustomerTypeName [ProjectTypeName], CustomerTypeName [ProjectTypeFullName], CustomerTypeID [ProjectTypeReferenceID], Cast(TimeCreated as datetime) [Created], cast(TimeModified as DateTime) [Modified] 
		from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[SLCustomerTypes]
		Where IsActive = 1 and CustomerTypeID Not In (SELECT ProjectTypeReferenceID FROM [refProjectType])

	--New Clients (Done)

		Insert Into [dbo].[ClientTb]
		(
			ClientName, ReferenceID, ClientCreateDate, ClientModifiedDate, ClientState
		)
		select CustName [Name], CustomerID, Cast(TimeCreated as datetime), cast(TimeModified as DateTime), [State]
		from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[SLCustomers]
		Where IsActive = 1 and CustomerID not in (select ReferenceID from [dbo].[ClientTb] where ReferenceID is not null)

	--New Projects (Done)

		Insert Into [dbo].[ProjectTb]
		(
			ProjectName, ProjectClientID, ProjectType, ProjectReferenceID, ProjectUrlPrefix, ProjectProjectTypeReferenceID, ProjectQBProjectID, ProjectCreateDate, ProjectModifiedDate, ProjectStatus, ProjectState, ProjectClass, ProjectLandingPage
		)
		
		select ProjectName [ProjectFullName] 
		, c.ClientID
		, ISNULL([Type].CustomerTypeName, 'UnKnown') [ProjectTypeFullName]
		, Project.ProjectID
		,CASE
			WHEN Project.CustomerID = 'YOR01' THEN @YorkURL --York
			WHEN Project.CustomerID = 'SCA01' THEN @ScanaURL --Scana
			ELSE @URL
		END [ProjectUrlPrefix]
		, Project.CustomerTypeID [ProjectTypeID]
		, Project.ProjectID [ClinetID]
		,Cast(Project.TimeCreated as datetime) [Created]
		,cast(Project.TimeModified as DateTime) [Modified]
		,Project.isactive [ProjectStatus]
		,Project.State [State]
		,'' [Class] --????????
		, [dbo].[fnGetLandingPage](ISNULL([Type].CustomerTypeName, 'UnKnown'))
		from (Select * from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[SLProjects] where isactive = 1) [Project]
		Left Join ClientTb c on Project.CustomerID = c.ReferenceID
		Left Join (select * from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[SLCustomerTypes] where IsActive = 1) [Type] on Project.CustomerTypeID = [Type].CustomerTypeID
		Where Project.ProjectID not in (Select ProjectReferenceID from [dbo].[ProjectTb] where ProjectReferenceID is not NULL)


	
	--New Task (Done)

		Truncate Table [dbo].[refTask]
		
		Insert Into [dbo].[refTask]
		(
			TaskName, TaskReferenceID, TaskCreatedDate, TaskModifiedDate, Category
		)
		select 
			RefTask.TaskName [FullName]
			,RefTask.TaskID [TaskReferenceID]
			, Min(Cast(RefTask.TimeCreated as datetime))
			, Max(cast(RefTask.TimeModified as DateTime))
			, RefTask.Category 
			--,RefTask.ProjectID
		from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[SLTasks] RefTask
		 
		--Where RefTask.TaskID not in (select TaskReferenceID from refTask)
		Group by RefTask.TaskName
			,RefTask.TaskID
			,RefTask.Category


	--New Task and Project Associations (Done)

		Truncate Table [dbo].[refTaskProject]
		
		Insert Into refTaskProject
		(
			TaskID, ProjectID
		)		
		Select distinct Task.TaskID, Project.ProjectID from 
		(select * from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[SLTasks] Where IsActive = 1) SLTASK
		Join [refTask] Task on SLTASK.TaskID = Task.TaskReferenceID
		join [ProjectTb] Project on SLTASK.ProjectID = Project.ProjectReferenceID
		Left Join refTaskProject refTask on Task.TaskID = refTask.TaskID and Project.ProjectID = refTask.Projectid
		Where refTask.ID is null

	

	--New ChartOfAccounts (questions)

		Insert Into refChartOfAccount
		(
		ChartOfAccountID, ChartOfAccountDescription, ChartOfAccountUnits, ChartOfAccountUsage
		)
		select
		COAID, [Description], Units, Usage
		From [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[ChartOfAccounts]
		Where COAID not in (select ChartOfAccountID From refChartOfAccount )

	--New PaySource  (questions)

		Insert Into [dbo].[refPaySource]
		(PaySourceID, CompanyName)
		select PaySourceID, CompanyName from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[PaySource]
		Where PaySourceID not in (Select PaySourceID from [dbo].[refPaySource])

	--New EmployeeIDMap, this is used to determine the employee Oasis, ADP, and QB IDs  (questions)

		Insert Into refEmployeeIDMapTb
		(SourceEmployeeID, PaySourceID, SCCEmployeeID)
		select ref.SourceEmployeeID, ref.PaySourceID, ref.SCCEmployeeID from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[EmployeeIDMap] ref
		Left Join [dbo].[refEmployeeIDMapTb] lref on ref.SourceEmployeeID = lref.SourceEmployeeID and ref.SCCEmployeeID = lref.SCCEmployeeID
		Where lref.SourceEmployeeID is null


/*******************************************************

--  Now we update data

*******************************************************/

	--Update ProjectType Based on Modified Date (Done)

		
		Update PT
		Set ProjectTypeName = refPT.ProjectTypeName
		, ProjectTypeFullName = refPT.ProjectTypeFullName
		, ProjectTypeModifiedDate = refPT.Modified
		From refProjectType PT
		Join (select CustomerTypeName [ProjectTypeName], CustomerTypeName [ProjectTypeFullName], CustomerTypeID [ProjectTypeReferenceID], Cast(TimeCreated as datetime) [Created], cast(TimeModified as DateTime) [Modified] 
			from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[SLCustomerTypes]) refPT
				On PT.ProjectTypeReferenceID = refPT.ProjectTypeReferenceID 
					and CASE WHEN PT.ProjectTypeModifiedDate IS NULL THEN PT.ProjectTypeCreatedDate ELSE PT.ProjectTypeModifiedDate END < 
						CASE WHEN refPT.Modified IS NULL THEN refPT.Created else refPT.Modified END
		

	--Update Clients Based on Modified Date (Done)


		Update C
		Set ClientName = refC.Name
		, ClientState = refC.[State]
		, ClientActiveFlag = refC.ActiveFlag
		, ClientModifiedBy = 'Automation'
		, ClientModifiedDate = refC.Modified
		From ClientTb C
		join (select CustName [Name], CustomerID [CustomerID], Cast(TimeCreated as datetime) [Created], cast(TimeModified as DateTime) Modified, [State] [State], IsActive [ActiveFlag]
			from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[SLCustomers]) refC
				On C.ReferenceID = refC.CustomerID
					and CASE WHEN c.ClientModifiedDate is null then c.ClientCreateDate ELSE c.ClientModifiedDate END <
						CASE WHEN refC.Modified is null then refC.Created ELSE refC.Modified END


	--Update Projects.  Because this is a combination of many tables we can't use a date modified, so we just update all. (Done)

		
		Update P
		Set ProjectName = refP.ProjectName
		, ProjectClientID = c.ClientID
		, ProjectType = ISNULL([refT].CustomerType, 'UnKnown')
		, ProjectUrlPrefix = CASE
					WHEN refP.CustomerID = 'YOR01' THEN @YorkURL --York
					WHEN refP.CustomerID = 'SCA01' THEN @ScanaURL --Scana
					ELSE @URL
				END
		, ProjectProjectTypeReferenceID = refP.CustomerTypeID
		, ProjectModifiedDate = cast(refP.TimeModified as DateTime)
		, ProjectStatus = refP.isactive
		, ProjectState = refP.[State]
		, ProjectLandingPage = [dbo].[fnGetLandingPage](ISNULL(refT.CustomerType, 'UnKnown'))
		From ProjectTb P
		Join (Select * from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[SLProjects]) [refP] on P.ProjectReferenceID = refP.ProjectID
		Left Join ClientTb c on refP.CustomerID = c.ReferenceID
		Left Join (select * from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[SLCustomerTypes]) [refT]  on refP.CustomerTypeID = refT.CustomerTypeID
		
/*
	
	Update Task and Task to Project has been removed due to the new schema.  refTask and refTaskProject are now truncated everytime an sp runs and reloaded
	
	--Update Task Based on Modified Date (Done)


		Update T
		Set TaskName = reftask.FullName
		,TaskModifiedDate = reftask.Modified
		,Category = refTask.Category
		From refTask T
			JOIN (select 
					TaskName [FullName]
					,TaskID [TaskReferenceID]
					, Min(Cast(TimeCreated as datetime)) [Created]
					, Max(cast(TimeModified as DateTime)) [Modified]
					, Category [Category]
					--,RefTask.ProjectID
				from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[SLTasks]
				Group by TaskName
					,TaskID
					,Category) RefTask
				On T.TaskReferenceID = Reftask.TaskReferenceID
					and CASE WHEN t.TaskModifiedDate is NULL THEN t.TaskCreatedDate else t.TaskModifiedDate END <
						CASE WHEN refTask.Modified IS NULL THEN RefTask.Created ELSE refTask.Modified END

	-- update refTaskProject. Delete any Task/Project join where the Task has been either Deleted or Marked InActive in the Ref DB

		Delete From TP
		From refTaskProject TP
		Join (Select distinct Task.TaskID, Project.ProjectID from 
				(select * from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[SLTasks] Where IsActive = 0) SLTASK
				Join [refTask] Task on SLTASK.TaskID = Task.TaskReferenceID
				join [ProjectTb] Project on SLTASK.ProjectID = Project.ProjectReferenceID) InactiveTask on TP.TaskID = InactiveTask.TaskID and TP.ProjectID = InactiveTask.ProjectID


*/
	-- In CT Dev Give all projects all of the task
	
	

		If @CurrentDB = 'CometTracker' 
		BEGIN

			Insert Into refTaskProject
			(
			TaskID, ProjectID
			)
			select p.ProjectID, t.taskid from 
			(select ProjectID from ProjectTb where ProjectStatus = 1 and ProjectName in ('Demo','SOUTHERN CROSS:CT2','SCANA-SCE&G')) p
			Cross Join (select TaskID from refTask where ActiveFlag = 1) t
			Left Join refTaskProject r on p.ProjectID = r.ProjectID and t.TaskID = r.TaskID
			where r.ID is null

		END

END TRY
BEGIN CATCH
  THROW;
END CATCH

	
	
	--ChartOfAccounts.  No Update, only adds

	--PaySource.  No Update, only adds

	--EmployeeIDMap.  No Update, only adds

--All above this line is Update 3/13/2019



























GO


