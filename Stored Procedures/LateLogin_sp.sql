USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[LateLogin_sp]    Script Date: 4/23/2019 1:20:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****************************************************************************************************
***************************************************************************************************
    View Name:			LateLogin_sp						 				 
		Created By:			Sandra Jackson, Senior DBA (SQL Code) 									 
		Create Date:		03/30/2016
		Modified Date:														 
		Create Reason:		View All Time Cards With Sum of Hours Worked							         
		Prerequisite:		LateLogin_vw, late login records the current day	
		Child:				LateLogin job															 
		Utilized By:		CometTracker App, Supervisor, Admin										 
		Version:			1.0			
		Modified By :		Alekya Kondam
        Modified Date:		03/29/2019	
        Modifications made : added Exceptional handling 													 
					 
																									 
*****************************************************************************************************/

CREATE PROCEDURE [dbo].[LateLogin_sp]

AS
BEGIN TRY

  INSERT INTO [dbo].[LateLoginTb] ([LateLoginUserID], [LateLoginUserName], [LateLoginUserFirstName], [LateLoginUserLastName], [LateLoginLateBalance])
    SELECT
      [UserID],
      [UserName],
      [UserFirstName],
      [UserLastName],
      [LateBalance]
    FROM [dbo].[LateLogin_vw]

END TRY
BEGIN CATCH
  THROW;
END CATCH







GO


