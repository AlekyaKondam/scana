USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptEmployeeTaskOutDetails]    Script Date: 4/24/2019 9:09:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spRptEmployeeTaskOutDetails]
(
	@StartDate			DATE	= NULL
	,@EndDate			DATE	= NULL
	,@TaskOutType         varchar(200) = NULL     
 
)

AS

Set NOCOUNT ON
BEGIN TRY
/*

	Testing

	Exec spRptEmployeeTaskOutDetails '3/10/2019', '3/16/2019', NULL
	
	Exec spRptEmployeeTaskOutDetails '3/10/2019', '3/16/2019', 'Field Services (Other)'

*/

BEGIN

	Select
		@StartDate =  DateAdd(d, - (DatePart(dw, @StartDate) -1), @StartDate)
		,@EndDate  =  DateAdd(d, 6, DateAdd(d, - (DatePart(dw, @StartDate) -1), @StartDate))



	IF @TaskOutType is null
	BEGIN

				
		Create Table #DD
		(
			[Drop_Down1] varchar(200) NULL
			,[Drop_Down2] varchar(200) NULL    
		)
		
		Insert Into #DD
		 Select Distinct MapGrid  As [Drop_Down1]
				,MapGrid  As [Drop_Down2]
		
		--INTO #DD
		From tTaskOut where Cast([SrcDTLT] as date) between @StartDate and @EndDate
		and MapGrid in ('Field Services (Other)','Leak Survey - AC','Locate','Meter Service')

		IF (Select Count(*) from #DD) = 0
		BEGIN

			Insert Into #DD
			VALUES ('No data returned for the date(s) provided','No data returned for the date(s) provided')

		END

		
		SELECT Drop_Down1 , Drop_Down2 FROM #DD

		DROP TABLE #DD

	END
	ELSE
	IF @TaskOutType <> 'No data returned for the date(s) provided'
	BEGIN

		--The following could have been done using a dynamic SQL
		
		IF @TaskOutType = 'Field Services (Other)'
		BEGIN

			Select [to].SrcDTLT [Date],
				u.UserLastName + ', ' + u.UserFirstName + ' (' + u.UserName + ')' [Tech],
				p.ProjectName,
				[to].Comments
				--The following is unique to the TaskOutType
				, FieldServiceWorkOrdersSkipped
				, FieldServiceWorkOrdersCompleted
				, FieldServiceRemediationsCompleted
			
			From tTaskOut [to]
			join ActivityTb a on [to].ActivityID = a.ActivityID
			join ProjectTb p on a.ActivityProjectID = p.ProjectID
			Join UserTb u on [to].CreatedUserID = u.UserID
			where Cast([to].SrcDTLT as date) between @StartDate and @EndDate
			and MapGrid = @TaskOutType


		END
		ELSE
		IF @TaskOutType = 'Leak Survey - AC'
		BEGIN

			Select [to].SrcDTLT [Date],
				u.UserLastName + ', ' + u.UserFirstName + ' (' + u.UserName + ')' [Tech],
				p.ProjectName,
				[to].Comments
				--The following is unique to the TaskOutType
				, SurveySurveyFootage
				, SurveyNoOfServices
				, SurveyCGEs
				, SurveyBelowGroundGrade1
				, SurveyBelowGroundGrade2
				, SurveyBelowGroundGrade3
				, SurveyTotalBelowGround
				, SurveyAboveGroundGrade1
				, SurveyAboveGroundGrade2
				, SurveyAboveGroundGrade3
				, SurveyTotalAboveGround
				, ACWorkOrdersSkipped
				, ACWorkOrdersCompleted
				, ACRemediationsCompleted
			From tTaskOut [to]
			join ActivityTb a on [to].ActivityID = a.ActivityID
			join ProjectTb p on a.ActivityProjectID = p.ProjectID
			Join UserTb u on [to].CreatedUserID = u.UserID
			where Cast([to].SrcDTLT as date) between @StartDate and @EndDate
			and MapGrid = @TaskOutType

		END
		ELSE
		IF @TaskOutType = 'Locate'
		BEGIN

			Select [to].SrcDTLT [Date],
				u.UserLastName + ', ' + u.UserFirstName + ' (' + u.UserName + ')' [Tech],
				p.ProjectName,
				[to].Comments
				--The following is unique to the TaskOutType
				, LocatingTicketsCompleted
				, LocatingServicesCompleted
			From tTaskOut [to]
			join ActivityTb a on [to].ActivityID = a.ActivityID
			join ProjectTb p on a.ActivityProjectID = p.ProjectID
			Join UserTb u on [to].CreatedUserID = u.UserID
			where Cast([to].SrcDTLT as date) between @StartDate and @EndDate
			and MapGrid = @TaskOutType

		END
		IF @TaskOutType = 'Meter Service'
		BEGIN

			Select [to].SrcDTLT [Date],
				u.UserLastName + ', ' + u.UserFirstName + ' (' + u.UserName + ')' [Tech],
				p.ProjectName,
				[to].Comments
				--The following is unique to the TaskOutType
				, MeterServiceWorkOrdersSkipped
				, MeterServiceWorkOrdersCompleted
			From tTaskOut [to]
			join ActivityTb a on [to].ActivityID = a.ActivityID
			join ProjectTb p on a.ActivityProjectID = p.ProjectID
			Join UserTb u on [to].CreatedUserID = u.UserID
			where Cast([to].SrcDTLT as date) between @StartDate and @EndDate
			and MapGrid = @TaskOutType

		END
		
	
	END


END


END TRY
BEGIN CATCH
  THROW;
END CATCH














GO


