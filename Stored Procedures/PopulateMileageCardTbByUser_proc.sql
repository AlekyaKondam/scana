USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[PopulateMileageCardTbByUser_proc]    Script Date: 4/23/2019 1:20:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*****************************************************************************************************/
/*****************************************************************************************************/

/*  	Function Name:		PopulateMileageCardTbByUser_proc					 					 */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		01/25/2016																 */
/*		Create Reason:		Populate Mileage Cards by User for the year								 */
/*		Parameters:			None																	 */
/*		Prerequisites(1):	Truncate [MileageCardTb] table											 */
/*		Utilized By:		CometTracker App, Supervisor, Admin										 */
/*		Call methods(1):	1. execute dbo.PopulateMileageCardTbByUser_proc							 */
/*		Validation:			select count(*)* 53 FROM [dbo].[UserTb] WHERE [UserActiveFlag]			 */
/*                          NOT IN ('INACTIVE','TERMINATED','RETIRED','PENDING')					 */
/*							select count(*) from [MileageCardTb]  --test to validate it worked		 */
/*																									 */

/* TO BE RUN 1 TIME A YEAR TO CREATE TIME CARD PERIODS                                                */
/******************************************************************************************************/
/******************************************************************************************************/


CREATE PROCEDURE [dbo].[PopulateMileageCardTbByUser_proc]

AS

  DECLARE @TECHID int
  BEGIN TRY
    DECLARE TEST SCROLL CURSOR FOR
    SELECT
      194
    FROM [dbo].[UserTb]

    OPEN TEST
    FETCH NEXT FROM TEST
    INTO @TECHID
    WHILE @@FETCH_STATUS = 0
    BEGIN
      INSERT INTO [dbo].[MileageCardTb] ([MileageStartDate], [MileageEndDate], [MileageCardProjectID], [MileageCardTechID], [MileageCardCreateDate], [MileageCardCreatedBy])
        SELECT
          [TimeCardSunday] AS [MileageStartDate],
          [TimeCardSaturday] AS [MileageEndDate],
          1006 AS [MileageCardProjectID],
          @TECHID AS [MileageCardTechID],
          GETDATE() AS [MileageCardCreateDate],
          SUSER_NAME() AS [MileageCardCreatedBy]
        FROM [dbo].[TimeCardPeriodsTb]

      FETCH NEXT FROM TEST
      INTO @TECHID

    END
    CLOSE TEST
    DEALLOCATE TEST
  END TRY

  BEGIN CATCH
    THROW;
  END CATCH








GO


