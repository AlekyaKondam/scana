USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptGenerateOasisTimeCardByProject]    Script Date: 4/24/2019 9:10:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE  Procedure [dbo].[spRptGenerateOasisTimeCardByProject](@WeekStartingDate date, @WeekEndingDate date , @ProjectIDJSON varchar(max) = null)


AS


SET NOCOUNT ON
BEGIN TRY
Create Table #TimeCard
(
	[Employee Id] varchar(50) NULL
	,[COA] varchar(50) NULL    
	,[QTY] varchar(50) NULL
	,[Week Ending] Date NULL
	,[Employee Name] varchar(200) NULL
)

/*   
1088
1108
	
	Exec spRptGenerateOasisTimeCardByProject  '2018-05-13', '2018-05-19' ,NULL
	Exec [spRptGenerateOasisTimeCardByProject] '2018-05-13', '2018-05-19','["2787"]'
	Exec [spRptGenerateOasisTimeCardByProject]  '2019-02-25', '2019-03-01' ,'<All>'




	select  timecardsubmittedoasis, TimeCardSubmttedQuickBooks, TimeCardSubmttedADP, * 
	from timecardtb 
	where timecardprojectid = '1423' and timecardstartdate >= '2018-03-25' and timecardenddate <= '2018-03-35'

	update timecardtb 
	Set timecardsubmittedoasis = NULL, TimeCardSubmttedQuickBooks = NULL, TimeCardSubmttedADP = NULL
	where timecardprojectid = '1423' and timecardstartdate >= '2018-03-25' and timecardenddate <= '2018-03-31'


*/


BEGIN
 

IF @ProjectIDJSON is null
	BEGIN
		--select @WeekStartingDate = dateadd(d, -datepart(dw, @WeekStartingDate)+ 1, @WeekStartingDate)

		--select @WeekEndingDate = dateadd(d, 7-datepart(dw, @WeekEndingDate), @WeekEndingDate)

		SELECT  -- 20160111 Added
			'<All>'  as Drop_Down, 'ALL' AS Project_Name
		INTO #DD
		
		UNION ALL

      select distinct cast([ProjectID] as varchar(20)) as [DropDown] , [ProjectName] As [Project_Name] 
		from fnGeneratePayrollDataByProject ('<All>', @WeekStartingDate, @WeekEndingDate, 'Oasis', 'Report')

    	SELECT Drop_Down, Project_Name FROM #DD -- DROP TABLE #DD

	END
ELSE
BEGIN
	 Insert Into #TimeCard
	 (
 		[Employee Id], [COA], [QTY] , [Week Ending], [Employee Name]
	 )
 
 
	 select 
		[Employee Id],  
		[Payroll Item] as COA , 
		--format(sum(Cast([Hours to Import] as float)), '0.00') as QTY , 
		 Format((Cast(Sum([Hours to Import]) as float)) / Cast(3600 as float), '0.00') as QTY
		,[Transaction Date] as [Week Ending],[First Name] + ' ' +  [Last Name] as [Employee Name]
	 from fnGeneratePayrollDataByProject(@ProjectIDJSON, @WeekStartingDate, @WeekEndingDate, 'Oasis', 'Report')
	 group by [Employee Id],  [Payroll Item] , [Transaction Date],[First Name] + ' ' +  [Last Name]

	 DECLARE @CountTest INT = (Select count(*) From #TimeCard)

		IF @CountTest = 0
		BEGIN
 
			SELECT 'No data returned for the date(s) provided' AS [Report Alert]  
            
		END
		Else
		BEGIN

			Select * from #TimeCard

		END

END

Drop Table #TimeCard


SET NOCOUNT OFF

END


END TRY
BEGIN CATCH
  THROW;
END CATCH


GO


