USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[PopulateTimeCardTbByUser_proc]    Script Date: 4/23/2019 1:21:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*****************************************************************************************************/
/*****************************************************************************************************/

/*  	Function Name:		PopulateTimeCardTbByUser_proc					 						 */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		01/25/2016																 */
/*		Create Reason:		Populate Time Cards by User for the year								 */
/*		Parameters:			None																	 */
/*		Prerequisites(1):	Truncate [TimeCardTb] table												 */
/*		Utilized By:		CometTracker App, Supervisor, Admin										 */
/*		Call methods(1):	1. execute dbo.PopulateTimeCardTbByUser_proc							 */
/*		Validation:			select count(*)* 53 FROM [dbo].[UserTb] WHERE [UserActiveFlag]			 */
/*                          NOT IN ('INACTIVE','TERMINATED','RETIRED','PENDING')					 */
/*							select count(*) from [TimeCardTb]  --test to validate it worked			 */
/*		Status:				UserActiveFlag = 1 inactive = 0 terminated = 9 pending = 2	

          Modified By :		Alekya Kondam
         Modified Date:		03/29/2019	
         Modifications made : added Exceptional handling 			 */

/* TO BE RUN 1 TIME A YEAR TO CREATE TIME CARD PERIODS                                                */
/******************************************************************************************************/
/******************************************************************************************************/


CREATE PROCEDURE [dbo].[PopulateTimeCardTbByUser_proc]

AS
BEGIN TRY
  DECLARE @TECHID int
  DECLARE TEST SCROLL CURSOR FOR
  SELECT
    331
  FROM [dbo].[UserTb]
  WHERE [UserActiveFlag] NOT IN (0, 9, 2)

  OPEN TEST
  FETCH NEXT FROM TEST
  INTO @TECHID
  WHILE @@FETCH_STATUS = 0
  BEGIN
    INSERT INTO [TimeCardTb] ([TimeCardStartDate], [TimeCardEndDate], [TimeCardProjectID], [TimeCardTechID], [TimeCardCreateDate], [TimeCardCreatedBy])
      SELECT
        [TimeCardSunday] AS [TimeCardStartDate],
        [TimeCardSaturday] AS [TimeCardEndDate],
        1 AS [TimeCardProjectID],
        @TECHID AS [TimeCardTechID],
        GETDATE() AS [TimeCardCreateDate],
        SUSER_NAME() AS [TimeCardCreatedBy]
      FROM [dbo].[TimeCardPeriodsTb]

    FETCH NEXT FROM TEST
    INTO @TECHID

  END
  CLOSE TEST
  DEALLOCATE TEST
END TRY
BEGIN CATCH
  THROW;
END CATCH






GO


