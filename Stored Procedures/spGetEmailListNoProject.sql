USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spGetEmailListNoProject]    Script Date: 4/23/2019 1:29:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetEmailListNoProject]
(
      @ReportName varchar(200) = ''
	  ,@EmailTOList varchar(max) Output
	  ,@EmailCCList varchar(max) Output
)
AS 
Begin
BEGIN TRY
	--Declare @emaillist varchar(max)

	--WAITFOR DELAY '00:00:10'

	Select @EmailTOList = coalesce(@EmailTOList + '; ','') + el.Email
	From 
		[dbo].[rEmailList] el
		Join [dbo].[xReportNameEmailList] xref on xref.EmailID = el.emaillistID
		Join [dbo].[rEmailReportName] r on r.rReportNameID = xref.ReportID
		Where r.ReportName = @ReportName 
			and xref.EmailType = 'TO'
			and el.ActiveFlag = 1
			and xref.ActiveFlag = 1
			and r.ActiveFlag = 1
		Order by xref.SortOrder, el.LastName

	Select @EmailCCList = coalesce(@EmailCCList + '; ','') + el.Email
	From 
		[dbo].[rEmailList] el
		Join [dbo].[xReportNameEmailList] xref on xref.EmailID = el.emaillistID
		Join [dbo].[rEmailReportName] r on r.rReportNameID = xref.ReportID
		Where r.ReportName = @ReportName 
			and xref.EmailType = 'CC'
			and el.ActiveFlag = 1
			and xref.ActiveFlag = 1
			and r.ActiveFlag = 1
		Order by xref.SortOrder, el.LastName
	END TRY
BEGIN CATCH
  THROW;
END CATCH

END
	










GO


