USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[GetAllUsersByProjectByUser_proc]    Script Date: 4/23/2019 1:19:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/****************************************************************************************************
***************************************************************************************************
		View Name:			GetUsersByProjectByUser_proc						 				 
		Created By:			Sandra Jackson, Senior DBA												 
		Create Date:		03/27/2016																 
		Create Reason:		View users by project by user						         
		Prerequisite:		AllUsersByProjectByUser_vw																	 
		Utilized By:		CometTracker App, Supervisor, Admin										 
		Version:			1.0
		Modified By :		Alekya Kondam
        Modified Date:		03/29/2019	
        Modifications made : added Exceptional handling 																		 
																									 
*****************************************************************************************************/



CREATE PROCEDURE [dbo].[GetAllUsersByProjectByUser_proc]  @PARAMETER1 INT
AS

BEGIN TRY
-- Declare @PARAMETER1 INT set @PARAMETER1 = '162'
SELECT DISTINCT [UserID]
      ,[UserName]
      ,[UserFirstName]
      ,[UserLastName]
      ,[UserEmployeeType]
      ,[UserPhone]
      ,[UserCompanyName]
      ,[UserCompanyPhone]
      ,[UserAppRoleType]
      ,[UserComments]
      ,[UserKey]
      ,[UserActiveFlag]
      ,[UserCreateDTLTOffset]
      ,[UserModifiedDTLTOffset]
      ,[UserInactiveDTLTOffset]
      ,[UserCreatedDate]
      ,[UserCreatedBy]
      ,[UserModifiedDate]
      ,[UserModifiedBy] 
      ,[ProjectName]
      ,[ProjectDescription]
      ,[ProjectNotes]
      ,[ProjectType]
      ,[ProjectStatus]
      ,[ProjectClientID]
      ,[ProjectStartDate]
      ,[ProjectEndDate]
      ,[ProjectCreateDate]
      ,[ProjectCreatedBy]
      ,[ProjectModifiedDate]
      ,[ProjectModifiedBy]
	  
	   FROM AllUsersByProjectByUser_vw
WHERE [UserID] = @PARAMETER1
ORDER BY [UserID]
END TRY
BEGIN CATCH
  THROW;
END CATCH


GO


