USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[PopulateTimeCardTbForNewUser_proc]    Script Date: 4/23/2019 1:21:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*****************************************************************************************************/
/*****************************************************************************************************/
/*		Function Name:		PopulateTimeCardTbForNewUser_proc			 		  				     */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		01/25/2016																 */
/*		Create Reason:		Populate Time Cards For New User for the year							 */
/*		Parameters(1):		UserID OF New User														 */
/*		Prerequisites:																				 */
/*		Utilized By:		CometTracker App, Supervisor, Admin										 */
/*		Call methods(1):	1. execute dbo.PopulateTimeCardTbForNewUser_proc						 */
/*		Validation:			select count(*)* 53 FROM [dbo].[UserTb] WHERE [UserActiveFlag]			 */
/*                          NOT IN ('INACTIVE','TERMINATED','RETIRED','PENDING')					 */
/*							select count(*) from [TimeCardTb]  --test to validate it worked			 */
/*		Status:				UserActiveFlag = 1 inactive = 0 terminated = 9 pending = 2		
Modified By :		Alekya Kondam
Modified Date:		03/29/2019	
Modifications made : added Exceptional handling 	
		 */
/* TO BE RUN 1 TIME A YEAR TO CREATE TIME CARD PERIODS                                                */
/******************************************************************************************************/
/******************************************************************************************************/
CREATE PROCEDURE [dbo].[PopulateTimeCardTbForNewUser_proc] @PARAMETER1 INT
AS
  BEGIN try
      DECLARE @TECHID INT
      DECLARE test scroll CURSOR FOR
        SELECT [userid]
        FROM   [dbo].[usertb]
        WHERE  [useractiveflag] NOT IN ( 0, 9, 2 )
               AND [userid] = @PARAMETER1
        ORDER  BY [userid];

      OPEN test

      FETCH next FROM test INTO @TECHID

      WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT INTO [timecardtb]
                        ([timecardstartdate],
                         [timecardenddate],
                         [timecardprojectid],
                         [timecardtechid],
                         [timecardcreatedate],
                         [timecardcreatedby])
            SELECT [timecardsunday]   AS [TimeCardStartDate],
                   [timecardsaturday] AS [TimeCardEndDate],
                   1006               AS [TimeCardProjectID],
                   @TECHID            AS [TimeCardTechID],
                   Getdate()          AS [TimeCardCreateDate],
                   Suser_name()       AS [TimeCardCreatedBy]
            FROM   [dbo].[timecardperiodstb]

            FETCH next FROM test INTO @TECHID
        END

      CLOSE test

      DEALLOCATE test
  END try

  BEGIN catch
      THROW;
  END catch


GO


