USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[PopulateMileageCardTbForNewUserCatchErrors_proc]    Script Date: 4/23/2019 1:21:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





/*****************************************************************************************************/
/*****************************************************************************************************/

/*		Function Name:		PopulateMileageCardTbForNewUserCatchErrors_proc			 		  		 */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		02/01/2016																 */
/*		Create Reason:		Populate Mileage Cards For New User for the year						 */
/*		Parameters(1):		UserID OF New User														 */
/*		Prerequisites:		Create The New User in the User Table 									 */
/*		Utilized By:		CometTracker App, Supervisor, Admin										 */
/*		Call methods(1):	1. execute dbo.PopulateMileageCardTbForNewUserCatchErrors_proc	232		 */
/*		Validation:			select count(*)* 53 FROM [dbo].[UserTb] WHERE [UserActiveFlag]			 */
/*                          NOT IN ('INACTIVE','TERMINATED','RETIRED','PENDING')					 */
/*							select count(*) from [MileageTb]  --test to validate it worked			 */
/*		Status:				UserActiveFlag active = 1 inactive = 0 terminated = 9 pending = 2		 */

/* TO BE RUN 1 TIME A YEAR TO CREATE TIME CARD PERIODS                                                */
/******************************************************************************************************/
/******************************************************************************************************/

CREATE PROCEDURE [dbo].[PopulateMileageCardTbForNewUserCatchErrors_proc] @PARAMETER1 INT

AS

DECLARE @TECHID INT 
DECLARE TESTMC scroll CURSOR 
FOR
		SELECT [UserID] FROM [dbo].[UserTb] 
		WHERE [UserActiveFlag] NOT IN (0,9,2)
		AND [UserID] = @PARAMETER1
		ORDER BY [UserID];

OPEN TESTMC
	FETCH NEXT FROM TESTMC
	INTO @TECHID
	WHILE @@FETCH_STATUS = 0
		BEGIN TRY
			BEGIN TRANSACTION INSRTMC
				INSERT INTO [dbo].[MileageCardTb](	 [MileageStartDate], [MileageEndDate], [MileageCardProjectID],	[MileageCardTechID]	,[MileageCardCreateDate], [MileageCardCreatedBy])
  				SELECT							[TimeCardSunday] as [MileageStartDate], [TimeCardSaturday] as [MileageEndtDate], 1006 AS [MileageCardProjectID], @TECHID AS [MileageCardTechID], getdate() as [MileageCardCreateDate], SUSER_NAME() as [MileageCardCreatedBy]
				FROM [dbo].[TimeCardPeriodsTb]
	
				FETCH NEXT FROM TESTMC
				INTO @TECHID
			COMMIT TRAN INSRT
END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
        ROLLBACK TRAN INSRTMC
	

    RAISERROR(7770301, 10, 1)
END CATCH
	
CLOSE TESTMC
DEALLOCATE TESTMC





GO


