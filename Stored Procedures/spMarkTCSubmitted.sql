USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spMarkTCSubmitted]    Script Date: 4/23/2019 1:30:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spMarkTCSubmitted]
(
	@Type varchar(50) --Oasis or QB
	,@TimeCardID varchar(max)  --String of TC IDs
	--,@TimeEntryDate Date
)
AS

/*

Example

	Exec spDeactivateTimeEntry TimecardID, TimeEntryTitle, TimeEntryDate



*/
BEGIN TRY
Set NOCOUNT ON

		Declare @TimeCardIDs Table (ID INT, TimeCardID int)
		Declare @SubmittedDateTime DateTime = getdate()

		Insert Into @TimeCardIDs
		select * from [dbo].[SPlit](@TimeCardID, ',')

		IF @Type = 'Oasis'
		BEGIN

			Update TimeCardTb
			Set TimeCardModifiedDate = @SubmittedDateTime
				, TimeCardSubmittedOasis = @SubmittedDateTime
			Where TimeCardID in (Select TimeCardID From @TimeCardIDs)

		END

		IF @Type = 'QB'
		BEGIN

			Update TimeCardTb
			Set TimeCardModifiedDate = @SubmittedDateTime
				, TimeCardSubmttedQuickBooks = @SubmittedDateTime
			Where TimeCardID in (Select TimeCardID From @TimeCardIDs)

		END


Set NOCOUNT OFF

END TRY
BEGIN CATCH
  THROW;
END CATCH





GO


