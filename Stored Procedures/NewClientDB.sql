USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[NewClientDB]    Script Date: 4/23/2019 1:20:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[NewClientDB] @NewDB varchar(25)

/*****************************************************************************************************/
/*****************************************************************************************************/

/*    Procedure Name:														 */
/*		Created By:														 */
/*		Create Date:																	 */
/*		Create Reason:																	     */
/*		Parameter(1):																 */
/*		Utilized By:							 */
/*		Version :						 */
/*		Modified By :		Alekya Kondam
        Modified Date:		03/29/2019	
        Modifications made : added Exceptional handling 																							 */
/*		Status:																				 */
/*--    example																		 */
/*																									 */
/******************************************************************************************************/
/******************************************************************************************************/

AS
BEGIN TRY
  BEGIN


    /* Make sure RedGate database exists, and writes to it don't block transactions in Snapshot isolation level */
    IF DB_ID(N'@NewDB') IS NULL
      CREATE DATABASE RedGate
    ALTER DATABASE RedGate SET RECOVERY SIMPLE
    ALTER DATABASE RedGate SET ALLOW_SNAPSHOT_ISOLATION ON
    ALTER DATABASE RedGate SET READ_COMMITTED_SNAPSHOT ON

  END

END TRY
BEGIN CATCH
  THROW;
END CATCH

GO


