USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptEmployeeProductivityDetailsPivot]    Script Date: 4/24/2019 9:08:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spRptEmployeeProductivityDetailsPivot]
(
	@StartDate			DATE	= NULL
	,@EndDate			DATE	= NULL
	,@UserName         varchar(200) = NULL     
 
)

AS

Set NOCOUNT ON

/*

	Testing
	Exec spRptEmployeeProductivityDetailsPivot  '1/6/2019', '1/6/2019', NULL
	Exec spRptEmployeeProductivityDetailsPivot '12/09/2018', '12/17/2018', 'Technician, Jose (jtech) [SCANA - SCEG:15440-Survey]'
	

*/

BEGIN

BEGIN TRY
	Declare @BCActivityType varchar(100)
			,@BCSpeedAtt varchar(100)
			,@BCSunTime varchar(20)
			,@BCMonTime varchar(20)
			,@BCTueTime varchar(20)
			,@BCWedTime varchar(20)
			,@BCThuTime varchar(20)
			,@BCFriTime varchar(20)
			,@BCSatTime varchar(20)
			,@BCSunDist varchar(20)
			,@BCMonDist varchar(20)
			,@BCTueDist varchar(20)
			,@BCWedDist varchar(20)
			,@BCThuDist varchar(20)
			,@BCFriDist varchar(20)
			,@BCSatDist varchar(20)
			,@TimeCardID int
			,@UserID int
			,@UserShortName varchar(20)
			,@ProjectName varchar(200)
			,@ProjectID int
			,@HasSat Int
			,@HasSun Int
			,@Rows varchar(max)
			,@Cols varchar(max)
			,@DynamicPivotQuery nvarchar(max)


	Select
		@StartDate =  DateAdd(d, - (DatePart(dw, @StartDate) -1), @StartDate)
		,@EndDate  =  DateAdd(d, 6, DateAdd(d, - (DatePart(dw, @StartDate) -1), @StartDate))



	IF @UserName is null
	BEGIN


		Select Distinct u.UserLastName + ', ' + u.UserFirstName +' (' + u.UserName + ')' + ' [' + ProjectName + ']' As [Drop_Down1]
				, u.UserLastName + ', ' + u.UserFirstName +' (' + u.UserName + ')' + ' [' + ProjectName + ']' As [Drop_Down2]
		INTO #DD
		From (Select * from UserTb where UserAppRoleType like '%Technician%') u
			Join (Select Distinct ActivityCreatedUserUID, p.ProjectName
				from ActivityTb a
				Join TimeEntryTb te on te.TimeEntryActivityID = a.ActivityID
				Join TimeCardTb tc on te.TimeEntryTimeCardID = tc.TimeCardID
				Join ProjectTb p on tc.TimeCardProjectID = p.ProjectID
				where Cast(ActivityStartTime as date) between @StartDate and @EndDate
				And (ActivityTitle like '%task%' or ActivityTitle in ('AdminActivity', 'Break2Activity', 'LunchActivity','BreakActivity')) and ActivityTitle  not like '%TaskOut%'
				and ActivityCreatedUserUID is not null) a on a.ActivityCreatedUserUID = Case WHEN ISNUMERIC(a.ActivityCreatedUserUID) = 1 THEN cast(u.UserID as varchar(50)) ELSE u.UserName END
		ORDER BY 1

		SELECT Drop_Down1 , Drop_Down2 FROM #DD

		DROP TABLE #DD

	END
	ELSE
	BEGIN

		select @UserShortName = SUBSTRING(@UserName, CHARINDEX('(', @UserName) + 1, CHARINDEX(')', @UserName) - (CHARINDEX('(', @UserName) + 1))

		select @ProjectName = SUBSTRING(@UserName, CHARINDEX('[', @UserName) + 1, CHARINDEX(']', @UserName) - (CHARINDEX('[', @UserName) + 1))

		select @ProjectID = ProjectID from ProjectTb where ProjectName = @ProjectName

		select @UserID = UserID from UserTb where UserName = @UserShortName
		
		Select @timecardid = TimeCardid 
		From 
		(select Distinct TimeCardID from TimeCardTb TC  
		join TimeEntryTb TE on te.TimeEntryTimeCardID = tc.TimeCardID
		where TimeCardTechID = @UserID and TimeCardStartDate = @StartDate and TimeCardProjectID = @ProjectID) Source


		IF @TimeCardID > 0
		BEGIN

			/*
			select 
				@UserID = u.UserName
				,@StartDate = Cast(tc.TimeCardStartDate as date)
				,@EndDate = Cast(tc.TimeCardEndDate as date)
			from TimeCardTb tc
			Join UserTb u on u.UserID = tc.TimeCardTechID
				where TimeCardID = @TimeCardID
			*/

			Create Table #TimeCard
			(
				[Task] varchar(100) NULL    
				,[SunTime] varchar(100) NULL        
				,[SunStand] varchar(100) NULL   
				,[SunWalk] varchar(100) NULL  
				,[SunDrive] varchar(100) NULL
				,[MonTime] varchar(100) NULL        
				,[MonStand] varchar(100) NULL   
				,[MonWalk] varchar(100) NULL  
				,[MonDrive] varchar(100) NULL
				,[TueTime] varchar(100) NULL        
				,[TueStand] varchar(100) NULL   
				,[TueWalk] varchar(100) NULL  
				,[TueDrive] varchar(100) NULL
				,[WedTime] varchar(100) NULL        
				,[WedStand] varchar(100) NULL   
				,[WedWalk] varchar(100) NULL  
				,[WedDrive] varchar(100) NULL
				,[ThrTime] varchar(100) NULL        
				,[ThrStand] varchar(100) NULL   
				,[ThrWalk] varchar(100) NULL  
				,[ThrDrive] varchar(100) NULL
				,[FriTime] varchar(100) NULL        
				,[FriStand] varchar(100) NULL   
				,[FriWalk] varchar(100) NULL  
				,[FriDrive] varchar(100) NULL
				,[SatTime] varchar(100) NULL        
				,[SatStand] varchar(100) NULL   
				,[SatWalk] varchar(100) NULL  
				,[SatDrive] varchar(100) NULL
	       
			)


			/*
		
			Insert Into #TimeCard
			Values ( 
			'Task'
			 ,'Time'
			 ,'Stand'
			 ,'Walk'
			 ,'Drive'
			 ,'Time'
			 ,'Stand'
			 ,'Walk'
			 ,'Drive'
			 ,'Time'
			 ,'Stand'
			 ,'Walk'
			 ,'Drive'
			 ,'Time'
			 ,'Stand'
			 ,'Walk'
			 ,'Drive'
			 ,'Time'
			 ,'Stand'
			 ,'Walk'
			 ,'Drive'
			 ,'Time'
			 ,'Stand'
			 ,'Walk'
			 ,'Drive'
			 ,'Time'
			 ,'Stand'
			 ,'Walk'
			 ,'Drive'

			)

			*/


			Insert Into #TimeCard
			([Task]  
				,[SunTime]      
				,[MonTime]     
				,[TueTime]      
				,[WedTime]     
				,[ThrTime]     
				,[FriTime]    
				,[SatTime]  
			)
				Select
					ActivityTitle
					,CASE WHEN Sum(Day1) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day1), 0), 0), 108) ELSE '0' END
					,CASE WHEN Sum(Day2) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day2), 0), 0), 108) ELSE '0' END
					,CASE WHEN Sum(Day3) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day3), 0), 0), 108) ELSE '0' END
					,CASE WHEN Sum(Day4) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day4), 0), 0), 108) ELSE '0' END
					,CASE WHEN Sum(Day5) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day5), 0), 0), 108) ELSE '0' END
					,CASE WHEN Sum(Day6) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day6), 0), 0), 108) ELSE '0' END
					,CASE WHEN Sum(Day7) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day7), 0), 0), 108) ELSE '0' END
				From
					(Select 
						ActivityTitle
						 ,Case WHEN DatePart(dw, TimeEntryDate) = 1 THEN TotalTime ELSE 0 END [Day1]
						 ,Case WHEN DatePart(dw, TimeEntryDate) = 2 THEN TotalTime ELSE 0 END [Day2]
						 ,Case WHEN DatePart(dw, TimeEntryDate) = 3 THEN TotalTime ELSE 0 END [Day3]
						 ,Case WHEN DatePart(dw, TimeEntryDate) = 4 THEN TotalTime ELSE 0 END [Day4]
						 ,Case WHEN DatePart(dw, TimeEntryDate) = 5 THEN TotalTime ELSE 0 END [Day5]
						 ,Case WHEN DatePart(dw, TimeEntryDate) = 6 THEN TotalTime ELSE 0 END [Day6]
						 ,Case WHEN DatePart(dw, TimeEntryDate) = 7 THEN TotalTime ELSE 0 END [Day7]
					from 
		 
						 (Select ActivityTitle
									,TimeEntryDate
									,Sum(TotalSec) [TotalTime]
									--,Right('0' + Cast(Sum(TotalSec) / 3600 as varchar),2) + ':' +
										--Right('0' + Cast((Sum(TotalSec) / 60) % 60 as varchar),2) + ':' +
										--Right('0' + Cast(Sum(TotalSec) % 60 as varchar),2) [TotalTime]
									From
									(
									select 
										a.ActivityTitle
										,Cast(t.TimeEntryStartTime as date) TimeEntryDate
										, Cast(DateDiff(s, t.TimeEntryStartTime, t.TimeEntryEndTime) as bigint) [TotalSec]
									from 
												 (Select * from [dbo].[TimeEntryTb] where TimeEntryTimeCardID = @TimeCardID and TimeEntryActiveFlag = 1) t
												 Join (select * from ActivityTb where (ActivityTitle like '%Task%' or ActivityTitle in ('AdminActivity')) and ActivityTitle  not like '%TaskOut%') a on t.TimeEntryActivityID = a.ActivityID
							) Source
							Group By 
							ActivityTitle
							,TimeEntryDate
							--Having Sum(TotalSec) > 60
						) Summary) Summary2 Group By ActivityTitle


				select Distinct
					a.ActivityUID 
				Into #ActivityUID
				from 
					(Select * from [dbo].[TimeEntryTb] where TimeEntryTimeCardID = @TimeCardID and TimeEntryActiveFlag = 1) t
					 Join (select * from ActivityTb where (ActivityTitle like '%Task%' or ActivityTitle in ('AdminActivity')) and ActivityTitle  not like '%TaskOut%') a on t.TimeEntryActivityID = a.ActivityID
						
		
				;With cteBC
				AS	
				(Select BreadcrumbID, BreadcrumbActivityType, BreadcrumbActivityUID, BreadcrumbSrcDTLT, BreadcrumbLatitude, BreadcrumbLongitude, SpeedAttribute, DistanceTraveled, Cast(BreadcrumbSrcDTLT as date) BCDate, Row_Number() Over (order By BreadcrumbSrcDTLT) RowID From BreadcrumbTb where 
				BreadcrumbCreatedUserUID = @UserShortName
				and Cast(BreadcrumbSrcDTLT as date) between @StartDate and @EndDate)
				Select 
				ctebc1.BreadcrumbActivityType
				,ctebc1.SpeedAttribute
				,ctebc1.BCDate
				,CASE WHEN ctebc1.BCDate = ctebc2.BCDate THEN DATEDIFF(s, ctebc2.BreadcrumbSrcDTLT, ctebc1.BreadcrumbSrcDTLT) ELSE 0 END TotalSecBetweenBC
				--,CASE WHEN ctebc1.BCDate = ctebc2.BCDate THEN [dbo].[fnGetDistance](cteBC1.BreadcrumbLatitude, cteBC1.BreadcrumbLongitude, cteBC2.BreadcrumbLatitude, cteBC2.BreadcrumbLongitude, 'Feet') ELSE 0 END FeetBetweenBC
				,ctebc1.DistanceTraveled * 3.28 FeetBetweenBC
				Into #BCData
				From cteBC cteBC1
				Left Join cteBC cteBC2 on cteBC1.RowID - 1 = cteBC2.RowID
				 where
					cteBC1.BreadcrumbActivityUID in
				 (select ActivityUID from #ActivityUID)
				


			Declare curBC Cursor Static
			For
		
			Select
					BreadcrumbActivityType
					,SpeedAttribute
					,CASE WHEN Sum(Day1Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day1Time), 0), 0), 108) ELSE '0' END [SunTime]
					,CASE WHEN Sum(Day2Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day2Time), 0), 0), 108) ELSE '0' END [MonTime]
					,CASE WHEN Sum(Day3Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day3Time), 0), 0), 108) ELSE '0' END [TueTime]
					,CASE WHEN Sum(Day4Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day4Time), 0), 0), 108) ELSE '0' END [WedTime]
					,CASE WHEN Sum(Day5Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day5Time), 0), 0), 108) ELSE '0' END [ThuTime]
					,CASE WHEN Sum(Day6Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day6Time), 0), 0), 108) ELSE '0' END [FriTime]
					,CASE WHEN Sum(Day7Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day7Time), 0), 0), 108) ELSE '0' END [SatTime]
					,CASE WHEN Sum(Day1Feet) > 0 THEN Cast(Sum(Day1Feet) as varchar(10)) ELSE '0' END [SunDist]
					,CASE WHEN Sum(Day2Feet) > 0 THEN Cast(Sum(Day2Feet) as varchar(10)) ELSE '0' END [MonDist]
					,CASE WHEN Sum(Day3Feet) > 0 THEN Cast(Sum(Day3Feet) as varchar(10)) ELSE '0' END [TueDist]
					,CASE WHEN Sum(Day4Feet) > 0 THEN Cast(Sum(Day4Feet) as varchar(10)) ELSE '0' END [WedDist]
					,CASE WHEN Sum(Day5Feet) > 0 THEN Cast(Sum(Day5Feet) as varchar(10)) ELSE '0' END [ThuDist]
					,CASE WHEN Sum(Day6Feet) > 0 THEN Cast(Sum(Day6Feet) as varchar(10)) ELSE '0' END [FriDist]
					,CASE WHEN Sum(Day7Feet) > 0 THEN Cast(Sum(Day7Feet) as varchar(10)) ELSE '0' END [SatDist]
			From
			(Select 
						BreadcrumbActivityType
						,SpeedAttribute
						 ,Case WHEN DatePart(dw, BCDate) = 1 THEN TotalSec ELSE 0 END [Day1Time]
						 ,Case WHEN DatePart(dw, BCDate) = 2 THEN TotalSec ELSE 0 END [Day2Time]
						 ,Case WHEN DatePart(dw, BCDate) = 3 THEN TotalSec ELSE 0 END [Day3Time]
						 ,Case WHEN DatePart(dw, BCDate) = 4 THEN TotalSec ELSE 0 END [Day4Time]
						 ,Case WHEN DatePart(dw, BCDate) = 5 THEN TotalSec ELSE 0 END [Day5Time]
						 ,Case WHEN DatePart(dw, BCDate) = 6 THEN TotalSec ELSE 0 END [Day6Time]
						 ,Case WHEN DatePart(dw, BCDate) = 7 THEN TotalSec ELSE 0 END [Day7Time]
						 ,Case WHEN DatePart(dw, BCDate) = 1 THEN TotalFeet ELSE 0 END [Day1Feet]
						 ,Case WHEN DatePart(dw, BCDate) = 2 THEN TotalFeet ELSE 0 END [Day2Feet]
						 ,Case WHEN DatePart(dw, BCDate) = 3 THEN TotalFeet ELSE 0 END [Day3Feet]
						 ,Case WHEN DatePart(dw, BCDate) = 4 THEN TotalFeet ELSE 0 END [Day4Feet]
						 ,Case WHEN DatePart(dw, BCDate) = 5 THEN TotalFeet ELSE 0 END [Day5Feet]
						 ,Case WHEN DatePart(dw, BCDate) = 6 THEN TotalFeet ELSE 0 END [Day6Feet]
						 ,Case WHEN DatePart(dw, BCDate) = 7 THEN TotalFeet ELSE 0 END [Day7Feet]
					from 
			(select BreadcrumbActivityType, SpeedAttribute, BCDate, sum(totalsecbetweenbc) TotalSec, Cast(Sum(FeetBetweenBC) as decimal(15,3)) TotalFeet 
			from #BCData
			--Where SpeedAttribute = 'Standstill'
			Group By BreadcrumbActivityType, SpeedAttribute, BCDate) BC) BC
			Group By BreadcrumbActivityType, SpeedAttribute

			Open curBC

			Fetch Next from curBC into @BCActivityType, @BCSpeedAtt, @BCSunTime, @BCMonTime, @BCTueTime, @BCWedTime, @BCThuTime, @BCFriTime, @BCSatTime, @BCSunDist, @BCMonDist, @BCTueDist, @BCWedDist, @BCThuDist, @BCFriDist, @BCSatDist


			WHILE @@FETCH_STATUS = 0
			BEGIN

				Update #TimeCard
				Set SunStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCSunTime ELSE '0' END
					, SunWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCSunDist ELSE '0' END
					, SunDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCSunDist ELSE '0' END

					, MonStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCMonTime ELSE '0' END
					, MonWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCMonDist ELSE '0' END
					, MonDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCMonDist ELSE '0' END

					, TueStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCTueTime ELSE '0' END
					, TueWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCTueDist ELSE '0' END
					, TueDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCTueDist ELSE '0' END

					, WedStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCWedTime ELSE '0' END
					, WedWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCWedDist ELSE '0' END
					, WedDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCWedDist ELSE '0' END

					, ThrStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCThuTime ELSE '0' END
					, ThrWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCThuDist ELSE '0' END
					, ThrDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCThuDist ELSE '0' END

					, FriStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCFriTime ELSE '0' END
					, FriWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCFriDist ELSE '0' END
					, FriDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCFriDist ELSE '0' END

					, SatStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCSatTime ELSE '0' END
					, SatWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCSatDist ELSE '0' END
					, SatDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCSatDist ELSE '0' END
				Where Task = @BCActivityType

				Fetch Next from curBC into @BCActivityType, @BCSpeedAtt, @BCSunTime, @BCMonTime, @BCTueTime, @BCWedTime, @BCThuTime, @BCFriTime, @BCSatTime, @BCSunDist, @BCMonDist, @BCTueDist, @BCWedDist, @BCThuDist, @BCFriDist, @BCSatDist

			END

			Close curBC
			Deallocate curBC

			DECLARE @CountTest INT = (Select count(*) From #TimeCard)

			IF @CountTest = 0
			BEGIN
 
				SELECT 'No data returned for the date(s) provided' AS [Report Alert]  
            
			END
			ELSE
			BEGIN
		
				Select @HasSun = Count(*) from #TimeCard
				Where
					ISNULL(SunTime, '') <> ''
					or ISNULL(SunStand , '') <> ''
					or ISNULL(SunWalk , 0) > 0
					or ISNULL(SunDrive , 0) > 0

				Select @HasSat = Count(*) from #TimeCard
				Where
					ISNULL(SatTime, '') > ''
					or ISNULL(SatStand , '') > ''
					or ISNULL(SatWalk , 0) > 0
					or ISNULL(SatDrive , 0) > 0
			
				IF @HasSun = 0 and @HasSat = 0
				BEGIN

				
					select 
						[Task]   
					
						,ISNULL([MonTime],'0') [01MonTime]  
						,ISNULL([MonStand],'0') [02MonStand] 
						,ISNULL([MonWalk],'0') [03MonWalk] 
						,ISNULL([MonDrive],'0') [04MonDrive] 
						,ISNULL([TueTime],'0') [05TueTime] 
						,ISNULL([TueStand],'0') [06TueStand]
						,ISNULL([TueWalk],'0') [07TueWalk] 
						,ISNULL([TueDrive],'0') [08TueDrive] 
						,ISNULL([WedTime],'0') [09WedTime] 
						,ISNULL([WedStand],'0') [10WedStand] 
						,ISNULL([WedWalk],'0') [11WedWalk] 
						,ISNULL([WedDrive],'0') [12WedDrive] 
						,ISNULL([ThrTime],'0') [13ThrTime]  
						,ISNULL([ThrStand],'0') [14ThrStand] 
						,ISNULL([ThrWalk],'0') [15ThrWalk] 
						,ISNULL([ThrDrive],'0') [16ThrDrive] 
						,ISNULL([FriTime],'0') [17FriTime]    
						,ISNULL([FriStand],'0') [18FriStand] 
						,ISNULL([FriWalk],'0') [19FriWalk] 
						,ISNULL([FriDrive],'0') [20FriDrive] 
					Into #FinalResults1	
					from #TimeCard

					Select @Rows = ISNULL(@Rows + ',','') + '[' + Task + ']' From #FinalResults1

					Select @Cols = ISNULL(@Cols + ',','') + '[' + [Name] + ']' From
					(select C.name
					from tempdb.sys.columns c
					where c.object_id = OBJECT_ID('tempdb..#FinalResults1')
					) [Column]

					Select @Cols = Replace(@Cols, '[Task],', '')

					Set @DynamicPivotQuery = N' select * Into #FinalResults from #FinalResults1
						unpivot (value for [Date] in (' + @Cols + ')) unp
						pivot (max(value) for Task in  (' + @Rows + ')) p;
						Select SubString(Date, 3, 50) [Day], ' + @Rows + ' from #FinalResults Order By Date'

					exec sp_executesql @DynamicPivotQuery

					Drop Table #FinalResults1

				END
				ELSE
				IF @HasSun > 0 and @HasSat = 0
				BEGIN

					select 
						[Task]   
						,ISNULL([SunTime], '0') [01SunTime]   
						,ISNULL([SunStand], '0') [02SunStand] 
						,ISNULL([SunWalk], '0')  [03SunWalk]
						,ISNULL([SunDrive], '0') [04SunDrive]  
						,ISNULL([MonTime], '0') [05MonTime]  
						,ISNULL([MonStand], '0') [06MonStand] 
						,ISNULL([MonWalk], '0') [07MonWalk] 
						,ISNULL([MonDrive], '0') [08MonDrive] 
						,ISNULL([TueTime], '0') [09TueTime] 
						,ISNULL([TueStand], '0') [10TueStand]
						,ISNULL([TueWalk], '0') [11TueWalk] 
						,ISNULL([TueDrive], '0') [12TueDrive] 
						,ISNULL([WedTime], '0') [13WedTime] 
						,ISNULL([WedStand], '0') [14WedStand] 
						,ISNULL([WedWalk], '0') [15WedWalk] 
						,ISNULL([WedDrive], '0') [16WedDrive] 
						,ISNULL([ThrTime], '0') [17ThrTime]  
						,ISNULL([ThrStand], '0') [18ThrStand] 
						,ISNULL([ThrWalk], '0') [19ThrWalk] 
						,ISNULL([ThrDrive], '0') [20ThrDrive] 
						,ISNULL([FriTime], '0') [21FriTime]    
						,ISNULL([FriStand], '0') [22FriStand] 
						,ISNULL([FriWalk], '0') [23FriWalk] 
						,ISNULL([FriDrive], '0') [24FriDrive] 
					Into #FinalResults2
					from #TimeCard

					Select @Rows = ISNULL(@Rows + ',','') + '[' + Task + ']' From #FinalResults2

					Select @Cols = ISNULL(@Cols + ',','') + '[' + [Name] + ']' From
					(select C.name
					from tempdb.sys.columns c
					where c.object_id = OBJECT_ID('tempdb..#FinalResults2')
					) [Column]

					Select @Cols = Replace(@Cols, '[Task],', '')

					Set @DynamicPivotQuery = N' select * Into #FinalResults from #FinalResults2
						unpivot (value for [Date] in (' + @Cols + ')) unp
						pivot (max(value) for [Task] in  (' + @Rows + ')) p;
						Select SubString(Date, 3, 50) [Day], ' + @Rows + ' from #FinalResults Order By Date'

					exec sp_executesql @DynamicPivotQuery

					Drop Table #FinalResults2


				END
				ELSE
				IF @HasSun = 0 and @HasSat > 0
				BEGIN

					select 
						[Task]   
					
						,ISNULL([MonTime], '0') [01MonTime]  
						,ISNULL([MonStand], '0') [02MonStand] 
						,ISNULL([MonWalk], '0') [03MonWalk] 
						,ISNULL([MonDrive], '0') [04MonDrive] 
						,ISNULL([TueTime], '0') [05TueTime] 
						,ISNULL([TueStand], '0') [06TueStand]
						,ISNULL([TueWalk], '0') [07TueWalk] 
						,ISNULL([TueDrive], '0') [08TueDrive] 
						,ISNULL([WedTime], '0') [09WedTime] 
						,ISNULL([WedStand], '0') [10WedStand] 
						,ISNULL([WedWalk], '0') [11WedWalk] 
						,ISNULL([WedDrive], '0') [12WedDrive] 
						,ISNULL([ThrTime], '0') [13ThrTime]  
						,ISNULL([ThrStand], '0') [14ThrStand] 
						,ISNULL([ThrWalk], '0') [15ThrWalk] 
						,ISNULL([ThrDrive], '0') [16ThrDrive] 
						,ISNULL([FriTime], '0') [17FriTime]    
						,ISNULL([FriStand], '0') [18FriStand] 
						,ISNULL([FriWalk], '0') [19FriWalk] 
						,ISNULL([FriDrive], '0') [20FriDrive] 
						,ISNULL([SatTime], '0') [21SatTime]      
						,ISNULL([SatStand], '0') [22SatStand]  
						,ISNULL([SatWalk], '0') [23SatWalk] 
						,ISNULL([SatDrive], '0') [24SatDrive] 
					Into  #FinalResults3
					from #TimeCard

					Select @Rows = ISNULL(@Rows + ',','') + '[' + Task + ']' From #FinalResults3

					Select @Cols = ISNULL(@Cols + ',','') + '[' + [Name] + ']' From
					(select C.name
					from tempdb.sys.columns c
					where c.object_id = OBJECT_ID('tempdb..#FinalResults3')
					) [Column]

					Select @Cols = Replace(@Cols, '[Task],', '')

					Set @DynamicPivotQuery = N' select * Into #FinalResults from #FinalResults3
						unpivot (value for [Date] in (' + @Cols + ')) unp
						pivot (max(value) for [Task] in  (' + @Rows + ')) p;
						Select SubString(Date, 3, 50) [Day], ' + @Rows + ' from #FinalResults Order By Date'

					exec sp_executesql @DynamicPivotQuery

					Drop Table #FinalResults3


				END
				ELSE
				IF @HasSun > 0 and @HasSat > 0
				BEGIN

					select 
						[Task]   
						,ISNULL([SunTime], '0') [01SunTime]   
						,ISNULL([SunStand], '0') [02SunStand] 
						,ISNULL([SunWalk], '0')  [03SunWalk]
						,ISNULL([SunDrive], '0') [04SunDrive]  
						,ISNULL([MonTime], '0') [05MonTime]  
						,ISNULL([MonStand], '0') [06MonStand] 
						,ISNULL([MonWalk], '0') [07MonWalk] 
						,ISNULL([MonDrive], '0') [08MonDrive] 
						,ISNULL([TueTime], '0') [09TueTime] 
						,ISNULL([TueStand], '0') [10TueStand]
						,ISNULL([TueWalk], '0') [11TueWalk] 
						,ISNULL([TueDrive], '0') [12TueDrive] 
						,ISNULL([WedTime], '0') [13WedTime] 
						,ISNULL([WedStand], '0') [14WedStand] 
						,ISNULL([WedWalk], '0') [15WedWalk] 
						,ISNULL([WedDrive], '0') [16WedDrive] 
						,ISNULL([ThrTime], '0') [17ThrTime]  
						,ISNULL([ThrStand], '0') [18ThrStand] 
						,ISNULL([ThrWalk], '0') [19ThrWalk] 
						,ISNULL([ThrDrive], '0') [20ThrDrive] 
						,ISNULL([FriTime], '0') [21FriTime]    
						,ISNULL([FriStand], '0') [22FriStand] 
						,ISNULL([FriWalk], '0') [23FriWalk] 
						,ISNULL([FriDrive], '0') [24FriDrive] 
						,ISNULL([SatTime], '0') [25SatTime]      
						,ISNULL([SatStand], '0') [26SatStand]  
						,ISNULL([SatWalk], '0') [27SatWalk] 
						,ISNULL([SatDrive], '0') [28SatDrive]
					Into #FinalResults4
					from #TimeCard

					Select @Rows = ISNULL(@Rows + ',','') + '[' + Task + ']' From #FinalResults4

					Select @Cols = ISNULL(@Cols + ',','') + '[' + [Name] + ']' From
					(select C.name
					from tempdb.sys.columns c
					where c.object_id = OBJECT_ID('tempdb..#FinalResults4')
					) [Column]

					Select @Cols = Replace(@Cols, '[Task],', '')

					Set @DynamicPivotQuery = N' select * Into #FinalResults from #FinalResults4
						unpivot (value for [Date] in (' + @Cols + ')) unp
						pivot (max(value) for [Task] in  (' + @Rows + ')) p;
						Select SubString(Date, 3, 50) [Day], ' + @Rows + ' from #FinalResults Order By Date'

					exec sp_executesql @DynamicPivotQuery

					Drop Table #FinalResults4
		
			
				END
			END

			
			Drop Table #TimeCard
			Drop Table #ActivityUID
			Drop Table #BCData

		END

	END
END TRY
BEGIN CATCH
  THROW;
END CATCH


END















GO


