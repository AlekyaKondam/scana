USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[CreateNewClient_proc]    Script Date: 4/23/2019 1:18:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[CreateNewClient_proc] @ClientAccountID int, @ClientName varchar(50),@ClientContactTitle varchar(50)
           ,@ClientContactFName varchar(50) ,@ClientContactMI varchar(50), @ClientContactLName varchar(50)
           ,@ClientPhone varchar(15) ,@ClientEmail varchar(50) ,@ClientAddr1 varchar(50)
           ,@ClientAddr2 varchar(50) ,@ClientCity varchar(50)  ,@ClientState varchar(50)
           ,@ClientZip4 varchar(10) ,@ClientTerritory nvarchar(50) ,@ClientActiveFlag int
           ,@ClientDivisionsFlag int ,@ClientComment varchar(100)  ,@ClientFilesPath varchar(800)
           ,@ClientCreateDate datetime ,@ClientCreatorUserID varchar(50) ,@ClientModifiedDate datetime
           ,@ClientModifiedBy varchar(50)


/****************************************************************************************************
***************************************************************************************************
		Procedure Name:		CreateNewClient_proc					 				 
		Created By:				
		Modified BY:        Alekya Kondam
		Create Date:		05/03/2016	
		Modified Date:																 
		Create Reason:		Activate Mileage Cards by Use By MileageCard Id	 
		Changes Mades:       Added Exceptional Handling to SP        
		Prerequisite:		None																	 
		Utilized By:												 
					 
																									 
*****************************************************************************************************/
AS
BEGIN TRY
			BEGIN
					INSERT INTO [dbo].[ClientTb]
							   ([ClientAccountID]
							   ,[ClientName]
							   ,[ClientContactTitle]
							   ,[ClientContactFName]
							   ,[ClientContactMI]
							   ,[ClientContactLName]
							   ,[ClientPhone]
							   ,[ClientEmail]
							   ,[ClientAddr1]
							   ,[ClientAddr2]
							   ,[ClientCity]
							   ,[ClientState]
							   ,[ClientZip4]
							   ,[ClientTerritory]
							   ,[ClientActiveFlag]
							   ,[ClientDivisionsFlag]
							   ,[ClientComment]
							   ,[ClientFilesPath]
							   ,[ClientCreateDate]
							   ,[ClientCreatorUserID]
							   ,[ClientModifiedDate]
							   ,[ClientModifiedBy])
						 VALUES
							   (@ClientAccountID, @ClientName, @ClientContactTitle,@ClientContactFName, @ClientContactMI, @ClientContactLName
							   ,@ClientPhone,@ClientEmail, @ClientAddr1,@ClientAddr2,@ClientCity,@ClientState,@ClientZip4,@ClientTerritory 
							   ,@ClientActiveFlag,@ClientDivisionsFlag,@ClientComment,@ClientFilesPath,@ClientCreateDate,@ClientCreatorUserID 
							   ,@ClientModifiedDate,@ClientModifiedBy)

			END

END TRY
BEGIN CATCH
  THROW;
END CATCH

GO


