USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spNoLogout]    Script Date: 4/23/2019 1:30:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[spNoLogout]
AS

Declare @NoLogoutList varchar(max) = NULL
	,@EmailSubject varchar(100) = 'No Logout From CT'
	,@EmailReportName varchar(100) = 'NoLogOut'
	,@EmailBody varchar(max) = 'The following individuals did not log out today from CT.'
	,@CRLF varchar(10) = char(13) + char(10)
	,@ProjectID int = 0
	,@ExpectedVersion varchar(10)
	,@EmailTO varchar(max)
	,@EmailCC varchar(max)

BEGIN TRY
	Select @NoLogoutList = coalesce(@NoLogoutList + @CRLF,'') + u.UserLastName + ', ' + u.UserFirstName + ' (' + u.UserName + ')'
	From UserTb u
	Join [dbo].[vTodayNoLogout] NoLogOut on NoLogOut.ActivityCreatedUserUID = u.UserName

	IF @NoLogoutList is not NULL
	BEGIN

		Exec [dbo].[spGetEmailList] @EmailReportName, @ProjectID, @EmailTO OUTPUT, @EmailCC OUTPUT
				
		Set @EmailBody = @EmailBody + @CRLF + @CRLF + @NoLogoutList

		exec [dbo].[spSendEmail] @EmailTO, @EmailCC, @EmailSubject, @EmailBody

	END

END TRY
BEGIN CATCH
  THROW;
END CATCH







GO


