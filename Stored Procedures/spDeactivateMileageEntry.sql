USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spDeactivateMileageEntry]    Script Date: 4/23/2019 1:26:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[spDeactivateMileageEntry]
(
	@MileageCardID int
	,@MileageEntryTitleJSON varchar(1000)
	,@MileageEntryDate Date
	,@UserName varchar(50)
)
AS

/*

Example

	Exec spDeactivateMileageEntry 192310, '["MileageActivity"]', '1/28/2019', 'mdavis'



*/

Set NOCOUNT ON


--Before we start any thing need to save what was passed to the SP

Insert into [dbo].[tSPInsertedData]
	(SPName, InsertedData )
	Values
	('spDeactivateMileageEntry', 
	Cast(@MileageCardID as varchar(20)) + ', ' + 
	ISNULL(@MileageEntryTitleJSON, 'NULL') + ', ' + 
	ISNULL(Cast(@MileageEntryDate as varchar(20)), 'NULL') + ', ' + 
	ISNULL(@UserName, 'NULL'))
	




Declare @MileageEntryTitle varchar(200)
	,@MileageEntryID int

	select * Into #MileageEntryTitle From [dbo].[parseJSON](@MileageEntryTitleJSON)

	Declare curMileageEntryTitle Cursor Static
	For
	select StringValue from #MileageEntryTitle Where Name is null

	Open curMileageEntryTitle

	BEGIN TRY

		BEGIN TRANSACTION DeactivateTimeEntry

			Fetch Next From curMileageEntryTitle into @MileageEntryTitle

			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				Declare curMileageEntryID CURSOR STATIC
				For
				Select MileageEntryID
				From
				MileageEntryTb m
				Join ActivityTb A on m.MileageEntryActivityID = a.ActivityID
				Where MileageEntryMileageCardID = @MileageCardID 
					and a.ActivityTitle = @MileageEntryTitle
					and Cast(m.MileageEntryStartDate as date) = ISNULL(@MileageEntryDate, Cast(m.MileageEntryStartDate as date))
					and m.MileageEntryActiveFlag = 1

				Open curMileageEntryID

				Fetch Next From curMileageEntryID into @MileageEntryID

				WHILE @@FETCH_STATUS = 0
				BEGIN

					insert into [dbo].[tMileageEntryEventHistory]
					(
						MileageEntryID
						, MileageEntryStartingMileage
						, MileageEntryEndingMileage
						, MileageEntryStartDate
						, MileageEntryEndDate
						, MileageEntryWeekDay
						, MileageEntryType
						, MileageEntryMileageCardID
						, MileageEntryActivityID
						, MileageEntryApprovedBy
						, MileageEntryComment
						, MileageEntryCreatedBy
						, MileageEntryModifiedDate
						, MileageEntryModifiedBy
						, MileageEntryUserName
						, MileageEntrySrcDTLT
						, MileageEntrySrvDTLT
						, MileageEntryActiveFlag
						, ChangeMadeBy
						, Change
						, Comments
					)

					Select
						MileageEntryID
						, MileageEntryStartingMileage
						, MileageEntryEndingMileage
						, MileageEntryStartDate
						, MileageEntryEndDate
						, MileageEntryWeekDay
						, MileageEntryType
						, MileageEntryMileageCardID
						, MileageEntryActivityID
						, MileageEntryApprovedBy
						, MileageEntryComment
						, MileageEntryCreatedBy
						, MileageEntryModifiedDate
						, MileageEntryModifiedBy
						, MileageEntryUserName
						, MileageEntrySrcDTLT
						, MileageEntrySrvDTLT
						, MileageEntryActiveFlag 
						, @UserName --ChangeMadeBy, 
						, 'Delete' --Change, 
						, '' --Comments
					From MileageEntryTb where MileageEntryID = @MileageEntryID

					Delete from MileageEntryTb where MileageEntryID = @MileageEntryID

					Fetch Next From curMileageEntryID into @MileageEntryID


				END

				close curMileageEntryID
				Deallocate curMileageEntryID

				Fetch Next From curMileageEntryTitle into @MileageEntryTitle

			END
			
		COMMIT TRANSACTION DeactivateTimeEntry

	END TRY
	BEGIN CATCH

		ROLLBACK TRANSACTION DeactivateTimeEntry
		--Set @Result = 0

	END CATCH

	close curMileageEntryTitle
	deallocate curMileageEntryTitle

Set NOCOUNT OFF














GO


