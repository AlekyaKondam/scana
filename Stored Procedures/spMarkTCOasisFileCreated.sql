USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spMarkTCOasisFileCreated]    Script Date: 4/23/2019 1:29:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spMarkTCOasisFileCreated]
(
	@ProjectID int 
	,@WeekStartingDate date
	,@WeekEndingDate  Date
)
AS

/*

Example

	



*/
BEGIN TRY
	Set NOCOUNT ON

	Update TimeCardTb 
	set TimeCardSubmittedOasis = getdate() 
	where Cast(TimeCardStartDate as date) >= @WeekStartingDate 
		and Cast(TimeCardEndDate as date) <= @WeekEndingDate 
		and TimeCardProjectID = @ProjectID 
		and TimeCardSubmittedOasis is null


	Set NOCOUNT OFF

	END TRY
BEGIN CATCH
  THROW;
END CATCH






GO


