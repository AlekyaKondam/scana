USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateMSDynamicsMileageCardByProject]    Script Date: 4/23/2019 1:27:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE Procedure [dbo].[spGenerateMSDynamicsMileageCardByProject](@ProjectIDJSON varchar(MAX), @WeekStartingDate date, @WeekEndingDate date)
AS


Create Table #MileageCardDetail
(
	[Employee Id] varchar(50) NULL    
	,[Class (Department)] varchar(50) NULL
	,[Transaction Date] Date NULL
	,[Client Code] varchar(50) NULL
	,[Service Items] varchar(50) NULL
	,[Payroll Item]  varchar(100) NULL
	,[ExpensePayAccount]  varchar(50) NULL
	,[ExpenseChargeAccount] varchar(50) NULL
	,[Miles to Import] Int NULL
	,[Billable]  char(1) NULL
	,[ReferenceNumber]  int NULL
	,[First Name] Varchar(100) NULL
	,[Last Name] varchar(100) NULL
)

Create Table #MileageCard
(
	[Employee Id] varchar(50) NULL
	,[COA] varchar(50) NULL    
	,[QTY] varchar(50) NULL
	,[Week Ending] Date NULL
	,[Employee Name] varchar(200) NULL
	--,[TotalSec] int NULL
)



/*

	Testing

	

	Exec spGenerateMSDynamicsMileageCardByProject '["1086"]', '2019-03-17', '2019-03-23'

	select * from fnGenerateMileageDataByProject('["1086"]', '2019-03-17', '2019-03-23', 'MSDynamics', 'report')
	
*/

BEGIN

	Declare @ProjectID int
			
	--Before we start any thing need to save what was passed to the SP

	Declare @SPName varchar(100) = Object_Name(@@ProcID)

	Insert into [dbo].[tSPInsertedData]
	(SPName, InsertedData )
	Values
	(@SPName, ISNULL(@ProjectIDJSON, 'NULL') + ', ' + ISNULL(Cast(@WeekStartingDate as varchar(20)), 'NULL') + ', ' + ISNULL(cast(@WeekEndingDate as varchar(20)), 'NULL'))
	

	BEGIN TRY

		BEGIN TRAN CREATEQBFILE


			IF @ProjectIDJSON IS NOT NULL and @WeekStartingDate is not null and @WeekEndingDate is not null
			BEGIN

				select @WeekStartingDate = dateadd(d, -datepart(dw, @WeekStartingDate)+ 1, @WeekStartingDate)

				select @WeekEndingDate = dateadd(d, 7-datepart(dw, @WeekEndingDate), @WeekEndingDate)


				Insert Into #MileageCardDetail
				(
					 [Employee Id]
					,[Class (Department)]
					,[Transaction Date]
					,[Client Code]
					,[Service Items] 
					,[Payroll Item]
					,[ExpensePayAccount]
					,[ExpenseChargeAccount] 
					,[Miles to Import] 
					,[Billable] 
					,[ReferenceNumber] 
					,[First Name]
					,[Last Name]
				)
				select 
				
					[Employee Id]
					,[Class (Department)]
					,[Transaction Date]
					,[Client Code]
					,[Service Items] 
					,[Payroll Item]
					,[ExpensePayAccount]
					,[ExpenseChargeAccount] 
					,[Miles to Import] 
					,[Billable] 
					,[ReferenceNumber] 
					,[First Name]
					,[Last Name] 
				from fnGenerateMileageDataByProject(@ProjectIDJSON, @WeekStartingDate, @WeekEndingDate, 'MSDynamics', 'FTP') where [Employee Id] is not null

				select * Into #ProjectNames from [dbo].[parseJSON](@ProjectIDJSON)

				Declare curProjectNames Cursor
				For
				select StringValue from #ProjectNames Where Name is null

				Open curProjectNames

				Fetch Next from curProjectNames into @ProjectID

				WHILE @@FETCH_STATUS = 0
				BEGIN 
			
					Update mc 
					set MileageCardSubmittedMSDynamics = getdate() 
					, MileageCardModifiedDate = getdate()
					From MileageCardTb mc
					Join (Select * from UserTb where UserAppRoleType in ('Technician') and UserActiveFlag = 1 and UserPayMethod = 'H') u on u.UserID = mc.MileageCardTechID
					where mc.MileageCardApprovedFlag = 1
						and Cast(mc.MileageStartDate as date) >= @WeekStartingDate 
						and Cast(mc.MileageEndDate as date) <= @WeekEndingDate 
						and mc.MileageCardProjectID = @ProjectID 
						and mc.MileageCardActiveFlag = 1
						and mc.MileageCardSubmittedMSDynamics is null
			
					Fetch Next from curProjectNames into @ProjectID

				END --Fetch Next from curProjectNames into @ProjectID

			END -- @ProjectIDJSON IS NOT NULL and @WeekStartingDate is not null and @WeekEndingDate is not null

		COMMIT TRAN CREATEQBFILE

		Insert Into #MileageCard
		select [Employee Id]
			, [Payroll Item]
			, Cast(Sum([Miles to Import]) as float)
			, [Transaction Date]
			, [First Name] + ' ' + [Last Name]  
		from #MileageCardDetail
		where [Employee Id] is not null
		Group By [Employee Id], [Payroll Item], [Transaction Date], [First Name] + ' ' + [Last Name]

		select * From #MileageCard where QTY <> '0.00'

		Drop Table #ProjectNames


	END TRY
	BEGIN CATCH

		ROLLBACK TRAN CREATEQBFILE
		
		--DROP TABLE #TimeCardDetail
		
		--Select 'ERROR'

		select * from [dbo].[NonExist]



	END CATCH

--	select * from #TimeCardDetail

	Drop Table #MileageCardDetail
	Drop Table #MileageCard

	--Select * from #MileageCardDetail
	
END












































GO


