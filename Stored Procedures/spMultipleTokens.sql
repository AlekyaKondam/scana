USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spMultipleTokens]    Script Date: 4/23/2019 1:30:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spMultipleTokens]
(
@LoginDateTime datetime
,@UserName varchar(50)

)
AS

Declare @MissingBCList varchar(max) = NULL
	,@EmailSubject varchar(100) = 'Multiple Tokens'
	,@EmailReportName varchar(100) = 'MultipleTokens'
	,@EmailBody varchar(max) = 'The following individual potentially had multiple tokens today.'
	,@CRLF varchar(10) = char(13) + char(10)
	,@ProjectID int
	--,@ExpectedVersion varchar(10)
	,@EmailTO varchar(max)
	,@EmailCC varchar(max)
	,@LoginCount int
	,@IsTech bit = 0
	,@UserFullName varchar(100)
	,@DBName varchar(50) = DB_Name()
	


Set @EmailSubject =
	CASE 
		WHEN @DBName = 'CometTracker' THEN 'CT DEV - ' + @EmailSubject
		WHEN @DBName = 'CometTracker_Stage' THEN 'CT STAGE - ' + @EmailSubject
		WHEN @DBName = 'CometTracker_DEMO' THEN 'CT DEMO - ' + @EmailSubject
		WHEN @DBName = 'AZCometTracker_PROD' THEN 'CT PROD - ' + @EmailSubject

	END

BEGIN TRY
select 
@UserFullName = UserFirstName + ' ' + UserLastName + ' (' + @UserName + ')'
,@IsTech = Case WHEN UserAppRoleType like '%Technician%' THEN 1 ELSE 0 END 
from UserTb where UserName = @UserName and UserActiveFlag = 1

Select @LoginCount = Count(*) from [history].[HistoryAuth_Assignment]where HistItem_Name Like '%Auth_Token%' and Cast(CreateDate as Date) = Cast(@LoginDateTime AS date) and CreatedBy = @UserName

IF @LoginCount > 0 and @LoginCount < 3 and @IsTech = 1
and @UserName not in
(
'jtest'
,'scanaTech'
,'RHAMMOND'
,'dualDispatchTech1'
,'dualDispatchTech2'
,'ytech'
,'scctTech'
,'test1'
,'test2'
)
BEGIN

	Set @EmailBody = @EmailBody + @CRLF + @CRLF + @UserFullName + ' - ' + Cast(@LoginDateTime as varchar(25))+ ' - ' + cast(@LoginCount + 1 as varchar(5))

	Exec [dbo].[spGetEmailList] @EmailReportName, @ProjectID, @EmailTO OUTPUT, @EmailCC OUTPUT

	exec [dbo].[spSendEmail] @EmailTO, @EmailCC, @EmailSubject, @EmailBody


END


END TRY
BEGIN CATCH
  THROW;
END CATCH


GO


