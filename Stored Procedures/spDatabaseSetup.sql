USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spDatabaseSetup]    Script Date: 4/23/2019 1:26:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE  PROCEDURE [dbo].[spDatabaseSetup] 

AS

/*
Testing

	Exec [spDatabaseSetup] 

	Process

	Step 1. Call spTruncateDB to Truncate all tables 
	Step 2. Call spCreateRefDB to get all data from Azure RefDB
	Step 3. Call spModuleRelation to update ModuleRelation to all Project ID's
	Step 4. Call spSetLandingPage to update Landing Page to all Project ID's

	-- spModuleRelation
    select count(*) from [menus].[ProjectModuleTb]

	--spSetLandingPage
    select count(*) from projectTb
    Select ProjectLandingPage, * from ProjectTb

*/

BEGIN TRY
Set NOCOUNT ON


Declare @ProjectID int
  Execute [dbo].[spTruncateDB]
  Execute [dbo].[spCreateRefDB]

Declare ProjectIDList Cursor Static
For
select projectID from ProjectTb


Open ProjectIDList

Fetch Next From ProjectIDList into @ProjectID

While @@FETCH_STATUS = 0
BEGIN

	Exec spModuleRelation @projectID
	Exec spSetLandingPage @projectID
	Fetch Next From ProjectIDList into @ProjectID

END

CLOSE ProjectIDList
DEALLOCATE ProjectIDList

	
Set NOCOUNT OFF
END TRY
BEGIN CATCH
  THROW;
END CATCH



GO


