USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[RemoveTimeCards_Paramproc]    Script Date: 4/23/2019 1:22:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/*****************************************************************************************************/
/*****************************************************************************************************/

/*		Procedure Name:		RemoveTimeCards_Paramproc												 */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		02/02/2016																 */
/*		Create Reason:		Remove All Employee Pay Periods	For a User in TimeCard Tb		         */
/*		Prerequisite:		Gather the userid										                 */
/*		Parameters(1):		@Parameter1 int = userid                                                 */
/*		Utilized By:		TimeCardTb table                                                         */
/******************************************************************************************************/
/******************************************************************************************************/





CREATE PROCEDURE  [dbo].[RemoveTimeCards_Paramproc] @Parameter1 int
as 
BEGIN TRY
DELETE 
FROM [dbo].[TimeCardTb]
WHERE [TimeCardID] >= @Parameter1
END TRY
BEGIN CATCH
  THROW;
END CATCH






GO


