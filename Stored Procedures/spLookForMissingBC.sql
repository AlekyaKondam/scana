USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spLookForMissingBC]    Script Date: 4/23/2019 1:29:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spLookForMissingBC]
AS

Declare @MissingBCList varchar(max) = NULL
	,@EmailSubject varchar(100) = 'No BC After Login'
	,@EmailReportName varchar(100) = 'NoBC'
	,@EmailBody varchar(max) = 'The following individuals are not sending up BCs.'
	,@CRLF varchar(10) = char(13) + char(10)
	,@ProjectID int
	,@ExpectedVersion varchar(10)
	,@EmailTO varchar(max)
	,@EmailCC varchar(max)
	
BEGIN TRY
select * Into #NoBC from [dbo].[vNoBCAfterLogin]

IF (select Count(*) from #NoBC) > 0
BEGIN

	Declare curProjectNoBC Cursor Static
	For
	Select Distinct ActivityProjectID from #NoBC

	Open curProjectNoBC

	Fetch Next From curProjectNoBC into @ProjectID

	While @@FETCH_STATUS = 0 
	BEGIN


		select @ExpectedVersion = ProjectMinimumAppVersion from ProjectTb where ProjectID = @ProjectID

		Select @MissingBCList = coalesce(@MissingBCList + @CRLF,'') + TechName + ', ' + Cast(LoginDatetime as varchar(20)) + ', ' + ActivityAppVersion + ', ' + @ExpectedVersion
			from #NoBC 
			Where ActivityProjectID = @ProjectID

		Exec [dbo].[spGetEmailList] @EmailReportName, @ProjectID, @EmailTO OUTPUT, @EmailCC OUTPUT
				
		Set @EmailBody = @EmailBody + @CRLF + @CRLF + @MissingBCList

		exec [dbo].[spSendEmail] @EmailTO, @EmailCC, @EmailSubject, @EmailBody

		Fetch Next From curProjectNoBC into @ProjectID

	END --Fetch Next From curProjectNoBC into @ProjectID

END
END TRY
BEGIN CATCH
  THROW;
END CATCH





GO


