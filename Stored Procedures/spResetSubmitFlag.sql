USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spResetSubmitFlag]    Script Date: 4/23/2019 1:31:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spResetSubmitFlag](@ProjectIDJSON varchar(max), @WeekStartingDate date, @WeekEndingDate date, @Type varchar(50) = 'BOTH')


AS


/*

	Testing
	
	Exec spResetSubmitFlag '["1111"]', '2018-03-11', '2018-03-17', 'BOTH'


	select  timecardsubmittedoasis, TimeCardSubmttedQuickBooks, * 
	from timecardtb 
	where timecardprojectid = '1111' and timecardstartdate = '2018-03-11' and timecardenddate = '2018-03-17'

*/

BEGIN


	--Before we start any thing need to save what was passed to the SP

SET NOCOUNT ON
BEGIN TRY	
	Insert into [dbo].[tSPInsertedData]
	(SPName, InsertedData )
	Values
	('spResetSubmitFlag', ISNULL(@ProjectIDJSON, 'NULL') + ', ' + ISNULL(Cast(@WeekStartingDate as varchar(20)), 'NULL') + ', ' + ISNULL(cast(@WeekEndingDate as varchar(20)), 'NULL') + ', ' + @Type )
	
	
	IF @ProjectIDJSON IS NOT NULL and @WeekStartingDate is not null and @WeekEndingDate is not null
	BEGIN
		
		
		select @WeekStartingDate = dateadd(d, -datepart(dw, @WeekStartingDate)+ 1, @WeekStartingDate)

		select @WeekEndingDate = dateadd(d, 7-datepart(dw, @WeekEndingDate), @WeekEndingDate)
		
		select * Into #ProjectID from [dbo].[parseJSON](@ProjectIDJSON)

		Update TimeCardTb
		Set
			TimeCardSubmittedOasis = CASE WHEN @Type = 'Oasis' or @Type = 'BOTH' or @Type = 'ALL' THEN NULL ELSE TimeCardSubmittedOasis END
		   ,TimeCardSubmttedQuickBooks = CASE WHEN @Type = 'QuickBooks' or @Type = 'BOTH' or @Type = 'ALL' THEN NULL ELSE TimeCardSubmttedQuickBooks END
		   ,TimeCardSubmttedADP = CASE WHEN @Type = 'ADP' or @Type = 'BOTH' or @Type = 'ALL' THEN NULL ELSE TimeCardSubmttedADP END
		   
		Where TimeCardProjectID in (select StringValue from #ProjectID Where Name is null)
		AND TimeCardStartDate >= @WeekStartingDate 
		AND TimeCardEndDate <= @WeekEndingDate

	END

	END TRY
BEGIN CATCH
  THROW;
END CATCH


SET NOCOUNT OFF


END































GO


