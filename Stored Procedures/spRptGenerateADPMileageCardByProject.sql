USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptGenerateADPMileageCardByProject]    Script Date: 4/24/2019 9:09:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE  Procedure [dbo].[spRptGenerateADPMileageCardByProject](@WeekStartingDate date, @WeekEndingDate date , @ProjectIDJSON varchar(max) = null)


AS


SET NOCOUNT ON

Create Table #MileageCard
(
	[Employee Id] varchar(50) NULL
	,[COA] varchar(50) NULL    
	,[QTY] varchar(50) NULL
	,[Week Ending] Date NULL
	,[Employee Name] varchar(200) NULL
)

/*   
1088
1108
	
	Exec spRptGenerateOasisMileageCardByProject  '2019-01-13', '2019-01-19' ,NULL


	select  timecardsubmittedoasis, TimeCardSubmttedQuickBooks, TimeCardSubmttedADP, * 
	from timecardtb 
	where timecardprojectid = '1423' and timecardstartdate >= '2018-03-25' and timecardenddate <= '2018-03-35'

	update timecardtb 
	Set timecardsubmittedoasis = NULL, TimeCardSubmttedQuickBooks = NULL, TimeCardSubmttedADP = NULL
	where timecardprojectid = '1423' and timecardstartdate >= '2018-03-25' and timecardenddate <= '2018-03-31'


*/


BEGIN
BEGIN TRY
 

IF @ProjectIDJSON is null
	BEGIN
		--select @WeekStartingDate = dateadd(d, -datepart(dw, @WeekStartingDate)+ 1, @WeekStartingDate)

		--select @WeekEndingDate = dateadd(d, 7-datepart(dw, @WeekEndingDate), @WeekEndingDate)

		SELECT  -- 20160111 Added
			'<All>'  as Drop_Down, 'ALL' AS Project_Name
		INTO #DD
		
		UNION ALL

      select distinct cast([ProjectID] as varchar(20)) as [DropDown] , [ProjectName] As [Project_Name] 
		from fnGenerateMileageDataByProject ('<All>', @WeekStartingDate, @WeekEndingDate, 'ADP', 'Report')

    	SELECT Drop_Down, Project_Name FROM #DD -- DROP TABLE #DD

	END
ELSE
BEGIN

	Insert Into #MileageCard
	 (
 		[Employee Id], [COA], [QTY] , [Week Ending], [Employee Name]
	 )

	 select 
		[Employee Id],  
		[Payroll Item] as COA , 
		--format(sum(Cast([Hours to Import] as float)), '0.00') as QTY , 
		 Cast(Sum([Miles to Import]) as float) as Miles
		,[Transaction Date] as [Week Ending],[First Name] + ' ' +  [Last Name] as [Employee Name]
	 from fnGenerateMileageDataByProject(@ProjectIDJSON, @WeekStartingDate, @WeekEndingDate, 'ADP', 'Report')
	 group by [Employee Id],  [Payroll Item] , [Transaction Date],[First Name] + ' ' +  [Last Name]

	 DECLARE @CountTest INT = (Select count(*) From #MileageCard)
	 
	 IF @CountTest = 0
	BEGIN
 
		SELECT 'No data returned for the date(s) provided' AS [Report Alert]  
            
	END
	Else
	BEGIN

		Select * from #MileageCard

	END

END

Drop Table #MileageCard

END TRY
BEGIN CATCH
  THROW;
END CATCH

SET NOCOUNT OFF

END






















































































GO


