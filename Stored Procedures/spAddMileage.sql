USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spAddMileage]    Script Date: 4/23/2019 1:25:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spAddMileage]
(
	@MileageCardID int
	,@Date varchar(20)
	,@TotalMiles Float
	,@MileageType varchar(20)
	,@CreatedByUserName varchar(50)
	
	
)
AS

/*

Exec spAddMileage @MileageCardID, @TaskName, @Date, @TotalMiles, @MileageType, @CreatedByUserName


*/

Set NOCOUNT ON
BEGIN TRY
--Before we start any thing need to save what was passed to the SP

	Insert into [dbo].[tSPInsertedData]
	(SPName, InsertedData )
	Values
	('spAddMileage', 'Time Card ID: ' + ISNULL(Cast(@MileageCardID as varchar(20)), 'NULL') 
	     --+ ', Task Name: ' + ISNULL(@TaskName, 'NULL')  
		 + ', Date: ' + ISNULL(@Date, 'NULL')  
		 + ', Total Miles: ' + ISNULL(Cast(@TotalMiles as varchar(50)), 'NULL')  
		 + ', Mileage Type: ' + ISNULL(@MileageType, 'NULL')  
		 + ', Created By: ' + ISNULL(@CreatedByUserName, 'NULL')  
		 
		 )
	



	Declare @Next_ID Int
		,@UserID int
		,@CreatedUserID int
		,@ProjectID int
		--,@StartDatetime datetime = @Date + ' ' + @StartTime
		--,@EndDatetime datetime = @Date + ' ' + @EndTime
		,@SourceID varchar(50) = 'spAddMileage'
		,@RandomNum  Decimal(18,18) = Rand()
		,@ActivityUID varchar(200)
		,@NewUnipueID varchar(50)
		,@CurrentDateTime datetime = getdate()
		,@Comments varchar(200) = 'Manually Added by ' + @CreatedByUserName
		,@Seperator char(1) = '_'
		,@maxRandomValue int = 10
		,@minRandomValue int = 0
		,@UserName varchar(20)
		,@WeekDay varchar(20) = DateName( dw, @Date)
		,@ActivityID int
		,@TaskName varchar(100) = 'MileageActivity'
	
If EXISTS (Select * from MileageCardTb where MileageCardID = @MileageCardID)
BEGIN


	--select @UserID = MileageCardTechID  from MileageCardTb where MileageCardID = @MileageCardID
	
	Select  @CreatedUserID = UserID from UserTb where UserName = @CreatedByUserName
	
	select @ProjectID = MileageCardProjectID, @UserID = MileageCardTechID from MileageCardTb where MileageCardID = @MileageCardID

	Select @UserName = UserName from UserTb where UserID = @UserID
	
	select @Next_ID = IDENT_CURRENT('ActivityTb') + 1

	Set @Next_ID = ISNULL(@Next_ID, 1)

	Select @NewUnipueID = REPLACE(Cast(Cast(((@maxRandomValue + 1) - @minRandomValue) 
	* @RandomNum + @minRandomValue As decimal(7,4)) * Cast(@Next_ID as int) as varchar(20)), '.', '')

	SELECT @ActivityUID = @TaskName + '_' 
		+ Ltrim(Rtrim(@NewUnipueID)) 
		+ @Seperator
		+ Cast(DatePart(year, @CurrentDateTime) as char(4)) 
		+ Right('00' + Ltrim(rtrim(Cast(datepart(Month, @CurrentDateTime) as Char(2)))),2)
		+ Right('00' + Ltrim(rtrim(Cast(datepart(Day, @CurrentDateTime) as Char(2)))),2)
		+ Right('00' + Ltrim(rtrim(Cast(datepart(Hour, @CurrentDateTime) as Char(2)))),2)
		+ Right('00' + Ltrim(rtrim(Cast(datepart(Minute, @CurrentDateTime) as Char(2)))),2)
		+ Right('00' + Ltrim(rtrim(Cast(datepart(Second, @CurrentDateTime) as Char(2)))),2)
		+ @Seperator
		+ LTrim(RTrim('WEB'))

	Insert Into ActivityTb
	(
		ActivityStartTime, 
		ActivityEndTime, 
		ActivityTitle, 
		ActivityCode, 
		ActivityPayCode, 
		ActivityArchiveFlag, 
		ActivityCreateDate, 
		ActivityCreatedUserUID, 
		ActivityUID, 
		ActivityProjectID, 
		ActivitySourceID,
		ActivitySrcDTLT,
		ActivityComments,
		ActivityAppVersion, 
		ActivityAppVersionName
		
	)
	Values
	(
		@Date, --ActivityStartTime, 
		@Date, --ActivityEndTime, 
		@TaskName, --ActivityTitle, 
		0, --ActivityCode, 
		0, --ActivityPayCode, 
		0 , --ActivityArchiveFlag, 
		@CurrentDateTime, --ActivityCreateDate, 
		@CreatedByUserName, --ActivityCreatedUserUID, 
		@ActivityUID, --ActivityUID, 
		@ProjectID, --ActivityProjectID, 
		@SourceID, --ActivitySourceID,
		@CurrentDateTime, --ActivitySrcDTLT,
		@Comments, --ActivityComments,
		@SourceID, --ActivityAppVersion, 
		'LIGHTHOUSE' --ActivityAppVersionName
	)

	
	select @ActivityID = ActivityID from ActivityTb where ActivityUID = @ActivityUID
	

	Insert Into MileageEntryTb
	(
		MileageEntryStartingMileage, 
		MileageEntryEndingMileage, 
		MileageEntryStartDate, 
		MileageEntryEndDate, 
		MileageEntryMileageCardID, 
		MileageEntryActivityID, 
		MileageEntryCreatedBy, 
		MileageEntryUserName, 
		MileageEntryActiveFlag, 
		MileageEntryMileageType,
		MileageEntryTotalMiles
	)
	Values	
	(
		0, --MileageEntryStartingMileage, 
		0, --MileageEntryEndingMileage, 
		@Date, --MileageEntryStartDate, 
		@Date, --MileageEntryEndDate, 
		@MileageCardID, --MileageEntryMileageCardID, 
		@ActivityID, --MileageEntryActivityID, 
		@CreatedByUserName, --MileageEntryCreatedBy, 
		@UserName, --MileageEntryUserName, 
		1, --MileageEntryActiveFlag, 
		@MileageType, --MileageEntryMileageType,
		@TotalMiles --MileageEntryTotalMiles
	)

END

END TRY
BEGIN CATCH
  THROW;
END CATCH
Set NOCOUNT OFF















GO


