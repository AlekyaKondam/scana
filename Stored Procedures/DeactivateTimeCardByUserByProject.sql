USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[DeactivateTimeCardByUserByProject_proc]    Script Date: 4/23/2019 1:18:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****************************************************************************************************
***************************************************************************************************
		Procedure Name:		DeactivateTimeCardByUserByProject					 				 
		Created By:			Sandra Jackson, Senior DBA											 
		Create Date:		05/03/2016	
		Modified By :		Alekya Kondam
        Modified Date:		03/29/2019	
        Modifications made : added Exceptional handling 														 
		Create Reason:		Deactivate Time Cards by Use By Project			         
		Prerequisite:		None																	 
		Utilized By:		CometTracker App, Supervisor, Admin										 
		Version:			1.0																		 
		Modified:			1.0 = creation 
							
		Status:				Active or Inactive					 
																									 
*****************************************************************************************************/
CREATE PROCEDURE [dbo].[DeactivateTimeCardByUserByProject_proc]  @UserParam int, @ProjectParam int
AS


BEGIN TRY
UPDATE [CometTracker].[dbo].[TimeCardTb]
----changed below by FS as part of TimecardFlag change to 0 or 1 
 SET [TimeCardActiveFlag] = 0
--SET [TimeCardActiveFlag] = 'Inactive'
WHERE [TimeCardTechID] = @UserParam
and [TimeCardProjectID] = @ProjectParam
END TRY
BEGIN CATCH
  THROW;
END CATCH







GO


