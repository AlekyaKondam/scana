USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[PopulateMileageCardTbForNewUser_proc]    Script Date: 4/23/2019 1:21:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*****************************************************************************************************/
/*****************************************************************************************************/

/*  	Function Name:		PopulateMileageCardTbForNewUser_proc				  				     */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		01/29/2016																 */
/*		Create Reason:		Populate Time Cards For New User for the year							 */
/*		Parameters(1):		UserID OF New User														 */
/*		Prerequisites:												                                 */
/*		Utilized By:		CometTracker App, Supervisor, Admin										 */
/*		Call methods(1):	1. execute dbo.PopulateMileageCardTbForNewUser_proc						 */
/*		Validation:			select count(*)* 53 FROM [dbo].[UserTb] WHERE [UserActiveFlag]			 */
/*                          NOT IN ('INACTIVE','TERMINATED','RETIRED','PENDING')					 */
/*							select count(*) from [TimeCardTb]  --test to validate it worked			 */
/*		Status:				UserActiveFlag = 1 inactive = 0 terminated = 9 pending = 2				 */

--Modified By :		Alekya Kondam
--Modified Date:		03/29/2019	
--Modifications made : added Exceptional handling 	

/* TO BE RUN 1 TIME A YEAR TO CREATE TIME CARD PERIODS                                                */
/******************************************************************************************************/
/******************************************************************************************************/


CREATE PROCEDURE [dbo].[PopulateMileageCardTbForNewUser_proc] @PARAMETER1 int

AS

  DECLARE @TECHID int
  BEGIN TRY
    DECLARE TEST SCROLL CURSOR FOR
    SELECT
      [UserID]
    FROM [dbo].[UserTb]
    WHERE [UserActiveFlag] NOT IN (0, 9, 2)
    AND [UserID] = @PARAMETER1
    ORDER BY [UserID];

    OPEN TEST
    FETCH NEXT FROM TEST
    INTO @TECHID
    WHILE @@FETCH_STATUS = 0
    BEGIN

      INSERT INTO [dbo].[MileageCardTb] ([MileageStartDate], [MileageEndDate], [MileageCardProjectID], [MileageCardTechID], [MileageCardCreateDate], [MileageCardCreatedBy])
        SELECT
          [TimeCardSunday] AS [MileageStartDate],
          [TimeCardSaturday] AS [MileageEndDate],
          1006 AS [MileageCardProjectID],
          @TECHID AS [MileageCardTechID],
          GETDATE() AS [MileageCardCreateDate],
          SUSER_NAME() AS [MileageCardCreatedBy]
        FROM [dbo].[TimeCardPeriodsTb]

      FETCH NEXT FROM TEST
      INTO @TECHID

    END
    CLOSE TEST
    DEALLOCATE TEST

  END TRY
  BEGIN CATCH
    THROW;
  END CATCH


GO


