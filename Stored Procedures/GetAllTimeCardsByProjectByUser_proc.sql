USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[GetAllTimeCardsByProjectByUser_proc]    Script Date: 4/23/2019 1:19:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/****************************************************************************************************
***************************************************************************************************
		View Name:			GetAllTimeCardsByProjectByUser_proc						 				 
		Created By:			Sandra Jackson, Senior DBA												 
		Create Date:		03/23/2016																 
		Create Reason:		View all time card by project by user
		Modified By :		Alekya Kondam
         Modified Date:		03/29/2019	
        Modifications made : added Exceptional handling 						         
		Prerequisite:		AllTimeCardsByProjectByUser_vw																	 
		Utilized By:		CometTracker App, Supervisor, Admin										 
		Version:			1.0																		 
																									 
*****************************************************************************************************/



CREATE PROCEDURE [dbo].[GetAllTimeCardsByProjectByUser_proc]  @PARAMETER1 INT
AS
BEGIN TRY
-- Declare @PARAMETER1 INT set @PARAMETER1 = '1'
SELECT DISTINCT TimeCardID
	  ,[TimeCardStartDate]
      ,[TimeCardEndDate]
      ,[TimeCardProjectID]
      ,[TimeCardTechID]
      ,[TimeCardApprovedFlag]
      ,[TimeCardApprovedBy]
      ,[TimeCardSupervisorName]
      ,[TimeCardComment]
      ,[TimeCardCreateDate]
      ,[TimeCardCreatedBy]
      ,[TimeCardModifiedDate]
      ,[TimeCardModifiedBy]
	  ,[ProjectID]
      ,[ProjectName]
      ,[ProjectDescription]
      ,[ProjectNotes]
      ,[ProjectType]
      ,[ProjectStatus]
      ,[ProjectClientID]
      ,[ProjectStartDate]
      ,[ProjectEndDate]
      ,[ProjectCreateDate]
      ,[ProjectCreatedBy]
      ,[ProjectModifiedDate]
      ,[ProjectModifiedBy]
	  ,[UserID]
      ,[UserName]
      ,[UserFirstName]
      ,[UserLastName]
      ,[UserEmployeeType]
      ,[UserPhone]
      ,[UserCompanyName]
      ,[UserCompanyPhone]
      ,[UserAppRoleType]
      ,[UserComments]
      ,[UserKey]
      ,[UserActiveFlag]
      ,[UserCreateDTLTOffset]
      ,[UserModifiedDTLTOffset]
      ,[UserInactiveDTLTOffset]
      ,[UserCreatedDate]
      ,[UserCreatedBy]
      ,[UserModifiedDate]
      ,[UserModifiedBy]
	   FROM AllTimeCardsByProjectByUser_vw
WHERE [UserID] = @PARAMETER1
ORDER BY TimeCardID
END TRY
BEGIN CATCH
  THROW;
END CATCH



GO


