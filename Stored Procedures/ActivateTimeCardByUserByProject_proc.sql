USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[ActivateTimeCardByUserByProject_proc]    Script Date: 4/23/2019 1:17:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






/****************************************************************************************************
***************************************************************************************************
		Procedure Name:		ActivateTimeCardByUserByProject 					 				 
		Created By:			Sandra Jackson, Senior DBA											 
		Create Date:		05/03/2016	
		Modified By :		Alekya Kondam
		Modified Date:		03/29/2019														 
		Create Reason:		Activate Time Cards by Use By TimeCard		         
		Prerequisite:		None																	 
		Utilized By:		CometTracker App, Supervisor, Admin										 
		Version:			1.0																		 
		Modified:			1.0 = creation 
		Modifications made : added Exceptional handling 					
		Status:				Active or Inactive					 
																									 
*****************************************************************************************************/
CREATE PROCEDURE [dbo].[ActivateTimeCardByUserByProject_proc]  @UserParam int, @ProjectParam int
AS
BEGIN TRY
UPDATE TC 
SET tc.TimeCardActiveFlag = 1
, tc.TimeCardApprovedFlag = ISNULL(TimeCardStatus.TimeCardApprovedFlag, 0)
, tc.TimeCardPMApprovedFlag = ISNULL(TimeCardStatus.TimeCardPMApprovedFlag, 0)
From [TimeCardTb] TC
Left join
(
select distinct TimeCardStartDate, TimeCardApprovedFlag, TimecardPMapprovedFlag
			from TimeCardTb 
			where TimeCardProjectID = @ProjectParam
				and (TimeCardApprovedFlag = 1 or TimecardPMapprovedFlag = 1)
) TimeCardStatus on TC.TimeCardStartDate = TimeCardStatus.TimeCardStartDate
WHERE TC.TimeCardTechID = @UserParam
and tc.TimeCardProjectID = @ProjectParam

END TRY
BEGIN CATCH
  THROW;
END CATCH






GO


