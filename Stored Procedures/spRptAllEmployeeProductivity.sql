USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptAllEmployeeProductivity]    Script Date: 4/23/2019 1:45:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spRptAllEmployeeProductivity]
(
       @StartDate                 DATETIME      = NULL
       ,@EndDate                  DATETIME      = NULL
       --,@EmployeeID          int
       ,@EmployeeName varchar(100) = NULL
 
) AS
 
/*****************************************************************************************************************
NAME:        [spRptAllEmployeeProductivity]
SERVER:     SC1-DEV01
DATABASE:   ScctTemplate
    
HISTORY
DATE            DEVELOPER         COMMENTS
-------------   ---------------   -------------------------------------------------------------------
2017-07-03          Fshaik         Initial Build with Parameters
 
Test Data
***********************************************************************************************************
        EXECUTE  dbo.spRptAllEmployeeProductivity '9/7/2017', '9/7/2017' , 'tech, d (dtech)'
       -- EXECUTE  dbo.spRptAllEmployeeProductivity '8/28/2017', '9/5/2017' , '77'
       --EXECUTE  dbo.spRptAllEmployeeProductivity '1/1/2018', '1/10/2018' , 'mtech'
       --EXECUTE  dbo.spRptAllEmployeeProductivity '1/1/2017', '8/12/2017' , 'tech, d (dtech)'
       --EXECUTE  dbo.spRptAllEmployeeProductivity '8/28/2017', '9/22/2017' , '< All >'
 
         EXECUTE  dbo.spRptAllEmployeeProductivity '1/1/2018', '1/15/2018' , '< All >'
       --EXECUTE  dbo.spRptAllEmployeeProductivity '1/1/2017', '8/12/2017' , NULL
       --EXECUTE  dbo.spRptAllEmployeeProductivity_InProgress '9/11/2017', '9/15/2017' , 'Lute, Ciara (clute)'
  
The following are not considered for Total Time calculations.
InspectionActivity -- Do not consider
WebLogoutActivity -- Do not consider
AutoSyncActivity -- Do not consider
 
 
The following are considered for total time. NP means Non productive time.
HomeActivity  -- NP
LoginActivity  -- NP
WorkQueueActivity -- NP
CalibrationActivity -- NP
 
TaskOutActivity  -- PRODUCTIVE
SurveyActivity  -- PRODUCTIVE
     
 select distinct ActivityTitle from activitytb
******************************************************************************************************************/

--declare @StartDate as Date 
--declare @EndDate as Date
--declare @EmployeeName as varchar(100)

--set  @StartDate = '1/1/2018'
--set  @EndDate = '1/10/2018'
--set  @EmployeeName = '< All >'

--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
SET NOCOUNT ON -- Must have when calling from MS Excel to only limit one result set
--SET NOCOUNT OFF -- Must have when calling from MS Excel to only limit one result set
 
--added newline below 
DECLARE @NewLineChar AS VarCHAR(20) =  '<br>'   
--DECLARE @ConsideredActivityList AS VarCHAR(500) = "'SurveyActivity','TaskOutActivity','LoginActivity','HomeActivity'"
 
---- Set Overall Defaults for the Report ------------------------------------------------------------------------------
SELECT
@StartDate         = CASE
                                 WHEN @StartDate IS NULL AND @EndDate IS NOT NULL THEN COALESCE(@EndDate, getdate())
                                 ELSE COALESCE(@StartDate, GETDATE())
                             END
,@EndDate           = CASE 
                                 WHEN @EndDate IS NULL AND @StartDate IS NOT NULL THEN COALESCE(@StartDate, GETDATE())
                                 ELSE COALESCE(@EndDate, GETDATE())
                             END
 
---- Create The Report Data --------------------------------------------------
--- CHARINDEX('AutoSyncActivity', ActivityTitle) = 0
 BEGIN TRY
-- select * from tinspection where createdby = 25 and iscgeflag = 1 and cast(createddate as date) ='8/30/2017' 
IF @EmployeeName IS NULL
BEGIN
 
             SELECT  -- 20160111 Added
             '< All >' AS Drop_Down
             INTO #DD
             
             UNION ALL
 
             Select Distinct U.UserLastName + ', ' +  U.UserFirstName + ' (' + U.UserName + ')' from 
             (select * 
             from [dbo].[ActivityTb] 
             where 
			 --CHARINDEX('Web', ActivityTitle) = 0 
			 --and CHARINDEX('AutoSyncActivity', ActivityTitle) = 0 
			 --and  CHARINDEX('InspectionActivity', ActivityTitle) = 0 
			 --and  CHARINDEX('WebLogoutActivity', ActivityTitle) = 0 
			 ActivityTitle in ('SurveyActivity','TaskOutActivity','LoginActivity','HomeActivity','CalibrationActivity','WorkQueueActivity')
			    -- commented on 1/11/18     and ISNUMERIC(ActivityCreatedUserUID) = 1 
			 and Cast(ActivitySrcDTLT as date) between @StartDate and @EndDate) Activity
             Join (Select * from UserTb where UserAppRoleType like '%Technician%') u on u.Username= Activity.ActivityCreatedUserUID
             Order By 1
             
             SELECT Drop_Down FROM #DD -- DROP TABLE #DD
 
END
ELSE
BEGIN
 
       IF OBJECT_ID('tempdb..#EmployeeActivity', 'U') IS NOT NULL
             DROP TABLE #EmployeeActivity             
 
       Create Table #EmployeeActivity  -- Drop Table #EmployeeActivity
       (
      [Report Date] date NULL,
       --[Report Date Range] varchar(50) NULL,
       --Surveyor varchar(100) NULL,
       --new line
             [EmployeeName]  varchar(100) NULL,
             
       [Inspector]  varchar(100) NULL,

              -- commented on 1/11/18      EmployeeID  int,
         EmployeeID  varchar(100) NULL,
             
       [Survey Date] date null,
       [First Activity] varchar(100) NULL,
       [Last Activity] varchar(100) NULL,
       [Activity Start and End Time] varchar(50) NULL,
       [Total Time] varchar(10) NULL,
       [Productive Time] varchar(10) NULL,
       [Non-Productive Time] varchar(10) NULL,
       [# Inspection] int NULL,
       [# Leak] int NULL,
       [# CGE]int NULL,
       [Feet Of Main]int NULL,
          --added 12/7
       [Feet Of Transmission] int Null,
       [Feet Of High Pressure] int Null 
      
       )
 
             
             Insert Into #EmployeeActivity
             
             Select
         Cast(getdate() as date) [Date]
       --     ,Cast(Cast(@StartDate as date) as varchar(20)) + ' - ' + Cast(Cast(@EndDate as date) as varchar(20))
             --, u.UserFirstName + ' ' + u.UserLastName [Surveyor]
             --newline added 
             ,U.UserName as EmployeeName
             ,U.UserLastName + ', ' +  U.UserFirstName + ' (' + U.UserName + ')' as Inspector
             -- ,u.UserName  [Inspector]  
			     -- commented on 1/11/18    , u.UserID  [EmployeeID]  
               ,u.UserName  [EmployeeID]  
 
             --, Activity.ActivityCreatedUserUID
             , Activity.ActivityDate [SurveyDate]
             , FA.ActivityTitle + @NewLineChar + FA.FATime /*ActivityTime.StartTime*/ [FirstActivity]
             , LA.ActivityTitle + @NewLineChar + LA.LATime /* ActivityTime.EndTime */  [LastActivity]
             , ActivityTime.StartTime + ' - ' + ActivityTime.EndTime [SurveyStartEnd]
             ,Convert(char(8),DateAdd(second, ISNULL(ActivityTime.[Total Time In Seconds], 0), 0), 108) [Total Time]
             ,Convert(char(8),DateAdd(second, ISNULL(ActivityTime.[Total Productive Time In Seconds], 0), 0), 108) [Productive Time]
             ,Convert(char(8),DateAdd(second, ISNULL(ActivityTime.[Total Non-Productive Time In Seconds], 0), 0), 108) [Non-Productive Time]
             , IsNull(Inspections.InspectionCount, 0) [# Inspection]
             , IsNull([Events].IndicationCount, 0) [# Leak]
             , IsNull([Events].CGECount, 0) [# CGE]
             , ISNULL(tto.TotalFeet, 0) [Feet Of Main]
                          --added 12/7
             , ISNULL(tto.FT, 0) [Feet Of Transmission] 
             , ISNULL(tto.FP, 0) [Feet Of High Pressure]
             From
                    (select ActivityCreatedUserUID, Cast(ActivitySrcDTLT as date) [ActivityDate], Min(ActivityID) [FirstActivity], Max(ActivityID) [LastActivity] 
                    ,Convert(varchar(10), Min(ActivityStartTime), 108) [StartTime], Convert(varchar(10), Max(ActivityEndTime), 108) [EndTime]
                    from [dbo].[ActivityTb] where 
                    CHARINDEX('Web', ActivityTitle) = 0 and CHARINDEX('AutoSyncActivity', ActivityTitle) = 0 and CHARINDEX('WebLogoutActivity', ActivityTitle) = 0 
                    and ActivityTitle in ('SurveyActivity','TaskOutActivity','LoginActivity','HomeActivity','CalibrationActivity','WorkQueueActivity')
                    --and ActivityTitle in (@ConsideredActivityList)
                       -- commented on 1/11/18     and ISNUMERIC(ActivityCreatedUserUID) = 1 
					and Cast(ActivitySrcDTLT as date) between @StartDate and @EndDate
                    Group By ActivityCreatedUserUID, Cast(ActivitySrcDTLT as date)) Activity
       
                    Join (select Distinct ActivityCreatedUserUID, ActivityID, ActivityTitle, Convert(varchar(10), ActivityStartTime, 108) FATime from [dbo].[ActivityTb]) FA on FA.ActivityCreatedUserUID = Activity.ActivityCreatedUserUID and FA.FATime = Activity.StartTime
       
                    Join (select Distinct ActivityCreatedUserUID, ActivityID, ActivityTitle, Convert(varchar(10), ActivityEndTime, 108) LATime  from [dbo].[ActivityTb]) LA on LA.ActivityCreatedUserUID = Activity.ActivityCreatedUserUID and LA.LATime = Activity.EndTime
                     -- - changed below U.userid to u.username  on 1/11/18   
                    Left Join UserTb u on u.UserName = Activity.ActivityCreatedUserUID
 
 
                    Left Join (select ActivityCreatedUserUID, 
                           SurveyDate ,
                           Convert(varchar(10), Min(ActivityStartTime), 108) [StartTime],
                           Convert(varchar(10), Max(ActivityEndTime), 108) [EndTime],
                           sum(CASE WHEN ActivityTitle in ('SurveyActivity','TaskOutActivity','LoginActivity','HomeActivity','CalibrationActivity','WorkQueueActivity') THEN ISNULL(DateDiff(ss, ActivityStartTime, ActivityEndTime), 0) ELSE 0 END)  [Total Time In Seconds],
                           Sum(CASE WHEN ActivityTitle in ('SurveyActivity','TaskOutActivity') THEN ISNULL(DateDiff(ss, ActivityStartTime, ActivityEndTime), 0) ELSE 0 END) [Total Productive Time In Seconds],
                           Sum(CASE WHEN ActivityTitle in ('LoginActivity','HomeActivity','WorkQueueActivity','CalibrationActivity') THEN ISNULL(DateDiff(ss, ActivityStartTime, ActivityEndTime), 0) ELSE 0 END) [Total Non-Productive Time In Seconds]
                    From                       
                           (select 
                                 ActivityCreatedUserUID, 
                                 Cast(ActivitySrcDTLT as Date) SurveyDate,
                                 ActivityTitle,
                                 ActivityStartTime,
                                 Max(ActivityEndTime) [ActivityEndTime]
                           from [dbo].[ActivityTb] 
                           where 
						   -- commented on 1/11/18      ISNUMERIC(ActivityCreatedUserUID) = 1 and 
                            CHARINDEX('Web', ActivityTitle) = 0 and CHARINDEX('AutoSyncActivity', ActivityTitle) = 0  and CHARINDEX('WebLogoutActivity', ActivityTitle) = 0 
                           and ActivityTitle in ('SurveyActivity','TaskOutActivity','LoginActivity','HomeActivity','CalibrationActivity','WorkQueueActivity')
                           Group By  ActivityCreatedUserUID, 
                                 Cast(ActivitySrcDTLT as Date),
                                 ActivityTitle,
                                 ActivityStartTime) Source
                    Group By 
                    ActivityCreatedUserUID, 
                    SurveyDate) ActivityTime on Activity.ActivityCreatedUserUID = ActivityTime.ActivityCreatedUserUID and Activity.ActivityDate = ActivityTime.SurveyDate
 
 
                    Left Join (select U1.UserName, Cast(SrvDTLT as Date) SurveyDate, count(*) [InspectionCount]
                           from [dbo].[tInspection]
						   Join dbo.UserTb U1 on U1.UserID = CreatedBy -- Added this to get the corresponding UserName
                           Group By UserName, Cast(SrvDTLT as Date)) Inspections on  Activity.ActivityCreatedUserUID = Inspections.UserName and  Activity.ActivityDate = Inspections.SurveyDate
 
 
                    Left Join (Select
                           --CreatedBy,
						   UserName, -- Replaced CreatedBy with UserName
                           Cast(CreatedDate as Date) [SurveyDate],
                           ISNULL(Sum(CGICount), 0) [CGECount], 
                           ISNULL(Sum(IndicationCount), 0) [IndicationCount]
                           From
                           (select
                           Case WHEN sl.StatusDescription = 'CGE' THEN 1 ELSE 0 END [CGICount]
                           ,Case WHEN sl.StatusDescription ='Indication' THEN 1 ELSE 0 END [IndicationCount]
                           ,i.CreatedBy
						   ,U1.UserName -- Added to get UserName
                           ,i.CreatedDate
                           from [dbo].[tEvent] e
                           Join (select * from [dbo].[tInspection])  i on e.InspectionID = i.ID
						   Join dbo.UserTb U1 on U1.UserID = i.CreatedBy -- Added this to get the corresponding UserName
                           Left Join (Select * from [dbo].[rStatusLookup] where StatusType = 'Event') sl on e.EventType = sl.StatusCode) Source
                           Group By UserName,
                           Cast(CreatedDate as Date)) [Events] on [Events].UserName = Activity.ActivityCreatedUserUID and [Events].SurveyDate = Activity.ActivityDate
 
                    Left Join (Select U1.UserName, Cast(SrcDTLT as Date) TaskoutDate, Sum(FeetOfMain) TotalFeet 
                                                        --- added 12/7
                                       , sum(FeetOfTransmission) FT
                                       , sum(FeetOfHighPressure) FP
 
                                        from tTaskOut 
										Join dbo.UserTb U1 on U1.UserID = CreatedUserID -- Added this to get the corresponding UserName

                                        Group By UserName, Cast(SrcDTLT as Date)) tto on  tto.UserName = Activity.ActivityCreatedUserUID and  tto.TaskoutDate = Activity.ActivityDate
 
             
             order by Activity.ActivityCreatedUserUID
             , Activity.ActivityDate
 
 
 
       DECLARE @CountTest INT = (SELECT COUNT(*) FROM #EmployeeActivity where [Inspector] = CASE WHEN @EmployeeName = '< All >' THEN [Inspector] ELSE @EmployeeName END)
 
 
       IF @CountTest > 10000
             BEGIN
 
                    --Drop Table #LandMarkFlatFile
                    SELECT 
                     CAST(GETDATE() AS DATE) AS [Report Date]
                    -- , Cast(Cast(@StartDate as date) as varchar(20)) + ' - ' + Cast(Cast(@EndDate as date) as varchar(20)) [Report Date Range]
                    ,'Inspector Activity' AS [Report Title]
                    ,'Greater than 10,000 records returned. Please limit your date filters and try again.' AS [Report Alert]
             END
       ELSE 
             BEGIN
                    IF @CountTest = 0
                    BEGIN
 
                           --Drop Table #LandMarkFlatFile
                           SELECT 
                            CAST(GETDATE() AS DATE) AS [Report Date]
                    --     , Cast(Cast(@StartDate as date) as varchar(20)) + ' - ' + Cast(Cast(@EndDate as date) as varchar(20)) [Report Date Range]
                           ,'Inspector Activity' AS [Report Title]
                           ,'No data returned for the date(s) provided' AS [Report Alert]  
                           --,CONCAT('No Data Returned for the date(s) provided: ',FORMAT(@OverallStartDate, 'M/dd/yyyy'), ' and ', FORMAT(@OverallEndDate, 'M/dd/yyyy'))
                    END
                    ELSE 
                    BEGIN
       
                           select distinct 
                                 [Report Date] ,
                    --           [Report Date Range] ,
                                 [Inspector]  ,
                                 [Survey Date] ,
                                 --[First Activity] ,
                                 --[Last Activity] ,
                                 --[Activity Start and End Time],
                                 [Total Time] ,
                                 [Productive Time] ,
                                 [Non-Productive Time] ,
                                 [# Inspection] ,
                                 [# Leak] ,
                                 [# CGE],
                                 [Feet Of Main],
                     -- added 12/7
                                 [Feet Of Transmission],
                                 [Feet Of High Pressure]
                           from #EmployeeActivity
                           where [Inspector] = CASE WHEN @EmployeeName = '< All >' THEN [Inspector] ELSE @EmployeeName END
                           order by [Inspector], [Survey Date]
             
                           
                           Drop Table #EmployeeActivity
 
                    END
             END
       END
	   END TRY
BEGIN CATCH
  THROW;
END CATCH

SET NOCOUNT OFF
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 








GO


