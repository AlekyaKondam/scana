USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spSendEmail]    Script Date: 4/24/2019 9:11:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spSendEmail]
(
	@SendTO varchar(2000)
	,@SendCC varchar(2000)
	,@Subject varchar(200)
	,@MessageBody varchar(max)
)
AS
BEGIN
BEGIN TRY
Set NOCOUNT ON


	Declare 
			@ProfileName varchar(200) = 'SC_Automated_Reporting'
			,@EmailFrom varchar(200) = 'SC_Automated_Reporting@southerncrossinc.com'


		EXEC msdb.dbo.sp_send_dbmail
		@profile_name = @ProfileName,
		@from_Address = @EmailFrom,
		@recipients = @SendTo,
		@copy_recipients = @SendCC,
		@body = @MessageBody,
		@subject = @Subject ,
		@exclude_query_output = 1




Set NOCOUNT OFF
END TRY
BEGIN CATCH
  THROW;
END CATCH

END



GO


