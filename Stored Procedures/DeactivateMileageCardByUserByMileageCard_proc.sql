USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[DeactivateMileageCardByUserByMileageCard_proc]    Script Date: 4/23/2019 1:18:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





/****************************************************************************************************
***************************************************************************************************
		Procedure Name:		DeactivateMileageCardByUserByMileageCard					 				 
		Created By:			Sandra Jackson, Senior DBA											 
		Create Date:		05/03/2016	
		Modified By :		Alekya Kondam
        Modified Date:		03/29/2019	
        Modifications made : added Exceptional handling 
	    Create Reason:		Deactivate Mileage Cards by Use By MileageCard		         
		Prerequisite:		None																	 
		Utilized By:		CometTracker App, Supervisor, Admin										 
		Version:			1.0																		 
		Modified:			1.0 = creation 
							
		Status:				Active or Inactive					 
																									 
*****************************************************************************************************/

CREATE PROCEDURE [dbo].[DeactivateMileageCardByUserByMileageCard_proc]  @UserParam int, @MileageCardParam int
AS
BEGIN TRY
UPDATE [CometTracker].[dbo].[MileageCardTb]
SET [MileageCardActiveFlag] = 0
WHERE [MileageCardTechID] = @UserParam
and [MileageCardID] = @MileageCardParam

END TRY
BEGIN CATCH
  THROW;
END CATCH




GO


