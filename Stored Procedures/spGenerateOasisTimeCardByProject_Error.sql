USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateOasisTimeCardByProject_Error]    Script Date: 4/23/2019 1:28:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE  Procedure [dbo].[spGenerateOasisTimeCardByProject_Error](@ProjectIDJSON varchar(max), @WeekStartingDate date, @WeekEndingDate date)


AS


SET ANSI_WARNINGS OFF

Create Table #TimeCard
(
	[Payee] varchar(50) NULL
	,[Charge Account] varchar(50) NULL    
	,[Amount] varchar(50) NULL
	,[Transaction Date] Date NULL
)

/*

	Testing
	
	Exec spGenerateOasisTimeCardByProject_Error '["1111"]', '2018-03-11', '2018-03-17'


	select  timecardsubmittedoasis, TimeCardSubmttedQuickBooks, * 
	from timecardtb 
	where timecardprojectid = '1111' and timecardstartdate = '2018-03-11' and timecardenddate = '2018-03-17'

	update timecardtb 
	Set timecardsubmittedoasis = NULL, TimeCardSubmttedQuickBooks = NULL
	where timecardprojectid = '1111' and timecardstartdate = '2018-03-11' and timecardenddate = '2018-03-17'


*/

BEGIN


	--Before we start any thing need to save what was passed to the SP

	Insert into [dbo].[tSPInsertedData]
	(SPName, InsertedData )
	Values
	('spGenerateOasisTimeCardByProject', ISNULL(@ProjectIDJSON, 'NULL') + ', ' + ISNULL(Cast(@WeekStartingDate as varchar(20)), 'NULL') + ', ' + ISNULL(cast(@WeekEndingDate as varchar(20)), 'NULL'))


 BEGIN TRAN CREATEOASISFILE
	
  BEGIN TRY



	Declare @40HoursTotalSec int = 60 * 60 * 40 -- 60 Seconds * 60 Minutes * 40 Hours = 144,000 Seconds
	,@RegularTimeChargeAccount int = '5105'
	,@OverTimeChargeAccount int = '5110'
	,@ChartOfAccount varchar(50)
	,@TotalSec int = 0
	,@RegularTimeTotalSec int = 0
	,@OverTimeTotalSec int = 0
	,@TimeCardAppoved varchar(10)
	--,@userOasisID int
	,@userOasisID varchar(50)
	,@TransDate date
	--,@WeekStartingDate date = DateAdd(wk, -1, DateAdd(d,- datepart(dw, getdate()) + 1, getdate()))
	,@SelectTimeCardID int
	,@ProjectID int
	,@ProjectName varchar(200)

	
	IF @ProjectIDJSON IS NOT NULL and @WeekStartingDate is not null and @WeekEndingDate is not null
	BEGIN
		
		
		select @WeekStartingDate = dateadd(d, -datepart(dw, @WeekStartingDate)+ 1, @WeekStartingDate)

		select @WeekEndingDate = dateadd(d, 7-datepart(dw, @WeekEndingDate), @WeekEndingDate)
		
		select * Into #ProjectID from [dbo].[parseJSON](@ProjectIDJSON)

		Declare curProjectNames Cursor
		For
		select StringValue from #ProjectID Where Name is null

		Open curProjectNames

		Fetch Next from curProjectNames into @ProjectID

		WHILE @@FETCH_STATUS = 0
		BEGIN 
		
			--To make this simple I get the Project ID
			--select @ProjectID = ProjectID from ProjectTb where ProjectName = @ProjectName
		
			IF (select Count(*) from TimeCardTb tc
			Join (select * from UserTb where UserPayMethod = 'H') u on u.UserID = tc.TimeCardTechID
		--changed below by FS as part of TimecardFlag change to 0 or 1  
		where tc.TimeCardApprovedFlag = 0 		
		--where tc.TimeCardApprovedFlag = 'NO' 
				and Cast(tc.TimeCardStartDate as date) >= @WeekStartingDate 
				and Cast(tc.TimeCardEndDate as date) <= @WeekEndingDate 
				and tc.TimeCardProjectID = @ProjectID 
				----changed below by FS as part of TimecardFlag change to 0 or 1 
				and tc.TimeCardActiveFlag = 1
				--and tc.TimeCardActiveFlag = 'Active'
				--and tc.TimeCardSubmittedOasis is null
				) = 0
			BEGIN 

				--Update the Project Table with Oasis File Creation Date

				/*
				Update TimeCardTb 
				set TimeCardSubmittedOasis = getdate() 
				, TimeCardModifiedDate = getdate()
				where Cast(TimeCardStartDate as date) >= @WeekStartingDate 
					and Cast(TimeCardEndDate as date) <= @WeekEndingDate 
					and TimeCardProjectID = @ProjectID 
					and TimeCardSubmittedOasis is null
					and TimeCardActiveFlag = 'Active'

				*/

				--EXEC spMarkTCOasisFileCreated @ProjectID, @WeekStartingDate ,@WeekEndingDate 

				Declare curTimeCardID Cursor
				For
				Select TimeCardID from TimeCardTb tc
				Join (select * from UserTb where UserPayMethod = 'H') u on u.UserID = tc.TimeCardTechID
			----changed below by FS as part of TimecardFlag change to 0 or 1  
			where tc.TimeCardApprovedFlag = 1 
			--	where tc.TimeCardApprovedFlag = 'YES' 
					and tc.TimeCardSubmittedOasis is null 
					--and TimeCardSubmttedQuickBooks is null 
					and Cast(tc.TimeCardStartDate as date) >= @WeekStartingDate
					and Cast(tc.TimeCardEndDate as date) <= @WeekEndingDate
					and tc.TimeCardProjectID = @ProjectID
				----changed below by FS as part of TimecardFlag change to 0 or 1 
					and tc.TimeCardActiveFlag = 1
				--	and tc.TimeCardActiveFlag = 'Active'
			
				Open curTimeCardID

				Fetch Next From curTimeCardID into @SelectTimeCardID
			
				While @@FETCH_STATUS = 0
				BEGIN

					Declare curTimeCard Cursor Static
					For
					select
						u.UserOasisID
						,CASE WHEN te.TimeEntryTimeCardID is null THEN '0'
								ELSE te.TotalSec END
						, Cast(tc.TimeCardEndDate as date)
						, te.ChartOfAccount
					from [dbo].[TimeCardTb]   tc
					Join UserTb u on u.UserID = tc.TimeCardTechID
					Left Join ProjectTb p on p.ProjectID = tc.TimeCardProjectID
					Join (SELECT t.TimeEntryTimeCardID, ISNULL(coa.ChartOfAccountID, 0) [ChartOfAccount], sum(DATEDIFF(s, t.TimeEntryStartTime, t.TimeEntryEndTime)) TotalSec
						FROM (Select * from [dbo].[TimeEntryTb] where TimeEntryActiveFlag = 1)   t
						Join (select * from ActivityTb where  (ActivityTitle like '%Task%' or ActivityTitle in ('AdminActivity')) and ActivityTitle  not like '%TaskOut%') a on t.TimeEntryActivityID = a.ActivityID
						Left Join [dbo].[refChartOfAccount] coa on coa.ChartOfAccountDescription = t.TimeEntryChartOfAccount
						Group By TimeEntryTimeCardID, ISNULL(coa.ChartOfAccountID, 0)) te on tc.TimeCardID = te.TimeEntryTimeCardID
					Where tc.TimeCardID =  @SelectTimeCardID

					Open curTimeCard
			
					Fetch Next From curTimeCard Into
					@userOasisID, @TotalSec, @TransDate, @ChartOfAccount

					While @@FETCH_STATUS = 0
					BEGIN

						If @ChartOfAccount = 0
						BEGIN

							Set @RegularTimeTotalSec = Case WHEN @TotalSec > @40HoursTotalSec THEN @40HoursTotalSec ELSE @TotalSec END

							Set @OverTimeTotalSec = Case WHEN @TotalSec > @40HoursTotalSec THEN @TotalSec - @40HoursTotalSec ELSE 0 END

							Insert Into #TimeCard
							(
								[Payee], [Charge Account], [Amount], [Transaction Date] 
							)
							Values
							(
								@userOasisID,
								@RegularTimeChargeAccount,
								CASE WHEN @RegularTimeTotalSec = 0 THEN '0'
								ELSE
								  Right('0' + Cast(@RegularTimeTotalSec / 3600 as varchar),2) + ':' +
								  Right('0' + Cast((@RegularTimeTotalSec / 60) % 60 as varchar),2) + ':' +
								  Right('0' + Cast(@RegularTimeTotalSec % 60 as varchar),2) -- + ' ' + Cast(@SelectTimeCardID as varchar(10))
								END,
								@TransDate
							)


							IF @OverTimeTotalSec > 0
							BEGIN

								Insert Into #TimeCard
								(
									[Payee], [Charge Account], [Amount], [Transaction Date] 
								)
								Values
								(
									@userOasisID,
									@OverTimeChargeAccount,
									Right('0' + Cast(@OverTimeTotalSec / 3600 as varchar),2) + ':' +
									  Right('0' + Cast((@OverTimeTotalSec / 60) % 60 as varchar),2) + ':' +
									  Right('0' + Cast(@OverTimeTotalSec % 60 as varchar),2),
									@TransDate
								)

							END

						END
						ELSE
						BEGIN

							Insert Into #TimeCard
							(
								[Payee], [Charge Account], [Amount], [Transaction Date] 
							)
							Values
							(
								@userOasisID,
								@ChartOfAccount,
								CASE WHEN @RegularTimeTotalSec = 0 THEN '0'
								ELSE
								  Right('0' + Cast(@RegularTimeTotalSec / 3600 as varchar),2) + ':' +
								  Right('0' + Cast((@RegularTimeTotalSec / 60) % 60 as varchar),2) + ':' +
								  Right('0' + Cast(@RegularTimeTotalSec % 60 as varchar),2) -- + ' ' + Cast(@SelectTimeCardID as varchar(10))
								END,
								@TransDate
							)

						END

						Fetch Next From curTimeCard Into
						@userOasisID, @TotalSec, @TransDate, @ChartOfAccount
					
					
					END

					
					Close curTimeCard
					Deallocate curTimeCard

					Fetch Next From curTimeCardID into @SelectTimeCardID

				END

				Close curTimeCardID
				Deallocate curTimeCardID

				Update tc
				set TimeCardSubmittedOasis = getdate() 
				, TimeCardModifiedDate = getdate()
				From TimeCardTb tc
				Join (select * from UserTb where UserPayMethod = 'H') u on u.UserID = tc.TimeCardTechID
				where Cast(TimeCardStartDate as date) >= @WeekStartingDate 
					and Cast(TimeCardEndDate as date) <= @WeekEndingDate 
					and TimeCardProjectID = @ProjectID 
					and TimeCardSubmittedOasis is null
					----changed below by FS as part of TimecardFlag change to 0 or 1 
					 	and TimeCardActiveFlag = 1
					---and TimeCardActiveFlag = 'Active'
				
			END

			Fetch Next from curProjectNames into @ProjectID

		END

		Close curProjectNames
		Deallocate curProjectNames

		Drop Table #ProjectID
					
	END

	COMMIT TRAN CREATEOASISFILE

	select * from #TimeCard
	
  END TRY
  BEGIN CATCH

	ROLLBACK TRAN CREATEOASISFILE
	
	--Select 'ERROR'

	--Select 1/0

	select * from [dbo].[NonExist]

  END CATCH
  
--    select * from #TimeCard

	Drop Table #TimeCard

	SET ANSI_WARNINGS ON

END


































GO


