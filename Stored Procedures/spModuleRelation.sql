USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spModuleRelation]    Script Date: 4/23/2019 1:30:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spModuleRelation] (@ProjectID int)
AS

/*
Testing
    select * from [CometTracker].[menus].[ProjectModuleTb]  where ProjectModulesProjectID = 1085 
	select * from [CometTracker].[menus].[ProjectModuleTb]  where ProjectModulesProjectID = 1110
	select * from [CometTracker].[menus].[ProjectModuleTb]  where ProjectModulesProjectID = 1167
	Exec spModuleRelation 1085
	
	Exec spModuleRelation 1167
	Exec spModuleRelation 1110
*/

BEGIN TRY

Set NOCOUNT ON

 -- Get the project ID from input parameter
 -- get the corresponding projectType from projectTb table
 	Declare @ProjectType varchar(100)

    select @ProjectType=projectType  from projectTb where projectID = @ProjectID

 -- remove all records that belong to @ProjectID from [CometTracker].[menus].[ProjectModuleTb]
    Delete from [CometTracker].[menus].[ProjectModuleTb] where projectModulesProjectID = @ProjectID

 -- Apply the following logic to find the modules to be inserted in [CometTracker].[menus].[ProjectModuleTb]
 -- if ProjectType = 'AC, LS', then insert CometTracker, Dispatch modules

	Insert Into [menus].[ProjectModuleTb]
	(
	ProjectModulesProjectID , ProjectModulesName , ProjectModulesCreateDate , ProjectModulesCreatedBy , ProjectModulesModifiedDate , ProjectModulesModifiedBy
	)
	VALUES
	(
	 @ProjectID , 
	    Case when @ProjectType in ( 'Development' , 'INTERCOMPANY', 'MANUFACTURING', 'MANUFACTURING:Products Sales' , 'MANUFACTURING:Repair' ,
                              'OVERHEAD' , 'TECHNOLOGY:Consulting' , 'UnKnown' ,  'Work Force Management') 
							  THEN 'Home'
							  ELSE  'CometTracker'
							  END
		, getDate(), 'fshaik', NULL, NULL
	)

	Insert Into [menus].[ProjectModuleTb]
	(
	ProjectModulesProjectID , ProjectModulesName , ProjectModulesCreateDate , ProjectModulesCreatedBy , ProjectModulesModifiedDate , ProjectModulesModifiedBy
	)
	VALUES
	(
	 @ProjectID , 
	    Case when @ProjectType in ( 'Development' , 'INTERCOMPANY', 'MANUFACTURING', 'MANUFACTURING:Products Sales' , 'MANUFACTURING:Repair' ,
                              'OVERHEAD' , 'TECHNOLOGY:Consulting' , 'UnKnown' ,  'Work Force Management') 
							  THEN 'CometTracker'
							  ELSE  'Dispatch'
							  END
		, getDate(), 'fshaik', NULL, NULL
	)

Set NOCOUNT OFF

END TRY
BEGIN CATCH
  THROW;
END CATCH








GO


