USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spProjectSetup]    Script Date: 4/23/2019 1:30:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[spProjectSetup] (@ProjectID int)

AS

/*
Testing

	Exec [spProjectSetup] 1583

*/

BEGIN TRY

Set NOCOUNT ON
	Exec spModuleRelation @projectID
	Exec spSetLandingPage @projectID
	
Set NOCOUNT OFF
END TRY
BEGIN CATCH
  THROW;
END CATCH


GO


