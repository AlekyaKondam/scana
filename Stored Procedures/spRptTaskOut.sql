USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptTaskOut]    Script Date: 4/24/2019 9:10:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spRptTaskOut]
(
	@StartDate			DATE	= NULL
	,@EndDate			DATE	= NULL

) AS

/*****************************************************************************************************************
NAME:		[spRptInspectorActivity]
SERVER:     SC1-DEV01
DATABASE:   ScctTemplate
    
HISTORY
DATE            DEVELOPER         COMMENTS
-------------   ---------------   -------------------------------------------------------------------
2017-06-13		GWheeler		  Initial Build with Parameters

Test Data
***********************************************************************************************************

	-- EXECUTE  dbo.spRptTaskOut '7/1/2017', '7/19/2017'

******************************************************************************************************************/

--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
SET NOCOUNT ON -- Must have when calling from MS Excel to only limit one result set
--SET NOCOUNT OFF -- Must have when calling from MS Excel to only limit one result set


---- Set Overall Defaults for the Report ------------------------------------------------------------------------------
BEGIN TRY
IF @StartDate is null and @EndDate is NULL

Select @StartDate = min(StartDTLT), @EndDate = max(StartDTLT) from tTaskOut


/*

SELECT
 @StartDate		= CASE
					WHEN @StartDate IS NULL AND @EndDate IS NOT NULL THEN COALESCE(@EndDate, getdate())
					ELSE COALESCE(@StartDate, GETDATE())
				  END
,@EndDate		= CASE 
					WHEN @EndDate IS NULL AND @StartDate IS NOT NULL THEN COALESCE(@StartDate, GETDATE())
					ELSE COALESCE(@EndDate, GETDATE())
				  END

*/

---- Create The Report Data --------------------------------------------------



IF OBJECT_ID('tempdb..#InspectorActivity', 'U') IS NOT NULL
    DROP TABLE #InspectorActivity		

	--	Create Table #TaskOut  -- Drop Table #InspectorActivity
	--	(
	--	[Report Date] date NULL,
	--	[Survey Date Range] varchar(50) NULL,
	--	--[End Date] date NULL,
	----	[Surveyor] varchar(100) NULL,
	--    [Inspector] varchar(100) NULL,
	--	[Task Out Date] datetime  null,
	--	[Map/Grid] varchar(100) NULL,
	--	[Services] INT NULL,
	--	[Feet Of Main] INT NULL,
	--	[Comments] varchar(500) NULL,
		
	--	)

		
	--	Insert Into #TaskOut
		
		Select
		Cast(getdate() as date) [Report Date],
		Cast(Cast(@StartDate as date) as varchar(20)) + ' - ' + Cast(Cast(@EndDate as date) as varchar(20)) [Survey Date Range],
		-- u.UserFirstName + ' ' + u.UserLastName [Surveyor], 
		U.UserLastName + ', ' +  U.UserFirstName + ' (' + U.UserName + ')' as Inspector,
	--	cast(StartDTLT as datetime) [Task Out Date], 
	     format(StartDTLT,'yyyy-MM-dd hh:mm:ss')  [Task Out Date],
		MapGrid, 
		ServicesCount, 
		FeetOfMain
		--, Comments
		, [dbo].[fnRemoveChar](Comments) [Comments]
		into #TaskOut
		FROM [dbo].[tTaskOut] t
		Join UserTb u on t.CreatedUserID = u.UserID
		Where cast(StartDTLT as date) between Cast(@StartDate as date) and Cast(@EndDate as date)



		DECLARE @CountTest INT = (
								SELECT COUNT(*) 
								FROM #TaskOut
							)


	IF @CountTest > 10000
		BEGIN

			--Drop Table #LandMarkFlatFile
			SELECT 
			 CAST(GETDATE() AS DATE) AS [Report Date]
			 , Cast(Cast(@StartDate as date) as varchar(20)) + ' - ' + Cast(Cast(@EndDate as date) as varchar(20)) [Survey Date Range]
			,'Inspector Activity' AS [Report Title]
			,'Greater than 10,000 records returned. Please limit your date filters and try again.' AS [Report Alert]
		END
	ELSE 
		BEGIN
			IF @CountTest = 0
			BEGIN

				--Drop Table #LandMarkFlatFile
				SELECT 
				 CAST(GETDATE() AS DATE) AS [Report Date]
				 , Cast(Cast(@StartDate as date) as varchar(20)) + ' - ' + Cast(Cast(@EndDate as date) as varchar(20)) [Survey Date Range]
				,'Inspector Taskout' AS [Report Title]
				,'No data returned for the date(s) provided' AS [Report Alert]  
				--,CONCAT('No Data Returned for the date(s) provided: ',FORMAT(@OverallStartDate, 'M/dd/yyyy'), ' and ', FORMAT(@OverallEndDate, 'M/dd/yyyy'))
			END
			ELSE 
			BEGIN
				
				select 
				*
				from #TaskOut
				order by Inspector, [Task Out Date]

				Drop Table #TaskOut

			END
		END

	--END
END TRY
BEGIN CATCH
  THROW;
END CATCH
SET NOCOUNT OFF


























GO


