USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spSendAccountantEmail]    Script Date: 4/24/2019 9:11:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spSendAccountantEmail]
(
	@StartDate date
	,@EndDate date

)
AS
BEGIN TRY
Set NOCOUNT ON


If (Select * from [dbo].[fnSubmitAccountant](@StartDate, @EndDate)) = 1
BEGIN

	Declare @SendTO varchar(2000) = NULL
			,@SendCC varchar(2000) = ''
			,@MessageBody varchar(max) = 'Time Cards for the time period of  ' + Cast(@StartDate as varchar(20)) + ' - ' + Cast(@EndDate as varchar(20)) + ' are ready to be submitted.'
			,@Subject varchar(200) = 'Time Cards Ready to Submit'



	Select @SendTO = coalesce(@SendTO + '; ','') + u.UserPreferredEmail
	From usertb u
	Where UserAppRoleType = 'Accountant' and UserPreferredEmail is not null
	Order by u.UserLastName, u.UserFirstName

	if @SendTO is not null
	BEGIN

		EXEC spSendEmail @SendTO, @SendCC, @Subject, @MessageBody

	END

END


Set NOCOUNT OFF
END TRY
BEGIN CATCH
  THROW;
END CATCH






GO


