USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spSetLandingPage]    Script Date: 4/24/2019 9:12:18 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spSetLandingPage]
(
	@ProjectID int
)
AS
/*

Testing

	Select ProjectLandingPage, * from ProjectTb where ProjectID = 1112

	Update ProjectTB
	Set ProjectLandingPage = 'Wrong'
	where ProjectID = 1112

	Exec spSetLandingPage 1112




*/
BEGIN TRY
Set NOCOUNT ON

	Declare @LandingPage varchar(100)
		
	Update p
	Set ProjectLandingPage = [dbo].[fnGetLandingPage](p.ProjectType)
	From ProjectTb p
	Where p.ProjectID = @ProjectID
	

Set NOCOUNT OFF


END TRY
BEGIN CATCH
  THROW;
END CATCH







GO


