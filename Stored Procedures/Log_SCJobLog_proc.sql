USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[Log_SCJobLog_proc]    Script Date: 4/23/2019 1:20:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*****************************************************************************************************/
/*****************************************************************************************************/

/*  	Procedure Name:		CTShrinkIntegrityCheck													 */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		07/14/2016																 */
/*		Create Reason:		Log Job  															     */
/*		Parameter(1):		None														 */
/*		Utilized By:		Database Administrator	- SQL Agent Maintenance Job						 */
/*		Version :			1 .. Creation  expect version 2 to archive flagged records				 */
/*		Modified By :		Alekya Kondam
        Modified Date:		03/29/2019	
        Modifications made : added Exceptional handling 																							 */
/*		Status:				Enabled 																 */
/*--    example				exec BackUp																 */
/*																									 */
/******************************************************************************************************/
/******************************************************************************************************/


CREATE PROCEDURE [dbo].[Log_SCJobLog_proc]
AS
BEGIN TRY

  INSERT INTO CometTracker.dbo.SCJobLogTb

  EXEC msdb.dbo.sp_help_jobsteplog @job_name = N'CTShrinkIntegrityCheck'

END TRY
BEGIN CATCH
  THROW;
END CATCH



GO


