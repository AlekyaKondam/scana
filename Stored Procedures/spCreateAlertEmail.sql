USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spCreateAlertEmail]    Script Date: 4/23/2019 1:25:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[spCreateAlertEmail]
(
	@Title varchar(100)
	,@CreatedDate dateTime
	,@UserName varchar(50)
	,@ProjectID varchar(50)

)
AS

Set NOCOUNT ON

BEGIN TRY

	Declare @EmailReportName varchar(20) = 'Alerts'
			,@SendTO varchar(2000) = NULL
			,@SendCC varchar(2000) = NULL
			,@MessageBody varchar(max) = 'The following individual had An ' + @Title + ' Alert'
			,@Subject varchar(200) = @Title + ' Alert'
			,@Tech varchar(100)
			,@ProjectName varchar(200)
			,@BodyMessage2 varchar(200)
			,@CRLF varchar(10) = char(13) + char(10)
			,@Return int
			,@SourceDB varchar(200)
			,@DBNAME varchar(50) = DB_NAME()

	Select @Tech = UserLastName + ', ' + UserFirstName + ' (' + UserName + ')' from UserTb where UserName = @UserName

	Select @ProjectName = ProjectName from ProjectTb where ProjectID = @ProjectID

	Set @BodyMessage2 = @Tech +', ' + @ProjectName + ', ' + Cast(@CreatedDate as varchar(50)) 

	Set @MessageBody = @MessageBody + @CRLF + @CRLF + @BodyMessage2

	Set @SourceDB =
		CASE 
			WHEN @DBNAME = 'CometTracker' THEN 'CT Dev - '
			WHEN @DBNAME = 'CometTracker_Stage' THEN 'CT Stage - '
			WHEN @DBNAME = 'AZCometTracker_PROD' THEN 'CT Prod - '
			WHEN @DBNAME = 'CometTracker_Demo' THEN 'CT Demo - ' 
		END

	Set @Subject = @SourceDB + @Subject

	

	Exec [dbo].[spGetEmailList] @EmailReportName, @ProjectID, @SendTO OUTPUT, @SendCC OUTPUT
	

	if @SendTO is not null
	BEGIN

		EXEC spSendEmail @SendTO, @SendCC, @Subject, @MessageBody

	END


END TRY
BEGIN CATCH
  THROW;
END CATCH

Set NOCOUNT OFF








GO


