USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptEmployeeMileageSummary]    Script Date: 4/24/2019 9:07:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[spRptEmployeeMileageSummary]
(
	@StartDate			DATETIME	= NULL
	,@EndDate			DATETIME	= NULL
	,@TechName varchar(100) = NULL     
     

) AS

/*****************************************************************************************************************
NAME:		[spRptEmployeeMileageSummary]
DATABASE:   CT
    
HISTORY
DATE            DEVELOPER         COMMENTS
-------------   ---------------   -------------------------------------------------------------------
2019/01/29		Gary Wheeler	  Initial Build with Parameters

Test Data
***********************************************************************************************************

	--EXECUTE  dbo.spRptEmployeeMileageSummary '2/24/2019', '3/2/2019' , NULL
	--EXECUTE  dbo.spRptEmployeeMileageSummary '2/24/2019', '3/2/2019' , '< All >'
	--EXECUTE  dbo.spRptEmployeeMileageSummary '2/24/2019', '3/2/2019' , 'Jose Technician (jtech)'
	

******************************************************************************************************************/

SET NOCOUNT ON 

---- Set Overall Defaults for the Report ------------------------------------------------------------------------------
SELECT
 @StartDate		= CASE
					WHEN @StartDate IS NULL AND @EndDate IS NOT NULL THEN COALESCE(@EndDate, getdate())
					ELSE COALESCE(@StartDate, GETDATE())
				  END
,@EndDate		= CASE 
					WHEN @EndDate IS NULL AND @StartDate IS NOT NULL THEN COALESCE(@StartDate, GETDATE())
					ELSE COALESCE(@EndDate, GETDATE())
				  END


---- Create The Report Data --------------------------------------------------

-- added now
BEGIN TRY
IF @TechName is null
	BEGIN

		SELECT  -- 20160111 Added
		'< All >' AS Drop_Down1
		,'ALL'  AS Drop_Down2
		INTO #DD
		
		UNION ALL

		Select Distinct [Tech], [Tech]
		from vMileageSummary
		where [Date] between @StartDate and @EndDate
		AND
		(
			[Odometer Miles] > 0 
		Or
			[Business Calculated Miles] > 0
		Or
			[Personal Calculated Miles] > 0
		Or
			[Total Admin Miles] > 0
	
		)
		ORDER BY
		1

		SELECT Drop_Down1,  Drop_Down2  FROM #DD -- DROP TABLE #DD

	END
ELSE
	BEGIN

		DECLARE @CountTest INT = (Select count(*) From vMileageSummary
		where [Date] between @StartDate and @EndDate
		and [Tech] = CASE WHEN @TechName Like '%All%' THEN [Tech] ELSE @TechName END
		and
		(
			[Odometer Miles] > 0 
		Or
			[Business Calculated Miles] > 0
		Or
			[Personal Calculated Miles] > 0
		Or
			[Total Admin Miles] > 0
	
		))
		
		IF @CountTest = 0
		BEGIN
 
			SELECT 'No data returned for the date(s) provided' AS [Report Alert]  
            
		END
		Else
		BEGIN	
		
			Select 
			Date
			, Tech
			, Project
			--, [Mileage Type]
			, [Odometer Starting Mileage]
			, [Odometer Ending Mileage]
			, [Odometer Miles] [Total Odometer Miles]
			--, [Odo Business Miles] [Business Odometer Miles]
			, [Odo Personal Miles] [Personal Odometer Miles]
			, [Business Calculated Miles] + [Personal Calculated Miles] AS [Calc Miles]
			--, [Business Calculated Miles]
			, [Personal Calculated Miles] [Personal Calc Miles]
			, [Total Admin Miles] + [Odo Business Miles] AS [Total Business Miles]

			
			from vMileageSummary
			where [Date] between @StartDate and @EndDate
			and [Tech] = CASE WHEN @TechName Like '%All%' THEN [Tech] ELSE @TechName END
			and
			(
				[Odometer Miles] > 0 
			Or
				[Business Calculated Miles] > 0
			Or
				[Personal Calculated Miles] > 0 
			Or
				[Total Admin Miles] > 0
	
			)

		END

	END
	END TRY
BEGIN CATCH
  THROW;
END CATCH

SET NOCOUNT OFF
















GO


