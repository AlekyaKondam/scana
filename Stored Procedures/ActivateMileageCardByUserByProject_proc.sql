USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[ActivateMileageCardByUserByProject_proc]    Script Date: 4/23/2019 1:16:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





/****************************************************************************************************
***************************************************************************************************
		Procedure Name:		ActivateMileageCardByUserByProject 					 				 
		Created By:			Sandra Jackson, Senior DBA											 
		Create Date:		05/03/2016	
		Modified By :		Alekya Kondam
		Modified Date:		03/29/2019														 
		Create Reason:		Activate Mileage Cards by Use By Project Id		         
		Prerequisite:		None																	 
		Utilized By:		CometTracker App, Supervisor, Admin										 
		Version:			1.0																		 
		Modified:			1.0 = creation 
							
		Status:				Active or Inactive					 
																									 
*****************************************************************************************************/
CREATE PROCEDURE [dbo].[ActivateMileageCardByUserByProject_proc]  @UserParam int, @ProjectParam int
AS
BEGIN TRY
UPDATE [CometTracker].[dbo].[MileageCardTb]
SET [MileageCardActiveFlag] = 1
WHERE [MileageCardTechID] = @UserParam
and [MileageCardProjectID] = @ProjectParam
END TRY
BEGIN CATCH
  THROW;
END CATCH






GO


