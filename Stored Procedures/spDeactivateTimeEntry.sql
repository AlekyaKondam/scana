USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spDeactivateTimeEntry]    Script Date: 4/23/2019 1:26:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO












CREATE PROCEDURE [dbo].[spDeactivateTimeEntry]
(
	@TimeCardID int
	,@TimeEntryTitleJSON varchar(1000)
	,@TimeEntryDate Date
	,@UserName varchar(50)
)
AS

/*

Examplet

	Exec spDeactivateTimeEntry TimecardID, TimeEntryTitle, TimeEntryDate, UserName



*/

Set NOCOUNT ON


--Before we start any thing need to save what was passed to the SP

Insert into [dbo].[tSPInsertedData]
	(SPName, InsertedData )
	Values
	('spDeactivateTimeEntry', 
	Cast(@TimeCardID as varchar(20)) + ', ' + 
	ISNULL(@TimeEntryTitleJSON, 'NULL') + ', ' + 
	ISNULL(Cast(@TimeEntryDate as varchar(20)), 'NULL') + ', ' + 
	ISNULL(@UserName, 'NULL'))
	




Declare @TimeEntryTitle varchar(200)
	,@TimeEntryID int

	select * Into #TimeEntryTitle From [dbo].[parseJSON](@TimeEntryTitleJSON)

	Declare curTimeEntryTitle Cursor Static
	For
	select StringValue from #TimeEntryTitle Where Name is null

	Open curTimeEntryTitle

	BEGIN TRY

		BEGIN TRANSACTION DeactivateTimeEntry

			Fetch Next From curTimeEntryTitle into @TimeEntryTitle

			WHILE @@FETCH_STATUS = 0
			BEGIN
			
				Declare curTimeEntryID CURSOR STATIC
				For
				Select TimeEntryID
				From
				TimeEntryTb t
				Join ActivityTb A on t.TimeEntryActivityID = a.ActivityID
				Where TimeEntryTimeCardID = @TimeCardID 
					and a.ActivityTitle = @TimeEntryTitle 
					and Cast(t.timeentrystarttime as date) = ISNULL(@TimeEntryDate, Cast(t.timeentrystarttime as date))
					and t.TimeEntryActiveFlag = 1

				Open curTimeEntryID

				Fetch Next From curTimeEntryID into @TimeEntryID

				WHILE @@FETCH_STATUS = 0
				BEGIN

					Insert Into [dbo].[tTimeEntryEventHistory]
					(
						TimeEntryID, 
						TimeEntryUserID, 
						TimeEntryStartTime, 
						TimeEntryEndTime, 
						TimeEntryWeekDay, 
						TimeEntryTimeCardID, 
						TimeEntryActivityID, 
						TimeEntryComment, 
						TimeEntryCreatedBy, 
						TimeEntryModifiedDate, 
						TimeEntryModifiedBy, 
						TimeEntryUserName, 
						TimeEntrySrcDTLT, 
						TimeEntrySvrDTLT, 
						TimeEntryActiveFlag, 
						TimeEntryChartOfAccount, 
						ChangeMadeBy, 
						Change, 
						Comments
					)
					Select
						TimeEntryID, 
						TimeEntryUserID, 
						TimeEntryStartTime, 
						TimeEntryEndTime, 
						TimeEntryWeekDay, 
						TimeEntryTimeCardID, 
						TimeEntryActivityID, 
						TimeEntryComment, 
						TimeEntryCreatedBy, 
						TimeEntryModifiedDate, 
						TimeEntryModifiedBy, 
						TimeEntryUserName, 
						TimeEntrySrcDTLT, 
						TimeEntrySvrDTLT, 
						TimeEntryActiveFlag, 
						TimeEntryChartOfAccount, 
						@UserName, --ChangeMadeBy, 
						'Delete', --Change, 
						'' --Comments
					From TimeEntryTb where TimeEntryID = @TimeEntryID

					Delete from TimeEntryTb where TimeEntryID = @TimeEntryID

					Fetch Next From curTimeEntryID into @TimeEntryID

				END

				close curTimeEntryID
				Deallocate curTimeEntryID

				Fetch Next From curTimeEntryTitle into @TimeEntryTitle

			END
			
		COMMIT TRANSACTION DeactivateTimeEntry

	END TRY
	BEGIN CATCH

		ROLLBACK TRANSACTION DeactivateTimeEntry
		--Set @Result = 0

	END CATCH

	close curTimeEntryTitle
	deallocate curTimeEntryTitle

Set NOCOUNT OFF












GO


