USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptEmployeeCalibration]    Script Date: 4/24/2019 9:07:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spRptEmployeeCalibration]
(
	@StartDate			DATETIME	= NULL
	,@EndDate			DATETIME	= NULL
	,@TechName varchar(100) = NULL     
     

) AS

/*****************************************************************************************************************
NAME:		[spRptEmployeeMileageSummary]
DATABASE:   CT
    
HISTORY
DATE            DEVELOPER         COMMENTS
-------------   ---------------   -------------------------------------------------------------------
2019/01/29		Gary Wheeler	  Initial Build with Parameters

Test Data
***********************************************************************************************************

	--EXECUTE  dbo.spRptEmployeeCalibration '3/4/2019', '3/4/2019' , NULL
	--EXECUTE  dbo.spRptEmployeeCalibration '2/24/2019', '3/2/2019' , '< All >'
	--EXECUTE  dbo.spRptEmployeeCalibration '3/4/2019', '3/4/2019' , 'Technician, SouthernCross (scctTech)'
	

******************************************************************************************************************/

SET NOCOUNT ON 

BEGIN TRY
/*
Declare @SPName varchar(100) = Object_Name(@@ProcID)

	Insert into [dbo].[tSPInsertedData]
	(SPName, InsertedData )
	Values
	(@SPName, ISNULL(Cast(@StartDate as varchar(20)), 'NULL') + ', ' + ISNULL(Cast(@EndDate as varchar(20)), 'NULL') + ', ' + ISNULL(@TechName, 'NULL'))
	
*/
---- Set Overall Defaults for the Report ------------------------------------------------------------------------------
SELECT
 @StartDate		= CASE
					WHEN @StartDate IS NULL AND @EndDate IS NOT NULL THEN COALESCE(@EndDate, getdate())
					ELSE COALESCE(@StartDate, GETDATE())
				  END
,@EndDate		= CASE 
					WHEN @EndDate IS NULL AND @StartDate IS NOT NULL THEN COALESCE(@StartDate, GETDATE())
					ELSE COALESCE(@EndDate, GETDATE())
				  END


---- Create The Report Data --------------------------------------------------

-- added now

IF @TechName is null
	BEGIN

		SELECT  -- 20160111 Added
		'< All >' AS Drop_Down1
		,'ALL'  AS Drop_Down2
		INTO #DD
		
		UNION ALL

		Select Distinct [Tech], [Tech]
		from vCalibrationDetails
		where [Calibration Date] between @StartDate and @EndDate
		ORDER BY
		1

		SELECT Drop_Down1,  Drop_Down2  FROM #DD -- DROP TABLE #DD

	END
ELSE
	BEGIN

		
		DECLARE @CountTest INT = (Select count(*) From vCalibrationDetails
			where [Calibration Date] between @StartDate and @EndDate
			and [Tech] = CASE WHEN @TechName Like '%All%' THEN [Tech] ELSE @TechName END)
		
		
		IF @CountTest = 0
		BEGIN
 
			SELECT 'No data returned for the date(s) provided' AS [Report Alert]  
            
		END
		Else
		BEGIN
		
			Select 
			[Calibration Date]
			, Tech
			, ProjectName
			, EquipmentType
			, SerialNumber
			, CalibrationLevel
			from vCalibrationDetails
			where [Calibration Date] between @StartDate and @EndDate
			and [Tech] = CASE WHEN @TechName Like '%All%' THEN [Tech] ELSE @TechName END
		
		END

	END
END TRY
BEGIN CATCH
  THROW;
END CATCH

SET NOCOUNT OFF

















GO


