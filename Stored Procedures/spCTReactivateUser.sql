USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spCTReactivateUser]    Script Date: 4/23/2019 1:26:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[spCTReactivateUser]
(
	@UserName varchar(50)
	,@Project varchar(50)
	--,@Result bit OUTPUT 
)
AS

Set NOCOUNT ON

	

	Declare @UserID int
		,@ProjectID int
		,@UserRoleType varchar(50)
		,@UnixEpoch BigInt

	select @UserID = UserID, @UserRoleType = ISNULL(UserAppRoleType, '') from UserTb where UserName = @UserName

	Select @ProjectID = ProjectID from ProjectTb where ProjectUrlPrefix = @Project

	SELECT @UnixEpoch = CONVERT(VARCHAR,getdate(),112)

	--Set @Result = 1

	BEGIN TRY

		BEGIN TRANSACTION Reactivate

			Update UserTb
			Set UserActiveFlag = 1
			Where UserID = @UserID

			Update TimeCardTb
			------changed below by FS as part of TimecardFlag change to 0 or 1 
				Set TimeCardActiveFlag = 1
			--Set TimeCardActiveFlag = 'Active'
			Where TimeCardProjectID = @ProjectID
				And TimeCardTechID = @UserID

			Update MileageCardTb
			----changed below by FS as part of MileageCardFlag change to 0 or 1 
		----	Set MileageCardActiveFlag = 1
			Set MileageCardActiveFlag = 1
			Where MileageCardProjectID = @ProjectID
				And MileageCardTechID = @UserID


			IF (Select Count(*) from [rbac].[auth_assignment] where user_id = @UserID) = 0
			BEGIN

				Insert Into [rbac].[auth_assignment]
				(item_name, user_id, created_at)
				Values
				(@UserRoleType, @UserID, @UnixEpoch)

			END

		COMMIT TRANSACTION Reactivate

	END TRY
	BEGIN CATCH

		ROLLBACK TRANSACTION Reactivate
		--Set @Result = 0

	END CATCH

Set NOCOUNT OFF








GO


