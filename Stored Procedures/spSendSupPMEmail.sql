USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spSendSupPMEmail]    Script Date: 4/24/2019 9:12:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spSendSupPMEmail]

AS
BEGIN TRY
Set NOCOUNT ON

/*

	Testing

	Exec spSendSupPMEmail



*/

	Declare @SendTO varchar(2000) = NULL
			,@SendCC varchar(2000) = ''
			,@BaseMessageBody varchar(max) = 'Last Week''''s Time Cards for the project ' 
			,@Subject varchar(200) = 'Time Cards Ready to Appove/Submit'
			,@ProjectName varchar(200)
			,@MessageBody varchar(max)


	Select * into #ProjectWithTimeCardsNeedingApproval from [dbo].[vProjectWithTimeCardsNeedingApproval] where UserPreferredEmail is not null

	Declare curProjects Cursor Static
	For
	Select Distinct ProjectName from #ProjectWithTimeCardsNeedingApproval

	Open curProjects

	Fetch Next From curProjects into @ProjectName

	While @@FETCH_STATUS = 0
	BEGIN

		Set @SendTO = NULL
		
		Select @SendTO = coalesce(@SendTO + '; ','') + u.UserPreferredEmail
		From #ProjectWithTimeCardsNeedingApproval u
		Where ProjectName = @ProjectName
		Order by u.UserPreferredEmail

		Set @MessageBody = @BaseMessageBody + @ProjectName + ' need to be approved/submitted.'

		EXEC spSendEmail @SendTO, @SendCC, @Subject, @MessageBody

		Fetch Next From curProjects into @ProjectName

	END

Close curProjects
Deallocate curProjects

Set NOCOUNT OFF


END TRY
BEGIN CATCH
  THROW;
END CATCH




GO


