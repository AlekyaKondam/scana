USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[SetUserActiveByUserName_proc]    Script Date: 4/23/2019 1:23:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  /*****************************************************************************************************/
/*****************************************************************************************************/

/*		Procedure Name:		SetUserActiveByUserName_proc						 				     */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		01/27/2016																 */
/*		Create Reason:		Set User Active															 */
/*		Parameter(1):		User Name varchar(50)													 */
/*		Utilized By:		CometTracker App, Supervisor, Admin										 */
/*																									 */
/*		Status:				active = 1 inactive = 0 terminated = 9 pending = 2						 */
/******************************************************************************************************/
/******************************************************************************************************/


CREATE PROCEDURE [dbo].[SetUserActiveByUserName_proc] @PARAMETER1 VARCHAR(50)
AS
BEGIN TRY
  UPDATE [dbo].[UserTb]
  SET [UserActiveFlag] = 1
  WHERE [UserActiveFlag] <> 1
  AND [UserName] = @PARAMETER1
  
END TRY
BEGIN CATCH
  THROW;
END CATCH

GO


