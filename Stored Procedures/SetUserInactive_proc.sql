USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[SetUserInactive_proc]    Script Date: 4/23/2019 1:23:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  /*****************************************************************************************************/
/*****************************************************************************************************/

/*		Procedure Name:		SetUserInActive_proc						 							 */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		07/14/2016																 */
/*		Create Reason:		Set User Active															 */
/*		Parameter(1):		User ID integer															 */
/*		Utilized By:		CometTracker App, Supervisor, Admin									     */
/*		Version :			1 .. Creation  expect version 2 to archive flagged records				 */
/*																									 */
/*		Status:				active = 1 inactive = 0 terminated = 9 pending = 2 					     */		
/*-- example				exec SetUserInActive_proc 548											 */
/*																									 */
/******************************************************************************************************/
/******************************************************************************************************/


CREATE  PROCEDURE [dbo].[SetUserInactive_proc] @PARAMETER1 INT
AS

Declare @UserName varchar(50)
BEGIN TRY
Select @UserName = UserName from UserTb where userid = @PARAMETER1
----prestep update [history].[HistoryLogTb]

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 1 starting',@PARAMETER1, GETDATE())



--PRINT '1. PreStep  UPDATE HistoryLogTb '  PRINT @PARAMETER1 
		INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		VALUES('SetUserInActive_proc starting',@parameter1, GETDATE())


		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 1 ending',@PARAMETER1, GETDATE())


--PRINT  '2.  Update AppRoles Table ' PRINT @PARAMETER1 

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 2 starting',@PARAMETER1, GETDATE())
/*
		UPDATE [dbo].[AppRolesTb]
		SET [AppRoleStatus] = 'Inactive',
			[AppRoleArchiveFlag] = 'Yes',
			[AppRoleModifiedDate] = GETDATE(),
			[AppRoleModifiedBy]  = USER_NAME()
		WHERE [AppRoleStatus] = 'Active'
		AND [AppRoleUserID] in (@PARAMETER1)
*/
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 2 ending',@PARAMETER1, GETDATE())


--PRINT  '3.  Update User Table ' PRINT @PARAMETER1 


		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 3 starting',@PARAMETER1, GETDATE())

		UPDATE [dbo].[UserTb]
		SET [UserActiveFlag] = 0,
			--[UserArchiveFlag] = 'Yes', --- removed by fasiha
			--[UserArchiveFlag] = 1 , -- added by fasiha
			--[UserAppRoleType] = NULL,
			[UserModifiedDate] = GETDATE(),
		   --[UserModifiedBy] = 1  -- removed by fasiha
			[UserModifiedUID] = USER_NAME() --- added by fasiha
		WHERE [UserActiveFlag]  = 1
		AND [UserID] in (@PARAMETER1)

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 3 ending',@PARAMETER1, GETDATE())
	
--PRINT  '4.  Update TimeCard Table ' PRINT @PARAMETER1 


		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 4 starting',@PARAMETER1, GETDATE())

		UPDATE [dbo].[TimeCardTb]
		----changed below by FS as part of TimecardFlag change to 0 or 1 
		SET [TimeCardActiveFlag] = 0 ,
		--SET [TimeCardActiveFlag] = 'Inactive',
		----changed below by FS as part of TimecardFlag change to 0 or 1 
		[TimeCardArchiveFlag] = 1,
		---	[TimeCardArchiveFlag] = 'Yes',
			[TimeCardModifiedDate] = GETDATE(),
			[TimeCardModifiedBy]  = USER_NAME()
		----changed below by FS as part of TimecardFlag change to 0 or 1 
			WHERE [TimeCardActiveFlag] = 1
		---WHERE [TimeCardActiveFlag] = 'Active'
		AND [TimeCardTechID]  in (@PARAMETER1)

	
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 4 ending',@PARAMETER1, GETDATE())

--PRINT '5.  Update TimeEntry Table ' PRINT @PARAMETER1 

		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 5 starting',@PARAMETER1, GETDATE())


		UPDATE [dbo].[TimeEntryTb]
		SET [TimeEntryActiveFlag] = 1,
			--[TimeEntryArchiveFlag] = 'Yes',
			[TimeEntryModifiedDate] = GETDATE(),
			[TimeEntryModifiedBy] = 0
		WHERE [TimeEntryActiveFlag] = 1
		AND [TimeEntryUserName] = @UserName -- (@PARAMETER1)  Modified on 10/30/2017 by Gary Wheeler

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 5 ending',@PARAMETER1, GETDATE())
		
--PRINT '6.  Update MileageCard Table ' PRINT @PARAMETER1 
		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 6 starting',@PARAMETER1, GETDATE())

		UPDATE [dbo].[MileageCardTb]
		--	----changed below by FS as part of MileageCardFlag change to 0 or 1 
		----SET [MileageCardActiveFlag]  = 0 ,
		SET [MileageCardActiveFlag]  = 'Inactive',
		----changed below by FS as part of MileageCardFlag change to 0 or 1 
		----[MileageCardArchiveFlag] = 1,
			[MileageCardArchiveFlag] = 'Yes',
			[MileageCardModifiedDate] = GETDATE(),
			[MileageCardModifiedBy] = 0

	   ----changed below by FS as part of MileageCardFlag change to 0 or 1 
		----WHERE [MileageCardActiveFlag] = 1
		WHERE [MileageCardActiveFlag] = 'Active'
		AND [MileageCardTechID] in (@PARAMETER1)

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 6 ending',@PARAMETER1, GETDATE())

--PRINT '7.  Update MileageEntry Table ' PRINT @PARAMETER1 

		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 7 starting',@PARAMETER1, GETDATE())

		UPDATE [dbo].[MileageEntryTb]
		SET [MileageEntryActiveFlag] = 0,
			--[MileageEntryArchiveFlag] = 'Yes',
			[MileageEntryModifiedDate] = GETDATE(),
			[MileageEntryModifiedBy] = 0
		WHERE [MileageEntryActiveFlag] = 1
		AND [MileageEntryUserName] = @UserName -- (@PARAMETER1)  Modified on 10/30/2017 by Gary Wheeler

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 7 ending',@PARAMETER1, GETDATE())

--PRINT '8. Update Equipment Table ' PRINT @PARAMETER1 

		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 8 starting',@PARAMETER1, GETDATE())
/*
		UPDATE [dbo].[EquipmentTb]
		SET [EquipmentAssignedUserID] = NULL,
			[EquipmentModificationReason] = 'User set to inactive status',
			[EquipmentModifiedDate]= GETDATE(),
			[EquipmentModifiedBy] = 0
		WHERE [EquipmentAssignedUserID]  in (@PARAMETER1)
*/

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 8 ending',@PARAMETER1, GETDATE())


--PRINT '9.  Update Project_User Table '  PRINT @PARAMETER1  -- there is no active flag these records simply get deleted

		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 9 starting',@PARAMETER1, GETDATE())


		UPDATE [dbo].[Project_User_Tb]
		SET [ProjUserArchiveFlag] = 'Yes',
			[ProjUserComment] = 'User set to inactive status and row archived',
			[ProjUserModifiedDate]  = GETDATE(),
			[ProjUserModifiedBy] = USER_NAME()
		WHERE  [ProjUserUserID] in (@PARAMETER1)	

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 9 ending',@PARAMETER1, GETDATE())
		
--PRINT '10.  Archive Project_User Table '  PRINT @PARAMETER1 

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 10.  Archive Project_User Table Starting',@PARAMETER1, GETDATE())

		INSERT INTO [history].[HistoryProject_User_Tb]
		SELECT * FROM [dbo].[Project_User_Tb] 
		WHERE [ProjUserArchiveFlag] =  'Yes'
		AND [ProjUserUserID]  in (@PARAMETER1)

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 10.  Archive Project_User Table Ending',@PARAMETER1, GETDATE())


--PRINT '11.  Delete all user rows in Project_User Table .. delete rows Project_User_Tb '  PRINT @PARAMETER1 

		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 11.  Delete Rows Project_User Table Starting',@PARAMETER1, GETDATE())

		-- delete rows
			--BEGIN

				--DELETE FROM [dbo].[Project_User_Tb]
				--WHERE [ProjUserUserID]  in (@PARAMETER1)
			--END

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 11.  Delete Rows Project_User Table Ending',@PARAMETER1, GETDATE())

			
--PRINT '12.   Drop constraintS on rbac auth assignment table ' PRINT @PARAMETER1 

		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 12 starting',@PARAMETER1, GETDATE())

/* Commented out after discussion with Michael Davis

		ALTER TABLE [rbac].[auth_assignment] DROP CONSTRAINT [FK__auth_assi__item%]
	
*/
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 12 ending',@PARAMETER1, GETDATE())

--PRINT '13.   Archive rbac auth assignment table '  PRINT @PARAMETER1 

		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 13 starting',@PARAMETER1, GETDATE())


		INSERT INTO [history].[HistoryAuth_Assignment]
           ([HistItem_Name]
           ,[HistUser_Id]
           ,[HistCreated_At])
		SELECT	item_name, User_id , created_at
		FROM	[rbac].[auth_assignment]		 
		-- WHERE	[user_id] in (@PARAMETER1) removed by fasiha 
		  WHERE	[user_id] = cast (@PARAMETER1 as varchar(64))  -- added by fasiha 


		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 13 ending',@PARAMETER1, GETDATE())

--PRINT '14.   Delete rows from rbac auth assignment table '  PRINT @PARAMETER1 

		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 14 starting',@PARAMETER1, GETDATE())

		DELETE FROM [rbac].[auth_assignment]
		-- WHERE [user_id]  in (@PARAMETER1) changed by fasiha 
		WHERE	[user_id] = cast (@PARAMETER1 as varchar(64))  -- added by fasiha 

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 14 ending',@PARAMETER1, GETDATE())

--PRINT '15.   Add constraintS rbac auth assignment table '  PRINT @PARAMETER1 

		
		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 15 starting',@PARAMETER1, GETDATE())

/* Commented out after discussion with Michael Davis
ALTER TABLE [rbac].[auth_assignment]  WITH CHECK ADD FOREIGN KEY([item_name])
REFERENCES [rbac].[auth_item] ([name])
ON UPDATE CASCADE
ON DELETE CASCADE
*/

		--INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		--VALUES('STEP 15 ending',@PARAMETER1, GETDATE())

--PRINT '16.  PostStep  UPDATE HistoryLogTb '  PRINT @PARAMETER1 
		INSERT INTO [history].[HistoryLogTb]([LogData],[LogDetail],[LogDateTime])
		VALUES('SetUserInActive_proc ending',@PARAMETER1, GETDATE())



		
END TRY
BEGIN CATCH
  THROW;
END CATCH









GO


