USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spTruncateDB]    Script Date: 4/24/2019 9:12:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





--copy of dev / CT _stage can be purged 
/*

-- Verify the record counts before and after running this SP

select count(*) from  [refChartOfAccount]  -- update by SP Execute [spUpdateFromAzure]
select count(*) from   [refPaySource]      -- update by SP Execute [spUpdateFromAzure]
select count(*) from   [refProjectType]  -- update by SP Execute [spUpdateFromAzure] 
select count(*) from  [refTask]          -- update by SP Execute [spUpdateFromAzure]
select count(*) from  [refTaskProject]

*/

CREATE Procedure [dbo].[spTruncateDB]

AS 
BEGIN TRY
-------------------Truncate 
Truncate Table  [ActivityTb]
Truncate Table  [refChartOfAccount]  -- update by SP Execute [spUpdateFromAzure]
Truncate Table  [refPaySource]       -- update by SP Execute [spUpdateFromAzure]
Truncate Table  [refProjectType]     -- update by SP Execute [spUpdateFromAzure] 
Truncate Table  [refTask]            -- update by SP Execute [spUpdateFromAzure]
Truncate Table  [refTaskProject]     -- update by SP Execute [spUpdateFromAzure]
Truncate Table  [AuthTb]
Truncate Table  [BreadcrumbTb]
Truncate Table  [EquipmentStatusTb]
Truncate Table  [EquipmentTb]
Truncate Table  [MileageEntryTb]
Truncate Table  [Project_User_Tb]
Truncate Table  [Project_User_Tb_Backup_20170915]
Truncate Table 	[refEmployeeIDMapTb]   -- update by SP Execute [spUpdateFromAzure]  
Truncate Table 	[TimeEntryTb]
Truncate Table  [tSPInsertedData]
Truncate Table 	[tTabletDataInsertArchive]
Truncate Table 	[tTabletDataInsertBreadcrumbArchive]
Truncate Table  [tTabletJSONDataInsertError]
Truncate Table  [tTaskOut]
Truncate Table  [tTimeCardEventHistory]
Truncate Table  [tWebDataInsertArchive]
Truncate Table  [tWebJSONDataInsertError]
Truncate Table  [history].[HistoryAuth_Assignment]
Truncate Table  [history].[HistoryLogTb]
Truncate Table  [history].[HistoryProject_User_Tb]
Truncate Table  [menus].[ProjectModuleTb]  ---- update by SP Execute  [spModuleRelation] 
Truncate Table  [rbac].[auth_assignment]
Truncate Table  [rbac].[auth_item_child]
Truncate Table  [ActivityCodeTb]
----------------------
 Delete From    [TimeCardTb]
 Delete From    [ProjectTb]     -- update by SP Execute [spUpdateFromAzure] + [spSetLandingPage]
 Delete From    [MileageCardTb]
 Delete From    [UserTb]       -- update by SP Execute [spUpdateUsers]    
 Delete From    [rbac].[auth_item]
 Delete From    [ClientTb]       -- update by SP Execute [spUpdateFromAzure]

------------------------
/* BELOW TABLES ARE NOT TRUNCATED */
--SELECT *  FROM [AppRolesTb]
--select * from  [rReport]
--Select * from  [EmployeeTypeTb]
--Select * from [EquipmentTypeTb]
--Select * from [PayCodeTb]
--Select * from  [rDropDown]
--Select * from  [StateCodeTb]
--Select * from  [tGPSFrequency]
--Select * from   [TimeCardPeriodsTb]
--Select * from  [menus].[ModuleMenuTb]
--Select * from  [menus].[ModuleSubMenuTb]
--Select * from  [EquipmentConditionTb]



END TRY
BEGIN CATCH
  THROW;
END CATCH



GO


