USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateUsers]    Script Date: 4/24/2019 9:13:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









CREATE proc [dbo].[spUpdateUsers] 
as


Set NoCount On

Declare @UserName varchar(100)
	,@FirstName varchar(50)
	,@LastName varchar(50)
	,@JobTitle varchar(50)
	,@PreferredPhone varchar(20)
	,@CompanyPhone varchar(20)
	,@Roletype varchar(50)
	,@PaySourceID int
	,@SCCEmployeeID int
	,@Address varchar(100)
	,@City varchar(50)
	,@State varchar(20)
	,@Zip varchar(20)
	,@Location varchar(200)
	,@PayMethod varchar(20)
	,@PreferredEmail varchar(100)

	,@CompanyName varchar(50) = 'Southern Cross'
	,@ActiveFlag bit = 1
	,@CreatedDateTime Datetime = getdate()
	,@OasisID varchar(20) = ''
	,@CreatedUserID varchar(20) = 'ReferenceDB'
	,@RefreshDatetime Datetime = getdate()
	,@QuickBooksID varchar(20) = ''

	,@UserNameCount int
	--,@UserName varchar(200)
	,@PasswordHash varchar(Max) = NULL
	,@UserID int
	,@LastRefreshDate datetime
	,@ADPID varchar(20)
	,@MSDynamicsID varchar(50)
	,@OriginalCTID varchar(50)
BEGIN TRY
Declare Users Cursor Static
For
select 
ed.FirstName,
ed.LastName,
ed.JobTitle,
ed.PreferredPhone,
ed.WorkPhone,
CASE
	WHEN ed.JobTitle like '%Supervisor%' THEN 'Supervisor'
	WHEN ed.JobTitle like '%Manager%' and JobTitle Not Like '%Project%' THEN 'Supervisor'
	WHEN ed.JobTitle Like '%Project Manager%' THEN 'ProjectManager'
	ELSE 'Technician'
END [AppRoleType],
--ed.JobTitle,
ed.PaySourceID,
ed.SCCEmployeeID,
ed.Address,
ed.City,
ed.State,
ed.Zip,
ed.Location,
ed.PayMethod,
ed.PreferredEmail,
eimOasis.SourceEmployeeID,
eimQB.SourceEmployeeID,
eimADP.SourceEmployeeID,
eimMSDynamics.SourceEmployeeID
--eimOriginalCT.SourceEmployeeID
from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[EmployeeData] ed
Left Join (select * from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[EmployeeIDMap] where paysourceid = 1)  eimOasis on ed.SCCEmployeeID = eimOasis.SCCEmployeeID --Oasis
Left Join (select * from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[EmployeeIDMap] where paysourceid = 3)  eimQB on ed.SCCEmployeeID = eimQB.SCCEmployeeID --QB
Left Join (select * from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[EmployeeIDMap] where paysourceid = 2)  eimADP on ed.SCCEmployeeID = eimADP.SCCEmployeeID --ADP
Left Join (select * from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[EmployeeIDMap] where paysourceid = 4)  eimMSDynamics on ed.SCCEmployeeID = eimMSDynamics.SCCEmployeeID --MSDynamics
--Left Join (select * from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[EmployeeIDMap] where paysourceid = 5)  eimOriginalCT on ed.SCCEmployeeID = eimOriginalCT.SCCEmployeeID --OriginalCT
where department in ('Survey', 'Oh/Train/Quality', 'FieldServices') and ed.JobTitle like 'TECHNICIAN%' and ed.statusid = 1
and ed.SCCEmployeeID not in (1)

Open Users

Fetch Next From Users into
 @FirstName
,@LastName
,@JobTitle
,@PreferredPhone
,@CompanyPhone
,@Roletype
,@PaySourceID
,@SCCEmployeeID
,@Address
,@City
,@State
,@Zip
,@Location
,@PayMethod
,@PreferredEmail
,@OasisID
,@QuickBooksID
,@ADPID
,@MSDynamicsID
--,@OriginalCTID

While @@FETCH_STATUS = 0
BEGIN

	IF Exists (Select * from UserTb where SCCEmployeeID = @SCCEmployeeID)
	BEGIN

		--Need to write update code here
		Select @UserID = UserID from UserTb where SCCEmployeeID = @SCCEmployeeID

		Set @UserName = Left(@FirstName, 1) + @LastName

		Select @PasswordHash =[dbo].[fnCreatePasswordHASH](@UserName)

		If Len(@PasswordHash) > 75
		BEGIN

			Set @PasswordHash = Left(@PasswordHash, 75)

		END

		set @PasswordHash = NULL

		Update UserTb
		Set
			UserFirstName = @FirstName, 
			UserLastName = @LastName, 
			UserEmployeeType = @JobTitle, 
			UserPhone = @PreferredPhone, 
			UserCompanyName = @CompanyName, 
			UserCompanyPhone = @CompanyPhone, 
			UserAppRoleType = @Roletype, 
			UserActiveFlag = @ActiveFlag, 
			UserCreatedUID = @CreatedUserID, 
			UserPaySourceID = @PaySourceID, 
			UserOasisID = @OasisID, 
			UserAddress = @Address, 
			UserCity = @City, 
			UserState = @State, 
			UserZip = @Zip, 
			UserLocation = @Location, 
			UserPayMethod = @PayMethod, 
			UserPreferredEmail = @PreferredEmail, 
			UserRefreshDateTime = @RefreshDatetime,
			UserQuickBooksID = @QuickBooksID,
			UserADPID = @ADPID,
			UserMSDynamics = @MSDynamicsID
			--UserOriginalCT = @OriginalCTID
		Where SCCEmployeeID = @SCCEmployeeID
			

	END
	ELSE
	BEGIN

		Set @UserName = Left(@FirstName, 1) + @LastName

		Select @PasswordHash =[dbo].[fnCreatePasswordHASH](@UserName)

		If Len(@PasswordHash) > 75
		BEGIN

			Set @PasswordHash = Left(@PasswordHash, 75)

		END

		set @PasswordHash = NULL

		select @UserNameCount = Count(*) from UserTb where UserName like @UserName + '%' 

		if @UserNameCount > 0
		BEGIN

			set @UserName = @UserName + cast(@UserNameCount as varchar(5))

		END

		--Select * from 



		Insert Into UserTb
		(
			UserName, 
			UserFirstName, 
			UserLastName, 
			UserEmployeeType, 
			UserPhone, 
			UserCompanyName, 
			UserCompanyPhone, 
			UserAppRoleType, 
			UserActiveFlag, 
			UserCreatedDate, 
			UserCreatedUID, 
			UserPaySourceID, 
			UserOasisID, 
			SCCEmployeeID, 
			UserAddress, 
			UserCity, 
			UserState, 
			UserZip, 
			UserLocation, 
			UserPayMethod, 
			UserPreferredEmail, 
			UserRefreshDateTime,
			UserPassword,
			UserQuickBooksID,
			UserADPID,
			UserMSDynamics
			--UserOriginalCT
		)
		Values
		(
			@UserName,
			@FirstName,
			@LastName,
			@JobTitle,
			@PreferredPhone,
			@CompanyName,
			@CompanyPhone,
			@Roletype,
			@ActiveFlag,
			@CreatedDateTime,
			@CreatedUserID,
			@PaySourceID,
			@OasisID,
			@SCCEmployeeID,
			@Address,
			@City,
			@State,
			@Zip,
			@Location,
			@PayMethod,
			@PreferredEmail,
			@RefreshDatetime,
			@PasswordHash,
			@QuickBooksID,
			@ADPID,
			@MSDynamicsID
			--@OriginalCTID

		)


	END

	
	Fetch Next From Users into
	 @FirstName
	,@LastName
	,@JobTitle
	,@PreferredPhone
	,@CompanyPhone
	,@Roletype
	,@PaySourceID
	,@SCCEmployeeID
	,@Address
	,@City
	,@State
	,@Zip
	,@Location
	,@PayMethod
	,@PreferredEmail
	,@OasisID
	,@QuickBooksID
	,@ADPID
	,@MSDynamicsID
	--,@OriginalCTID

END

CLOSE Users
DEALLOCATE Users

--Now we mark anyone in CT that isn't in the REF DB as a tech

Update u
Set UserActiveFlag = 0
From (Select * from UserTb where ISNULL(SCCEmployeeID, 0) <> 0 and UserActiveFlag = 1
And SCCEmployeeID not in (select Distinct SCCEmployeeID from [AZUREREFERENCEDB].[sql-use-prod-reference].[dbo].[EmployeeData] ref
	where ref.JobTitle like 'TECHNICIAN%' and ref.statusid = 1)
and UserTb.SCCEmployeeID not in (0, 1)) u


DECLARE @CountTest INT = (Select count(*) From [vUsersWithMissingIDs])
			
If @CountTest > 0
BEGIN

	EXEC spCreateMissingIDEmail

END


Set NoCount Off


END TRY
BEGIN CATCH
  THROW;
END CATCH























GO


