USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[PopulateTimeCardTbForNewUserCatchErrors_proc]    Script Date: 4/23/2019 1:22:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/*****************************************************************************************************/
/*****************************************************************************************************/

/*		Function Name:		PopulateTimeCardTbForNewUserCatchErrors_proc			 		  		 */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		01/25/2016																 */
/*		Create Reason:		Populate Time Cards For New User for the year							 */
/*		Parameters(1):		UserID OF New User														 */
/*		Prerequisites:		Create The New User in the User Table 									 */
/*		Utilized By:		CometTracker App, Supervisor, Admin										 */
/*		Call methods(1):	1. execute dbo.PopulateTimeCardTbForNewUserCatchErrors_proc	237			 */
/*		Validation:			select count(*)* 53 FROM [dbo].[UserTb] WHERE [UserActiveFlag]			 */
/*                          NOT IN ('INACTIVE','TERMINATED','RETIRED','PENDING')					 */
/*							select count(*) from [TimeCardTb]  --test to validate it worked			 */
/*		Status:				UserActiveFlag active = 1 inactive = 0 terminated = 9 pending = 2		 */

/* TO BE RUN 1 TIME A YEAR TO CREATE TIME CARD PERIODS                                                */
/******************************************************************************************************/
/******************************************************************************************************/


CREATE PROCEDURE [dbo].[PopulateTimeCardTbForNewUserCatchErrors_proc] @PARAMETER1 INT

AS

DECLARE @TECHID INT 
DECLARE TEST scroll CURSOR 
FOR
		SELECT [UserID] FROM [dbo].[UserTb] 
		WHERE [UserActiveFlag] NOT IN (0,9,2)
		AND [UserID] = @PARAMETER1
		ORDER BY [UserID];

OPEN TEST
	FETCH NEXT FROM TEST
	INTO @TECHID
	WHILE @@FETCH_STATUS = 0
		BEGIN TRY
			BEGIN TRANSACTION INSRT
				INSERT INTO [TimeCardTb] (	 [TimeCardStartDate], [TimeCardEndDate],            [TimeCardProjectID],			[TimeCardTechID],			   [TimeCardCreateDate],                 [TimeCardCreatedBy])
  				SELECT							[TimeCardSunday] as [TimeCardStartDate], [TimeCardSaturday] as [TimeCardEndDate], 1006 AS [TimeCardProjectID], @TECHID AS [TimeCardTechID], getdate() as [TimeCardCreateDate], SUSER_NAME() as [TimeCardCreatedBy]
				FROM [dbo].[TimeCardPeriodsTb]

				FETCH NEXT FROM TEST
				INTO @TECHID
			COMMIT TRAN INSRT
END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
        ROLLBACK TRAN INSRT
	

    RAISERROR(7770001, 10, 1)
END CATCH
	
CLOSE TEST
DEALLOCATE TEST




GO


