USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spGetEmailList]    Script Date: 4/23/2019 1:29:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetEmailList]
(
      @ReportName varchar(200) = ''
	  ,@ProjectID int = 0
	  ,@EmailTOList varchar(max) Output
	  ,@EmailCCList varchar(max) Output
)
AS 
Begin
	--Declare @emaillist varchar(max)

	--WAITFOR DELAY '00:00:10'
	BEGIN TRY

	Declare @ProjectSuppervisors varchar(max) = NULL
	
	
	Select @EmailTOList = coalesce(@EmailTOList + '; ','') + el.Email
	From 
		[dbo].[rEmailList] el
		Join [dbo].[xReportNameEmailList] xref on xref.EmailID = el.emaillistID
		Join [dbo].[rEmailReportName] r on r.rReportNameID = xref.ReportID
		Where r.ReportName = @ReportName 
			and (xref.ProjectID = @ProjectID or xref.ProjectID = 0)
			and xref.EmailType = 'TO'
			and el.ActiveFlag = 1
			and xref.ActiveFlag = 1
			and r.ActiveFlag = 1
		Order by xref.SortOrder, el.LastName

	Select @EmailCCList = coalesce(@EmailCCList + '; ','') + el.Email
	From 
		[dbo].[rEmailList] el
		Join [dbo].[xReportNameEmailList] xref on xref.EmailID = el.emaillistID
		Join [dbo].[rEmailReportName] r on r.rReportNameID = xref.ReportID
		Where r.ReportName = @ReportName 
			and (xref.ProjectID = @ProjectID or xref.ProjectID = 0)
			and xref.EmailType = 'CC'
			and el.ActiveFlag = 1
			and xref.ActiveFlag = 1
			and r.ActiveFlag = 1
		Order by xref.SortOrder, el.LastName

	If @ProjectID > 0 -- Add Project Sups
	BEGIN

		Select @ProjectSuppervisors = coalesce(@ProjectSuppervisors + '; ','') + u.UserPreferredEmail
		From 
		(Select * From UserTb  where UserAppRoleType = 'Supervisor' and ISNULL(UserPreferredEmail, '') <> '' ) u
		Join (Select * from Project_User_Tb Where ProjUserProjectID = @ProjectID) pu on pu.ProjUserUserID = u.UserID
		Order by u.username

		If @ProjectSuppervisors is not NULL
		BEGIN

			Set @EmailTOList = @ProjectSuppervisors + '; ' + @EmailTOList


		END

	END


	-- Return the result of the function
	RETURN 100
	END TRY
BEGIN CATCH
  THROW;
END CATCH

END




GO


