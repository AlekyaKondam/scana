USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[GetAllMileageCardsByProjectByUser_proc]    Script Date: 4/23/2019 1:19:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/****************************************************************************************************
***************************************************************************************************
		View Name:			GetMileageCardsByProjectByUser_proc						 				 
		Created By:			Sandra Jackson, Senior DBA												 
		Create Date:		03/23/2016																 
		Create Reason:		View all mileage cards by project by user						         
		Prerequisite:		AllTimeCardsByProjectByUser_vw																	 
		Utilized By:		CometTracker App, Supervisor, Admin	
		Modified By :		Alekya Kondam
        Modified Date:		03/29/2019	
        Modifications made : added Exceptional handling 									 
		Version:			1.0																		 
																									 
*****************************************************************************************************/



CREATE PROCEDURE [dbo].[GetAllMileageCardsByProjectByUser_proc]  @PARAMETER1 INT
AS
BEGIN TRY
-- Declare @PARAMETER1 INT set @PARAMETER1 = '1'
SELECT DISTINCT [MileageCardID]
      ,[MileageCardTechID]
      ,[MileageCardProjectID]
      ,[MileageStartDate]
      ,[MileageEndDate]
      ,[MileageCardBusinessMiles]
      ,[MileageCardPersonalMiles]
      ,[MileageCardApprovedFlag]
      ,[MileageCardApprovedBy]
      ,[MileageCardSupervisorName]
      ,[MileageCardCreateDate]
      ,[MileageCardCreatedBy]
      ,[MileageCardModifiedDate]
      ,[MileageCardModifiedBy]
	  ,[ProjectID]
      ,[ProjectName]
      ,[ProjectDescription]
      ,[ProjectNotes]
      ,[ProjectType]
      ,[ProjectStatus]
      ,[ProjectClientID]
      ,[ProjectStartDate]
      ,[ProjectEndDate]
      ,[ProjectCreateDate]
      ,[ProjectCreatedBy]
      ,[ProjectModifiedDate]
      ,[ProjectModifiedBy]
	  ,[UserID]
      ,[UserName]
      ,[UserFirstName]
      ,[UserLastName]
      ,[UserEmployeeType]
      ,[UserPhone]
      ,[UserCompanyName]
      ,[UserCompanyPhone]
      ,[UserAppRoleType]
      ,[UserComments]
      ,[UserKey]
      ,[UserActiveFlag]
      ,[UserCreateDTLTOffset]
      ,[UserModifiedDTLTOffset]
      ,[UserInactiveDTLTOffset]
      ,[UserCreatedDate]
      ,[UserCreatedBy]
      ,[UserModifiedDate]
      ,[UserModifiedBy]
	   FROM AllMileageCardsByProjectByUser_vw
WHERE [UserID] = @PARAMETER1
ORDER BY MileageCardID

END TRY
BEGIN CATCH
  THROW;
END CATCH


GO


