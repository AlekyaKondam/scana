USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[PopulateTimeCardTbForUserToProjectCatchErrors_proc]    Script Date: 4/23/2019 1:22:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










/*****************************************************************************************************/
/*****************************************************************************************************/

/*		Function Name:		PopulateTimeCardTbForUserToProjectCatchErrors_proc			 		  	 */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		01/25/2016																 */
/*		Modified Date:		04/18/2016																 */

/*		Modified By:		Gary Wheeler, Senior DBA												 */
/*		Modified Date:		01/09/2018																 */
/*		Modify Reason:		Populate Time Cards For User for the year if other Milecards exist	     */

/*		Create Reason:		Populate Time Cards For New User for the year							 */
/*		Parameters(2):		UserID OF New User, ProjectID											 */
/*		Prerequisites:		Create The New User in the User Table 									 */
/*		Utilized By:		CometTracker App, Supervisor, Admin										 */
/*		Call methods(1):	1. execute dbo.PopulateTimeCardTbForNewUserCatchErrors_proc	237			 */
/*		Validation:			select count(*)* 53 FROM [dbo].[UserTb] WHERE [UserActiveFlag]			 */
/*                          NOT IN ('INACTIVE','TERMINATED','RETIRED','PENDING')					 */
/*							select count(*) from [TimeCardTb]  --test to validate it worked			 */
/*		Status:				UserActiveFlag active = 1 inactive = 0 terminated = 9 pending = 2		 */
/*		Version:			1.3				  					  					     *************/
/*		Modified Reason:																 *************/
/*		1.0 Created Stored Procedure		PopulateTIMECARDSTbForNewUser_proc			 *************/
/*		1.1 Caught Errors		renamed		PopulateTIMECARDSTbForNewUserCatchErrors_proc	**********/
/*		1.2 Added  proj id param rename		PopulateTIMECARDSTbForUserToProjectCatchErrors_proc	******/
/*		1.3 Checks contents of column TimeCardProjectGroupID on TimeCardTb to creates		    ******/
/*		unique Timecards by project															**********/
/* TO BE RUN TO GENERATE SET OF USER TIMECARDS BY PROJECT (TO CREATE TIME CARD PERIODS)              */
/******************************************************************************************************/
/******************************************************************************************************/


CREATE PROCEDURE [dbo].[PopulateTimeCardTbForUserToProjectCatchErrors_proc] @TechID INT,  @ProjectID INT

AS


DECLARE @StarteDate datetime = Cast(Cast(Cast(DateAdd(d, -(DatePart(dw, getdate()) - 1), getdate()) as date) as varchar(20)) + ' 00:00:00 AM' as datetime)
, @ProjTimecardPMApprovedFlag bit = 0
, @ProjTimecardApprovedflag bit = 0


	BEGIN TRY
		BEGIN TRANSACTION INSRT

			
			

			SELECT	
				tcp.[TimeCardSunday] as [TimeCardStartDate], 
				tcp.[TimeCardSaturday] as [TimeCardEndDate], 
				@ProjectID AS [TimeCardProjectID], 
				@TECHID AS [TimeCardTechID], 
				1 [TimeCardActiveFlag],
				0 [TimeCardApprovedFlag],
				'' as [TimeCardApproveBy],
				0 [TimeCardArchiveFlag],
				getdate() as [TimeCardCreateDate], 
				'gwheeler' as [TimeCardCreatedBy]
				,0 [TimecardPMapprovedFlag]
			INTO #NewTimeCards
			FROM (Select * From [dbo].[TimeCardPeriodsTb] where TimeCardSunday >= @StarteDate or TimeCardSaturday >= @StarteDate) tcp
			ORDER BY tcp.[TimeCardSunday]

			select distinct @ProjTimecardApprovedflag = ISNULL(TimeCardApprovedFlag, 0), @ProjTimecardPMApprovedFlag = ISNULL(TimecardPMapprovedFlag, 0) 
			from TimeCardTb 
			where TimeCardProjectID = @ProjectID
				and TimeCardStartDate = @StarteDate
				and (TimeCardApprovedFlag = 1 or TimecardPMapprovedFlag = 1)

			Update #NewTimeCards
			Set TimeCardApprovedFlag = @ProjTimecardApprovedflag
			, TimecardPMapprovedFlag = @ProjTimecardPMApprovedFlag
			Where TimeCardStartDate = @StarteDate


			Insert Into TimeCardTb
			(
				TimeCardStartDate, 
				TimeCardEndDate, 
				TimeCardProjectID, 
				TimeCardTechID, 
				TimeCardActiveFlag,
				TimeCardApprovedFlag, 
				TimeCardApprovedBy, 
				TimeCardArchiveFlag, 
				TimeCardCreateDate, 
				TimeCardCreatedBy,
				TimeCardPMApprovedFlag

			)
			Select NewTC.* from #NewTimeCards NewTC
			Left Join TimeCardTb TC on TC.TimeCardProjectID = NewTC.TimeCardProjectID 
										and TC.TimeCardTechID = NewTC.TimeCardTechID 
										and Cast(TC.TimeCardStartDate as date) = Cast(NewTC.TimeCardStartDate as Date)
			Where TC.TimeCardID is null

			Drop Table #NewTimeCards

		COMMIT TRAN INSRT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN INSRT
			RAISERROR(7770001, 10, 1)
		END
	END CATCH
	
--CLOSE TEST
--DEALLOCATE TEST


--END
















GO


