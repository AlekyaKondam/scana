USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spReactivateUser]    Script Date: 4/23/2019 1:31:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spReactivateUser]
(
	@JSON_Str VarChar(Max)

)
AS

Set NOCOUNT ON

	Declare @UserName varchar(25)
		,@ProjectURLPrefix varchar(50)
		,@SingleQuote char(1) = CHAR(39)
		,@NameObjectID int
BEGIN TRY
	Select * Into #JSON_Parse From [dbo].[parseJSON](@JSON_Str)

	Select @ProjectURLPrefix = ISNULL((Select StringValue From #JSON_Parse Where Name = 'ProjectUrlPrefix'), '')

	Select @NameObjectID = ISNULL((Select StringValue From #JSON_Parse Where Name = 'Usernames'), '')

	Declare objProcessingObj Cursor For
	Select StringValue From #JSON_Parse Where parent_ID = @NameObjectID

	Open objProcessingObj

	Fetch Next From objProcessingObj Into @UserName

	While @@FETCH_STATUS = 0
	BEGIN 

		--Update UserTb 
		--set UserActiveFlag = 1
		--, UserInActiveFlag = 0
		--, UserInactiveDTLT = NULL
		--, UserInactiveDTLTOffset = NULL
		--Where UserName = @UserName

		EXEC dbo.spCTReactivateUser @UserName, @ProjectURLPrefix
		 
		Fetch Next From objProcessingObj Into @UserName

	END

	Close objProcessingObj
	Deallocate objProcessingObj
END TRY
BEGIN CATCH
  THROW;
END CATCH

Set NOCOUNT OFF



GO


