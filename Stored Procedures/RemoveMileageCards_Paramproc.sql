USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[RemoveMileageCards_Paramproc]    Script Date: 4/23/2019 1:22:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*****************************************************************************************************/
/*****************************************************************************************************/
/*		Procedure Name:		RemoveTimeCards_Paramproc												 */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		02/02/2016																 */
/*		Create Reason:		Remove All MileageCards For a User From Mileage Card Table			         */
/*		Prerequisite:		Gather the userid to be removed										     */
/*		Parameters(1):		@Parameter1 int = userid                                                 */
/*		Utilized By:		MileageCardTb table   
         Modified By :		Alekya Kondam
         Modified Date:		03/29/2019	
          Modifications made : added Exceptional handling                                         */
/******************************************************************************************************/
/******************************************************************************************************/
CREATE PROCEDURE [dbo].[RemoveMileageCards_Paramproc] @Parameter1 INT
AS
  BEGIN try
      DELETE FROM [dbo].[mileagecardtb]
      WHERE  [mileagecardtechid] >= @Parameter1
  END try

  BEGIN catch
      THROW;
  END catch


GO


