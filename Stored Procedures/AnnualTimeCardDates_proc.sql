USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[AnnualTimeCardDates_proc]    Script Date: 4/23/2019 1:17:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/*****************************************************************************************************/
/*****************************************************************************************************/

/*		Procedure Name:		AnnualTimeCardDates_proc												 */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		01/21/2016																 */
/*		Create Reason:		Create the Employee Pay Periods for the Year(s)	

        Modified By :		Alekya Kondam
         Modified Date:		03/29/2019	
         Modifications made : added Exceptional handling 							 */
/*		Prerequisite:		Gather the first Sunday of the Year										 */
/*		Parameters(2):		@startdate1 = 1st sunday of the year, @enddate1 = (@startdate1 + 6 days) */
/*		Utilized By:		TimeCardTb table and MileageCardTb table								 */

/******************************************************************************************************/
/******************************************************************************************************/


CREATE Procedure [dbo].[AnnualTimeCardDates_proc] @startdate1 date, @enddate1 date

as

declare @startdate date set @startdate = @startdate1
declare @enddate date set @enddate = @enddate1 ;

BEGIN TRY

with [dates] as (
    select convert(datetime, @startdate) as TimeCardStartDate,  dateadd(day, 6, convert(datetime, @startdate)) as TimeCardEndDate 
    union all
    select dateadd(day, 7, [TimeCardStartDate]),  dateadd(day, 7, [TimeCardEndDate])
    from [dates]
    where [TimeCardStartDate] < @enddate --end
)
select TimeCardStartDate, TimeCardEndDate 
from [dates]
where [TimeCardStartDate] between @startdate and @enddate
option (maxrecursion 0)

/*


execute AnnualTimeCardDates '01/03/2016', '01/01/2017'

declare @startdate date
declare @enddate date --example 
set @startdate = '01/03/2016'
set  @enddate = '01/02/2017';

with [dates] as (
    select convert(datetime, @startdate) as TimeCardStartDate,  dateadd(day, 6, convert(datetime, @startdate)) as TimeCardEndDate 
    union all
    select dateadd(day, 7, [TimeCardStartDate]),  dateadd(day, 7, [TimeCardEndDate])
    from [dates]
    where [TimeCardStartDate] < @enddate --end
)
select TimeCardStartDate, TimeCardEndDate 
from [dates]
where [TimeCardStartDate] between @startdate and @enddate
option (maxrecursion 0)*/

END TRY
BEGIN CATCH
  THROW;
END CATCH

GO


