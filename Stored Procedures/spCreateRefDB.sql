USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spCreateRefDB]    Script Date: 4/23/2019 1:25:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE Procedure [dbo].[spCreateRefDB]

AS 

BEGIN TRY
--- [spUpdateFromAzure]  gets New ProjectTypes , New Clients, New Projects, New Task,New PaySource, New Taks and Project Association, New Chart of Accounts from AzureRefDB  
--  [spUpdateUsers] gets Users from Azure RefDB 

Execute [spUpdateFromAzure] --- ADD new Db name if not exist in this SP
Execute [spUpdateUsers]


END TRY
BEGIN CATCH
  THROW;
END CATCH




GO


