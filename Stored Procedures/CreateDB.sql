USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[CreateDB]    Script Date: 4/23/2019 1:17:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*****************************************************************************************************/
/*****************************************************************************************************/

--		Procedure Name:		CreateDB												
--		Created By:															 
--		Create Date:																		
--		Create Reason:			

--         Modified By :		Alekya Kondam
 --        Modified Date:		03/29/2019	
--         Modifications made : added Exceptional handling 							 


/******************************************************************************************************/
/******************************************************************************************************/

CREATE proc [dbo].[CreateDB] @dbname sysname
as
declare @sql nvarchar(max)
BEGIN TRY
set @sql = 'create database ' + QUOTENAME(@dbname)
exec (@sql)
END TRY
BEGIN CATCH
  THROW;
END CATCH

GO


