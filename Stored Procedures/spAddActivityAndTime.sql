USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spAddActivityAndTime]    Script Date: 4/23/2019 1:25:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spAddActivityAndTime]
(
	@TimeCardID int
	,@TaskName varchar(100) 
	,@Date varchar(20)
	,@StartTime varchar(20)
	,@EndTime varchar(20)
	,@CreatedByUserName varchar(50)
	,@ChargeOfAccountType varchar(50)
	
)
AS

/*

Exec spAddActivityAndTime 91323, 'SHOP WORK & REPAIRS:Hawk Servicing', '1/25/2018', '07:00', '16:00', 'GEWF'


*/

Set NOCOUNT ON
BEGIN TRY

--Before we start any thing need to save what was passed to the SP

	Insert into [dbo].[tSPInsertedData]
	(SPName, InsertedData )
	Values
	('spAddActivityAndTime', 'Time Card ID: ' + ISNULL(Cast(@TimeCardID as varchar(20)), 'NULL') 
	     + ', Task Name: ' + ISNULL(@TaskName, 'NULL')  
		 + ', Date: ' + ISNULL(@Date, 'NULL')  
		 + ', Start Time: ' + ISNULL(@StartTime, 'NULL')  
		 + ', End Time: ' + ISNULL(@EndTime, 'NULL')  
		 + ', Created By: ' + ISNULL(@CreatedByUserName, 'NULL')  
		 + ', Charge Of Account Type: ' + ISNULL(@ChargeOfAccountType, 'NULL') 
		 )
	



	Declare @Next_ID Int
		,@UserID int
		,@CreatedUserID int
		,@ProjectID int
		,@StartDatetime datetime = @Date + ' ' + @StartTime
		,@EndDatetime datetime = @Date + ' ' + @EndTime
		,@SourceID varchar(50) = 'spAddActivityAndTime'
		,@RandomNum  Decimal(18,18) = Rand()
		,@ActivityUID varchar(200)
		,@NewUnipueID varchar(50)
		,@CurrentDateTime datetime = getdate()
		,@Comments varchar(200) = 'Manually Added by ' + @CreatedByUserName
		,@Seperator char(1) = '_'
		,@maxRandomValue int = 10
		,@minRandomValue int = 0
		,@UserName varchar(20)
		,@WeekDay varchar(20) = DateName( dw, @Date)

	
If EXISTS (Select * from TimeCardTb where TimeCardID = @TimeCardID)
BEGIN


	select @UserID = TimeCardTechID  from TimeCardtb where timecardid = @TimeCardID
	
	Select  @CreatedUserID = UserID from UserTb where UserName = @CreatedByUserName
	
	select @ProjectID = TimeCardProjectID, @UserID = TimeCardTechID from TimeCardTb where timecardid = @TimeCardID

	Select @UserName = UserName from UserTb where UserID = @UserID
	
	select @Next_ID = IDENT_CURRENT('ActivityTb') + 1

	Set @Next_ID = ISNULL(@Next_ID, 1)

	Select @NewUnipueID = REPLACE(Cast(Cast(((@maxRandomValue + 1) - @minRandomValue) 
	* @RandomNum + @minRandomValue As decimal(7,4)) * Cast(@Next_ID as int) as varchar(20)), '.', '')

	SELECT @ActivityUID = @TaskName + '_' 
		+ Ltrim(Rtrim(@NewUnipueID)) 
		+ @Seperator
		+ Cast(DatePart(year, @CurrentDateTime) as char(4)) 
		+ Right('00' + Ltrim(rtrim(Cast(datepart(Month, @CurrentDateTime) as Char(2)))),2)
		+ Right('00' + Ltrim(rtrim(Cast(datepart(Day, @CurrentDateTime) as Char(2)))),2)
		+ Right('00' + Ltrim(rtrim(Cast(datepart(Hour, @CurrentDateTime) as Char(2)))),2)
		+ Right('00' + Ltrim(rtrim(Cast(datepart(Minute, @CurrentDateTime) as Char(2)))),2)
		+ Right('00' + Ltrim(rtrim(Cast(datepart(Second, @CurrentDateTime) as Char(2)))),2)
		+ @Seperator
		+ LTrim(RTrim('WEB'))

	Insert Into ActivityTb
	(
		ActivityStartTime, 
		ActivityEndTime, 
		ActivityTitle, 
		ActivityCode, 
		ActivityPayCode, 
		ActivityArchiveFlag, 
		ActivityCreateDate, 
		ActivityCreatedUserUID, 
		ActivityUID, 
		ActivityProjectID, 
		ActivitySourceID,
		ActivitySrcDTLT,
		ActivityComments,
		ActivityAppVersion, 
		ActivityAppVersionName
		
	)
	Values
	(
		@StartDatetime, --ActivityStartTime, 
		@EndDatetime, --ActivityEndTime, 
		@TaskName, --ActivityTitle, 
		0, --ActivityCode, 
		0, --ActivityPayCode, 
		0 , --ActivityArchiveFlag, 
		@CurrentDateTime, --ActivityCreateDate, 
		@CreatedByUserName, --ActivityCreatedUserUID, 
		@ActivityUID, --ActivityUID, 
		@ProjectID, --ActivityProjectID, 
		@SourceID, --ActivitySourceID,
		@CurrentDateTime, --ActivitySrcDTLT,
		@Comments, --ActivityComments,
		@SourceID, --ActivityAppVersion, 
		'LIGHTHOUSE' --ActivityAppVersionName
	)

	Insert Into TimeEntryTb
	(
		TimeEntryStartTime, 
		TimeEntryEndTime, 
		TimeEntryTimeCardID, 
		TimeEntryActivityID, 
		TimeEntryComment, 
		TimeEntryCreatedBy, 
		TimeEntryUserName, 
		TimeEntrySrcDTLT, 
		TimeEntryActiveFlag, 
		TimeEntryChartOfAccount
	)
	Values	
	(
		@StartDatetime, --TimeEntryStartTime, 
		@EndDatetime, --TimeEntryEndTime, 
		@TimeCardID, --TimeEntryTimeCardID, 
		@Next_ID, --TimeEntryActivityID, 
		@Comments, --TimeEntryComment, 
		@CreatedByUserName, --TimeEntryCreatedBy, 
		@UserID, --TimeEntryUserName, 
		@CurrentDateTime, --TimeEntrySrcDTLT, 
		1, --TimeEntryActiveFlag, 
		@ChargeOfAccountType --TimeEntryChartOfAccount
	)

END




END TRY
BEGIN CATCH
  THROW;
END CATCH


Set NOCOUNT OFF









GO


