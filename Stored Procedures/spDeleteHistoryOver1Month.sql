USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteHistoryOver1Month]    Script Date: 4/23/2019 1:26:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spDeleteHistoryOver1Month]

AS 

Declare @FirstOfLastMonth datetime = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0)
BEGIN TRY
delete from [history].[HistoryAuth_Assignment] where DateAdded < @FirstOfLastMonth
END TRY
BEGIN CATCH
  THROW;
END CATCH


GO


