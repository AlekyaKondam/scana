USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[ActivateMileageCardByUserByMileageCard_proc]    Script Date: 4/23/2019 1:16:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[ActivateMileageCardByUserByMileageCard_proc]  @UserParam int, @MileageCardParam int
AS

/****************************************************************************************************
***************************************************************************************************
		Procedure Name:		ActivateMileageCardByUserByMileageCard 					 				 
		Created By:			Sandra Jackson, Senior DBA	
		Modified BY:        Alekya Kondam
		Create Date:		05/03/2016	
		Modified Date:																 
		Create Reason:		Activate Mileage Cards by Use By MileageCard Id	 
		Changes Mades:       Added Exceptional Handling to SP        
		Prerequisite:		None																	 
		Utilized By:		CometTracker App, Supervisor, Admin										 
		Version:			1.0																		 
		Modified:			1.0 = creation 
							
		Status:				Active or Inactive					 
																									 
*****************************************************************************************************/
BEGIN TRY
UPDATE [CometTracker].[dbo].[MileageCardTb]
SET [MileageCardActiveFlag] = 1
WHERE [MileageCardTechID] = @UserParam
and [MileageCardID] = @MileageCardParam
END TRY
BEGIN CATCH
   THROW;
END CATCH


GO


