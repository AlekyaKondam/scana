USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptGenerateQBDummyPayrollByProject]    Script Date: 4/24/2019 9:10:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










CREATE Procedure [dbo].[spRptGenerateQBDummyPayrollByProject](@WeekStartingDate date, @WeekEndingDate date, @ProjectIDJSON varchar(MAX)= NULL)
AS


Create Table #TimeCard
(
	[Employee Id] varchar(50) NULL    
	,[Class (Department)] varchar(50) NULL
	,[Transaction Date] Date NULL
	,[Client Code] varchar(50) NULL
	,[Service Items] varchar(50) NULL
	,[Payroll Item]  varchar(100) NULL
	,[ExpensePayAccount]  varchar(50) NULL
	,[ExpenseChargeAccount] varchar(50) NULL
	,[Hours to Import] varchar(50) NULL
	,[Billable]  char(1) NULL
	,[ReferenceNumber]  int NULL
	,[First Name] Varchar(100) NULL
	,[Last Name] varchar(100) NULL
)



/*

<All>
1088
1108


		Exec spRptGenerateQBDummyPayrollByProject    '2018-05-1', '2018-05-19', '["2787"]'
		Exec spRptGenerateQBDummyPayrollByProject    '2018-05-1', '2018-05-19', NULL
	    Exec spRptGenerateQBDummyPayrollByProject    '2018-05-13', '2018-05-19','<All>'


	select  timecardsubmittedoasis, TimeCardSubmttedQuickBooks, TimeCardSubmttedADP, * 
	from timecardtb 
	where timecardprojectid = '1423' and timecardstartdate >= '2018-02-11' and timecardenddate <= '2018-02-24'

	update timecardtb 
	Set timecardsubmittedoasis = NULL, TimeCardSubmttedQuickBooks = NULL, TimeCardSubmttedADP = NULL
	where timecardprojectid = '1423' and timecardstartdate >= '2018-02-11' and timecardenddate <= '2018-02-24'

*/

BEGIN


IF @ProjectIDJSON is null
	BEGIN

		--select @WeekStartingDate = dateadd(d, -datepart(dw, @WeekStartingDate)+ 1, @WeekStartingDate)

		--select @WeekEndingDate = dateadd(d, 7-datepart(dw, @WeekEndingDate), @WeekEndingDate)

		SELECT  -- 20160111 Added
		'<All>' AS Drop_Down, 'ALL' AS Project_Name
		INTO #DD		
		UNION ALL		
		select distinct cast([ProjectID] as varchar(20)) as [DropDown] , [ProjectName] As [Project_Name] 
		from fnGeneratePayrollDataByProject('<All>', @WeekStartingDate, @WeekEndingDate, 'QB', 'Report')
		

    	SELECT Drop_Down, Project_Name FROM #DD -- DROP TABLE #DD

	END
ELSE


 select 
	[Employee Id] as [EMP ID],  [Class (Department)] , 	[Transaction Date],	[Client Code] ,[Service Items] ,[Payroll Item]  ,[ExpensePayAccount] as [EXP Pay ACC] ,[ExpenseChargeAccount] as [EXP Charge ACC],
	 Format((Cast(Sum([Hours to Import]) as float)) / Cast(3600 as float), '0.00') as [Hours to Import] 
	 ,[Billable]  , [First Name] + ' ' +  [Last Name] as [Employee Name]	
	 ,[ProjectName], [ProjectID]
 from fnGeneratePayrollDataByProject(@ProjectIDJSON, @WeekStartingDate, @WeekEndingDate, 'QB', 'Report')
 group by [Employee Id],  [Class (Department)] , 	[Transaction Date],	[Client Code] ,[Service Items] ,[Payroll Item]  ,[ExpensePayAccount]  ,[ExpenseChargeAccount] 
  ,[Billable]  ,[First Name] + ' ' +  [Last Name] ,[ProjectName], [ProjectID]

	
END






































GO


