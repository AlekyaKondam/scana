USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spDifferentDayList]    Script Date: 4/23/2019 1:27:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spDifferentDayList]
AS

Declare @DifferntDayActivityList varchar(max) = NULL
	,@EmailSubject varchar(100) = 'Activity With Different Days'
	,@EmailReportName varchar(100) = 'DifferentDay'
	,@EmailBody varchar(max) = 'The following individuals had activities with the Start Date and End Date on Different Days.'
	,@CRLF varchar(10) = char(13) + char(10)
	,@ProjectID int = 0
	,@ExpectedVersion varchar(10)
	,@EmailTO varchar(max)
	,@EmailCC varchar(max)

	BEGIN TRY
	Select @DifferntDayActivityList = coalesce(@DifferntDayActivityList + @CRLF,'') + u.UserLastName + ', ' + u.UserFirstName + ' (' + u.UserName + ')' + ' - ' + a.ActivityTitle +', ' + a.ActivityStartTime + ', ' + a.ActivityEndTime
	From UserTb u
	Join [dbo].[vActivityWithDifferntDays] a on a.ActivityCreatedUserUID = u.UserName

	IF @DifferntDayActivityList is not NULL
	BEGIN

		Exec [dbo].[spGetEmailList] @EmailReportName, @ProjectID, @EmailTO OUTPUT, @EmailCC OUTPUT
				
		Set @EmailBody = @EmailBody + @CRLF + @CRLF + @DifferntDayActivityList

		exec [dbo].[spSendEmail] @EmailTO, @EmailCC, @EmailSubject, @EmailBody

	END

END TRY
BEGIN CATCH
  THROW;
END CATCH







GO


