USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[Refresh_Proc]    Script Date: 4/23/2019 1:22:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*****************************************************************************************************/
/*****************************************************************************************************/
/*		Procedure Name:	    Refresh_Proc aka Refresh_DeleteAllFromSelectCometTrackerTables_proc	     */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		07/14/2016																 */
/*		Create Reason:		Delete From Select Comet Tracker Tables									 */
/*		Parameter(1):		User ID integer															 */
/*		Utilized By:		CometTracker App, Supervisor, Admin									     */
/*		Version :			1 .. Creation															 */
/*		 Modified By :		Alekya Kondam
         Modified Date:		03/29/2019	
         Modifications made : added Exceptional handling 																							 */
/*		Status:				MANUAL																	 */
/*-- example				exec Refresh_Proc 162, 34												 */
/*																									 */
/******************************************************************************************************/
/******************************************************************************************************/
CREATE PROCEDURE [dbo].[Refresh_Proc] @PARAMETER1 INT,
                                     @PARAMETER2 INT
AS
  BEGIN try
      INSERT INTO [history].[historylogtb]
                  ([logdata],
                   [logdetail],
                   [logdatetime])
      VALUES     ('Refresh_Proc STEP 1 starting',
                  @PARAMETER1,
                  Getdate())

      --1
      DELETE FROM [dbo].[activitytb]
      WHERE  [activityid] IS NOT NULL

      --2
      DELETE FROM [dbo].[assettb]
      WHERE  [assetid] IS NOT NULL

      --3
      DELETE FROM [dbo].[authtb]
      WHERE  [authuserid] IS NOT NULL--2

      --4
      DELETE FROM [dbo].[breadcrumbtb]
      WHERE  [breadcrumbid] IS NOT NULL

      --5
      DELETE FROM [dbo].[clientpathtb]
      WHERE  [clientpathid] IS NOT NULL

      -- 6
      --7
      DELETE FROM [dbo].[departmenttb]
      WHERE  [departmentid] IS NOT NULL

      --8
      DELETE FROM [dbo].[employeeoqtaskhistorytb]
      WHERE  [empoqtaskhistid] IS NOT NULL--2

      --9
      DELETE FROM [dbo].[employeetb]
      WHERE  [employeeid] IS NOT NULL

      --10
      DELETE FROM [dbo].[equipmenttb]
      WHERE  [equipmentid] IS NOT NULL

      --10 A
      PRINT 'STEP 10 A CLIENT'

      DELETE FROM [dbo].[clienttb]
      WHERE  [clientaccountid] IS NOT NULL

      --11
      DELETE FROM [dbo].[equipmentcalibrationtb]
      WHERE  [equipmentcalibrationid] IS NOT NULL

      --12
      DELETE FROM [dbo].[equipmenthistorytb]
      WHERE  [equipmenthistoryid] IS NOT NULL

      --13
      DELETE FROM [dbo].[inspectionstb]
      WHERE  [inspectionsid]IS NOT NULL

      --14
      PRINT 'STEP 14 KEY'

      DELETE FROM [dbo].[keytb]
      WHERE  [keyid] <> @PARAMETER2 --34

      --15
      DELETE FROM [dbo].[latelogintb]
      WHERE  [lateloginuserid] <> @PARAMETER1

      --16
      DELETE FROM [dbo].[logintb]
      WHERE  [loginuserid] <> @PARAMETER1 --162

      --17
      DELETE FROM [dbo].[mileagecardtb]
      WHERE  [mileagecardid] IS NOT NULL

      --18
      DELETE FROM [dbo].[mileageentrytb]
      WHERE  [mileageentryid] IS NOT NULL

      --19
      DELETE FROM [dbo].[oqtasktb]
      WHERE  [oqtaskid] IS NOT NULL

      --20
      DELETE FROM [dbo].[pipelinetb]
      WHERE  [pipelineid] IS NOT NULL

      --21
      DELETE FROM [dbo].[project_user_tb]
      WHERE  [projuserid] IS NOT NULL

      --22
      DELETE FROM [dbo].[projectoqrequirementstb]
      WHERE  [projectoqrequirementsid] IS NOT NULL

      --23
      --DELETE FROM [dbo].[ProjectTb]  NEED 1 PROJECT FOR PROJECT MENUS TO WORK
      --      WHERE [ProjectID] is not null
      --24
      DELETE FROM [dbo].[reversegeolookuptb]
      WHERE  [reversegeoid] IS NOT NULL

      --25
      DELETE FROM [dbo].[surveytypetb]
      WHERE  [surveytypeid] IS NOT NULL

      --26
      DELETE FROM [dbo].[timecardtb]
      WHERE  [timecardid] IS NOT NULL

      --27
      DELETE FROM [dbo].[timeentrytb]
      WHERE  [timeentryid]IS NOT NULL

      --28
      DELETE FROM [dbo].[userstatustb]
      WHERE  [userstatusid] IS NOT NULL

      --29
      DELETE FROM [dbo].[usertb]
      WHERE  [userid] <> @PARAMETER1

      --30
      DELETE FROM [rbac].[auth_assignment]
      WHERE  [item_name] IS NOT NULL

      --31
      DELETE FROM [rbac].[auth_item]
      WHERE  [name] IS NOT NULL

      --32
      DELETE FROM [rbac].[auth_item_child]
      WHERE  [parent] IS NOT NULL

      --33
      DELETE FROM [rbac].[auth_rule]
      WHERE  [name] IS NOT NULL

      INSERT INTO [history].[historylogtb]
                  ([logdata],
                   [logdetail],
                   [logdatetime])
      VALUES     ('Refresh_Proc STEP 32 ending',
                  @PARAMETER1,
                  Getdate())
  END try

  BEGIN catch
      THROW;
  END catch


GO


