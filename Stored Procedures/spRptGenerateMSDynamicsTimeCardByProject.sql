USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptGenerateMSDynamicsTimeCardByProject]    Script Date: 4/24/2019 9:09:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE Procedure [dbo].[spRptGenerateMSDynamicsTimeCardByProject](@WeekStartingDate date, @WeekEndingDate date, @ProjectIDJSON varchar(MAX)= NULL)
AS


Create Table #TimeCard
(
	[Employee Id] varchar(50) NULL    
	,[Class (Department)] varchar(50) NULL
	,[Transaction Date] Date NULL
	,[Client Code] varchar(50) NULL
	,[Service Items] varchar(50) NULL
	,[Payroll Item]  varchar(100) NULL
	,[ExpensePayAccount]  varchar(50) NULL
	,[ExpenseChargeAccount] varchar(50) NULL
	,[Hours to Import] varchar(50) NULL
	,[Billable]  char(1) NULL
	,[Employee Name]  varchar(200) NULL
	,[Project Name] Varchar(100) NULL
	,[Project ID] int NULL
)



/*


*/

BEGIN


IF @ProjectIDJSON is null
	BEGIN

		--select @WeekStartingDate = dateadd(d, -datepart(dw, @WeekStartingDate)+ 1, @WeekStartingDate)

		--select @WeekEndingDate = dateadd(d, 7-datepart(dw, @WeekEndingDate), @WeekEndingDate)

		SELECT  -- 20160111 Added
		'<All>' AS Drop_Down, 'ALL' AS Project_Name
		INTO #DD		
		UNION ALL		
		select distinct cast([ProjectID] as varchar(20)) as [DropDown] , [ProjectName] As [Project_Name] 
		from fnGeneratePayrollDataByProject('<All>', @WeekStartingDate, @WeekEndingDate, 'MSDynamics', 'Report')
		

    	SELECT Drop_Down, Project_Name FROM #DD -- DROP TABLE #DD

	END
ELSE
	BEGIN

		Insert Into #TimeCard
	 (
 		[Employee Id], [Class (Department)], [Transaction Date], [Client Code], [Service Items], [Payroll Item], [ExpensePayAccount], [ExpenseChargeAccount], [Hours to Import], [Billable], [Employee Name], [Project Name], [Project ID]
	  )
		 select 
			[Employee Id] as [EMP ID],  [Class (Department)] , 	[Transaction Date],	[Client Code] ,[Service Items] ,[Payroll Item]  ,[ExpensePayAccount] as [EXP Pay ACC] ,[ExpenseChargeAccount] as [EXP Charge ACC],
			 Format((Cast(Sum([Hours to Import]) as float)) / Cast(3600 as float), '0.00') as [Hours to Import] 
			 ,[Billable]  , [First Name] + ' ' +  [Last Name] as [Employee Name]	
			 ,[ProjectName], [ProjectID]
		 from fnGeneratePayrollDataByProject(@ProjectIDJSON, @WeekStartingDate, @WeekEndingDate, 'MSDynamics', 'Report')
		 Where  [Employee Id] is not NULL
		 group by [Employee Id],  [Class (Department)] , 	[Transaction Date],	[Client Code] ,[Service Items] ,[Payroll Item]  ,[ExpensePayAccount]  ,[ExpenseChargeAccount] 
		  ,[Billable]  ,[First Name] + ' ' +  [Last Name] ,[ProjectName], [ProjectID]

	
	
		DECLARE @CountTest INT = (Select count(*) From #TimeCard)

		IF @CountTest = 0
		BEGIN
 
			SELECT 'No data returned for the date(s) provided' AS [Report Alert]  
            
		END
		Else
		BEGIN

			Select * from #TimeCard

		END

	END

	Drop Table #TimeCard	

END










































GO


