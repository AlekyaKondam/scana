USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptEmployeeProductivitySummary]    Script Date: 4/24/2019 9:08:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spRptEmployeeProductivitySummary]
(
	@StartDate			DATE	= NULL
	,@EndDate			DATE	= NULL
	,@UserName         varchar(200) = NULL     
 
)
AS

Set NOCOUNT ON

/*

	Testing
	Exec spRptEmployeeProductivitySummary '4/1/2019', '4/1/2019', '<All>'
	Exec spRptEmployeeProductivitySummary '11/25/2018', '12/1/2018', 'Technician, SouthernCross (scctTech) [SOUTHERN CROSS:CT2]'
	

*/

Declare @TimeCardID int = 0
	,@UserID int
	,@UserShortName varchar(20)
	,@ProjectName varchar(200)
	,@ProjectID int


BEGIN
BEGIN TRY
Select
@StartDate =  DateAdd(d, - (DatePart(dw, @StartDate) -1), @StartDate)
,@EndDate  =  DateAdd(d, 6, DateAdd(d, - (DatePart(dw, @StartDate) -1), @StartDate))

IF ISNULL(@UserName, 'All') Like '%All%' or @UserName = 'No data returned for the date(s) provided'
	BEGIN

		--SELECT  -- 20160111 Added
		--'<All>' AS Drop_Down1, 
        -- 'ALL' AS Drop_Down2
		--INTO #DD
		
		--UNION ALL
		
		Create Table #DD
		(
			[Drop_Down1] varchar(200) NULL
			,[Drop_Down2] varchar(200) NULL    
		)
		
		Insert Into #DD


		
		Select Distinct u.UserLastName + ', ' + u.UserFirstName +' (' + u.UserName + ')' + ' [' + ProjectName + ']' As [Drop_Down1]
		, u.UserLastName + ', ' + u.UserFirstName +' (' + u.UserName + ')' + ' [' + ProjectName + ']' As [Drop_Down2]
		--INTO #DD
		From (Select * from UserTb where UserAppRoleType like '%Technician%') u
		Join (Select Distinct ActivityCreatedUserUID, p.ProjectName
			from ActivityTb a
			Join TimeEntryTb te on te.TimeEntryActivityID = a.ActivityID
			Join TimeCardTb tc on te.TimeEntryTimeCardID = tc.TimeCardID
			Join ProjectTb p on tc.TimeCardProjectID = p.ProjectID
			where Cast(ActivityStartTime as date) between @StartDate and @EndDate
			And (ActivityTitle like '%task%' or ActivityTitle in ('AdminActivity', 'Break2Activity', 'LunchActivity','BreakActivity')) and ActivityTitle  not like '%TaskOut%'
			and ActivityCreatedUserUID is not null) a on a.ActivityCreatedUserUID = Case WHEN ISNUMERIC(a.ActivityCreatedUserUID) = 1 THEN cast(u.UserID as varchar(50)) ELSE u.UserName END

		ORDER BY 1

		IF (Select Count(*) from #DD) = 0
		BEGIN

			Insert Into #DD
			VALUES ('No data returned for the date(s) provided','No data returned for the date(s) provided')

		END
		
		SELECT Drop_Down1 , Drop_Down2 FROM #DD
		DROP TABLE #DD

	END
	ELSE
	BEGIN

		select @UserShortName = SUBSTRING(@UserName, CHARINDEX('(', @UserName) + 1, CHARINDEX(')', @UserName) - (CHARINDEX('(', @UserName) + 1))

		select @ProjectName = SUBSTRING(@UserName, CHARINDEX('[', @UserName) + 1, CHARINDEX(']', @UserName) - (CHARINDEX('[', @UserName) + 1))

		select @ProjectID = ProjectID from ProjectTb where ProjectName = @ProjectName

		select @UserID = UserID from UserTb where UserName = @UserShortName

		
		
		Select @timecardid = TimeCardid 
		From 
		(select Distinct TimeCardID from TimeCardTb TC  
		join TimeEntryTb TE on te.TimeEntryTimeCardID = tc.TimeCardID
		where TimeCardTechID = @UserID and TimeCardStartDate = @StartDate and TimeCardProjectID = @ProjectID) Source

		

		--select @timecardid =252881

		--select @timecardid

		IF @TimeCardID > 0
		BEGIN

		
		/*
			select 
				@UserID = u.UserName
				,@StartDate = Cast(tc.TimeCardStartDate as date)
				,@EndDate = Cast(tc.TimeCardEndDate as date)
			from TimeCardTb tc
			Join UserTb u on u.UserID = tc.TimeCardTechID
				where TimeCardID = @TimeCardID
		*/
		
				select Distinct
					a.ActivityUID 
				Into #ActivityUID
				from 
					(Select * from [dbo].[TimeEntryTb] where TimeEntryTimeCardID = @TimeCardID and TimeEntryActiveFlag = 1) t
					 Join (select * from ActivityTb where (ActivityTitle like '%Task%' or ActivityTitle in ('AdminActivity', 'Break2Activity', 'LunchActivity','BreakActivity')) and ActivityTitle  not like '%TaskOut%') a on t.TimeEntryActivityID = a.ActivityID
						
		
				;With cteBC
				AS	
				(Select BreadcrumbID, BreadcrumbActivityType, BreadcrumbActivityUID, BreadcrumbSrcDTLT, BreadcrumbLatitude, BreadcrumbLongitude, SpeedAttribute, DistanceTraveled, Cast(BreadcrumbSrcDTLT as date) BCDate, Row_Number() Over (order By BreadcrumbSrcDTLT) RowID From BreadcrumbTb where 
				BreadcrumbCreatedUserUID = @UserShortName
				and Cast(BreadcrumbSrcDTLT as date) between @StartDate and @EndDate)
				Select 
				ctebc1.BreadcrumbActivityType
				,ctebc1.SpeedAttribute
				,ctebc1.BCDate
				,CASE WHEN ctebc1.BCDate = ctebc2.BCDate THEN DATEDIFF(s, ctebc2.BreadcrumbSrcDTLT, ctebc1.BreadcrumbSrcDTLT) ELSE 0 END TotalSecBetweenBC
				--,CASE WHEN ctebc1.BCDate = ctebc2.BCDate THEN [dbo].[fnGetDistance](cteBC1.BreadcrumbLatitude, cteBC1.BreadcrumbLongitude, cteBC2.BreadcrumbLatitude, cteBC2.BreadcrumbLongitude, 'Feet') ELSE 0 END FeetBetweenBC
				,ctebc1.DistanceTraveled * 3.28 FeetBetweenBC
				Into #BCData
				From cteBC cteBC1
				Left Join cteBC cteBC2 on cteBC1.RowID - 1 = cteBC2.RowID
				 where
					cteBC1.BreadcrumbActivityUID in
				 (select ActivityUID from #ActivityUID)
				

			Select SpeedAttribute [Type]
			,CASE WHEN BreadcrumbActivityType not in ('BreakActivity', 'LunchActivity') THEN TOTALSec END [Production]
			,CASE WHEN BreadcrumbActivityType in ('BreakActivity', 'Break2Activity') THEN TOTALSec END [Break]
			,CASE WHEN BreadcrumbActivityType in ('LunchActivity') THEN TOTALSec END [Lunch]
			Into #Summary
			From 

			(
			select BreadcrumbActivityType, SpeedAttribute, Sum(TotalSecBetweenBC) [TotalSec] from #BCData
			Group By BreadcrumbActivityType, SpeedAttribute
			) BC
		

			Union Select
			'StandStill' [SpeedAttribute], 0, 0, 0

			Union Select
			'Walk' [SpeedAttribute], 0, 0, 0

			Union Select
			'Drive' [SpeedAttribute], 0, 0, 0


			DECLARE @CountTest INT = (Select count(*) From #Summary)
			
			IF @CountTest = 0
			BEGIN
 
				SELECT 'No data returned for the date(s) provided' AS [Report Alert]  
            
			END
			Else
			BEGIN		
		
				Select Summary.[Type]
				--,  Summary.Production 
				,CASE WHEN Summary.Production > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Summary.Production, 0), 0), 108) ELSE '0' END [Production]
				, Format(CASE WHEN ISNULL(ProdTotal.ProductionSum, 0) > 0 THEN Cast(Summary.Production as decimal(20,2)) / Cast(ProdTotal.ProductionSum as decimal(20,2)) ELSE 0 END, 'P')  [Prod %]
				,CASE WHEN Summary.[Break] > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Summary.[Break], 0), 0), 108) ELSE '0' END [Break]
				--, Summary.[Break]
				,CASE WHEN Summary.Lunch > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Summary.Lunch, 0), 0), 108) ELSE '0' END [Lunch]
				--, Summary.Lunch
				From 
				(select Type, Sum(isnull(Production, 0)) [Production], Sum(ISNULL([Break], 0)) [Break], Sum(isnull(Lunch,0)) [Lunch]
				from #Summary
				Group By Type) Summary
				Cross Join (Select Sum(isnull(Production, 0)) [ProductionSum] From #Summary) ProdTotal
				Order by
				CASE When [Type] = 'StandStill' Then 1
					When [Type] = 'Walk' Then 2
					When [Type] = 'Drive' Then 3
				END 

			END

			--select @TimeCardID, @UserShortName

			Drop Table #ActivityUID
			Drop Table #BCData
			Drop Table #Summary












		END

	END

END TRY
BEGIN CATCH
  THROW;
END CATCH

END

Set NOCOUNT OFF










GO


