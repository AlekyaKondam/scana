USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[PopulateAnnualTimeCardPeriodsTb_proc]    Script Date: 4/23/2019 1:20:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*****************************************************************************************************/
/*****************************************************************************************************/

/*  	Procedure Name:		PopulateAnnualTimeCardPeriodsTb_proc									 */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		01/25/2016																 */
/*		Create Reason:		Create the Employee Pay Periods for the Year(s)							 */
/*		Prerequisite:		Gather the first Sunday of the Year										 */
/*		Parameters(2):		@startdate1 = 1st sunday of the year, @enddate1 = (@startdate1 + 6 days) */
/*		Utilized By:		TimeCardPeriodsTb, TimeCardTb table and MileageCardTb table				 */
--							Run 1 time a year
/******************************************************************************************************/
/******************************************************************************************************/


CREATE PROCEDURE [dbo].[PopulateAnnualTimeCardPeriodsTb_proc] @startdate1 date, @enddate1 date

AS

  DECLARE @startdate date
  SET @startdate = @startdate1
  DECLARE @enddate date
  SET @enddate = @enddate1;

  BEGIN TRY

    WITH [dates]
    AS (SELECT
      CONVERT(datetime, @startdate) AS TimeCardStartDate,
      DATEADD(DAY, 6, CONVERT(datetime, @startdate)) AS TimeCardEndDate
    UNION ALL
    SELECT
      DATEADD(DAY, 7, [TimeCardStartDate]),
      DATEADD(DAY, 7, [TimeCardEndDate])
    FROM [dates]
    WHERE [TimeCardStartDate] < @enddate --end
    )
    SELECT
      TimeCardStartDate,
      TimeCardEndDate INTO TimeCardPeriodsTb
    FROM [dates]
    WHERE [TimeCardStartDate] BETWEEN @startdate AND @enddate
    OPTION (MAXRECURSION 0)


  END TRY
  BEGIN CATCH
    THROW;
  END CATCH


GO


