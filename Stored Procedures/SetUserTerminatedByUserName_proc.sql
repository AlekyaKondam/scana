USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[SetUserTerminatedByUserName_proc]    Script Date: 4/23/2019 1:24:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  /*****************************************************************************************************/
/*****************************************************************************************************/

/*		Procedure Name:		SetUserTerminatedByUserName_proc					 				     */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		01/27/2016																 */
/*		Create Reason:		Set User Active															 */
/*		Parameter(1):		User Name varchar(50)													 */
/*		Utilized By:		CometTracker App, Supervisor, Admin										 */
/*																									 */
/*		Status:				active = 1 inactive = 0 terminated = 9 pending = 2						 */
/******************************************************************************************************/
/******************************************************************************************************/


CREATE PROCEDURE [dbo].[SetUserTerminatedByUserName_proc] @PARAMETER1 VARCHAR(50)
AS
BEGIN TRY
  UPDATE [dbo].[UserTb]
  SET [UserActiveFlag] = 9
  WHERE [UserActiveFlag] <> 9
  AND [UserName] = @PARAMETER1
  END TRY
BEGIN CATCH
  THROW;
END CATCH

GO


