USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[SetUserInActiveByUserID_proc]    Script Date: 4/23/2019 1:24:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



  /*****************************************************************************************************/
/*****************************************************************************************************/

/*		Procedure Name:		SetUserActiveByUserID_proc							 				     */
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		05/11/2016																 */
/*		Create Reason:		Set User Active															 */
/*		Parameter(1):		User ID integer															 */
/*		Utilized By:		CometTracker App, Supervisor, Admin										 */
/*																									 */
/*		Status:				active = 1 inactive = 0 terminated = 9 pending = 2						 */
/******************************************************************************************************/
/******************************************************************************************************/


CREATE PROCEDURE [dbo].[SetUserInActiveByUserID_proc] @PARAMETER1 INT
AS
BEGIN TRY
  UPDATE [dbo].[UserTb]
  SET [UserActiveFlag] = 0
  WHERE [UserActiveFlag]  in ('ACTIVE')
  AND [UserID] = @PARAMETER1
END TRY
BEGIN CATCH
  THROW;
END CATCH


GO


