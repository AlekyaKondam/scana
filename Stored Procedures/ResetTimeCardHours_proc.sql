USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[ResetTimeCardHours_proc]    Script Date: 4/23/2019 1:23:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*****************************************************************************************************/
/*****************************************************************************************************/

/*		Procedure Name:		ResetTimeCardHours_proc													*/
/*		Created By:			Sandra Jackson, Senior DBA												 */
/*		Create Date:		01/22/2016																 */
/*		Deactivation Date:	03/23/2016																 */
/*		Create Reason:		Reset the Time Card For the Current User							     */
/*		Prerequisite:		User's ID																 */
/*		Parameters(1):		UserId = User's id,														 */
/*		Utilized By:		CometTracker App, Supervisor, Admin										 */

/******************************************************************************************************/
/******************************************************************************************************/


----------Deactivated----------

CREATE PROCEDURE [dbo].[ResetTimeCardHours_proc] @TECHID INT
AS/*
UPDATE [dbo].[TimeCardTb]
   SET [TimeCardHoursWorked] = 0
 WHERE [TimeCardHoursWorked] > 0
AND [TimeCardTechID] = @TECHID
AND [TimeCardApproved]  IS NULL
and GETDATE() between [TimeCardStartDate] and [TimeCardEndDate]
*/


GO


