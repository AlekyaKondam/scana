USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptTimeCardTimeDetailsByTimeCardID]    Script Date: 4/24/2019 9:11:04 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spRptTimeCardTimeDetailsByTimeCardID](@TimeCardID int = 0)
AS

/*

	Testing
	Exec spRptTimeCardTimeDetails 252881
	

*/

BEGIN
BEGIN TRY
	IF @TimeCardID > 0
	BEGIN

		Declare @UserID varchar(50)
			,@StartDate date
			,@EndDate date
			--,@TimeCardID int = 252881

			,@BCActivityType varchar(100)
			,@BCSpeedAtt varchar(100)
			,@BCSunTime varchar(20)
			,@BCMonTime varchar(20)
			,@BCTueTime varchar(20)
			,@BCWedTime varchar(20)
			,@BCThuTime varchar(20)
			,@BCFriTime varchar(20)
			,@BCSatTime varchar(20)
			,@BCSunDist varchar(20)
			,@BCMonDist varchar(20)
			,@BCTueDist varchar(20)
			,@BCWedDist varchar(20)
			,@BCThuDist varchar(20)
			,@BCFriDist varchar(20)
			,@BCSatDist varchar(20)


		select 
			@UserID = u.UserName
			,@StartDate = Cast(tc.TimeCardStartDate as date)
			,@EndDate = Cast(tc.TimeCardEndDate as date)
		from TimeCardTb tc
		Join UserTb u on u.UserID = tc.TimeCardTechID
			where TimeCardID = @TimeCardID

		Create Table #TimeCard
		(
			[Task] varchar(100) NULL    
			,[SunTime] varchar(100) NULL        
			,[SunStand] varchar(100) NULL   
			,[SunWalk] varchar(100) NULL  
			,[SunDrive] varchar(100) NULL
			,[MonTime] varchar(100) NULL        
			,[MonStand] varchar(100) NULL   
			,[MonWalk] varchar(100) NULL  
			,[MonDrive] varchar(100) NULL
			,[TueTime] varchar(100) NULL        
			,[TueStand] varchar(100) NULL   
			,[TueWalk] varchar(100) NULL  
			,[TueDrive] varchar(100) NULL
			,[WedTime] varchar(100) NULL        
			,[WedStand] varchar(100) NULL   
			,[WedWalk] varchar(100) NULL  
			,[WedDrive] varchar(100) NULL
			,[ThrTime] varchar(100) NULL        
			,[ThrStand] varchar(100) NULL   
			,[ThrWalk] varchar(100) NULL  
			,[ThrDrive] varchar(100) NULL
			,[FriTime] varchar(100) NULL        
			,[FriStand] varchar(100) NULL   
			,[FriWalk] varchar(100) NULL  
			,[FriDrive] varchar(100) NULL
			,[SatTime] varchar(100) NULL        
			,[SatStand] varchar(100) NULL   
			,[SatWalk] varchar(100) NULL  
			,[SatDrive] varchar(100) NULL
	       
		)



		
		Insert Into #TimeCard
		Values ( 
		'Task'
		 ,'Time'
		 ,'Stand'
		 ,'Walk'
		 ,'Drive'
		 ,'Time'
		 ,'Stand'
		 ,'Walk'
		 ,'Drive'
		 ,'Time'
		 ,'Stand'
		 ,'Walk'
		 ,'Drive'
		 ,'Time'
		 ,'Stand'
		 ,'Walk'
		 ,'Drive'
		 ,'Time'
		 ,'Stand'
		 ,'Walk'
		 ,'Drive'
		 ,'Time'
		 ,'Stand'
		 ,'Walk'
		 ,'Drive'
		 ,'Time'
		 ,'Stand'
		 ,'Walk'
		 ,'Drive'

		)


		Insert Into #TimeCard
		([Task]  
			,[SunTime]      
			,[MonTime]     
			,[TueTime]      
			,[WedTime]     
			,[ThrTime]     
			,[FriTime]    
			,[SatTime]  
		)
			Select
				ActivityTitle
				,CASE WHEN Sum(Day1) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day1), 0), 0), 108) ELSE '0' END
				,CASE WHEN Sum(Day2) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day2), 0), 0), 108) ELSE '0' END
				,CASE WHEN Sum(Day3) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day3), 0), 0), 108) ELSE '0' END
				,CASE WHEN Sum(Day4) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day4), 0), 0), 108) ELSE '0' END
				,CASE WHEN Sum(Day5) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day5), 0), 0), 108) ELSE '0' END
				,CASE WHEN Sum(Day6) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day6), 0), 0), 108) ELSE '0' END
				,CASE WHEN Sum(Day7) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day7), 0), 0), 108) ELSE '0' END
			From
				(Select 
					ActivityTitle
					 ,Case WHEN DatePart(dw, TimeEntryDate) = 1 THEN TotalTime ELSE 0 END [Day1]
					 ,Case WHEN DatePart(dw, TimeEntryDate) = 2 THEN TotalTime ELSE 0 END [Day2]
					 ,Case WHEN DatePart(dw, TimeEntryDate) = 3 THEN TotalTime ELSE 0 END [Day3]
					 ,Case WHEN DatePart(dw, TimeEntryDate) = 4 THEN TotalTime ELSE 0 END [Day4]
					 ,Case WHEN DatePart(dw, TimeEntryDate) = 5 THEN TotalTime ELSE 0 END [Day5]
					 ,Case WHEN DatePart(dw, TimeEntryDate) = 6 THEN TotalTime ELSE 0 END [Day6]
					 ,Case WHEN DatePart(dw, TimeEntryDate) = 7 THEN TotalTime ELSE 0 END [Day7]
				from 
		 
					 (Select ActivityTitle
								,TimeEntryDate
								,Sum(TotalSec) [TotalTime]
								--,Right('0' + Cast(Sum(TotalSec) / 3600 as varchar),2) + ':' +
									--Right('0' + Cast((Sum(TotalSec) / 60) % 60 as varchar),2) + ':' +
									--Right('0' + Cast(Sum(TotalSec) % 60 as varchar),2) [TotalTime]
								From
								(
								select 
									a.ActivityTitle
									,Cast(t.TimeEntryStartTime as date) TimeEntryDate
									, Cast(DateDiff(s, t.TimeEntryStartTime, t.TimeEntryEndTime) as bigint) [TotalSec]
								from 
											 (Select * from [dbo].[TimeEntryTb] where TimeEntryTimeCardID = @TimeCardID and TimeEntryActiveFlag = 1) t
											 Join (select * from ActivityTb where (ActivityTitle like '%Task%' or ActivityTitle in ('AdminActivity')) and ActivityTitle  not like '%TaskOut%') a on t.TimeEntryActivityID = a.ActivityID
						) Source
						Group By 
						ActivityTitle
						,TimeEntryDate
						--Having Sum(TotalSec) > 60
					) Summary) Summary2 Group By ActivityTitle


			select Distinct
				a.ActivityUID 
			Into #ActivityUID
			from 
				(Select * from [dbo].[TimeEntryTb] where TimeEntryTimeCardID = @TimeCardID and TimeEntryActiveFlag = 1) t
				 Join (select * from ActivityTb where (ActivityTitle like '%Task%' or ActivityTitle in ('AdminActivity')) and ActivityTitle  not like '%TaskOut%') a on t.TimeEntryActivityID = a.ActivityID
						
		
			;With cteBC
			AS	
			(Select BreadcrumbID, BreadcrumbActivityType, BreadcrumbActivityUID, BreadcrumbSrcDTLT, BreadcrumbLatitude, BreadcrumbLongitude, SpeedAttribute, Cast(BreadcrumbSrcDTLT as date) BCDate, Row_Number() Over (order By BreadcrumbSrcDTLT) RowID From BreadcrumbTb where 
			BreadcrumbCreatedUserUID = @UserID
			and Cast(BreadcrumbSrcDTLT as date) between @StartDate and @EndDate)
			Select 
			ctebc1.BreadcrumbActivityType
			,ctebc1.SpeedAttribute
			,ctebc1.BCDate
			,CASE WHEN ctebc1.BCDate = ctebc2.BCDate THEN DATEDIFF(s, ctebc2.BreadcrumbSrcDTLT, ctebc1.BreadcrumbSrcDTLT) ELSE 0 END TotalSecBetweenBC
			,CASE WHEN ctebc1.BCDate = ctebc2.BCDate THEN [dbo].[fnGetDistance](cteBC1.BreadcrumbLatitude, cteBC1.BreadcrumbLongitude, cteBC2.BreadcrumbLatitude, cteBC2.BreadcrumbLongitude, 'Feet') ELSE 0 END FeetBetweenBC
			Into #BCData
			From cteBC cteBC1
			Left Join cteBC cteBC2 on cteBC1.RowID - 1 = cteBC2.RowID
			 where
				cteBC1.BreadcrumbActivityUID in
			 (select ActivityUID from #ActivityUID)
				


		Declare curBC Cursor Static
		For
		
		Select
				BreadcrumbActivityType
				,SpeedAttribute
				,CASE WHEN Sum(Day1Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day1Time), 0), 0), 108) ELSE '0' END [SunTime]
				,CASE WHEN Sum(Day2Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day2Time), 0), 0), 108) ELSE '0' END [MonTime]
				,CASE WHEN Sum(Day3Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day3Time), 0), 0), 108) ELSE '0' END [TueTime]
				,CASE WHEN Sum(Day4Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day4Time), 0), 0), 108) ELSE '0' END [WedTime]
				,CASE WHEN Sum(Day5Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day5Time), 0), 0), 108) ELSE '0' END [ThuTime]
				,CASE WHEN Sum(Day6Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day6Time), 0), 0), 108) ELSE '0' END [FriTime]
				,CASE WHEN Sum(Day7Time) > 0 THEN Convert(char(8),DateAdd(second, ISNULL(Sum(Day7Time), 0), 0), 108) ELSE '0' END [SatTime]
				,CASE WHEN Sum(Day1Feet) > 0 THEN Cast(Sum(Day1Feet) as varchar(10)) ELSE '0' END [SunDist]
				,CASE WHEN Sum(Day2Feet) > 0 THEN Cast(Sum(Day2Feet) as varchar(10)) ELSE '0' END [MonDist]
				,CASE WHEN Sum(Day3Feet) > 0 THEN Cast(Sum(Day3Feet) as varchar(10)) ELSE '0' END [TueDist]
				,CASE WHEN Sum(Day4Feet) > 0 THEN Cast(Sum(Day4Feet) as varchar(10)) ELSE '0' END [WedDist]
				,CASE WHEN Sum(Day5Feet) > 0 THEN Cast(Sum(Day5Feet) as varchar(10)) ELSE '0' END [ThuDist]
				,CASE WHEN Sum(Day6Feet) > 0 THEN Cast(Sum(Day6Feet) as varchar(10)) ELSE '0' END [FriDist]
				,CASE WHEN Sum(Day7Feet) > 0 THEN Cast(Sum(Day7Feet) as varchar(10)) ELSE '0' END [SatDist]
		From
		(Select 
					BreadcrumbActivityType
					,SpeedAttribute
					 ,Case WHEN DatePart(dw, BCDate) = 1 THEN TotalSec ELSE 0 END [Day1Time]
					 ,Case WHEN DatePart(dw, BCDate) = 2 THEN TotalSec ELSE 0 END [Day2Time]
					 ,Case WHEN DatePart(dw, BCDate) = 3 THEN TotalSec ELSE 0 END [Day3Time]
					 ,Case WHEN DatePart(dw, BCDate) = 4 THEN TotalSec ELSE 0 END [Day4Time]
					 ,Case WHEN DatePart(dw, BCDate) = 5 THEN TotalSec ELSE 0 END [Day5Time]
					 ,Case WHEN DatePart(dw, BCDate) = 6 THEN TotalSec ELSE 0 END [Day6Time]
					 ,Case WHEN DatePart(dw, BCDate) = 7 THEN TotalSec ELSE 0 END [Day7Time]
					 ,Case WHEN DatePart(dw, BCDate) = 1 THEN TotalFeet ELSE 0 END [Day1Feet]
					 ,Case WHEN DatePart(dw, BCDate) = 2 THEN TotalFeet ELSE 0 END [Day2Feet]
					 ,Case WHEN DatePart(dw, BCDate) = 3 THEN TotalFeet ELSE 0 END [Day3Feet]
					 ,Case WHEN DatePart(dw, BCDate) = 4 THEN TotalFeet ELSE 0 END [Day4Feet]
					 ,Case WHEN DatePart(dw, BCDate) = 5 THEN TotalFeet ELSE 0 END [Day5Feet]
					 ,Case WHEN DatePart(dw, BCDate) = 6 THEN TotalFeet ELSE 0 END [Day6Feet]
					 ,Case WHEN DatePart(dw, BCDate) = 7 THEN TotalFeet ELSE 0 END [Day7Feet]
				from 
		(select BreadcrumbActivityType, SpeedAttribute, BCDate, sum(totalsecbetweenbc) TotalSec, Sum(FeetBetweenBC) TotalFeet 
		from #BCData
		--Where SpeedAttribute = 'Standstill'
		Group By BreadcrumbActivityType, SpeedAttribute, BCDate) BC) BC
		Group By BreadcrumbActivityType, SpeedAttribute

		Open curBC

		Fetch Next from curBC into @BCActivityType, @BCSpeedAtt, @BCSunTime, @BCMonTime, @BCTueTime, @BCWedTime, @BCThuTime, @BCFriTime, @BCSatTime, @BCSunDist, @BCMonDist, @BCTueDist, @BCWedDist, @BCThuDist, @BCFriDist, @BCSatDist


		WHILE @@FETCH_STATUS = 0
		BEGIN

			Update #TimeCard
			Set SunStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCSunTime ELSE '0' END
				, SunWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCSunDist ELSE '0' END
				, SunDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCSunDist ELSE '0' END

				, MonStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCMonTime ELSE '0' END
				, MonWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCMonDist ELSE '0' END
				, MonDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCMonDist ELSE '0' END

				, TueStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCTueTime ELSE '0' END
				, TueWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCTueDist ELSE '0' END
				, TueDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCTueDist ELSE '0' END

				, WedStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCWedTime ELSE '0' END
				, WedWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCWedDist ELSE '0' END
				, WedDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCWedDist ELSE '0' END

				, ThrStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCThuTime ELSE '0' END
				, ThrWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCThuDist ELSE '0' END
				, ThrDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCThuDist ELSE '0' END

				, FriStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCFriTime ELSE '0' END
				, FriWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCFriDist ELSE '0' END
				, FriDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCFriDist ELSE '0' END

				, SatStand = CASE WHEN @BCSpeedAtt = 'Standstill' THEN @BCSatTime ELSE '0' END
				, SatWalk = CASE WHEN @BCSpeedAtt = 'Walk' THEN @BCSatDist ELSE '0' END
				, SatDrive = CASE WHEN @BCSpeedAtt = 'Drive' THEN @BCSatDist ELSE '0' END
			Where Task = @BCActivityType

			Fetch Next from curBC into @BCActivityType, @BCSpeedAtt, @BCSunTime, @BCMonTime, @BCTueTime, @BCWedTime, @BCThuTime, @BCFriTime, @BCSatTime, @BCSunDist, @BCMonDist, @BCTueDist, @BCWedDist, @BCThuDist, @BCFriDist, @BCSatDist

		END

		Close curBC
		Deallocate curBC
		
		
		
		select 
		[Task]   
		,ISNULL([SunTime], '0') [SunTime]   
		,ISNULL([SunStand], '0') [SunStand] 
		,ISNULL([SunWalk], '0')  [SunWalk]
		,ISNULL([SunDrive], '0') [SunDrive]  
		,ISNULL([MonTime], '0') [MonTime]  
		,ISNULL([MonStand], '0') [MonStand] 
		,ISNULL([MonWalk], '0') [MonWalk] 
		,ISNULL([MonDrive], '0') [MonDrive] 
		,ISNULL([TueTime], '0') [TueTime] 
		,ISNULL([TueStand], '0') [TueStand]
		,ISNULL([TueWalk], '0') [TueWalk] 
		,ISNULL([TueDrive], '0') [TueDrive] 
		,ISNULL([WedTime], '0') [WedTime] 
		,ISNULL([WedStand], '0') [WedStand] 
		,ISNULL([WedWalk], '0') [WedWalk] 
		,ISNULL([WedDrive], '0') [WedDrive] 
		,ISNULL([ThrTime], '0') [ThrTime]  
		,ISNULL([ThrStand], '0') [ThrStand] 
		,ISNULL([ThrWalk], '0') [ThrWalk] 
		,ISNULL([ThrDrive], '0') [ThrDrive] 
		,ISNULL([FriTime], '0') [FriTime]    
		,ISNULL([FriStand], '0') [FriStand] 
		,ISNULL([FriWalk], '0') [FriWalk] 
		,ISNULL([FriDrive], '0') [FriDrive] 
		,ISNULL([SatTime], '0') [SatTime]      
		,ISNULL([SatStand], '0') [SatStand]  
		,ISNULL([SatWalk], '0') [SatWalk] 
		,ISNULL([SatDrive], '0') [SatDrive]  
	from #TimeCard



		Drop Table #TimeCard
		Drop Table #ActivityUID
		Drop Table #BCData

	END

END TRY
BEGIN CATCH
  THROW;
END CATCH


END







GO


