USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateOasisTimeCardByProject]    Script Date: 4/23/2019 1:28:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGenerateOasisTimeCardByProject](@ProjectIDJSON varchar(MAX), @WeekStartingDate date, @WeekEndingDate date)
AS


Create Table #TimeCardDetail
(
	[Employee Id] varchar(50) NULL    
	,[Class (Department)] varchar(50) NULL
	,[Transaction Date] Date NULL
	,[Client Code] varchar(50) NULL
	,[Service Items] varchar(50) NULL
	,[Payroll Item]  varchar(100) NULL
	,[ExpensePayAccount]  varchar(50) NULL
	,[ExpenseChargeAccount] varchar(50) NULL
	,[Hours to Import] Int NULL
	,[Billable]  char(1) NULL
	,[ReferenceNumber]  int NULL
	,[First Name] Varchar(100) NULL
	,[Last Name] varchar(100) NULL
)

Create Table #TimeCard
(
	[Employee Id] varchar(50) NULL
	,[COA] varchar(50) NULL    
	,[QTY] varchar(50) NULL
	,[Week Ending] Date NULL
	,[Employee Name] varchar(200) NULL
	--,[TotalSec] int NULL
)



/*

	Testing

	

	Exec spGenerateOasisTimeCardByProject_DEV20180517 '["4397"]', '2018-05-13', '2018-05-19'
	


	select  timecardsubmittedoasis, TimeCardSubmttedQuickBooks, TimeCardSubmttedADP, * 
	from timecardtb 
	where timecardprojectid = '4397' and timecardstartdate >= '2018-05-13' and timecardenddate <= '2018-05-19'


	select * from timeentrytb where timeentrytimecardid = 217894

	update timecardtb 
	Set timecardsubmittedoasis = NULL, TimeCardSubmttedQuickBooks = NULL, TimeCardSubmttedADP = NULL
	where timecardprojectid = '4397' and timecardstartdate >= '2018-05-13' and timecardenddate <= '2018-05-19'
	
*/

BEGIN

	Declare @ProjectID int
			
	--Before we start any thing need to save what was passed to the SP

	Declare @SPName varchar(100) = Object_Name(@@ProcID)

	Insert into [dbo].[tSPInsertedData]
	(SPName, InsertedData )
	Values
	(@SPName, ISNULL(@ProjectIDJSON, 'NULL') + ', ' + ISNULL(Cast(@WeekStartingDate as varchar(20)), 'NULL') + ', ' + ISNULL(cast(@WeekEndingDate as varchar(20)), 'NULL'))
	

	BEGIN TRY

		BEGIN TRAN CREATEQBFILE


			IF @ProjectIDJSON IS NOT NULL and @WeekStartingDate is not null and @WeekEndingDate is not null
			BEGIN

				select @WeekStartingDate = dateadd(d, -datepart(dw, @WeekStartingDate)+ 1, @WeekStartingDate)

				select @WeekEndingDate = dateadd(d, 7-datepart(dw, @WeekEndingDate), @WeekEndingDate)


				Insert Into #TimeCardDetail
				(
					 [Employee Id]
					,[Class (Department)]
					,[Transaction Date]
					,[Client Code]
					,[Service Items] 
					,[Payroll Item]
					,[ExpensePayAccount]
					,[ExpenseChargeAccount] 
					,[Hours to Import] 
					,[Billable] 
					,[ReferenceNumber] 
					,[First Name]
					,[Last Name]
				)
				select 
				
					[Employee Id]
					,[Class (Department)]
					,[Transaction Date]
					,[Client Code]
					,[Service Items] 
					,[Payroll Item]
					,[ExpensePayAccount]
					,[ExpenseChargeAccount] 
					,[Hours to Import] 
					,[Billable] 
					,[ReferenceNumber] 
					,[First Name]
					,[Last Name] 
				from fnGeneratePayrollDataByProject(@ProjectIDJSON, @WeekStartingDate, @WeekEndingDate, 'Oasis', 'FTP')

				select * Into #ProjectNames from [dbo].[parseJSON](@ProjectIDJSON)

				Declare curProjectNames Cursor
				For
				select StringValue from #ProjectNames Where Name is null

				Open curProjectNames

				Fetch Next from curProjectNames into @ProjectID

				WHILE @@FETCH_STATUS = 0
				BEGIN 
			
					Update tc 
					set TimeCardSubmittedOasis = getdate() 
					, TimeCardModifiedDate = getdate()
					From TimeCardTb tc
					Join (Select * from UserTb where UserAppRoleType in ('Technician') and UserActiveFlag = 1 and UserPayMethod = 'H') u on u.UserID = tc.TimeCardTechID
					where tc.TimeCardApprovedFlag = 1
						and Cast(tc.TimeCardStartDate as date) >= @WeekStartingDate 
						and Cast(tc.TimeCardEndDate as date) <= @WeekEndingDate 
						and tc.TimeCardProjectID = @ProjectID 
						and tc.TimeCardActiveFlag = 1
						and tc.TimeCardSubmittedOasis is null
			
					Fetch Next from curProjectNames into @ProjectID

				END --Fetch Next from curProjectNames into @ProjectID

			END -- @ProjectIDJSON IS NOT NULL and @WeekStartingDate is not null and @WeekEndingDate is not null

		COMMIT TRAN CREATEQBFILE

		Insert Into #TimeCard
		select [Employee Id]
			, [Payroll Item]
			, Format((Cast(Sum([Hours to Import]) as float)) / Cast(3600 as float), '0.00')
			, [Transaction Date]
			, [First Name] + ' ' + [Last Name]  from #TimeCardDetail
		Group By [Employee Id], [Payroll Item], [Transaction Date], [First Name] + ' ' + [Last Name]

		select * From #TimeCard where QTY <> '0.00'

		Drop Table #ProjectNames


	END TRY
	BEGIN CATCH

		ROLLBACK TRAN CREATEQBFILE
		
		--DROP TABLE #TimeCardDetail
		
		--Select 'ERROR'

		select * from [dbo].[NonExist]



	END CATCH

--	select * from #TimeCardDetail

	Drop Table #TimeCardDetail
	Drop Table #TimeCard
	
END








































GO


