USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spRptWorkDaySummary]    Script Date: 4/24/2019 9:11:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spRptWorkDaySummary]
(
	@StartDate			DATETIME	= NULL
	,@EndDate			DATETIME	= NULL
	,@UserName         varchar(100) = NULL     
 
) AS

/*****************************************************************************************************************
NAME:		[spRptWorkDaySummary]
DATABASE:   CometTracker
    
HISTORY
DATE            DEVELOPER         COMMENTS
-------------   ---------------   -------------------------------------------------------------------
2018-07-18		Gary Wheeler	  Initial Build with Parameters

Test Data
***********************************************************************************************************

	--EXECUTE  dbo.spRptWorkDaySummary '8/6/2018', '8/6/2018' , NULL
	--EXECUTE  dbo.spRptWorkDaySummary '11/27/2018', '11/27/2018' , '<All>'
	--EXECUTE  dbo.spRptWorkDaySummary '8/6/2018', '8/6/2018' , 'Technician, Scana (scanaTech)'
	

******************************************************************************************************************/

--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
SET NOCOUNT ON -- Must have when calling from MS Excel to only limit one result set


Declare @CountTest int
	,@CurrentTech				varchar(100)  
	,@CurrentActivityTitle varchar(100)
	,@CurrentActivityStartDateTime datetime  
	,@CurrentActivityEndDateTime datetime  
	,@CursorTech				varchar(100)  
	,@CursorActivityTitle varchar(100)
	,@CursorActivityStartDateTime datetime  
	,@CursorActivityEndDateTime datetime  
	,@AllowedSec int = 60

BEGIN TRY
IF @UserName is null
	BEGIN

		SELECT  -- 20160111 Added
		'<All>' AS Drop_Down1, 
         'ALL' AS Drop_Down2
		INTO #DD
		
		UNION ALL
		
		Select Distinct u.UserLastName + ', ' + u.UserFirstName +' (' + u.UserName + ')' As [Drop_Down1], u.UserLastName + ', ' + u.UserFirstName +' (' + u.UserName + ')' As [Drop_Down2]
		From UserTb u
		Join (Select Distinct ActivityCreatedUserUID
				from ActivityTb
				where Cast(ActivityStartTime as date) between @StartDate and @EndDate
				And (ActivityTitle like '%task%' or ActivityTitle in ('BreakActivity', 'LunchActivity', 'LoginActivity', 'AdminActivity')) and ActivityTitle  not like '%TaskOut%'
				and ActivityCreatedUserUID is not null) a on a.ActivityCreatedUserUID = Case WHEN ISNUMERIC(a.ActivityCreatedUserUID) = 1 THEN cast(u.UserID as varchar(50)) ELSE u.UserName END

		
		ORDER BY
		1

		SELECT Drop_Down1 , Drop_Down2 FROM #DD
		DROP TABLE #DD

	END
	ELSE
	BEGIN

		IF OBJECT_ID('tempdb..#WorkDaySummary', 'U') IS NOT NULL
		BEGIN
			DROP TABLE #WorkDaySummary	
		END	
		
		Select @CountTest =  Count(*)
		From (Select * from UserTb where UserLastName + ', ' + UserFirstName +' (' + UserName + ')' = CASE WHEN @UserName like '%ALL%' THEN UserLastName + ', ' + UserFirstName +' (' + UserName + ')' ELSE @UserName END) u
		Join (Select *
				from ActivityTb
				where Cast(ActivityStartTime as date) between @StartDate and @EndDate
				And (ActivityTitle like '%task%' or ActivityTitle in ('BreakActivity', 'LunchActivity', 'LoginActivity', 'AdminActivity')) and ActivityTitle  not like '%TaskOut%'
				and ActivityCreatedUserUID is not null) a on a.ActivityCreatedUserUID = Case WHEN ISNUMERIC(a.ActivityCreatedUserUID) = 1 THEN cast(u.UserID as varchar(50)) ELSE u.UserName END


		IF @CountTest > 10000
		BEGIN

			SELECT 
				CAST(GETDATE() AS DATE) AS [Report Date]
			,'Work Day Summary' AS [Report Title]
			,'Greater than 10,000 records returned. Please limit your date filters and try again.' AS [Report Alert]

		END
		ELSE 
		IF @CountTest = 0
		BEGIN
							
			SELECT 
				CAST(GETDATE() AS DATE) AS [Report Date]
			,'Work Day Summary' AS [Report Title]
			,'No data returned for the date(s) provided' AS [Report Alert]  
				
		END
		ELSE 
		BEGIN
			
			CREATE TABLE #Activity (
				[ActivityTitle] varchar(100) NULL,
				[Tech] varchar(255) NULL,
				[ActivityStartTime] datetime NULL,
				[ActivityEndTime] datetime
			)
			
			
			Select a.ActivityTitle
			, u.UserLastName + ', ' + u.UserFirstName +' (' + u.UserName + ')' [Tech]
			, a.ActivityStartTime
			, a.ActivityEndTime
			Into #TempActivity
			From (Select * from UserTb where UserLastName + ', ' + UserFirstName +' (' + UserName + ')' = CASE WHEN @UserName like '%ALL%' THEN UserLastName + ', ' + UserFirstName +' (' + UserName + ')' ELSE @UserName END) u
						Join (Select distinct ActivityCreatedUserUID
									, ActivityTitle
									--,ActivityStartTime
									--,ActivityEndTime
									, ActivityStartTime 
									, ActivityEndTime
									--, ActivityStartTime [RawActivityStartTime]
								from ActivityTb
								where Cast(ActivityStartTime as date) between @StartDate and @EndDate
								And (ActivityTitle like '%task%' or ActivityTitle in ('BreakActivity', 'LunchActivity', 'LoginActivity', 'AdminActivity')) and ActivityTitle  not like '%TaskOut%'
								and ActivityCreatedUserUID is not null) a on a.ActivityCreatedUserUID = Case WHEN ISNUMERIC(a.ActivityCreatedUserUID) = 1 THEN cast(u.UserID as varchar(50)) ELSE u.UserName END
			
			
			Declare curActivity Cursor Static
			For
			Select * from #TempActivity Order By Tech, ActivityStartTime

			Open curActivity

			Fetch Next From curActivity Into @CursorActivityTitle, @CursorTech, @CursorActivityStartDateTime, @CursorActivityEndDateTime

			Select @CurrentTech = @CursorTech
					, @CurrentActivityTitle = @CursorActivityTitle
					, @CurrentActivityStartDateTime = @CursorActivityStartDateTime
					, @CurrentActivityEndDateTime = @CursorActivityEndDateTime


			
				While @@FETCH_STATUS = 0
				BEGIN

					IF @CurrentTech = @CursorTech
					BEGIN

						IF @CursorActivityTitle In ('LoginActivity', 'TaskOutActivity')
						BEGIN

							Insert Into #Activity ([ActivityTitle], [Tech], [ActivityStartTime], [ActivityEndTime])
							Values (@CursorActivityTitle, @CursorTech, @CursorActivityStartDateTime, @CursorActivityEndDateTime)


						END --IF @CursorActivityTitle In ('LoginActivity', 'TaskOutActivity')

						IF @CursorActivityTitle Not In ('LoginActivity', 'TaskOutActivity')
						BEGIN

							IF @CurrentActivityTitle = @CursorActivityTitle
							BEGIN

								If Datediff(s, @CurrentActivityStartDateTime, @CursorActivityStartDateTime) <= @AllowedSec
								BEGIN

									Set @CurrentActivityEndDateTime = @CursorActivityEndDateTime

								END --If Datediff(s, @CurrentActivityStartDateTime, @CursorActivityStartDateTime) <= @AllowedSec

								If Datediff(s, @CurrentActivityStartDateTime, @CursorActivityStartDateTime) > @AllowedSec
								BEGIN

									Insert Into #Activity ([ActivityTitle], [Tech], [ActivityStartTime], [ActivityEndTime])
									Values (@CurrentActivityTitle, @CurrentTech, @CurrentActivityStartDateTime, @CurrentActivityEndDateTime)

									Select @CurrentActivityStartDateTime = @CursorActivityStartDateTime
										, @CurrentActivityEndDateTime = @CursorActivityEndDateTime

								END --If Datediff(s, @CurrentActivityStartDateTime, @CursorActivityStartDateTime) > @AllowedSec

							END --IF @CurrentActivityTitle = @CursorActivityTitle

							IF @CurrentActivityTitle <> @CursorActivityTitle
							BEGIN

								If @CurrentActivityTitle Not In ('LoginActivity', 'TaskOutActivity')
								BEGIN
				
									Insert Into #Activity ([ActivityTitle], [Tech], [ActivityStartTime], [ActivityEndTime])
									Values (@CurrentActivityTitle, @CurrentTech, @CurrentActivityStartDateTime, @CurrentActivityEndDateTime)

								END

								Select @CurrentTech = @CursorTech
									, @CurrentActivityTitle = @CursorActivityTitle
									, @CurrentActivityStartDateTime = @CursorActivityStartDateTime
									, @CurrentActivityEndDateTime = @CursorActivityEndDateTime

							END --IF @CurrentActivityTitle <> @CursorActivityTitle

						END --IF @CursorActivityTitle Not In ('LoginActivity', 'TaskOutActivity')

					END --IF @CurrentTech = @CursorTech
	
					IF @CurrentTech <> @CursorTech
					BEGIN

						Insert Into #Activity ([ActivityTitle], [Tech], [ActivityStartTime], [ActivityEndTime])
						Values (@CurrentActivityTitle, @CurrentTech, @CurrentActivityStartDateTime, @CurrentActivityEndDateTime)

						Select @CurrentTech = @CursorTech
							, @CurrentActivityTitle = @CursorActivityTitle
							, @CurrentActivityStartDateTime = @CursorActivityStartDateTime
							, @CurrentActivityEndDateTime = @CursorActivityEndDateTime

					END -- IF @CurrentTech <> @CursorTech
	
					Fetch Next From curActivity Into @CursorActivityTitle, @CursorTech, @CursorActivityStartDateTime, @CursorActivityEndDateTime

				END --While @@FETCH_STATUS = 0

				Close curActivity
				Deallocate curActivity


				Insert Into #Activity ([ActivityTitle], [Tech], [ActivityStartTime], [ActivityEndTime])
				Values (@CurrentActivityTitle, @CurrentTech, @CurrentActivityStartDateTime, @CurrentActivityEndDateTime)

				/*

				*****  Below is a CTE (Common Table Expression) that may be used in the future
					if you are not familar with CTEs then follow this link
					https://www.essentialsql.com/Introduction-common-table-expressions-ctes/

				;With cteActivity
				AS	
					(Select * , Row_Number() Over (order By Tech, ActivityStartTime) RowID From #Activity)
				Select ctea1.ActivityTitle
				, ctea1.Tech
				, CASE WHEN ctea1.Tech <> ISNULL(ctea2.Tech, '') or Cast(ctea1.ActivityStartTime as date) <> Cast(ctea2.ActivityStartTime as Date)
					THEN FORMAT(ctea1.ActivityStartTime, 'MM/dd/yyyy HH:mm:ss')
					ELSE FORMAT(ctea1.ActivityStartTime, 'HH:mm:ss')
					END [ActivityStartTime]
				,DateDiff(n, ctea1.ActivityStartTime, ctea1.ActivityEndTime) TotalTimeMinutes
				from cteActivity ctea1
				Left Join cteActivity ctea2 on ctea1.RowID - 1 = ctea2.RowID

				*/

				Select ActivityTitle
				, Tech
				, FORMAT(ActivityStartTime, 'MM/dd/yyyy HH:mm:ss') [ActivityStartTime]
				,DateDiff(n, ActivityStartTime, ActivityEndTime) [TotalTimeMinutes] 
				from #Activity
				Order by Tech, ActivityStartTime
				
				drop table #Activity
				drop table #TempActivity
	


					
		END			
		SET NOCOUNT OFF		
	END
	END TRY
BEGIN CATCH
  THROW;
END CATCH


























GO


