USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[PopulateMileageCardTbForUserToProjectCatchErrors_proc]    Script Date: 4/23/2019 1:21:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO












/****************************************************************************************************************************/
/****************************************************************************************************************************/

/*		Function Name:		[PopulateMileageCardTbForUserToProjectCatchErrors_proc]		 		  				*************************/
/*		Created By:			Sandra Jackson, Senior DBA															*************************/
/*		Create Date:		02/01/2016																			*************************/
/*		Create Reason:		Populate Mileage Cards For New User for the year									*************************/



/*		Modified By:		Gary Wheeler, Senior DBA															*************************/
/*		Modified Date:		01/09/2018																			*************************/
/*		Modify Reason:		Populate Mileage Cards For User for the year if other Milecards exist				*************************/




/*		Parameters(2):		UserID OF New User, ProjectID														*************************/
/*		Prerequisites:		Create The New User in the User Table 												*************************/
/*		Utilized By:		CometTracker App, Supervisor, Admin													*************************/
/*		Call methods(1):	1. execute dbo.PopulateMileageCardTbForNewUserCatchErrors_proc	232					*************************/
/*		Validation:			select count(*)* 53 FROM [dbo].[UserTb] WHERE [UserActiveFlag]						*************************/
/*                          NOT IN ('INACTIVE','TERMINATED','RETIRED','PENDING')								*************************/
/*							select count(*) from [MileageTb]  --test to validate it worked						*************************/
/*		Status:				UserActiveFlag active = 1 inactive = 0 terminated = 9 pending = 2					*************************/
/*		Version:			1.3																					*************************/
/*		Modified Reason:	1.0 Created Stored Procedure		PopulateMileageCardTbForNewUser_proc						*************/
/*							1.1 Caught Errors		renamed		PopulateMileageCardTbForNewUserCatchErrors_proc				*************/
/*							1.2 Added  proj id param rename		PopulateMileageCardTbForUserToProjectCatchErrors_proc		*************/
/*							1.3 Checks contents of column MileageCardProjectGroupID on MileageCardTb to creates				*************/
/*							unique Mileagecards by project																	*************/
/*																															*************/
/*																															*************/
/* TO BE RUN 1 TIME A YEAR TO CREATE TIME CARD PERIODS																*********************/
/****************************************************************************************************************************************/
/****************************************************************************************************************************************/

CREATE PROCEDURE [dbo].[PopulateMileageCardTbForUserToProjectCatchErrors_proc] @TechID INT,  @ProjectID INT

AS


--DECLARE @StateDate datetime = '1/1/' + Cast(Year(getdate()) as varchar(10)) + ' 00:00:00 AM'
--StartDate Changed by Gary Wheeler on 4/20/2018 per CT-826
DECLARE @StartDate datetime = Cast(Cast(Cast(DateAdd(d, -(DatePart(dw, getdate()) - 1), getdate()) as date) as varchar(20)) + ' 00:00:00 AM' as datetime)
, @ProjMileagecardPMApprovedFlag bit = 0
, @ProjMileagecardApprovedflag bit = 0
		BEGIN TRY
			BEGIN TRANSACTION INSRTMC

			/*  Modified Code By Gary Wheeler on 1/9/2018 */

			SELECT	
				tcp.[TimeCardSunday] as [MileageCardStartDate], 
				tcp.[TimeCardSaturday] as [MileageCardEndDate], 
				@ProjectID AS [MileageCardProjectID], 
				@TECHID AS [MileageCardTechID],
				1 [MileageCardActiveFlag],
				0 [MileageCardApprovedFlag],
				'' [MileageCardApproveBy],
				0 [MileageCardArchiveFlag],
				getdate() as [MileageCardCreateDate], 
				'gwheeler' as [MileageCardCreatedBy],
				0 [MileageCardPMApprovedFlag]
			INTO #NewMileageCards
			FROM (Select * From [dbo].[TimeCardPeriodsTb] where TimeCardSunday >= @StartDate or TimeCardSaturday >= @StartDate) tcp
			order by tcp.[TimeCardSunday]

			select distinct @ProjMileagecardPMApprovedFlag = ISNULL(MileageCardApprovedFlag, 0), @ProjMileagecardApprovedflag = ISNULL(MileageCardPMapprovedFlag, 0) 
			from MileageCardTb 
			where MileageCardProjectID = @ProjectID
				and MileageStartDate = @StartDate
				and (MileageCardApprovedFlag = 1 or MileagecardPMapprovedFlag = 1)
			
			
			Update #NewMileageCards
			Set MileageCardApprovedFlag = @ProjMileagecardApprovedflag
			, MileagecardPMapprovedFlag = @ProjMileagecardPMApprovedFlag
			Where MileageCardStartDate = @StartDate

			
			Insert Into [dbo].[MileageCardTb]
			(
				MileageStartDate, 
				MileageEndDate, 
				MileageCardProjectID, 
				MileageCardTechID, 
				MileageCardActiveFlag,
				MileageCardApprovedFlag, 
				MileageCardApprovedBy, 
				MileageCardArchiveFlag, 
				MileageCardCreateDate, 
				MileageCardCreatedBy,
				MileageCardPMApprovedFlag

			)
			Select NewMC.* from #NewMileageCards NewMC
			Left Join [dbo].[MileageCardTb] MC on MC.MileageCardProjectID = NewMC.MileageCardProjectID 
						and MC.MileageCardTechID = NewMC.MileageCardTechID 
						and Cast(MC.MileageStartDate as date) = Cast(NewMC.MileageCardStartDate as Date)
			Where MC.MileageCardID is null

			Drop Table #NewMileageCards

			COMMIT TRAN INSRT
END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
	BEGIN
        ROLLBACK TRAN INSRTMC
		RAISERROR(7770301, 10, 1)
	END
END CATCH
	








GO


