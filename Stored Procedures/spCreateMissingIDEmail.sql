USE [CometTracker]
GO

/****** Object:  StoredProcedure [dbo].[spCreateMissingIDEmail]    Script Date: 4/23/2019 1:25:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spCreateMissingIDEmail] 
AS 
    -- This is called from spUpdateUsers 
    SET nocount ON 

  BEGIN try 
      DECLARE @EmailReportName VARCHAR(20) = 'MissingID', 
              @SendTO          VARCHAR(2000) = NULL, 
              @SendCC          VARCHAR(2000) = NULL, 
              @MessageBody     VARCHAR(max) = 
              'The following individual are missing an ID ', 
              @Subject         VARCHAR(200) = 'Active Users With Missing IDs', 
              @Tech            VARCHAR(100), 
              @ProjectName     VARCHAR(200), 
              @BodyMessage2    VARCHAR(200) = NULL, 
              @CRLF            VARCHAR(10) = Char(13) + Char(10), 
              @Return          INT, 
              @SourceDB        VARCHAR(200), 
              @DBNAME          VARCHAR(50) = Db_name() 
      DECLARE @CountTest INT = (SELECT Count(*) 
         FROM   [vuserswithmissingids]) 

      IF @CountTest <= 20 
        BEGIN 
            SELECT @BodyMessage2 = COALESCE(@BodyMessage2 + @CRLF, '') 
                                   + uwmi.[last name] + ', ' + uwmi.[first name] 
                                   + ', ' + uwmi.[scc employee id] + ', ' 
                                   + uwmi.issues 
            FROM   vuserswithmissingids uwmi 

            SET @MessageBody = @MessageBody + @CRLF + @CRLF + @BodyMessage2 
        END 
      ELSE 
        BEGIN 
            SELECT @BodyMessage2 = COALESCE(@BodyMessage2 + @CRLF, '') 
                                   + s.[issue(s)] + ' - ' 
                                   + Cast(s.count AS VARCHAR(10)) 
            FROM   (SELECT issues   [Issue(s)], 
                           Count(*) [Count] 
                    FROM   vuserswithmissingids 
                    GROUP  BY issues) S 

            SET @MessageBody = 'The Follow is a count of missing IDs' 
                               + @CRLF + @CRLF + @BodyMessage2 
        END 

      SET @SourceDB = CASE 
                        WHEN @DBNAME = 'CometTracker' THEN 'CT Dev - ' 
                        WHEN @DBNAME = 'CometTracker_Stage' THEN 'CT Stage - ' 
                        WHEN @DBNAME = 'AZCometTracker_PROD' THEN 'CT Prod - ' 
                        WHEN @DBNAME = 'CometTracker_Demo' THEN 'CT Demo - ' 
                      END 
      SET @Subject = @SourceDB + @Subject 

      EXEC [dbo].[Spgetemaillist] 
        @EmailReportName, 
        0, 
        @SendTO output, 
        @SendCC output 

      IF @SendTO IS NOT NULL 
        BEGIN 
            EXEC Spsendemail 
              @SendTO, 
              @SendCC, 
              @Subject, 
              @MessageBody 
        END 

      SET nocount OFF 
  END try 

  BEGIN catch 
      THROW; 
  END catch 


GO


