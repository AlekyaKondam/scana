USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vLastWeekInsertErrors]    Script Date: 4/23/2019 9:39:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[vLastWeekInsertErrors]
AS

select Cast(svrdtlt as date) [Date], ErrorNumber, Count(*) [Error Count] 
from [dbo].[tTabletJSONDataInsertError] 
where Cast(svrdtlt as date) between cast(DateAdd(d, - (DatePart(dw, dateadd(wk,-1,getdate())) - 1), dateadd(wk,-1,getdate())) as date)
	and cast(DateAdd(d, 7 - DatePart(dw, dateadd(wk,-1,getdate())), dateadd(wk,-1,getdate())) as date)
group by Cast(svrdtlt as date), ErrorNumber
GO


