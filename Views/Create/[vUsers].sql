USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vUsers]    Script Date: 4/23/2019 9:42:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vUsers]
AS 
select

       UserID
	  ,[UserName]
	  ,[UserFirstName]
      ,[UserLastName]
      ,UserEmployeeType
	  ,UserAppRoleType 
	
  FROM [dbo].[UserTb]
  where UserActiveFlag = 1



GO


