USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vWebManagementMapView]    Script Date: 4/23/2019 9:43:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE View [dbo].[vWebManagementMapView]
AS

select
wo.ID,
wo.ClientWorkOrderID,
wo.LocationType [AssetType],
COALESCE(WO.HouseNumber, '')
+ ' ' 
+ COALESCE(WO.AptSuite, '')
+ ' ' 
+ COALESCE(WO.Street, '')
+ ' ' 
+ COALESCE(WO.City, '')
+ ' ' 
+ COALESCE(WO.State, '')
+ '. ' 
+ COALESCE(WO.Zip, '')  as Address,
wo.MapGrid,
wo.CompletedFlag,
0 as [LineID],
0 as [SegmentID],
0 as [VerticeID],
wo.LocationLatitude [Latitude],
wo.LocationLongitude [Longitude],
COALESCE(i.id, 0) as [InspectionID],
CASE WHEN i.id is NULL THEN 0 ELSE dbo.fnGetDistance(wo.LocationLatitude, wo.LocationLongitude, i.Latitude, i.Longitude, 'FEET') END as [Distance],
CASE WHEN i.id is NULL THEN 'N/A' ELSE 
	CASE WHEN dbo.fnGetDistance(wo.LocationLatitude, wo.LocationLongitude, i.Latitude, i.Longitude, 'FEET') <=30 THEN 'Yes' ELSE 'No' END END as [Verified]
from 
(Select * from tWorkOrder where LocationType in ('Service Location', 'Inactive Service', 'Adhoc Service Location')) wo
Left Join (select WorkOrderID, Max(ID) WorkQueueID from tWorkQueue 
		where WorkQueueStatus = 102 group by WorkOrderID) wq on wq.WorkOrderID = wo.ID
Left Join tInspection i on i.WorkQueueID = wq.WorkQueueID


Union Select


wo.ID,
wo.ClientWorkOrderID,
wo.LocationType [AssetType],
COALESCE(WO.HouseNumber, '')
+ ' ' 
+ COALESCE(WO.AptSuite, '')
+ ' ' 
+ COALESCE(WO.Street, '')
+ ' ' 
+ COALESCE(WO.City, '')
+ ' ' 
+ COALESCE(WO.State, '')
+ '. ' 
+ COALESCE(WO.Zip, '')  as Address,
wo.MapGrid,
wo.CompletedFlag,
p.LineID as [LineID],
p.Segment_ID as [SegmentID],
p.VerticeID as [VerticeID],
p.Latitude [Latutide],
p.Longitude [Longitude],
COALESCE(p.id, 0) as [InspectionID],
p.Distance as [Distance],
CASE WHEN p.Verified = 1 THEN 'Yes' ELSE 'No' END   as [Verified]
from 
(Select * from tWorkOrder where  LocationType in ('Pipeline', 'Adhoc Pipeline', 'Gas Main', 'Gas Main Celenese', 'Gas Main PEG')) wo
Left Join (select WorkOrderID, Max(ID) WorkQueueID from tWorkQueue 
		where WorkQueueStatus = 102 group by WorkOrderID) wq on wq.WorkOrderID = wo.ID
Left Join tInspection i on i.WorkQueueID = wq.WorkQueueID
Left Join [dbo].[tPipelinePoints] p on p.WorkOrderID = wo.id



GO


