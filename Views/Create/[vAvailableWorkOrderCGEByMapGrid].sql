USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vAvailableWorkOrderCGEByMapGrid]    Script Date: 4/23/2019 9:38:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE VIEW [dbo].[vAvailableWorkOrderCGEByMapGrid]
AS 
SELECT wo.MapGrid,  MapGridDetail.ComplianceStart, MapGridDetail.ComplianceEnd, CASE WHEN Count(distinct SectionNumber) > 1 then 1 ELSE 0 END as [SectionFlag], Count(*) AvailableWorkOrderCount,  CAST(0 as BIT) as ScheduleRequired
	     ---- new line added 12/2 
		 ,WO.BillingCode
		 ,WO.InspectionType
          ------ new line added 1/5/2017
		  ,wo.OfficeName 
FROM 
	(Select * From [dbo].[tWorkOrder] where CompletedFlag = 0 and InspectionAttemptCounter > 0 and ID In
	 (Select distinct WorkOrderID from tWorkQueue)) WO
Left Join (Select workorderId from [dbo].[tWorkQueue] where WorkQueueStatus <> 102 ) WQ On WO.id = wq.WorkOrderID
Join  
	(select MapGrid, 
	Min(ComplianceStart) [ComplianceStart], 
	Max(ComplianceEnd) [ComplianceEnd], 
	CASE WHEN Count(distinct SectionNumber) > 1 then 1 ELSE 0 END as [SectionFlag]
	from tWorkOrder
	Group By MapGrid) MapGridDetail on wo.MapGrid = MapGridDetail.MapGrid
where WQ.WorkOrderID is null
Group By wo.MapGrid, MapGridDetail.ComplianceStart, MapGridDetail.ComplianceEnd --, MapGridDetail.SectionFlag
	     ---- new line added 12/2 
		 ,WO.BillingCode
		 ,WO.InspectionType
          ------ new line added 1/5/2017
		  ,wo.OfficeName 








GO


