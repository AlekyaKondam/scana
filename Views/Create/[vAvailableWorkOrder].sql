USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vAvailableWorkOrder]    Script Date: 4/23/2019 9:37:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE View [dbo].[vAvailableWorkOrder] 
 As

SELECT 
        WO.ID [WorkOrderID],
        WO.ClientWorkOrderID, 
		WO.CreatedBy,
		WO.ModifiedBy,
		WO.CreatedDateTime,
		WO.ModifiedDateTime,
		WO.InspectionType,
		WO.HouseNumber,
		WO.Street,
		WO.AptSuite,
		WO.City,
		WO.State,
		WO.Zip,
		WO.MeterNumber,
		WO.MeterLocationDesc,
		WO.LocationType,
		WO.LocationLatitude,
		WO.LocationLongitude,
		WO.MapGrid,
		WO.ComplianceStart,
		WO.ComplianceEnd,
		WO.MapLatitudeBegin,
		WO.MapLongitudeBegin,
		WO.MapLatitudeEnd,
		WO.MapLongitudeEnd,
		WO.AccountNumber,
		WO.AccountName,
		WO.AccountTelephoneNumber,
		WO.Comments,
		WO.CompletedFlag,
		WO.CompletedDate,
		WO.InspectionAttemptCounter,
		WO.SequenceNumber,
		WO.SectionNumber,
		WO.Shape,
	     ---- new line added 12/2 
		 WO.BillingCode
          ------ new line added 12/2 
		,RTRIM(LTRIM(COALESCE(WO.HouseNumber, ''))) + ' ' + RTRIM(LTRIM(COALESCE(WO.AptSuite, ''))) + 
		case when RTRIM(LTRIM(COALESCE(WO.AptSuite, ''))) = '' then '' else ' ' end + 
		RTRIM(LTRIM(COALESCE(WO.Street, ''))) + ', ' + RTRIM(LTRIM(COALESCE(wo.City, ''))) [Address]
		,wo.OfficeName
    FROM (Select * From [dbo].[tWorkOrder] where CompletedFlag = 0 and InspectionAttemptCounter = 0) wo
	Left Join (Select * From tWorkQueue where WorkQueueStatus <> 102) wq on wo.ID = wq.WorkOrderID
	Where wq.id is null
	












GO


