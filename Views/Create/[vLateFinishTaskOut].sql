USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vLateFinishTaskOut]    Script Date: 4/23/2019 9:39:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vLateFinishTaskOut]
AS 
select u.UserFirstName + ' ' + u.UserLastName [Surveyor], Cast(cast(ActivitySrcDTLT as time) as varchar(8)) [ActivityTime]
from ActivityTb a
--join usertb u on cast(a.ActivityCreatedUserUID as varchar(50)) = cast(UserID as varchar(50))
Join (Select * From UserTb where TestUser = 0) u on CASE WHEN ISNUMERIC(A.ActivityCreatedUserUID) = 1 THEN Cast(u.UserID as varchar(10)) ELSE u.UserName END  = A.ActivityCreatedUserUID
where ActivityTitle = 'TaskOutActivity' 
and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
and cast(ActivitySrcDTLT as time) > '5:00 PM'







GO


