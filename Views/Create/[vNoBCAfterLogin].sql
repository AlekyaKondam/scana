USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vNoBCAfterLogin]    Script Date: 4/23/2019 9:41:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vNoBCAfterLogin]
AS 


/*

the end datetime on the between must be set 15 (-15) minutes prior to when this will be called.



*/
Select u1.UserLastName + ', ' + u1.UserFirstName + ' (' + u1.UserName + ')' [TechName], LoginDatetime
From
(
	Select UserName, LoginDatetime, Min(BreadcrumbSrcDTLT) FirstBC
	From
	(
		Select la.ActivityCreatedUserUID, u.UserName, la.LoginDatetime, bc.BreadcrumbSrcDTLT
		From
		(
			select ActivityCreatedUserUID, Min(ActivitySrcDTLT) LoginDatetime 
			from ActivityTb 
			where ActivitySrcDTLT between dateadd(n, -45, getdate()) and dateadd(n, -15, getdate()) and ActivityTitle = 'LoginActivity' 
			Group By ActivityCreatedUserUID
		) la
		Join (Select * from UserTb
			Where UserName Not In
			(
				'jtest'
			)
			) U on u.UserName = ActivityCreatedUserUID
		Left Join BreadcrumbTb bc on bc.BreadcrumbCreatedUserUID = u.UserName and bc.BreadcrumbSrcDTLT > la.LoginDatetime
	) BCandActivity
	Group by ActivityCreatedUserUID,  UserName, LoginDatetime
) Source
Left Join UserTb u1 on Source.UserName = u1.UserName
Where DateDiff(n, LoginDatetime, ISNULL(FirstBC, Dateadd(n, 15, LoginDatetime))) > 10



GO


