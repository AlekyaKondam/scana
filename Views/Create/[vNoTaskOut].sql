USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vNoTaskOut]    Script Date: 4/23/2019 9:41:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create View [dbo].[vNoTaskOut]
AS 

Select 
U.userlastname + ', ' + u.userfirstname + ' (' + username + ')' [User]
From
	(select ActivityCreatedUserUID, Max(ActivitySrcDTLT) [LastActivity] from [dbo].[ActivityTb] 
	where Activitytitle  in ('IndicationActivity','InspectionActivity','SurveyActivity')
	and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
	Group By ActivityCreatedUserUID) [LastLogin]
Left Join
	(select ActivityCreatedUserUID, Max(ActivitySrcDTLT) [LastTaskOut] from [dbo].[ActivityTb] 
	where Activitytitle  = 'TaskOutActivity' 
	and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
	Group By ActivityCreatedUserUID) [LastTaskOut] on [LastLogin].ActivityCreatedUserUID = [LastTaskOut].ActivityCreatedUserUID 
Left Join UserTb u on [LastLogin].ActivityCreatedUserUID = u.username
where 
[LastTaskOut].LastTaskOut is NULL
or [LastLogin].LastActivity > DateAdd(SECOND,30, [LastTaskOut].LastTaskOut)
GO


