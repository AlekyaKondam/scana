USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vWebManagementInspectionsByMapGridSectionNumber]    Script Date: 4/23/2019 9:43:18 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create View [dbo].[vWebManagementInspectionsByMapGridSectionNumber]
AS

Select wo.MapGrid
, wo.SectionNumber
, Min(wo.ComplianceStart) [ComplianceStart]
, max(wo.ComplianceEnd) [ComplianceEnd]
, count(*) [TotalInspections] 
, mgp.[Percent Completed] [PercentageComplete]
From 
(Select ID, MapGrid, isnull(SectionNumber, 1) [SectionNumber], ComplianceStart, ComplianceEnd from tWorkOrder) wo
Join (Select * from tWorkQueue where WorkQueueStatus = 102) wq on wq.WorkOrderID = wo.ID
left Join [dbo].[vMapGridSectionPercentage] mgp on mgp.MapGrid = wo.MapGrid and mgp.SectionNumber = wo.SectionNumber
where wo.MapGrid is not null
Group by wo.MapGrid, wo.SectionNumber, mgp.[Percent Completed]
GO


