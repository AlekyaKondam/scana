USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vMapGridPercentage_SNITBCON]    Script Date: 4/23/2019 9:40:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create View [dbo].[vMapGridPercentage_SNITBCON]
AS
Select Total.MapGrid, Total.SectionNumber, Total.InspectionType,Total.Billingcode, Total.OfficeName 
     , Total.Total , ISNULL(Completed.Total, 0) [Completed]
	 , Total.Total - ISNULL(Completed.Total, 0) [Remaining]
     , Cast((Cast(ISNULL(Completed.Total, 0) as float) / Cast(Total.Total as Float)) * 100 as numeric(3,0)) [Percent Completed] 
from 
(
  select MapGrid, SectionNumber, InspectionType,Billingcode, OfficeName, Count(*) Total 
  from tWorkOrder   
  Group By MapGrid, SectionNumber, InspectionType,Billingcode, OfficeName
) Total
Left Join 
( select MapGrid, SectionNumber, InspectionType,Billingcode, OfficeName ,Count(*) Total from tWorkOrder where CompletedFlag = 1 
  Group By MapGrid ,SectionNumber, InspectionType,Billingcode, OfficeName
 ) Completed
 on Total.MapGrid = Completed.MapGrid
  and  IsNULL(Total.SectionNumber, '0') =  IsNULL(Completed.SectionNumber ,'0')
 and  IsNULL(Total.BillingCode, '0') =  IsNULL(Completed.Billingcode ,'0')
 and IsNULL(Total.InspectionType,'0') = IsNULL(Completed.InspectionType ,'0')
 and IsNULL(Total.OfficeName,'0') = IsNULL(Completed.officename ,'0')








GO


