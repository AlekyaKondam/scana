USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vWebManagementInspectionsEvents]    Script Date: 4/23/2019 9:43:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE View [dbo].[vWebManagementInspectionsEvents]
AS

select  
sl.StatusDescription [EventType]
,Case 
	WHEN sl.StatusDescription = 'AOC' THEN e.ACGrade
	WHEN sl.StatusDescription = 'CGE' THEN e.CGEReason
	WHEN sl.StatusDescription = 'Indication' THEN 'Leak #: ' + Coalesce(e.LeakNumber, '') + '|'+ 'Grade: ' + ' ' + Coalesce(LeakGrade, '')  + ' ' + Coalesce(LeakAboveOrBelow, '')
	ELSE ''
END [Reason]
,Coalesce(e.Photo1Path, '') [Photo]
, Coalesce(e.Comments, '') [Comments]
, e.InspectionID
from tEvent e with (Nolock)
join [dbo].[rStatusLookup] sl with (Nolock) on e.EventType = sl.StatusCode
Join (select i.* from tInspection i with (Nolock)
		Join tWorkQueue WQ with (Nolock) on i.WorkQueueID = WQ.ID
		Join tWorkOrder WO with (Nolock) on WQ.WorkOrderID = WO.id) i on e.InspectionID = i.ID
where e.DeletedFlag = 0







--GO






GO


