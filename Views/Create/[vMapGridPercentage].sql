USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vMapGridPercentage]    Script Date: 4/23/2019 9:40:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[vMapGridPercentage]
AS
Select Total.MapGrid, Total.Total , ISNULL(Completed.Total, 0) [Completed], Total.Total - ISNULL(Completed.Total, 0) [Remaining]
, Cast((Cast(ISNULL(Completed.Total, 0) as float) / Cast(Total.Total as Float)) * 100 as numeric(3,0)) [Percent Completed] from 
(select MapGrid, Count(*) Total from tWorkOrder Group By MapGrid) Total
Left Join (select MapGrid, Count(*) Total from tWorkOrder where CompletedFlag = 1 Group By MapGrid) Completed on Total.MapGrid = Completed.MapGrid

GO


