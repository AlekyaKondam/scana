USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vWebManagementInspectionsByMapGrid]    Script Date: 4/23/2019 9:43:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create View [dbo].[vWebManagementInspectionsByMapGrid]
AS

Select wo.MapGrid
, Min(wo.ComplianceStart) [ComplianceStart]
, max(wo.ComplianceEnd) [ComplianceEnd]
, count(*) [TotalInspections] 
, mgp.[Percent Completed] [PercentageComplete]
From 
tWorkOrder wo
Join (Select * from tWorkQueue where WorkQueueStatus = 102) wq on wq.WorkOrderID = wo.ID
left Join [dbo].[vMapGridPercentage] mgp on mgp.MapGrid = wo.MapGrid
where wo.MapGrid is not null
Group by wo.MapGrid, mgp.[Percent Completed]
GO


