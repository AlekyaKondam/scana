USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vEarlyFinishTaskOut]    Script Date: 4/23/2019 9:38:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vEarlyFinishTaskOut]
AS 
--select u.UserFirstName + ' ' + u.UserLastName [Surveyor], Cast(cast(ActivitySrcDTLT as time) as varchar(8)) [ActivityTime]
--from ActivityTb a
--Join UserTb u on  CASE WHEN ISNUMERIC(A.ActivityCreatedUserUID) = 1 THEN Cast(u.UserID as varchar(10)) ELSE u.UserName END  = A.ActivityCreatedUserUID
--where ActivityTitle = 'TaskOutActivity' 
--and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
--and cast(ActivitySrcDTLT as time) < '4:00 PM'


select u.UserFirstName + ' ' + u.UserLastName [Surveyor], max(Cast(cast(ActivitySrcDTLT as time) as varchar(8))) [ActivityTime]
from ActivityTb a
Join (Select * From UserTb where TestUser = 0) u on  CASE WHEN ISNUMERIC(A.ActivityCreatedUserUID) = 1 THEN Cast(u.UserID as varchar(10)) ELSE u.UserName END  = A.ActivityCreatedUserUID
where ActivityTitle = 'TaskOutActivity' 
and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
and cast(ActivitySrcDTLT as time) < '4:00 PM'
group by u.UserFirstName + ' ' + u.UserLastName










GO


