USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vAvailableWorkOrderCGEByWODetails]    Script Date: 4/23/2019 9:38:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[vAvailableWorkOrderCGEByWODetails]
AS
select u.UserLastName + ', ' + u.UserFirstName + ' (' + u.UserName + ')' [Inspector], 
wo.HouseNumber + ' ' + wo.Street + ' ' + wo.City [Address], 
i.CreatedDate [InspectionDateTime], 
COALESCE(e.Photo1Path, '') [Image],
wo.ID
     ---- new line added 12/2 
		 ,WO.BillingCode
		 ,WO.InspectionType
       ------ new line added 1/5/2018 
	   ,wo.OfficeName

From
tWorkOrder wo
Join tWorkQueue wq on wo.id = wq.WorkOrderID
Join tInspection  i on i.WorkQueueID = wq.ID
Left Join (Select * from tEvent where EventType = 200) e on e.InspectionID = i.ID
Join UserTb u on i.CreatedBy = u.UserID




GO


