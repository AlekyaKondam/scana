USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vAssignedWorkQueueBySectionDetails]    Script Date: 4/23/2019 9:37:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 --BEGIN 
CREATE VIEW [dbo].[vAssignedWorkQueueBySectionDetails]
AS
select 
   WO.MapGrid 
 ,WO.InspectionType 
 ,WO.BillingCode
 ,WO.LocationType
  ,WQ.SectionNumber --case when WQ.SectionNumber is NULL then 1 else WQ.SectionNumber end as SectionNumber
  ,count(distinct WQ.WorkOrderID) as AssignedWorkQueueCount
  ,WQ.AssignedUserID 
  ,U.UserFirstName + ' ' +  U.UserLastName + ' (' + U.UserName + ')' as SearchString
	,mgp.[Percent Completed]
	,mgp.Total
	,mgp.Remaining
	,Case WHEN count(distinct InProgress.WorkOrderID) > 0 THEN 1 ELSE 0 END as InProgressFlag
	,wo.OfficeName
from 
  [dbo].[tWorkQueue] WQ left join  
  [dbo].[tWorkOrder] WO on WO.ID=WQ.workorderID
 -- Left Join [dbo].[vMapGridSectionPercentage] mgp on wo.MapGrid = mgp.MapGrid and wo.SectionNumber = mgp.SectionNumber
  Left Join [dbo].[vMapGridPercentage_SNITBCON] mgp on wo.MapGrid = mgp.MapGrid and wo.SectionNumber = mgp.SectionNumber
      and mgp.InspectionType = wo.InspectionType
	  and mgp.Billingcode = wo.BillingCode
	  and mgp.OfficeName = wo.OfficeName
  Left join (Select * from tWorkQueue where WorkQueueStatus = 101) as InProgress on wo.id = InProgress.WorkOrderID
  Left join UserTb U on U.Userid = WQ.AssignedUserID
  where WQ.WorkQueueStatus <> 102
  and WO.CompletedFlag = 0 -- added 6/29
 -- where WQ.sectionnumber is not null -- comment out if we need to display work order count for NULL sectionNumbers too
group by 
    wo.mapgrid 
	,wo.LocationType
   ,WQ.SectionNumber
    ,WQ.AssignedUserID
	,U.UserFirstName + ' ' +  U.UserLastName + ' (' + U.UserName + ')'
   ,mgp.[Percent Completed]
	,mgp.Total
	,mgp.Remaining
 ,WO.InspectionType 
 ,WO.BillingCode
 ,wo.OfficeName



GO


