USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vNotification]    Script Date: 4/23/2019 9:41:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[vNotification]
AS 

SELECT N.[ID]
      ,N.[UserID] 
	  ,u1.UserFirstName + ' ' +  u1.UserLastName + ' (' + u1.UserName + ')' [User]
      ,L.[StatusDescription] as NotificationType
     -- ,format(N.SrvDTLT, 'yyyy-mm-dd hh:mm:ss') SrvDTLT
	  ,format(N.SrvDTLT, 'yyyy-MM-dd hh:mm:ss') SrvDTLT
	   FROM [dbo].[tNotification] N 
      join [dbo].[rStatusLookup] L on N.NotificationType = L.StatusCode
	  join (Select * from  UserTb where TestUser = 0) U1 on U1.UserID = N.UserID 
	





GO


