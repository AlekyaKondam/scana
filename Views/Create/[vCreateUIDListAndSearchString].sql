USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vCreateUIDListAndSearchString]    Script Date: 4/23/2019 9:38:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/****** Object:  UserDefinedFunction [dbo].[Test3_fnCreateSearchString]    Script Date: 4/8/2019 11:34:23 AM ******/
CREATE VIEW [dbo].[vCreateUIDListAndSearchString]
AS
WITH tempdata 
     AS (SELECT mapgrid, 
                inspectiontype, 
                billingcode, 
                locationtype, 
                sectionnumber 
         FROM   [dbo].vassignedworkqueuebysectiondetails 
         GROUP  BY mapgrid, 
                   inspectiontype, 
                   billingcode, 
                   locationtype, 
                   sectionnumber) 
SELECT DISTINCT s.*, 
                Stuff ((SELECT DISTINCT ',' 
                                        + Cast(wq1.assigneduserid AS VARCHAR(20) 
                                        ) 
                        FROM   vassignedworkqueuebysectiondetails wq1 
                        WHERE  wq1.mapgrid = t.mapgrid 
                               AND wq1.inspectiontype = t.inspectiontype 
                               AND wq1.billingcode = t.billingcode 
                               AND wq1.locationtype = t.locationtype 
                               AND wq1.sectionnumber = t.sectionnumber 
                        FOR xml path('')), 1, 1, Space(0)) AS UIDList, 
                Stuff ((SELECT DISTINCT ',' + wq1.searchstring 
                        FROM   vassignedworkqueuebysectiondetails wq1 
                        WHERE  wq1.mapgrid = t.mapgrid 
                               AND wq1.inspectiontype = t.inspectiontype 
                               AND wq1.billingcode = t.billingcode 
                               AND wq1.locationtype = t.locationtype 
                               AND wq1.sectionnumber = t.sectionnumber 
                        FOR xml path('')), 1, 1, Space(0)) AS SearchString 
FROM   [dbo].vassignedworkqueuebysectiondetails t 
       INNER JOIN tempdata s 
               ON s.mapgrid = t.mapgrid 
                  AND s.inspectiontype = t.inspectiontype 
                  AND s.billingcode = t.billingcode 
                  AND s.locationtype = t.locationtype 
                  AND s.sectionnumber = t.sectionnumber 


	












GO


