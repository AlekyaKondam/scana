USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vAssignedWorkQueueByMapGrid]    Script Date: 4/23/2019 9:36:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vAssignedWorkQueueByMapGrid] 
AS 

/*****************************************************************************************************/
/*****************************************************************************************************/

/*		View Name:          vAssignedWorkQueueByMapGrid		     												 */
/*		Created By:														 */
/*		Create Date:																		 */
/*		Create Reason:		

        Modified By :		Alekya Kondam
        Modified Date:		04/2/2019	
        Modifications made : SC1415 performance tunning of vAssignedWorkQueueByMapGrid	
		                      COUNT(*) to Count(Mapgrid) 
							  changed from count(distinct WQ.WorkOrderID)  to count( WQ.WorkOrderID) 			
							  changed from count(distinct InProgress.WorkOrderID)  to count( InProgress.WorkOrderID)  */

/******************************************************************************************************/
/******************************************************************************************************/
  SELECT wo.mapgrid as MapGrid, 
         CASE 
           WHEN (SELECT Count(DISTINCT assigneduserid) 
                 FROM   tworkorder wo2, 
                        tworkqueue Wq2 
                 WHERE  wo2.mapgrid = WO.mapgrid 
                        AND WO2.billingcode = WO.billingcode 
                        AND WO2.inspectiontype = WO.inspectiontype 
                        AND WO2.officename = wo.officename 
                        AND wo2.id = wq2.workorderid 
                        AND wq2.workqueuestatus IN ( 100, 101 ) 
                        AND wo2.completedflag = 0) > 1 THEN 'MANY' 
           ELSE userfirstname + ' ' + userlastname + ' (' + username 
                + ')' 
         END                           [AssignedUser]
         --Min(compliancestart)          [ComplianceStart], 
         --Max(complianceend)            [ComplianceEnd], 
		 ,Min(ISNULL(WO.ComplianceEnd, Cast('12/31/' + Cast(Year(getdate()) as char(4)) as date))) AS [ComplianceStart]
		 ,Max(ISNULL(WO.ComplianceEnd, Cast('12/31/' + Cast(Year(getdate()) as char(4)) as date))) as [ComplianceEnd]
         ,Sum(inspectionattemptcounter) AS InspectionAttemptCounter, 
         CASE 
           WHEN ( 
                --SCC 1415 - COUNT(*) to Count(Mapgrid) 
                SELECT Count(mapgrid) 
                 FROM   [dbo].tworkorder WO1 
                 WHERE  WO1.mapgrid = WO.mapgrid 
                        AND WO1.sectionnumber IS NOT NULL) > 0 THEN 1 
           ELSE 0 
         END                           AS SectionFlag, 
         --,count(distinct WQ.WorkOrderID) as AssignedWorkOrderCount 
         --SCC-1415 - changed from count(distinct WQ.WorkOrderID)  to count( WQ.WorkOrderID) 
         
         Count(WQ.workorderid)         AS AssignedWorkOrderCount, 
         --,count(distinct InProgress.WorkOrderID) as TotalInProgress 
         --,CASE when count(distinct InProgress.WorkOrderID) > 0 THEN 1 ELSE 0 END [InProgressFlag] 
         --SCC-1415 - changed from count(distinct InProgress.WorkOrderID)  to count( InProgress.WorkOrderID) 
           CASE 
           WHEN Count(InProgress.workorderid) > 0 THEN 1 
           ELSE 0 
         END                           [InProgressFlag], 
         CASE 
           WHEN (SELECT Count(DISTINCT assigneduserid) 
                 FROM   tworkorder wo2, 
                        tworkqueue Wq2 
                 WHERE  wo2.mapgrid = WO.mapgrid 
                        AND WO2.billingcode = WO.billingcode 
                        AND WO2.inspectiontype = WO.inspectiontype 
                        AND WO2.officename = wo.officename 
                        AND wo2.id = wq2.workorderid 
                        AND wq2.workqueuestatus IN ( 100, 101 ) 
                        AND wo2.completedflag = 0) > 1 THEN 'MANY' 
           ELSE '1' 
         END                           [AssignedCount], 
         mgp.[percent completed] [Percent Completed], 
         mgp.total                     [Total], 
         mgp.remaining                 [Remaining], 
         WO.billingcode  [BillingCode]  , 
         WO.inspectiontype [InspectionType], 
         WO.officename  [OfficeName], 
         u.userfirstname [UserFirstName], 
         u.userlastname  [UserLastName], 
         u.username  [UserName]
         --,wo.MapGrid 
         , 
         wo.compliancestart            [ComplianceStartDate], 
         wo.complianceend              [CompliancenEndDate] 
  FROM   (SELECT * 
          FROM   [dbo].[tworkorder] 
          WHERE  completedflag = 0) WO 
         JOIN (SELECT * 
               FROM   [dbo].tworkqueue 
               WHERE  workqueuestatus IN ( 100, 101 )) WQ 
           ON WQ.workorderid = WO.id 
         LEFT JOIN (SELECT * 
                    FROM   [usertb]) U 
                ON WQ.assigneduserid = U.userid 
         LEFT JOIN [dbo].[vmapgridpercentage_itbcon] mgp 
                ON wo.mapgrid = mgp.mapgrid 
                   AND WO.billingcode = mgp.billingcode 
                   AND WO.inspectiontype = mgp.inspectiontype 
                   AND WO.officename = mgp.officename 
         LEFT JOIN (SELECT * 
                    FROM   tworkqueue 
                    WHERE  workqueuestatus = 101) InProgress 
                ON wo.id = InProgress.workorderid 
  /* 
   
      Where u.UserFirstName like @SearchString 
   
        or u.UserLastName  like @SearchString  
   
        or u.UserName  like @SearchString  
   
        or wo.MapGrid  like @SearchString  
   
        --- new line added below 
   
        or WO.officename like @searchstring 
   
        or WO.BillingCode like @searchstring 
   
        or WO.InspectionType like @searchstring 
   
           
   
        or Cast(wo.ComplianceStart as varchar(10)) like @SearchString  
   
        or Cast(wo.ComplianceEnd as varchar(10)) like @SearchString  
   
   
   
        */ 
  GROUP  BY wo.mapgrid, 
            mgp.[percent completed], 
            mgp.total, 
            mgp.remaining, 
            WO.billingcode, 
            WO.inspectiontype, 
            wo.officename, 
            u.userid, 
            userfirstname + ' ' + userlastname + ' (' + username 
            + ')', 
            u.userfirstname, 
            u.userlastname, 
            u.username 
            --,wo.MapGrid 
            , 
            wo.compliancestart, 
            wo.complianceend 



GO


