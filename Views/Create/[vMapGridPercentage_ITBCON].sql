USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vMapGridPercentage_ITBCON]    Script Date: 4/23/2019 9:40:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE View [dbo].[vMapGridPercentage_ITBCON]
AS
Select Total.MapGrid, Total.InspectionType,Total.Billingcode, Total.OfficeName 
     , Total.Total , ISNULL(Completed.Total, 0) [Completed]
	 , Total.Total - ISNULL(Completed.Total, 0) [Remaining]
     , Cast((Cast(ISNULL(Completed.Total, 0) as float) / Cast(Total.Total as Float)) * 100 as numeric(3,0)) [Percent Completed] 
from 
(
  select MapGrid, InspectionType,Billingcode, OfficeName, Count(*) Total 
  from tWorkOrder 
  Group By MapGrid, InspectionType,Billingcode, OfficeName
) Total
Left Join 
( select MapGrid, InspectionType,Billingcode, OfficeName ,Count(*) Total from tWorkOrder where CompletedFlag = 1 
  Group By MapGrid ,InspectionType,Billingcode, OfficeName
 ) Completed
 on Total.MapGrid = Completed.MapGrid
 and IsNull(Total.BillingCode, '0') =  IsNull(Completed.Billingcode , '0')
 and IsNull(Total.InspectionType, '0') = IsNull (Completed.InspectionType, '0') 
 and IsNull(Total.OfficeName, '0') = IsNull(Completed.officename , ' 0')

 


GO


