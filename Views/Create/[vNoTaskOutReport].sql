USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vNoTaskOutReport]    Script Date: 4/23/2019 9:41:48 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vNoTaskOutReport]
AS 
select distinct u.UserFirstName + ' ' + u.UserLastName + ' (' + u.UserName + ')' [User], LastLogin, LastAutosync, DATEDIFF(s, LastAutosync, LastLogin) [Diff]
From 
	(select Lastlogin.ActivityCreatedUserUID, Lastlogin.ActivityDate, Lastlogin.LastLogin, LastAutoSync.LastAutosync
	From
		(select ActivityCreatedUserUID, Cast(activitystarttime as date) [ActivityDate], Max(activityendtime) [LastLogin] 
		from [dbo].[ActivityTb] with (nolock)
		where Cast(activitystarttime as date) = Cast(getdate() as date)
		and ActivityTitle = 'LoginActivity'
		Group by ActivityCreatedUserUID
		, Cast(activitystarttime as date)) Lastlogin
	Left Join (select ActivityCreatedUserUID, Cast(activitystarttime as date) [ActivityDate], Max(activitystarttime) [LastAutosync] 
		from [dbo].[ActivityTb] with (nolock)
		where Cast(activitystarttime as date) = Cast(getdate() as date)
		and ActivityTitle = 'AutoSyncActivity'
		Group by ActivityCreatedUserUID
		, Cast(activitystarttime as date)) LastAutoSync on Lastlogin.ActivityCreatedUserUID = LastAutoSync.ActivityCreatedUserUID and Lastlogin.ActivityDate = LastAutoSync.ActivityDate
	Where Lastlogin.LastLogin > LastAutoSync.LastAutosync
	or LastAutoSync.LastAutosync is null) Nologout
Join (Select * from UserTb where UserName not in ('jtest')) u on u.username = ActivityCreatedUserUID
GO


