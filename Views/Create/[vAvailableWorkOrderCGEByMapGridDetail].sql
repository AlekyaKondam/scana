USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vAvailableWorkOrderCGEByMapGridDetail]    Script Date: 4/23/2019 9:38:20 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE view [dbo].[vAvailableWorkOrderCGEByMapGridDetail]
AS

SELECT 
       'Name: ' +  Case when Accountname is Null or Accountname = ' '  then 'N/A' else Accountname  end
	   + '<br>' + 
	   'Phone: ' +  case When AccountTelephoneNumber is null or AccountTelephoneNumber = '0' or AccountTelephoneNumber = '' then 'N/A' else
       '(' + substring(cast(AccountTelephoneNumber as varchar(10)),1,3)+')' + 
       ' ' + substring (cast(AccountTelephoneNumber as varchar(10)) , 4 , 3) +
           '-' + Substring (cast(AccountTelephoneNumber as varchar(10)) , 7 , 4) end 
             as [CustomerInfo],

--InspectionType as SurveyType,
InspectionType ,  -- Changed on 7/17/2017 by Gary Wheeler at the request of Michael Davis
u.UserLastName + ', ' + u.UserFirstName + ' (' + u.UserName + ')' [Inspector], 
wo.HouseNumber + ' ' + wo.Street + ' ' + wo.City [Address], 
format(i.CreatedDate, 'yyyy-MM-dd hh:mm:ss') as [InspectionDateTime],
COALESCE(e.Photo1Path, '') [Image],
wo.MapGrid,
wo.ID [WorkOrderID],
wo.SectionNumber
,  CAST(0 as BIT) as ScheduleRequired

 ---- new line added 12/2 
,WO.BillingCode
 ---- new line added 1/5/2018
 ,wo.OfficeName 

FROM 
       (Select * From [dbo].[tWorkOrder] where CompletedFlag = 0 and InspectionAttemptCounter > 0 and ID In
       (Select distinct WorkOrderID from tWorkQueue)) WO
Left Join (Select workorderId from [dbo].[tWorkQueue] where WorkQueueStatus <> 102 ) WQ On WO.id = wq.WorkOrderID
Join  
       (select MapGrid, 
       Min(ComplianceStart) [ComplianceStart], 
       Max(ComplianceEnd) [ComplianceEnd], 
       CASE WHEN Count(distinct SectionNumber) > 1 then 1 ELSE 0 END as [SectionFlag]
       from tWorkOrder
       Group By MapGrid) MapGridDetail on wo.MapGrid = MapGridDetail.MapGrid
Join (Select wq.WorkOrderID, Max(i.id) LastInspectionID
         From 
             (select * from tInspection where IsCGEFlag = 1) i
             Join tWorkQueue wq on i.WorkQueueID = wq.ID 
             Group by wq.WorkOrderID) LastInspection on wo.ID = LastInspection.WorkOrderID
Join tInspection i on i.id = LastInspection.LastInspectionID
Left Join (Select * from tEvent where EventType = 200) e on e.InspectionID = i.ID
Join UserTb u on i.CreatedBy = u.UserID
where WQ.WorkOrderID is null










GO


