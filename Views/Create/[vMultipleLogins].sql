USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vMultipleLogins]    Script Date: 4/23/2019 9:41:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create VIEW [dbo].[vMultipleLogins]
AS 

select ActivityCreatedUserUID, Count(*) [Login Count], Min(ActivitySrcDTLT) [First Login], Max(ActivitySrcDTLT) [Last Login] from ActivityTb
where ActivityTitle = 'LoginActivity' and Cast(ActivitySrcDTLT as date) = cast(getdate() as date)
Group By ActivityCreatedUserUID
Having Count(*) > 1
GO


