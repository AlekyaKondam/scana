USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vWebManagementInspectionsInspections]    Script Date: 4/23/2019 9:43:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[vWebManagementInspectionsInspections]
AS
/*****************************************************************************************************************
NAME:		[dbo].[vWebManagementInspectionsInspections]

    
HISTORY
DATE            DEVELOPER         COMMENTS
-------------   ---------------   -------------------------------------------------------------------
4-16-2019	    Alekya Kondam		SCC 1423 :  in select stament instead of using format 
                                                function used convert function for changing datetime format


***********************************************************************************************************/
Select wo.MapGrid
 ,WO.Division, WO.OfficeName --- added on 3/29
, wo.SectionNumber
, wo.ID [WorkOrderID]
--, u.UserFirstName + ' ' + u.UserLastName [Inspector]
, u.UserLastName + ', ' + u.UserFirstName + ' (' + U.UserName + ')' [Inspector]

,COALESCE(WO.HouseNumber, '')
				+ ' ' 
				+ COALESCE(WO.AptSuite, '')
				+ ' ' 
				+ COALESCE(WO.Street, '')
				+ ' ' 
				+ COALESCE(WO.City, '')
				+ ' ' 
				+ COALESCE(WO.State, '')
				+ '. ' 
				+ COALESCE(WO.Zip, '')
			AS [Address]
--SCC 1423
--, format (i.CreatedDate, 'yyyy-MM-dd hh:mm:ss') as  [InspectionDateTime]
, CONVERT(char(20),i.CreatedDate,120) as [InspectionDateTime]
, i.Latitude [InspectionLatutude]
, i.Longitude [InspectionLongitude]
, i.Photo1Path
, i.IsAdHocFlag [Adhoc]
, i.IsAOCFlag [AOC]
, i.IsCGEFlag [CGE]
, i.IsIndicationFlag
, i.ID [InspectionID]
, wo.CompletedFlag
, Case WHEN (Cast(i.IsCGEFlag as int) + Cast(i.IsAOCFlag as int) + Cast(IsIndicationFlag as int)) > 0 THEN 1 ELSE 0 END [HasEvents]
From 
(Select ID, MapGrid,
  Division, OfficeName, --- added on 3/29
 isnull(SectionNumber, 1) [SectionNumber], ComplianceStart, ComplianceEnd , Completedflag 
                 ,HouseNumber,aptsuite, street, city, state, zip from tWorkOrder where MapGrid is not null) wo
Join (Select * from tWorkQueue where WorkQueueStatus = 102) wq on wq.WorkOrderID = wo.Id
Join tInspection i on i.WorkQueueID = wq.ID
join UserTb u on i.CreatedBy = u.UserID











GO


