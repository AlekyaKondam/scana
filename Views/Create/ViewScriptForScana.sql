USE [vCAT_SCANA_GIS_DEV]
GO
/****** Object:  View [dbo].[vWebManagementInspectionsInspections]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [dbo].[vWebManagementInspectionsInspections]
AS
/*****************************************************************************************************************
NAME:		[dbo].[vWebManagementInspectionsInspections]

    
HISTORY
DATE            DEVELOPER         COMMENTS
-------------   ---------------   -------------------------------------------------------------------
4-16-2019	    Alekya Kondam		SCC 1423 :  in select stament instead of using format 
                                                function used convert function for changing datetime format


***********************************************************************************************************/
Select wo.MapGrid
 ,WO.Division, WO.OfficeName --- added on 3/29
, wo.SectionNumber
, wo.ID [WorkOrderID]
--, u.UserFirstName + ' ' + u.UserLastName [Inspector]
, u.UserLastName + ', ' + u.UserFirstName + ' (' + U.UserName + ')' [Inspector]

,COALESCE(WO.HouseNumber, '')
				+ ' ' 
				+ COALESCE(WO.AptSuite, '')
				+ ' ' 
				+ COALESCE(WO.Street, '')
				+ ' ' 
				+ COALESCE(WO.City, '')
				+ ' ' 
				+ COALESCE(WO.State, '')
				+ '. ' 
				+ COALESCE(WO.Zip, '')
			AS [Address]
--SCC 1423
--, format (i.CreatedDate, 'yyyy-MM-dd hh:mm:ss') as  [InspectionDateTime]
, CONVERT(char(20),i.CreatedDate,120) as [InspectionDateTime]
, i.Latitude [InspectionLatutude]
, i.Longitude [InspectionLongitude]
, i.Photo1Path
, i.IsAdHocFlag [Adhoc]
, i.IsAOCFlag [AOC]
, i.IsCGEFlag [CGE]
, i.IsIndicationFlag
, i.ID [InspectionID]
, wo.CompletedFlag
, Case WHEN (Cast(i.IsCGEFlag as int) + Cast(i.IsAOCFlag as int) + Cast(IsIndicationFlag as int)) > 0 THEN 1 ELSE 0 END [HasEvents]
From 
(Select ID, MapGrid,
  Division, OfficeName, --- added on 3/29
 isnull(SectionNumber, 1) [SectionNumber], ComplianceStart, ComplianceEnd , Completedflag 
                 ,HouseNumber,aptsuite, street, city, state, zip from tWorkOrder where MapGrid is not null) wo
Join (Select * from tWorkQueue where WorkQueueStatus = 102) wq on wq.WorkOrderID = wo.Id
Join tInspection i on i.WorkQueueID = wq.ID
join UserTb u on i.CreatedBy = u.UserID











GO
/****** Object:  View [dbo].[vMapGrid]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vMapGrid]
AS 

--SELECT distinct mapgrid  
-- FROM [dbo].[tWorkOrder]

select distinct mapgrid from 
[vWebManagementInspectionsInspections]


GO
/****** Object:  View [dbo].[vMapGridPercentage_ITBCON]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE View [dbo].[vMapGridPercentage_ITBCON]
AS
Select Total.MapGrid, Total.InspectionType,Total.Billingcode, Total.OfficeName 
     , Total.Total , ISNULL(Completed.Total, 0) [Completed]
	 , Total.Total - ISNULL(Completed.Total, 0) [Remaining]
     , Cast((Cast(ISNULL(Completed.Total, 0) as float) / Cast(Total.Total as Float)) * 100 as numeric(3,0)) [Percent Completed] 
from 
(
  select MapGrid, InspectionType,Billingcode, OfficeName, Count(*) Total 
  from tWorkOrder 
  Group By MapGrid, InspectionType,Billingcode, OfficeName
) Total
Left Join 
( select MapGrid, InspectionType,Billingcode, OfficeName ,Count(*) Total from tWorkOrder where CompletedFlag = 1 
  Group By MapGrid ,InspectionType,Billingcode, OfficeName
 ) Completed
 on Total.MapGrid = Completed.MapGrid
 and IsNull(Total.BillingCode, '0') =  IsNull(Completed.Billingcode , '0')
 and IsNull(Total.InspectionType, '0') = IsNull (Completed.InspectionType, '0') 
 and IsNull(Total.OfficeName, '0') = IsNull(Completed.officename , ' 0')

 


GO
/****** Object:  View [dbo].[zvAssignedWorkQueueByMapGrid2]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[zvAssignedWorkQueueByMapGrid2]
AS
SELECT wo.MapGrid
		,CASE WHEN 
						  (
						   select count(distinct AssignedUserID ) from 
						   tworkOrder wo2, tworkQueue Wq2
						   where wo2.Mapgrid = WO.Mapgrid 
						   and	WO2.BillingCode	= WO.BillingCode
					       and	WO2.InspectionType = WO.InspectionType
					       and	WO2.OfficeName = wo.OfficeName
						   and wo2.ID = wq2.WorkOrderID
						   and wq2.WorkQueueStatus in (100,101)
						   and wo2.completedflag=0
						   ) > 1 THEN  'MANY' ELSE UserFirstName + ' ' +  UserLastName + ' (' + UserName + ')'  END [AssignedUser]
			,Min(ComplianceStart) [ComplianceStart]
			,Max(ComplianceEnd) [ComplianceEnd]
			,sum(InspectionAttemptCounter) as  InspectionAttemptcounter 
			,CASE WHEN 
						  (
						   select count(*) from [dbo].tWorkOrder WO1
						   where WO1.Mapgrid = WO.Mapgrid 
						   and WO1.SectionNumber is not NULL
						   ) > 0 THEN 1 ELSE 0 END 
						   as SectionFlag
			,count(distinct WQ.WorkOrderID) as AssignedWorkOrderCount
			--,count(distinct InProgress.WorkOrderID) as TotalInProgress
			,CASE when count(distinct InProgress.WorkOrderID) > 0 THEN 1 ELSE 0 END [InProgressFlag]
			,CASE WHEN 
						  (
						   select count(distinct AssignedUserID ) from 
						   tworkOrder wo2, tworkQueue Wq2
						   where wo2.Mapgrid = WO.Mapgrid 
						      and	WO2.BillingCode	= WO.BillingCode
					       and	WO2.InspectionType = WO.InspectionType
					       and	WO2.OfficeName = wo.OfficeName
						   and wo2.ID = wq2.WorkOrderID
						   and wq2.WorkQueueStatus in (100,101)
							  and wo2.completedflag=0
						   ) > 1 THEN  'MANY' ELSE '1' END [AssignedCount]
			,mgp.[Percent Completed]
			,mgp.Total [Total]
			,mgp.Remaining [Remaining]
			,WO.BillingCode
			,WO.InspectionType
			,WO.OfficeName


			,u.UserFirstName
			,u.UserLastName
			,u.UserName
			--,wo.MapGrid
			,wo.ComplianceStart [ComplianceStartDate]
			,wo.ComplianceEnd [CompliancenEndDate]
			
		FROM (Select * From [dbo].[tWorkOrder]  where Completedflag = 0 ) WO 
			 join (select * from [dbo].tWorkQueue where WorkQueueStatus in (100,101))  WQ on WQ.Workorderid = WO.ID 
			Left join (select * from [UserTb] ) U on WQ.AssignedUserID = U.Userid 
			left join [dbo].[vMapGridPercentage_ITBCON] mgp on wo.MapGrid = mgp.MapGrid 
			and WO.BillingCode = mgp.Billingcode 
			and WO.InspectionType = mgp.InspectionType and WO.OfficeName = mgp.officename

			Left Join (Select * from tWorkQueue where WorkQueueStatus = 101) InProgress on wo.ID = InProgress.WorkOrderID

/*
		Where u.UserFirstName like @SearchString
			or u.UserLastName  like @SearchString 
			or u.UserName  like @SearchString 
			or wo.MapGrid  like @SearchString 
			--- new line added below
			or WO.officename like @searchstring
			or WO.BillingCode like @searchstring
			or WO.InspectionType like @searchstring
				
			or Cast(wo.ComplianceStart as varchar(10)) like @SearchString 
			or Cast(wo.ComplianceEnd as varchar(10)) like @SearchString 

			*/
		group by wo.Mapgrid		
			,mgp.[Percent Completed]
			, mgp.Total
			, mgp.Remaining
			,WO.BillingCode
			,WO.InspectionType
			,wo.OfficeName
			, u.UserID
			, UserFirstName + ' ' +  UserLastName + ' (' + UserName + ')' 
			
			
			,u.UserFirstName
			,u.UserLastName
			,u.UserName
			--,wo.MapGrid
			,wo.ComplianceStart
			,wo.ComplianceEnd


GO
/****** Object:  View [dbo].[vMapGridPercentage_SNITBCON]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create View [dbo].[vMapGridPercentage_SNITBCON]
AS
Select Total.MapGrid, Total.SectionNumber, Total.InspectionType,Total.Billingcode, Total.OfficeName 
     , Total.Total , ISNULL(Completed.Total, 0) [Completed]
	 , Total.Total - ISNULL(Completed.Total, 0) [Remaining]
     , Cast((Cast(ISNULL(Completed.Total, 0) as float) / Cast(Total.Total as Float)) * 100 as numeric(3,0)) [Percent Completed] 
from 
(
  select MapGrid, SectionNumber, InspectionType,Billingcode, OfficeName, Count(*) Total 
  from tWorkOrder   
  Group By MapGrid, SectionNumber, InspectionType,Billingcode, OfficeName
) Total
Left Join 
( select MapGrid, SectionNumber, InspectionType,Billingcode, OfficeName ,Count(*) Total from tWorkOrder where CompletedFlag = 1 
  Group By MapGrid ,SectionNumber, InspectionType,Billingcode, OfficeName
 ) Completed
 on Total.MapGrid = Completed.MapGrid
  and  IsNULL(Total.SectionNumber, '0') =  IsNULL(Completed.SectionNumber ,'0')
 and  IsNULL(Total.BillingCode, '0') =  IsNULL(Completed.Billingcode ,'0')
 and IsNULL(Total.InspectionType,'0') = IsNULL(Completed.InspectionType ,'0')
 and IsNULL(Total.OfficeName,'0') = IsNULL(Completed.officename ,'0')








GO
/****** Object:  View [dbo].[vAssignedWorkQueueBySectionDetails]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 --BEGIN 
CREATE VIEW [dbo].[vAssignedWorkQueueBySectionDetails]
AS
select 
   WO.MapGrid 
 ,WO.InspectionType 
 ,WO.BillingCode
 ,WO.LocationType
  ,WQ.SectionNumber --case when WQ.SectionNumber is NULL then 1 else WQ.SectionNumber end as SectionNumber
  ,count(distinct WQ.WorkOrderID) as AssignedWorkQueueCount
  ,WQ.AssignedUserID 
  ,U.UserFirstName + ' ' +  U.UserLastName + ' (' + U.UserName + ')' as SearchString
	,mgp.[Percent Completed]
	,mgp.Total
	,mgp.Remaining
	,Case WHEN count(distinct InProgress.WorkOrderID) > 0 THEN 1 ELSE 0 END as InProgressFlag
	,wo.OfficeName
from 
  [dbo].[tWorkQueue] WQ left join  
  [dbo].[tWorkOrder] WO on WO.ID=WQ.workorderID
 -- Left Join [dbo].[vMapGridSectionPercentage] mgp on wo.MapGrid = mgp.MapGrid and wo.SectionNumber = mgp.SectionNumber
  Left Join [dbo].[vMapGridPercentage_SNITBCON] mgp on wo.MapGrid = mgp.MapGrid and wo.SectionNumber = mgp.SectionNumber
      and mgp.InspectionType = wo.InspectionType
	  and mgp.Billingcode = wo.BillingCode
	  and mgp.OfficeName = wo.OfficeName
  Left join (Select * from tWorkQueue where WorkQueueStatus = 101) as InProgress on wo.id = InProgress.WorkOrderID
  Left join UserTb U on U.Userid = WQ.AssignedUserID
  where WQ.WorkQueueStatus <> 102
  and WO.CompletedFlag = 0 -- added 6/29
 -- where WQ.sectionnumber is not null -- comment out if we need to display work order count for NULL sectionNumbers too
group by 
    wo.mapgrid 
	,wo.LocationType
   ,WQ.SectionNumber
    ,WQ.AssignedUserID
	,U.UserFirstName + ' ' +  U.UserLastName + ' (' + U.UserName + ')'
   ,mgp.[Percent Completed]
	,mgp.Total
	,mgp.Remaining
 ,WO.InspectionType 
 ,WO.BillingCode
 ,wo.OfficeName



GO
/****** Object:  View [dbo].[zvAssignedWorkQueueBySectionNEW]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
SELECT ID,  abc = STUFF(
             (SELECT ',' + name 
              FROM temp1 t1
              WHERE t1.id = t2.id
              FOR XML PATH (''))
             , 1, 1, '') from temp1 t2
group by id;
*/


CREATE VIEW [dbo].[zvAssignedWorkQueueBySectionNEW]
 AS 


select 
  AQ.MapGrid 
 ,AQ.InspectionType 
 ,AQ.BillingCode
 ,AQ.LocationType
 ,AQ.SectionNumber 
 ,sum(AQ.AssignedWorkQueueCount) as AssignedWorkQueueCount
 ,CASE WHEN ( count(distinct AssignedUserID )  ) > 1 THEN 'MANY' ELSE '1' END as AssignedCount
 ,STUFF (( SELECT distinct ',' + cast(wq1.AssignedUserID as varchar(20)) 
           FROM  vAssignedWorkQueueBySectionDetails wq1
		  where wq1.MapGrid = AQ.MapGrid
		  and wq1.InspectionType = AQ.InspectionType
		  and wq1.BillingCode = AQ.BillingCode
		  and wq1.OfficeName = AQ.OfficeName
		  and ( ((wq1.sectionNumber is NULL) and (AQ.SectionNumber is NULL) )
		     or (wq1.sectionnumber = AQ.sectionnumber)  )	
            FOR XML PATH('') 
			),1,1, '') UIDList
 ,STUFF (( SELECT distinct ',' + wq1.SearchString 
           FROM  vAssignedWorkQueueBySectionDetails wq1
		  where wq1.MapGrid = AQ.MapGrid
		  and wq1.InspectionType = AQ.InspectionType
		  and wq1.BillingCode = AQ.BillingCode
		  and wq1.OfficeName = AQ.OfficeName
		  and ( ((wq1.sectionNumber is NULL) and (AQ.SectionNumber is NULL) )
		     or (wq1.sectionnumber = AQ.sectionnumber)  )	
            FOR XML PATH('') 
			),1,1, '') SearchString
 -- ,AQ.SearchString
	,AQ.[Percent Completed]
	,AQ.Total
	,AQ.Remaining
	,sum(AQ.InProgressFlag) as InProgressFlag
	,AQ.OfficeName

from vAssignedWorkQueueBySectionDetails AQ
 group by
 AQ.MapGrid 
 ,AQ.InspectionType 
 ,AQ.BillingCode 
 ,AQ.LocationType -- STUFF
  ,AQ.SectionNumber 
  --,AQ.AssignedWorkQueueCount
 -- ,AQ.SearchString
	,AQ.[Percent Completed]
	,AQ.Total
	,AQ.Remaining
	--,AQ.InProgressFlag
	,AQ.OfficeName

	/*
   wo.mapgrid 
   ,WQ.SectionNumber
   ,mgp.[Percent Completed]
	,mgp.Total
	,mgp.Remaining
 ,WO.InspectionType 
 ,WO.BillingCode
 ,wo.OfficeName
 */























GO
/****** Object:  View [dbo].[vCreateUIDListAndSearchString]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** Object:  UserDefinedFunction [dbo].[Test3_fnCreateSearchString]    Script Date: 4/8/2019 11:34:23 AM ******/
CREATE VIEW [dbo].[vCreateUIDListAndSearchString]
AS
WITH tempdata 
     AS (SELECT mapgrid, 
                inspectiontype, 
                billingcode, 
                locationtype, 
                sectionnumber 
         FROM   [dbo].vassignedworkqueuebysectiondetails 
         GROUP  BY mapgrid, 
                   inspectiontype, 
                   billingcode, 
                   locationtype, 
                   sectionnumber) 
SELECT DISTINCT s.*, 
                Stuff ((SELECT DISTINCT ',' 
                                        + Cast(wq1.assigneduserid AS VARCHAR(20) 
                                        ) 
                        FROM   vassignedworkqueuebysectiondetails wq1 
                        WHERE  wq1.mapgrid = t.mapgrid 
                               AND wq1.inspectiontype = t.inspectiontype 
                               AND wq1.billingcode = t.billingcode 
                               AND wq1.locationtype = t.locationtype 
                               AND wq1.sectionnumber = t.sectionnumber 
                        FOR xml path('')), 1, 1, Space(0)) AS UIDList, 
                Stuff ((SELECT DISTINCT ',' + wq1.searchstring 
                        FROM   vassignedworkqueuebysectiondetails wq1 
                        WHERE  wq1.mapgrid = t.mapgrid 
                               AND wq1.inspectiontype = t.inspectiontype 
                               AND wq1.billingcode = t.billingcode 
                               AND wq1.locationtype = t.locationtype 
                               AND wq1.sectionnumber = t.sectionnumber 
                        FOR xml path('')), 1, 1, Space(0)) AS SearchString 
FROM   [dbo].vassignedworkqueuebysectiondetails t 
       INNER JOIN tempdata s 
               ON s.mapgrid = t.mapgrid 
                  AND s.inspectiontype = t.inspectiontype 
                  AND s.billingcode = t.billingcode 
                  AND s.locationtype = t.locationtype 
                  AND s.sectionnumber = t.sectionnumber 


	












GO
/****** Object:  View [dbo].[vAssignedWorkQueueBySection_old]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vAssignedWorkQueueBySection_old]
 AS 

select s.MapGrid
, s.InspectionType
, s.BillingCode
, s.LocationType
, s.SectionNumber
, s.AssignedWorkQueueCount
, s.AssignedCount
, [dbo].[fnCreateSearchString](s.mapgrid, s.SectionNumber, 'ID', s.InspectionType, s.BillingCode) [UIDList]
, [dbo].[fnCreateSearchString](s.mapgrid, s.SectionNumber, 'Name', s.InspectionType, s.BillingCode) [SearchString]
, s.[Percent Completed]
, s.Total
, s.Remaining
, s.InProgressFlag
, s.OfficeName
From
(select 
  AQ.MapGrid 
 ,AQ.InspectionType 
 ,AQ.BillingCode
 ,AQ.LocationType
 ,AQ.SectionNumber 
 ,sum(AQ.AssignedWorkQueueCount) as AssignedWorkQueueCount
 ,CASE WHEN ( count(distinct AssignedUserID )  ) > 1 THEN 'MANY' ELSE '1' END as AssignedCount
 ,AQ.[Percent Completed]
	,AQ.Total
	,AQ.Remaining
	,sum(AQ.InProgressFlag) as InProgressFlag
	,AQ.OfficeName

from vAssignedWorkQueueBySectionDetails AQ
 group by
 AQ.MapGrid 
 ,AQ.InspectionType 
 ,AQ.BillingCode 
 ,AQ.LocationType -- STUFF
  ,AQ.SectionNumber 
 	,AQ.[Percent Completed]
	,AQ.Total
	,AQ.Remaining

	,AQ.OfficeName
	) s































GO
/****** Object:  View [dbo].[vMapGridPercentage]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [dbo].[vMapGridPercentage]
AS
Select Total.MapGrid, Total.Total , ISNULL(Completed.Total, 0) [Completed], Total.Total - ISNULL(Completed.Total, 0) [Remaining]
, Cast((Cast(ISNULL(Completed.Total, 0) as float) / Cast(Total.Total as Float)) * 100 as numeric(3,0)) [Percent Completed] from 
(select MapGrid, Count(*) Total from tWorkOrder Group By MapGrid) Total
Left Join (select MapGrid, Count(*) Total from tWorkOrder where CompletedFlag = 1 Group By MapGrid) Completed on Total.MapGrid = Completed.MapGrid

GO
/****** Object:  View [dbo].[vWebManagementInspectionsByMapGrid]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vWebManagementInspectionsByMapGrid]
AS

Select wo.MapGrid
, Min(wo.ComplianceStart) [ComplianceStart]
, max(wo.ComplianceEnd) [ComplianceEnd]
, count(*) [TotalInspections] 
, mgp.[Percent Completed] [PercentageComplete]
From 
tWorkOrder wo
Join (Select * from tWorkQueue where WorkQueueStatus = 102) wq on wq.WorkOrderID = wo.ID
left Join [dbo].[vMapGridPercentage] mgp on mgp.MapGrid = wo.MapGrid
where wo.MapGrid is not null
Group by wo.MapGrid, mgp.[Percent Completed]
GO
/****** Object:  View [dbo].[vMapGridSectionPercentage]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[vMapGridSectionPercentage]
AS

Select Total.MapGrid, Total.SectionNumber,
Total.Total
, ISNULL(Completed.Total, 0) [Completed]
, Total.Total - ISNULL(Completed.Total, 0) [Remaining]
, Cast((Cast(ISNULL(Completed.Total, 0) as float) / Cast(Total.Total as Float)) * 100 as numeric(5,0)) [Percent Completed] 
from 
	(select MapGrid, SectionNumber, Count(*) Total 
	from tWorkOrder where CHARINDEX('Adhoc', ClientWorkOrderID) = 0   Group By MapGrid, SectionNumber) Total
Left Join 
	(select MapGrid, SectionNumber, Count(*) Total 
	from tWorkOrder where CompletedFlag = 1 and CHARINDEX('Adhoc', ClientWorkOrderID) = 0 Group By MapGrid, SectionNumber) Completed on Total.MapGrid = Completed.MapGrid and total.SectionNumber = completed.SectionNumber






GO
/****** Object:  View [dbo].[vWebManagementInspectionsByMapGridSectionNumber]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vWebManagementInspectionsByMapGridSectionNumber]
AS

Select wo.MapGrid
, wo.SectionNumber
, Min(wo.ComplianceStart) [ComplianceStart]
, max(wo.ComplianceEnd) [ComplianceEnd]
, count(*) [TotalInspections] 
, mgp.[Percent Completed] [PercentageComplete]
From 
(Select ID, MapGrid, isnull(SectionNumber, 1) [SectionNumber], ComplianceStart, ComplianceEnd from tWorkOrder) wo
Join (Select * from tWorkQueue where WorkQueueStatus = 102) wq on wq.WorkOrderID = wo.ID
left Join [dbo].[vMapGridSectionPercentage] mgp on mgp.MapGrid = wo.MapGrid and mgp.SectionNumber = wo.SectionNumber
where wo.MapGrid is not null
Group by wo.MapGrid, wo.SectionNumber, mgp.[Percent Completed]
GO
/****** Object:  View [dbo].[vAssignedWorkQueueByMapGrid]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vAssignedWorkQueueByMapGrid] 
AS 

/*****************************************************************************************************/
/*****************************************************************************************************/

/*		View Name:          vAssignedWorkQueueByMapGrid		     												 */
/*		Created By:														 */
/*		Create Date:																		 */
/*		Create Reason:		

        Modified By :		Alekya Kondam
        Modified Date:		04/2/2019	
        Modifications made : SC1415 performance tunning of vAssignedWorkQueueByMapGrid	
		                      COUNT(*) to Count(Mapgrid) 
							  changed from count(distinct WQ.WorkOrderID)  to count( WQ.WorkOrderID) 			
							  changed from count(distinct InProgress.WorkOrderID)  to count( InProgress.WorkOrderID)  */

/******************************************************************************************************/
/******************************************************************************************************/
  SELECT wo.mapgrid as MapGrid, 
         CASE 
           WHEN (SELECT Count(DISTINCT assigneduserid) 
                 FROM   tworkorder wo2, 
                        tworkqueue Wq2 
                 WHERE  wo2.mapgrid = WO.mapgrid 
                        AND WO2.billingcode = WO.billingcode 
                        AND WO2.inspectiontype = WO.inspectiontype 
                        AND WO2.officename = wo.officename 
                        AND wo2.id = wq2.workorderid 
                        AND wq2.workqueuestatus IN ( 100, 101 ) 
                        AND wo2.completedflag = 0) > 1 THEN 'MANY' 
           ELSE userfirstname + ' ' + userlastname + ' (' + username 
                + ')' 
         END                           [AssignedUser]
         --Min(compliancestart)          [ComplianceStart], 
         --Max(complianceend)            [ComplianceEnd], 
		 ,Min(ISNULL(WO.ComplianceEnd, Cast('12/31/' + Cast(Year(getdate()) as char(4)) as date))) AS [ComplianceStart]
		 ,Max(ISNULL(WO.ComplianceEnd, Cast('12/31/' + Cast(Year(getdate()) as char(4)) as date))) as [ComplianceEnd]
         ,Sum(inspectionattemptcounter) AS InspectionAttemptCounter, 
         CASE 
           WHEN ( 
                --SCC 1415 - COUNT(*) to Count(Mapgrid) 
                SELECT Count(mapgrid) 
                 FROM   [dbo].tworkorder WO1 
                 WHERE  WO1.mapgrid = WO.mapgrid 
                        AND WO1.sectionnumber IS NOT NULL) > 0 THEN 1 
           ELSE 0 
         END                           AS SectionFlag, 
         --,count(distinct WQ.WorkOrderID) as AssignedWorkOrderCount 
         --SCC-1415 - changed from count(distinct WQ.WorkOrderID)  to count( WQ.WorkOrderID) 
         
         Count(WQ.workorderid)         AS AssignedWorkOrderCount, 
         --,count(distinct InProgress.WorkOrderID) as TotalInProgress 
         --,CASE when count(distinct InProgress.WorkOrderID) > 0 THEN 1 ELSE 0 END [InProgressFlag] 
         --SCC-1415 - changed from count(distinct InProgress.WorkOrderID)  to count( InProgress.WorkOrderID) 
           CASE 
           WHEN Count(InProgress.workorderid) > 0 THEN 1 
           ELSE 0 
         END                           [InProgressFlag], 
         CASE 
           WHEN (SELECT Count(DISTINCT assigneduserid) 
                 FROM   tworkorder wo2, 
                        tworkqueue Wq2 
                 WHERE  wo2.mapgrid = WO.mapgrid 
                        AND WO2.billingcode = WO.billingcode 
                        AND WO2.inspectiontype = WO.inspectiontype 
                        AND WO2.officename = wo.officename 
                        AND wo2.id = wq2.workorderid 
                        AND wq2.workqueuestatus IN ( 100, 101 ) 
                        AND wo2.completedflag = 0) > 1 THEN 'MANY' 
           ELSE '1' 
         END                           [AssignedCount], 
         mgp.[percent completed] [Percent Completed], 
         mgp.total                     [Total], 
         mgp.remaining                 [Remaining], 
         WO.billingcode  [BillingCode]  , 
         WO.inspectiontype [InspectionType], 
         WO.officename  [OfficeName], 
         u.userfirstname [UserFirstName], 
         u.userlastname  [UserLastName], 
         u.username  [UserName]
         --,wo.MapGrid 
         , 
         wo.compliancestart            [ComplianceStartDate], 
         wo.complianceend              [CompliancenEndDate] 
  FROM   (SELECT * 
          FROM   [dbo].[tworkorder] 
          WHERE  completedflag = 0) WO 
         JOIN (SELECT * 
               FROM   [dbo].tworkqueue 
               WHERE  workqueuestatus IN ( 100, 101 )) WQ 
           ON WQ.workorderid = WO.id 
         LEFT JOIN (SELECT * 
                    FROM   [usertb]) U 
                ON WQ.assigneduserid = U.userid 
         LEFT JOIN [dbo].[vmapgridpercentage_itbcon] mgp 
                ON wo.mapgrid = mgp.mapgrid 
                   AND WO.billingcode = mgp.billingcode 
                   AND WO.inspectiontype = mgp.inspectiontype 
                   AND WO.officename = mgp.officename 
         LEFT JOIN (SELECT * 
                    FROM   tworkqueue 
                    WHERE  workqueuestatus = 101) InProgress 
                ON wo.id = InProgress.workorderid 
  /* 
   
      Where u.UserFirstName like @SearchString 
   
        or u.UserLastName  like @SearchString  
   
        or u.UserName  like @SearchString  
   
        or wo.MapGrid  like @SearchString  
   
        --- new line added below 
   
        or WO.officename like @searchstring 
   
        or WO.BillingCode like @searchstring 
   
        or WO.InspectionType like @searchstring 
   
           
   
        or Cast(wo.ComplianceStart as varchar(10)) like @SearchString  
   
        or Cast(wo.ComplianceEnd as varchar(10)) like @SearchString  
   
   
   
        */ 
  GROUP  BY wo.mapgrid, 
            mgp.[percent completed], 
            mgp.total, 
            mgp.remaining, 
            WO.billingcode, 
            WO.inspectiontype, 
            wo.officename, 
            u.userid, 
            userfirstname + ' ' + userlastname + ' (' + username 
            + ')', 
            u.userfirstname, 
            u.userlastname, 
            u.username 
            --,wo.MapGrid 
            , 
            wo.compliancestart, 
            wo.complianceend 



GO
/****** Object:  View [dbo].[vAssignedWorkQueueBySection]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vAssignedWorkQueueBySection] 
AS 
  SELECT s.MapGrid, 
         s.InspectionType, 
         s.BillingCode, 
         s.LocationType, 
         s.SectionNumber, 
         s.AssignedWorkQueueCount, 
         s.AssignedCount 
         -- SCC-1425  because of performance issues below code is commented 
         --, [dbo].[fnCreateSearchString](s.mapgrid, s.SectionNumber, 'ID', s.InspectionType, s.BillingCode) [UIDList] 
         --, [dbo].[fnCreateSearchString](s.mapgrid, s.SectionNumber, 'Name', s.InspectionType, s.BillingCode) [SearchString] 
         , 
         t.UidList [UIDList], 
         t.SearchString, 
         s.[Percent Completed], 
         s.Total, 
         s.Remaining, 
         s.InProgressFlag, 
         s.OfficeName 
  FROM   (SELECT AQ.mapgrid, 
                 AQ.inspectiontype, 
                 AQ.billingcode, 
                 AQ.locationtype, 
                 AQ.sectionnumber, 
                 Sum(AQ.assignedworkqueuecount) AS AssignedWorkQueueCount, 
                 CASE 
                   WHEN ( Count(DISTINCT assigneduserid) ) > 1 THEN 'MANY' 
                   ELSE '1' 
                 END                            AS AssignedCount, 
                 AQ.[percent completed], 
                 AQ.total, 
                 AQ.remaining, 
                 Sum(AQ.inprogressflag)         AS InProgressFlag, 
                 AQ.officename 
          FROM   vassignedworkqueuebysectiondetails AQ 
          GROUP  BY AQ.mapgrid, 
                    AQ.inspectiontype, 
                    AQ.billingcode, 
                    AQ.locationtype -- STUFF 
                    , 
                    AQ.sectionnumber, 
                    AQ.[percent completed], 
                    AQ.total, 
                    AQ.remaining, 
                    AQ.officename) s 
         -- SCC-1425  instead of using [dbo].[fnCreateSearchString] below code is used for getting columns UIDList and SearchString 
         INNER JOIN [dbo].[vcreateuidlistandsearchstring] t 
                 ON t.mapgrid = s.mapgrid 
                    AND t.sectionnumber = s.sectionnumber 
                    AND t.inspectiontype = s.inspectiontype 
                    AND t.billingcode = s.billingcode 
                    AND t.locationtype = s.locationtype 



GO
/****** Object:  View [dbo].[vAssignedWorkQueue]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE View [dbo].[vAssignedWorkQueue]
AS
	SELECT 
	    WQ.ID [WorkQueueID],
        WO.ID [WorkOrderID],
        WO.ClientWorkOrderID, 
		WO.CreatedBy,
		WO.ModifiedBy,
		WO.CreatedDateTime,
		WO.ModifiedDateTime,
		WO.InspectionType,
		WO.HouseNumber,
		WO.Street,
		WO.AptSuite,
		WO.City,
		WO.State,
		WO.Zip,
		WO.MeterNumber,
		WO.MeterLocationDesc,
		WO.LocationType,
		WO.LocationLatitude,
		WO.LocationLongitude,
		WO.MapGrid,
		WO.ComplianceStart,
		WO.ComplianceEnd,
		WO.MapLatitudeBegin,
		WO.MapLongitudeBegin,
		WO.MapLatitudeEnd,
		WO.MapLongitudeEnd,
		WO.AccountNumber,
		WO.AccountName,
		WO.AccountTelephoneNumber,
		WO.Comments,
		WO.CompletedFlag,
		WO.CompletedDate,
		WO.InspectionAttemptCounter,
		WO.SequenceNumber,
		WO.SectionNumber,
		WO.Shape,
			     ---- new line added 12/2
		 WO.BillingCode,
          ------ new line added 12/2 

		WQ.WorkQueueStatus ,
       u1.UserFirstName + ' ' +  u1.UserLastName + ' (' + u1.UserName + ')' [AssignedTo], 
	   u2.UserFirstName + ' ' +  u2.UserLastName + ' (' + u2.UserName + ')'  [AssignedBy],
	   WQ.CreatedBy [AssignedByID],
	   WQ.AssignedUserID [AssignedToID],
	   RTRIM(LTRIM(COALESCE(WO.HouseNumber, ''))) + ' ' + RTRIM(LTRIM(COALESCE(WO.AptSuite, ''))) + 
		case when RTRIM(LTRIM(COALESCE(WO.AptSuite, ''))) = '' then '' else ' ' end + 
		RTRIM(LTRIM(COALESCE(WO.Street, ''))) + ', ' + RTRIM(LTRIM(COALESCE(wo.City, ''))) [Address]
		,wo.MeterLocation
		,wo.PipelineFootage
		,wo.SpecialInstructions
		,wo.OfficeName
		,wo.InspectionAttemptCounter [AttemptCounter]
		,wq.ScheduledDispatchDate
    FROM (Select * From [dbo].[tWorkOrder] where CompletedFlag = 0) WO 
	join (Select * from tWorkQueue where WorkQueueStatus  in (100, 101)) WQ on WO.[ID] = WQ.WorkOrderID
	Left Join (Select * from [UserTb] where UserActiveFlag = 1) u1 on wq.AssignedUserID = u1.UserID
	Left Join (Select * from [UserTb] where UserActiveFlag = 1) u2 on wq.CreatedBy = u2.UserID












GO
/****** Object:  View [dbo].[vAvailableWorkOrder]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE View [dbo].[vAvailableWorkOrder] 
 As

SELECT 
        WO.ID [WorkOrderID],
        WO.ClientWorkOrderID, 
		WO.CreatedBy,
		WO.ModifiedBy,
		WO.CreatedDateTime,
		WO.ModifiedDateTime,
		WO.InspectionType,
		WO.HouseNumber,
		WO.Street,
		WO.AptSuite,
		WO.City,
		WO.State,
		WO.Zip,
		WO.MeterNumber,
		WO.MeterLocationDesc,
		WO.LocationType,
		WO.LocationLatitude,
		WO.LocationLongitude,
		WO.MapGrid,
		WO.ComplianceStart,
		WO.ComplianceEnd,
		WO.MapLatitudeBegin,
		WO.MapLongitudeBegin,
		WO.MapLatitudeEnd,
		WO.MapLongitudeEnd,
		WO.AccountNumber,
		WO.AccountName,
		WO.AccountTelephoneNumber,
		WO.Comments,
		WO.CompletedFlag,
		WO.CompletedDate,
		WO.InspectionAttemptCounter,
		WO.SequenceNumber,
		WO.SectionNumber,
		WO.Shape,
	     ---- new line added 12/2 
		 WO.BillingCode
          ------ new line added 12/2 
		,RTRIM(LTRIM(COALESCE(WO.HouseNumber, ''))) + ' ' + RTRIM(LTRIM(COALESCE(WO.AptSuite, ''))) + 
		case when RTRIM(LTRIM(COALESCE(WO.AptSuite, ''))) = '' then '' else ' ' end + 
		RTRIM(LTRIM(COALESCE(WO.Street, ''))) + ', ' + RTRIM(LTRIM(COALESCE(wo.City, ''))) [Address]
		,wo.OfficeName
    FROM (Select * From [dbo].[tWorkOrder] where CompletedFlag = 0 and InspectionAttemptCounter = 0) wo
	Left Join (Select * From tWorkQueue where WorkQueueStatus <> 102) wq on wo.ID = wq.WorkOrderID
	Where wq.id is null
	












GO
/****** Object:  View [dbo].[vAvailableWorkOrderByMapGrid]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************************************************/
/*****************************************************************************************************/

/*		View Name:          vAvailableWorkOrderByMapGrid		     												 */
/*		Created By:														 */
/*		Create Date:																		 */
/*		Create Reason:		

        Modified By :		Alekya Kondam
        Modified Date:		04/1/2019	
        Modifications made : SC1411 performance tunning of vAvailableWorkOrderByMapGrid	
		                     changed count(distinct WO.ID) as AvailableWorkOrderCount
							 to  count( WO.ID) as AvailableWorkOrderCount			 */


/******************************************************************************************************/
/******************************************************************************************************/

CREATE View [dbo].[vAvailableWorkOrderByMapGrid] 
 As 
 
SELECT WO.MapGrid   

 ---- new line added 12/1
 ,WO.InspectionType 
  ------ new line added 12/1 

,Min(ISNULL(WO.ComplianceEnd, Cast('12/31/' + Cast(Year(getdate()) as char(4)) as date))) AS [ComplianceStart]
,Max(ISNULL(WO.ComplianceEnd, Cast('12/31/' + Cast(Year(getdate()) as char(4)) as date))) as [ComplianceEnd]
, CASE WHEN Count(distinct WO.SectionNumber) > 1 then 1 ELSE 0 END as [SectionFlag]
--, WO.SectionNumber as [SectionFlag]
--, count(distinct WO.ID) as AvailableWorkOrderCount commented for SC1411
, count( WO.ID) as AvailableWorkOrderCount
,wo.Frequency
,wo.Division
,wo.BillingCode
,wo.OfficeName
FROM 
	(Select * From [dbo].[tWorkOrder] where CompletedFlag = 0  and ISNULL(EventIndicator, 0) <> 2) WO 
	Left Join (Select workorderId from [dbo].[tWorkQueue] where WorkQueueStatus <> 102 ) WQ On WO.id = wq.WorkOrderID
Where wq.WorkOrderID is null
group by  WO.MapGrid
,wo.Frequency
,wo.Division
,wo.BillingCode --, WO.SectionNumber

------ new line added 12/1 
 ,WO.InspectionType 
-- ---- new line added 12/1 
,wo.OfficeName

  ---Using new view ABOVE created by Gary as he made changes in DOM staga and PROD by eliminating InspectionAttemptCounter



---Old View BELOW which uses InspectionAttemptCounter . 
--CREATE View [dbo].[vAvailableWorkOrderByMapGrid] 
-- As 
--SELECT MapGrid   
--,ComplianceStart,ComplianceEnd, InspectionAttemptCounter
--   ,CASE WHEN 
--      (
--       select count(*) from [dbo].tWorkorder WO1
--	   where WO1.Mapgrid = WO.Mapgrid 
--	   and WO1.SectionNumber is not NULL
--       ) > 0 THEN 1 ELSE 0 END 
--	   as SectionFlag
--	  , count(distinct WO.ID) as AvailableWorkOrderCount
--    FROM 
--	(Select * From [dbo].[tWorkOrder] where CompletedFlag = 0) WO 
--	where WO.Id not in (Select workorderId from [dbo].[tWorkQueue] where WorkQueueStatus <> 102 )
--	--left join 
-- --  (select * from  [ScctTemplate].[dbo].[tWorkQueue] where WorkQueueStatus not in (100,101)) WQ on WO.ID=WQ.workorderID
--  group by Mapgrid --, sectionFlag
--  ,ComplianceStart,ComplianceEnd, InspectionAttemptCounter

  






--GO













GO
/****** Object:  View [dbo].[vAvailableWorkOrderBySection]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vAvailableWorkOrderBySection] 
AS 
  SELECT mapgrid 
         --  ---- new line added 12/1  
         , 
         inspectiontype, 
         billingcode 
         ------ new line added 12/1  
         , 
         Stuff ((SELECT DISTINCT ', ' + locationtype 
                 FROM   tworkorder wo2 
                 WHERE  wo2.mapgrid = mapgrid 
                        AND wo2.sectionnumber = sectionnumber 
                 FOR xml path('')), 1, 1, '') LocationType, 
         sectionnumber 
         -- SCC1425 removed distinct from count( WO.ID)  
         , 
         Count(id)                            AS AvailableWorkOrderCount, 
         officename 
  FROM 
  -- comment out if we need to display work order count for NULL sectionNumbers too 
  (SELECT * 
   FROM   [dbo].[tworkorder] 
   WHERE  completedflag = 0 
          AND Isnull(eventindicator, 0) <> 2 
          AND sectionnumber IS NOT NULL 
          AND id NOT IN (SELECT workorderid 
                         FROM   [dbo].[tworkqueue] 
                         WHERE  workqueuestatus <> 102))a 
  --SCC1425 rewritten below code to above two lines  
  --    (select * from [dbo].[tWorkOrder] where completedflag = 0 and ISNULL(EventIndicator, 0) <> 2) WO 
  --where  
  --  WO.sectionnumber is not null -- comment out if we need to display work order count for NULL sectionNumbers too 
  --  and WO.ID not in ( Select Workorderid from [dbo].[tWorkQueue] where WorkQueueStatus <> 102) 
  GROUP  BY mapgrid, 
            sectionnumber 
            ------ new line added 12/1  
            , 
            inspectiontype, 
            billingcode 
            ------ new line added 12/1  
            , 
            officename 
GO
/****** Object:  View [dbo].[vAvailableWorkOrderCGEByMapGrid]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[vAvailableWorkOrderCGEByMapGrid]
AS 
SELECT wo.MapGrid,  MapGridDetail.ComplianceStart, MapGridDetail.ComplianceEnd, CASE WHEN Count(distinct SectionNumber) > 1 then 1 ELSE 0 END as [SectionFlag], Count(*) AvailableWorkOrderCount,  CAST(0 as BIT) as ScheduleRequired
	     ---- new line added 12/2 
		 ,WO.BillingCode
		 ,WO.InspectionType
          ------ new line added 1/5/2017
		  ,wo.OfficeName 
FROM 
	(Select * From [dbo].[tWorkOrder] where CompletedFlag = 0 and InspectionAttemptCounter > 0 and ID In
	 (Select distinct WorkOrderID from tWorkQueue)) WO
Left Join (Select workorderId from [dbo].[tWorkQueue] where WorkQueueStatus <> 102 ) WQ On WO.id = wq.WorkOrderID
Join  
	(select MapGrid, 
	Min(ComplianceStart) [ComplianceStart], 
	Max(ComplianceEnd) [ComplianceEnd], 
	CASE WHEN Count(distinct SectionNumber) > 1 then 1 ELSE 0 END as [SectionFlag]
	from tWorkOrder
	Group By MapGrid) MapGridDetail on wo.MapGrid = MapGridDetail.MapGrid
where WQ.WorkOrderID is null
Group By wo.MapGrid, MapGridDetail.ComplianceStart, MapGridDetail.ComplianceEnd --, MapGridDetail.SectionFlag
	     ---- new line added 12/2 
		 ,WO.BillingCode
		 ,WO.InspectionType
          ------ new line added 1/5/2017
		  ,wo.OfficeName 








GO
/****** Object:  View [dbo].[vAvailableWorkOrderCGEByMapGridDetail]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE view [dbo].[vAvailableWorkOrderCGEByMapGridDetail]
AS

SELECT 
       'Name: ' +  Case when Accountname is Null or Accountname = ' '  then 'N/A' else Accountname  end
	   + '<br>' + 
	   'Phone: ' +  case When AccountTelephoneNumber is null or AccountTelephoneNumber = '0' or AccountTelephoneNumber = '' then 'N/A' else
       '(' + substring(cast(AccountTelephoneNumber as varchar(10)),1,3)+')' + 
       ' ' + substring (cast(AccountTelephoneNumber as varchar(10)) , 4 , 3) +
           '-' + Substring (cast(AccountTelephoneNumber as varchar(10)) , 7 , 4) end 
             as [CustomerInfo],

--InspectionType as SurveyType,
InspectionType ,  -- Changed on 7/17/2017 by Gary Wheeler at the request of Michael Davis
u.UserLastName + ', ' + u.UserFirstName + ' (' + u.UserName + ')' [Inspector], 
wo.HouseNumber + ' ' + wo.Street + ' ' + wo.City [Address], 
format(i.CreatedDate, 'yyyy-MM-dd hh:mm:ss') as [InspectionDateTime],
COALESCE(e.Photo1Path, '') [Image],
wo.MapGrid,
wo.ID [WorkOrderID],
wo.SectionNumber
,  CAST(0 as BIT) as ScheduleRequired

 ---- new line added 12/2 
,WO.BillingCode
 ---- new line added 1/5/2018
 ,wo.OfficeName 

FROM 
       (Select * From [dbo].[tWorkOrder] where CompletedFlag = 0 and InspectionAttemptCounter > 0 and ID In
       (Select distinct WorkOrderID from tWorkQueue)) WO
Left Join (Select workorderId from [dbo].[tWorkQueue] where WorkQueueStatus <> 102 ) WQ On WO.id = wq.WorkOrderID
Join  
       (select MapGrid, 
       Min(ComplianceStart) [ComplianceStart], 
       Max(ComplianceEnd) [ComplianceEnd], 
       CASE WHEN Count(distinct SectionNumber) > 1 then 1 ELSE 0 END as [SectionFlag]
       from tWorkOrder
       Group By MapGrid) MapGridDetail on wo.MapGrid = MapGridDetail.MapGrid
Join (Select wq.WorkOrderID, Max(i.id) LastInspectionID
         From 
             (select * from tInspection where IsCGEFlag = 1) i
             Join tWorkQueue wq on i.WorkQueueID = wq.ID 
             Group by wq.WorkOrderID) LastInspection on wo.ID = LastInspection.WorkOrderID
Join tInspection i on i.id = LastInspection.LastInspectionID
Left Join (Select * from tEvent where EventType = 200) e on e.InspectionID = i.ID
Join UserTb u on i.CreatedBy = u.UserID
where WQ.WorkOrderID is null










GO
/****** Object:  View [dbo].[vAvailableWorkOrderCGEByWODetails]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[vAvailableWorkOrderCGEByWODetails]
AS
select u.UserLastName + ', ' + u.UserFirstName + ' (' + u.UserName + ')' [Inspector], 
wo.HouseNumber + ' ' + wo.Street + ' ' + wo.City [Address], 
i.CreatedDate [InspectionDateTime], 
COALESCE(e.Photo1Path, '') [Image],
wo.ID
     ---- new line added 12/2 
		 ,WO.BillingCode
		 ,WO.InspectionType
       ------ new line added 1/5/2018 
	   ,wo.OfficeName

From
tWorkOrder wo
Join tWorkQueue wq on wo.id = wq.WorkOrderID
Join tInspection  i on i.WorkQueueID = wq.ID
Left Join (Select * from tEvent where EventType = 200) e on e.InspectionID = i.ID
Join UserTb u on i.CreatedBy = u.UserID




GO
/****** Object:  View [dbo].[vEarlyFinishTaskOut]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vEarlyFinishTaskOut]
AS 
--select u.UserFirstName + ' ' + u.UserLastName [Surveyor], Cast(cast(ActivitySrcDTLT as time) as varchar(8)) [ActivityTime]
--from ActivityTb a
--Join UserTb u on  CASE WHEN ISNUMERIC(A.ActivityCreatedUserUID) = 1 THEN Cast(u.UserID as varchar(10)) ELSE u.UserName END  = A.ActivityCreatedUserUID
--where ActivityTitle = 'TaskOutActivity' 
--and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
--and cast(ActivitySrcDTLT as time) < '4:00 PM'


select u.UserFirstName + ' ' + u.UserLastName [Surveyor], max(Cast(cast(ActivitySrcDTLT as time) as varchar(8))) [ActivityTime]
from ActivityTb a
Join (Select * From UserTb where TestUser = 0) u on  CASE WHEN ISNUMERIC(A.ActivityCreatedUserUID) = 1 THEN Cast(u.UserID as varchar(10)) ELSE u.UserName END  = A.ActivityCreatedUserUID
where ActivityTitle = 'TaskOutActivity' 
and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
and cast(ActivitySrcDTLT as time) < '4:00 PM'
group by u.UserFirstName + ' ' + u.UserLastName










GO
/****** Object:  View [dbo].[vInActiveUsers]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[vInActiveUsers]
AS

Select UserID, UserName, UserLastName + ', ' + UserFirstName [Name], UserAppRoleType from UserTb where UserActiveFlag = 0
GO
/****** Object:  View [dbo].[vLastWeekInsertErrors]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [dbo].[vLastWeekInsertErrors]
AS

select Cast(svrdtlt as date) [Date], ErrorNumber, Count(*) [Error Count] 
from [dbo].[tTabletJSONDataInsertError] 
where Cast(svrdtlt as date) between cast(DateAdd(d, - (DatePart(dw, dateadd(wk,-1,getdate())) - 1), dateadd(wk,-1,getdate())) as date)
	and cast(DateAdd(d, 7 - DatePart(dw, dateadd(wk,-1,getdate())), dateadd(wk,-1,getdate())) as date)
group by Cast(svrdtlt as date), ErrorNumber
GO
/****** Object:  View [dbo].[vLateFinishTaskOut]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vLateFinishTaskOut]
AS 
select u.UserFirstName + ' ' + u.UserLastName [Surveyor], Cast(cast(ActivitySrcDTLT as time) as varchar(8)) [ActivityTime]
from ActivityTb a
--join usertb u on cast(a.ActivityCreatedUserUID as varchar(50)) = cast(UserID as varchar(50))
Join (Select * From UserTb where TestUser = 0) u on CASE WHEN ISNUMERIC(A.ActivityCreatedUserUID) = 1 THEN Cast(u.UserID as varchar(10)) ELSE u.UserName END  = A.ActivityCreatedUserUID
where ActivityTitle = 'TaskOutActivity' 
and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
and cast(ActivitySrcDTLT as time) > '5:00 PM'







GO
/****** Object:  View [dbo].[vMapGridSectionALLPercentage]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[vMapGridSectionALLPercentage]
AS
Select Total.MapGrid, Total.SectionNumber,Total.InspectionType,
Total.Total
, ISNULL(Completed.Total, 0) [Completed]
, Total.Total - ISNULL(Completed.Total, 0) [Remaining]
, Cast((Cast(ISNULL(Completed.Total, 0) as float) / Cast(Total.Total as Float)) * 100 as numeric(5,0)) [Percent Completed] 
from 
	(select distinct MapGrid, SectionNumber,InspectionType, Count(*) Total 
	from tWorkOrder where CHARINDEX('Adhoc', ClientWorkOrderID) = 0  
	 Group By MapGrid, SectionNumber,InspectionType) Total
Left Join 
	(select MapGrid, SectionNumber, InspectionType,Count(*) Total 
	from tWorkOrder where CompletedFlag = 1 and CHARINDEX('Adhoc', ClientWorkOrderID) = 0 Group By MapGrid, SectionNumber,InspectionType) Completed
	 on Total.MapGrid = Completed.MapGrid and total.SectionNumber = completed.SectionNumber




GO
/****** Object:  View [dbo].[vMapGridSectionInspBillingPercentage]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



Create View [dbo].[vMapGridSectionInspBillingPercentage]
AS

Select Total.MapGrid, Total.SectionNumber, Total.InspectionType, Total.BillingCode,
Total.Total
, ISNULL(Completed.Total, 0) [Completed]
, Total.Total - ISNULL(Completed.Total, 0) [Remaining]
, Cast((Cast(ISNULL(Completed.Total, 0) as float) / Cast(Total.Total as Float)) * 100 as numeric(5,0)) [Percent Completed] 
from 
	(select MapGrid, SectionNumber, InspectionType, BillingCode, Count(*) Total 
	from tWorkOrder where CHARINDEX('Adhoc', ClientWorkOrderID) = 0   Group By MapGrid, SectionNumber, InspectionType, BillingCode) Total
Left Join 
	(select MapGrid, SectionNumber, InspectionType, BillingCode, Count(*) Total 
	from tWorkOrder where CompletedFlag = 1 and CHARINDEX('Adhoc', ClientWorkOrderID) = 0 Group By MapGrid, SectionNumber, InspectionType, BillingCode) Completed 
	   on Total.MapGrid = Completed.MapGrid 
	      and total.SectionNumber = completed.SectionNumber
		  and total.InspectionType = completed.InspectionType
		  and total.BillingCode = Completed.BillingCode







GO
/****** Object:  View [dbo].[vMultipleLogins]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create VIEW [dbo].[vMultipleLogins]
AS 

select ActivityCreatedUserUID, Count(*) [Login Count], Min(ActivitySrcDTLT) [First Login], Max(ActivitySrcDTLT) [Last Login] from ActivityTb
where ActivityTitle = 'LoginActivity' and Cast(ActivitySrcDTLT as date) = cast(getdate() as date)
Group By ActivityCreatedUserUID
Having Count(*) > 1
GO
/****** Object:  View [dbo].[vNoBCAfterLogin]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vNoBCAfterLogin]
AS 


/*

the end datetime on the between must be set 15 (-15) minutes prior to when this will be called.



*/
Select u1.UserLastName + ', ' + u1.UserFirstName + ' (' + u1.UserName + ')' [TechName], LoginDatetime
From
(
	Select UserName, LoginDatetime, Min(BreadcrumbSrcDTLT) FirstBC
	From
	(
		Select la.ActivityCreatedUserUID, u.UserName, la.LoginDatetime, bc.BreadcrumbSrcDTLT
		From
		(
			select ActivityCreatedUserUID, Min(ActivitySrcDTLT) LoginDatetime 
			from ActivityTb 
			where ActivitySrcDTLT between dateadd(n, -45, getdate()) and dateadd(n, -15, getdate()) and ActivityTitle = 'LoginActivity' 
			Group By ActivityCreatedUserUID
		) la
		Join (Select * from UserTb
			Where UserName Not In
			(
				'jtest'
			)
			) U on u.UserName = ActivityCreatedUserUID
		Left Join BreadcrumbTb bc on bc.BreadcrumbCreatedUserUID = u.UserName and bc.BreadcrumbSrcDTLT > la.LoginDatetime
	) BCandActivity
	Group by ActivityCreatedUserUID,  UserName, LoginDatetime
) Source
Left Join UserTb u1 on Source.UserName = u1.UserName
Where DateDiff(n, LoginDatetime, ISNULL(FirstBC, Dateadd(n, 15, LoginDatetime))) > 10



GO
/****** Object:  View [dbo].[vNoTaskOut]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vNoTaskOut]
AS 

Select 
U.userlastname + ', ' + u.userfirstname + ' (' + username + ')' [User]
From
	(select ActivityCreatedUserUID, Max(ActivitySrcDTLT) [LastActivity] from [dbo].[ActivityTb] 
	where Activitytitle  in ('IndicationActivity','InspectionActivity','SurveyActivity')
	and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
	Group By ActivityCreatedUserUID) [LastLogin]
Left Join
	(select ActivityCreatedUserUID, Max(ActivitySrcDTLT) [LastTaskOut] from [dbo].[ActivityTb] 
	where Activitytitle  = 'TaskOutActivity' 
	and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
	Group By ActivityCreatedUserUID) [LastTaskOut] on [LastLogin].ActivityCreatedUserUID = [LastTaskOut].ActivityCreatedUserUID 
Left Join UserTb u on [LastLogin].ActivityCreatedUserUID = u.username
where 
[LastTaskOut].LastTaskOut is NULL
or [LastLogin].LastActivity > DateAdd(SECOND,30, [LastTaskOut].LastTaskOut)
GO
/****** Object:  View [dbo].[vNoTaskOutReport]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vNoTaskOutReport]
AS 
select distinct u.UserFirstName + ' ' + u.UserLastName + ' (' + u.UserName + ')' [User], LastLogin, LastAutosync, DATEDIFF(s, LastAutosync, LastLogin) [Diff]
From 
	(select Lastlogin.ActivityCreatedUserUID, Lastlogin.ActivityDate, Lastlogin.LastLogin, LastAutoSync.LastAutosync
	From
		(select ActivityCreatedUserUID, Cast(activitystarttime as date) [ActivityDate], Max(activityendtime) [LastLogin] 
		from [dbo].[ActivityTb] with (nolock)
		where Cast(activitystarttime as date) = Cast(getdate() as date)
		and ActivityTitle = 'LoginActivity'
		Group by ActivityCreatedUserUID
		, Cast(activitystarttime as date)) Lastlogin
	Left Join (select ActivityCreatedUserUID, Cast(activitystarttime as date) [ActivityDate], Max(activitystarttime) [LastAutosync] 
		from [dbo].[ActivityTb] with (nolock)
		where Cast(activitystarttime as date) = Cast(getdate() as date)
		and ActivityTitle = 'AutoSyncActivity'
		Group by ActivityCreatedUserUID
		, Cast(activitystarttime as date)) LastAutoSync on Lastlogin.ActivityCreatedUserUID = LastAutoSync.ActivityCreatedUserUID and Lastlogin.ActivityDate = LastAutoSync.ActivityDate
	Where Lastlogin.LastLogin > LastAutoSync.LastAutosync
	or LastAutoSync.LastAutosync is null) Nologout
Join (Select * from UserTb where UserName not in ('jtest')) u on u.username = ActivityCreatedUserUID
GO
/****** Object:  View [dbo].[vNotification]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[vNotification]
AS 

SELECT N.[ID]
      ,N.[UserID] 
	  ,u1.UserFirstName + ' ' +  u1.UserLastName + ' (' + u1.UserName + ')' [User]
      ,L.[StatusDescription] as NotificationType
     -- ,format(N.SrvDTLT, 'yyyy-mm-dd hh:mm:ss') SrvDTLT
	  ,format(N.SrvDTLT, 'yyyy-MM-dd hh:mm:ss') SrvDTLT
	   FROM [dbo].[tNotification] N 
      join [dbo].[rStatusLookup] L on N.NotificationType = L.StatusCode
	  join (Select * from  UserTb where TestUser = 0) U1 on U1.UserID = N.UserID 
	





GO
/****** Object:  View [dbo].[vRptCompletedWorkOrders]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO












CREATE View [dbo].[vRptCompletedWorkOrders]
AS

select distinct
    --WO.ID,
	--wo.ModifiedBy,
    U.UserLastName + ',' +  U.UserFirstName + ' (' + U.UserName + ')' + ',' + cast(COALESCE(wo.ModifiedBy, wo.createdby) as varchar(10)) as TechName
   ,WO.InspectionType
   ,COALESCE(WO.HouseNumber, '')
	+ ' ' 
	+ COALESCE(WO.AptSuite, '')
	+ ' ' 
	+ COALESCE(WO.Street, '')
	+ ' ' 
	+ COALESCE(WO.City, '')
	+ ' ' 
	+ COALESCE(WO.State, '')
	+ '. ' 
	+ COALESCE(WO.Zip, '') AS [Address]
    ,wo.MeterNumber
   
   
   ,WO.Mapgrid
	,WO.ComplianceStart
   ,WO.ComplianceEnd
   
   --,WO.CompletedDate
   ,format(WO.CompletedDate, 'yyyy-MM-dd hh:mm:ss') as CompletedDate
   ,WO.InspectionAttemptCounter
 --  ,WQ.AssignedUserID [CompletedBy]
FROM (Select * From [dbo].[tWorkOrder] where CompletedFlag = 1) WO 
--left join (Select * from tWorkQueue where WorkQueueStatus=102) WQ 
--left join (select * from [UserTb] where UserActiveFlag = 1 ) U on COALESCE(wo.ModifiedBy, wo.createdby) = U.Userid 
left join (select * from [UserTb]) U on COALESCE(wo.ModifiedBy, wo.createdby) = U.Userid  -- Where removed by Gary Wheeler on 10/6/2017
--on WO.Id = WQ.workorderId
--order by WO.MapGrid 
--       , WO.CompletedDate

  











GO
/****** Object:  View [dbo].[vTodayEarlyLogin]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE View [dbo].[vTodayEarlyLogin]
AS
select u.UserFirstName + ' ' + u.UserLastName [Surveyor], Cast(cast(Min(ActivitySrcDTLT) as time) as varchar(8)) [LoginTime]
from ActivityTb a
Join (Select * From UserTb where TestUser = 0) u on CASE WHEN ISNUMERIC(A.ActivityCreatedUserUID) = 1 THEN Cast(u.UserID as varchar(10)) ELSE u.UserName END  = A.ActivityCreatedUserUID
where ActivityTitle = 'LoginActivity' 
and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
and cast(ActivitySrcDTLT as time) < '8:00 AM'
Group By u.UserFirstName + ' ' + u.UserLastName







GO
/****** Object:  View [dbo].[vTodayLateLogin]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[vTodayLateLogin]
AS
select u.UserFirstName + ' ' + u.UserLastName [Surveyor], Cast(cast(a.FirstLogIn as time) as varchar(8)) [LoginTime]
from (Select ActivityCreatedUserUID, min(ActivitySrcDTLT) [FirstLogIn] 
	From ActivityTb 
	where ActivityTitle = 'LoginActivity' and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
	Group By ActivityCreatedUserUID) a
Join (Select * From UserTb where TestUser = 0) u on CASE WHEN ISNUMERIC(A.ActivityCreatedUserUID) = 1 THEN Cast(u.UserID as varchar(10)) ELSE u.UserName END  = A.ActivityCreatedUserUID
and cast(a.FirstLogIn as time) > '9:00 AM'





GO
/****** Object:  View [dbo].[vUsers]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vUsers]
AS 
select

       UserID
	  ,[UserName]
	  ,[UserFirstName]
      ,[UserLastName]
      ,UserEmployeeType
	  ,UserAppRoleType 
	
  FROM [dbo].[UserTb]
  where UserActiveFlag = 1



GO
/****** Object:  View [dbo].[vWebManagementBreadCrumbs]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vWebManagementBreadCrumbs]
AS

select
bc.BreadcrumbMapPlat [MapGrid]
,u.UserFirstName + ' ' + u.UserLastName + ' (' + u.UserName + ')' [User]
,bc.BreadcrumbLatitude [Latitude] 
,bc.BreadcrumbLongitude [Longitude]
From BreadcrumbTb bc
Join UserTb u on bc.BreadcrumbCreatedUserUID = u.UserName

GO
/****** Object:  View [dbo].[vWebManagementInspectionsEvents]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE View [dbo].[vWebManagementInspectionsEvents]
AS

select  
sl.StatusDescription [EventType]
,Case 
	WHEN sl.StatusDescription = 'AOC' THEN e.ACGrade
	WHEN sl.StatusDescription = 'CGE' THEN e.CGEReason
	WHEN sl.StatusDescription = 'Indication' THEN 'Leak #: ' + Coalesce(e.LeakNumber, '') + '|'+ 'Grade: ' + ' ' + Coalesce(LeakGrade, '')  + ' ' + Coalesce(LeakAboveOrBelow, '')
	ELSE ''
END [Reason]
,Coalesce(e.Photo1Path, '') [Photo]
, Coalesce(e.Comments, '') [Comments]
, e.InspectionID
from tEvent e with (Nolock)
join [dbo].[rStatusLookup] sl with (Nolock) on e.EventType = sl.StatusCode
Join (select i.* from tInspection i with (Nolock)
		Join tWorkQueue WQ with (Nolock) on i.WorkQueueID = WQ.ID
		Join tWorkOrder WO with (Nolock) on WQ.WorkOrderID = WO.id) i on e.InspectionID = i.ID
where e.DeletedFlag = 0







--GO






GO
/****** Object:  View [dbo].[vWebManagementMapView]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[vWebManagementMapView]
AS

select
wo.ID,
wo.ClientWorkOrderID,
wo.LocationType [AssetType],
COALESCE(WO.HouseNumber, '')
+ ' ' 
+ COALESCE(WO.AptSuite, '')
+ ' ' 
+ COALESCE(WO.Street, '')
+ ' ' 
+ COALESCE(WO.City, '')
+ ' ' 
+ COALESCE(WO.State, '')
+ '. ' 
+ COALESCE(WO.Zip, '')  as Address,
wo.MapGrid,
wo.CompletedFlag,
0 as [LineID],
0 as [SegmentID],
0 as [VerticeID],
wo.LocationLatitude [Latitude],
wo.LocationLongitude [Longitude],
COALESCE(i.id, 0) as [InspectionID],
CASE WHEN i.id is NULL THEN 0 ELSE dbo.fnGetDistance(wo.LocationLatitude, wo.LocationLongitude, i.Latitude, i.Longitude, 'FEET') END as [Distance],
CASE WHEN i.id is NULL THEN 'N/A' ELSE 
	CASE WHEN dbo.fnGetDistance(wo.LocationLatitude, wo.LocationLongitude, i.Latitude, i.Longitude, 'FEET') <=30 THEN 'Yes' ELSE 'No' END END as [Verified]
from 
(Select * from tWorkOrder where LocationType in ('Service Location', 'Inactive Service', 'Adhoc Service Location')) wo
Left Join (select WorkOrderID, Max(ID) WorkQueueID from tWorkQueue 
		where WorkQueueStatus = 102 group by WorkOrderID) wq on wq.WorkOrderID = wo.ID
Left Join tInspection i on i.WorkQueueID = wq.WorkQueueID


Union Select


wo.ID,
wo.ClientWorkOrderID,
wo.LocationType [AssetType],
COALESCE(WO.HouseNumber, '')
+ ' ' 
+ COALESCE(WO.AptSuite, '')
+ ' ' 
+ COALESCE(WO.Street, '')
+ ' ' 
+ COALESCE(WO.City, '')
+ ' ' 
+ COALESCE(WO.State, '')
+ '. ' 
+ COALESCE(WO.Zip, '')  as Address,
wo.MapGrid,
wo.CompletedFlag,
p.LineID as [LineID],
p.Segment_ID as [SegmentID],
p.VerticeID as [VerticeID],
p.Latitude [Latutide],
p.Longitude [Longitude],
COALESCE(p.id, 0) as [InspectionID],
p.Distance as [Distance],
CASE WHEN p.Verified = 1 THEN 'Yes' ELSE 'No' END   as [Verified]
from 
(Select * from tWorkOrder where  LocationType in ('Pipeline', 'Adhoc Pipeline', 'Gas Main', 'Gas Main Celenese', 'Gas Main PEG')) wo
Left Join (select WorkOrderID, Max(ID) WorkQueueID from tWorkQueue 
		where WorkQueueStatus = 102 group by WorkOrderID) wq on wq.WorkOrderID = wo.ID
Left Join tInspection i on i.WorkQueueID = wq.WorkQueueID
Left Join [dbo].[tPipelinePoints] p on p.WorkOrderID = wo.id



GO
/****** Object:  View [dbo].[vZipMapGridPercentage]    Script Date: 4/24/2019 9:30:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[vZipMapGridPercentage]
AS
Select 
Total.Zip, 
Total.MapGrid, 
Total.Total , 
ISNULL(Completed.Total, 0) [Completed], 
Total.Total - ISNULL(Completed.Total, 0) [Remaining]
, Cast((Cast(ISNULL(Completed.Total, 0) as float) / Cast(Total.Total as Float)) * 100 as numeric(3,0)) [Percent Completed] 
from 
(select zip,MapGrid, Count(*) Total from tWorkOrder Group By zip,MapGrid ) Total
Left Join 
(select zip,MapGrid, Count(*) Total from tWorkOrder where CompletedFlag = 1 
Group By zip,MapGrid) Completed on 
Total.MapGrid = Completed.MapGrid and 
--Total.zip = Completed.zip
((Total.zip is NULL and Completed.zip is NULL) or (Total.zip = Completed.zip))




GO
