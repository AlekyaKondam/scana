USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vInActiveUsers]    Script Date: 4/23/2019 9:39:03 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE View [dbo].[vInActiveUsers]
AS

Select UserID, UserName, UserLastName + ', ' + UserFirstName [Name], UserAppRoleType from UserTb where UserActiveFlag = 0
GO


