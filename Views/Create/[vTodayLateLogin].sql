USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vTodayLateLogin]    Script Date: 4/23/2019 9:42:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE View [dbo].[vTodayLateLogin]
AS
select u.UserFirstName + ' ' + u.UserLastName [Surveyor], Cast(cast(a.FirstLogIn as time) as varchar(8)) [LoginTime]
from (Select ActivityCreatedUserUID, min(ActivitySrcDTLT) [FirstLogIn] 
	From ActivityTb 
	where ActivityTitle = 'LoginActivity' and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
	Group By ActivityCreatedUserUID) a
Join (Select * From UserTb where TestUser = 0) u on CASE WHEN ISNUMERIC(A.ActivityCreatedUserUID) = 1 THEN Cast(u.UserID as varchar(10)) ELSE u.UserName END  = A.ActivityCreatedUserUID
and cast(a.FirstLogIn as time) > '9:00 AM'





GO


