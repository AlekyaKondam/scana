USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vMapGridSectionInspBillingPercentage]    Script Date: 4/23/2019 9:40:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




Create View [dbo].[vMapGridSectionInspBillingPercentage]
AS

Select Total.MapGrid, Total.SectionNumber, Total.InspectionType, Total.BillingCode,
Total.Total
, ISNULL(Completed.Total, 0) [Completed]
, Total.Total - ISNULL(Completed.Total, 0) [Remaining]
, Cast((Cast(ISNULL(Completed.Total, 0) as float) / Cast(Total.Total as Float)) * 100 as numeric(5,0)) [Percent Completed] 
from 
	(select MapGrid, SectionNumber, InspectionType, BillingCode, Count(*) Total 
	from tWorkOrder where CHARINDEX('Adhoc', ClientWorkOrderID) = 0   Group By MapGrid, SectionNumber, InspectionType, BillingCode) Total
Left Join 
	(select MapGrid, SectionNumber, InspectionType, BillingCode, Count(*) Total 
	from tWorkOrder where CompletedFlag = 1 and CHARINDEX('Adhoc', ClientWorkOrderID) = 0 Group By MapGrid, SectionNumber, InspectionType, BillingCode) Completed 
	   on Total.MapGrid = Completed.MapGrid 
	      and total.SectionNumber = completed.SectionNumber
		  and total.InspectionType = completed.InspectionType
		  and total.BillingCode = Completed.BillingCode







GO


