USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vMapGridSectionPercentage]    Script Date: 4/23/2019 9:40:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE View [dbo].[vMapGridSectionPercentage]
AS

Select Total.MapGrid, Total.SectionNumber,
Total.Total
, ISNULL(Completed.Total, 0) [Completed]
, Total.Total - ISNULL(Completed.Total, 0) [Remaining]
, Cast((Cast(ISNULL(Completed.Total, 0) as float) / Cast(Total.Total as Float)) * 100 as numeric(5,0)) [Percent Completed] 
from 
	(select MapGrid, SectionNumber, Count(*) Total 
	from tWorkOrder where CHARINDEX('Adhoc', ClientWorkOrderID) = 0   Group By MapGrid, SectionNumber) Total
Left Join 
	(select MapGrid, SectionNumber, Count(*) Total 
	from tWorkOrder where CompletedFlag = 1 and CHARINDEX('Adhoc', ClientWorkOrderID) = 0 Group By MapGrid, SectionNumber) Completed on Total.MapGrid = Completed.MapGrid and total.SectionNumber = completed.SectionNumber






GO


