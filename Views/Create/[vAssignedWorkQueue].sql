USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vAssignedWorkQueue]    Script Date: 4/23/2019 9:36:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE View [dbo].[vAssignedWorkQueue]
AS
	SELECT 
	    WQ.ID [WorkQueueID],
        WO.ID [WorkOrderID],
        WO.ClientWorkOrderID, 
		WO.CreatedBy,
		WO.ModifiedBy,
		WO.CreatedDateTime,
		WO.ModifiedDateTime,
		WO.InspectionType,
		WO.HouseNumber,
		WO.Street,
		WO.AptSuite,
		WO.City,
		WO.State,
		WO.Zip,
		WO.MeterNumber,
		WO.MeterLocationDesc,
		WO.LocationType,
		WO.LocationLatitude,
		WO.LocationLongitude,
		WO.MapGrid,
		WO.ComplianceStart,
		WO.ComplianceEnd,
		WO.MapLatitudeBegin,
		WO.MapLongitudeBegin,
		WO.MapLatitudeEnd,
		WO.MapLongitudeEnd,
		WO.AccountNumber,
		WO.AccountName,
		WO.AccountTelephoneNumber,
		WO.Comments,
		WO.CompletedFlag,
		WO.CompletedDate,
		WO.InspectionAttemptCounter,
		WO.SequenceNumber,
		WO.SectionNumber,
		WO.Shape,
			     ---- new line added 12/2
		 WO.BillingCode,
          ------ new line added 12/2 

		WQ.WorkQueueStatus ,
       u1.UserFirstName + ' ' +  u1.UserLastName + ' (' + u1.UserName + ')' [AssignedTo], 
	   u2.UserFirstName + ' ' +  u2.UserLastName + ' (' + u2.UserName + ')'  [AssignedBy],
	   WQ.CreatedBy [AssignedByID],
	   WQ.AssignedUserID [AssignedToID],
	   RTRIM(LTRIM(COALESCE(WO.HouseNumber, ''))) + ' ' + RTRIM(LTRIM(COALESCE(WO.AptSuite, ''))) + 
		case when RTRIM(LTRIM(COALESCE(WO.AptSuite, ''))) = '' then '' else ' ' end + 
		RTRIM(LTRIM(COALESCE(WO.Street, ''))) + ', ' + RTRIM(LTRIM(COALESCE(wo.City, ''))) [Address]
		,wo.MeterLocation
		,wo.PipelineFootage
		,wo.SpecialInstructions
		,wo.OfficeName
		,wo.InspectionAttemptCounter [AttemptCounter]
		,wq.ScheduledDispatchDate
    FROM (Select * From [dbo].[tWorkOrder] where CompletedFlag = 0) WO 
	join (Select * from tWorkQueue where WorkQueueStatus  in (100, 101)) WQ on WO.[ID] = WQ.WorkOrderID
	Left Join (Select * from [UserTb] where UserActiveFlag = 1) u1 on wq.AssignedUserID = u1.UserID
	Left Join (Select * from [UserTb] where UserActiveFlag = 1) u2 on wq.CreatedBy = u2.UserID












GO


