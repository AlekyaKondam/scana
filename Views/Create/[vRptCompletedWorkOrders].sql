USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vRptCompletedWorkOrders]    Script Date: 4/23/2019 9:42:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO













CREATE View [dbo].[vRptCompletedWorkOrders]
AS

select distinct
    --WO.ID,
	--wo.ModifiedBy,
    U.UserLastName + ',' +  U.UserFirstName + ' (' + U.UserName + ')' + ',' + cast(COALESCE(wo.ModifiedBy, wo.createdby) as varchar(10)) as TechName
   ,WO.InspectionType
   ,COALESCE(WO.HouseNumber, '')
	+ ' ' 
	+ COALESCE(WO.AptSuite, '')
	+ ' ' 
	+ COALESCE(WO.Street, '')
	+ ' ' 
	+ COALESCE(WO.City, '')
	+ ' ' 
	+ COALESCE(WO.State, '')
	+ '. ' 
	+ COALESCE(WO.Zip, '') AS [Address]
    ,wo.MeterNumber
   
   
   ,WO.Mapgrid
	,WO.ComplianceStart
   ,WO.ComplianceEnd
   
   --,WO.CompletedDate
   ,format(WO.CompletedDate, 'yyyy-MM-dd hh:mm:ss') as CompletedDate
   ,WO.InspectionAttemptCounter
 --  ,WQ.AssignedUserID [CompletedBy]
FROM (Select * From [dbo].[tWorkOrder] where CompletedFlag = 1) WO 
--left join (Select * from tWorkQueue where WorkQueueStatus=102) WQ 
--left join (select * from [UserTb] where UserActiveFlag = 1 ) U on COALESCE(wo.ModifiedBy, wo.createdby) = U.Userid 
left join (select * from [UserTb]) U on COALESCE(wo.ModifiedBy, wo.createdby) = U.Userid  -- Where removed by Gary Wheeler on 10/6/2017
--on WO.Id = WQ.workorderId
--order by WO.MapGrid 
--       , WO.CompletedDate

  











GO


