USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vTodayEarlyLogin]    Script Date: 4/23/2019 9:42:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE View [dbo].[vTodayEarlyLogin]
AS
select u.UserFirstName + ' ' + u.UserLastName [Surveyor], Cast(cast(Min(ActivitySrcDTLT) as time) as varchar(8)) [LoginTime]
from ActivityTb a
Join (Select * From UserTb where TestUser = 0) u on CASE WHEN ISNUMERIC(A.ActivityCreatedUserUID) = 1 THEN Cast(u.UserID as varchar(10)) ELSE u.UserName END  = A.ActivityCreatedUserUID
where ActivityTitle = 'LoginActivity' 
and cast(ActivitySrcDTLT as date) = Cast(getdate() as date)
and cast(ActivitySrcDTLT as time) < '8:00 AM'
Group By u.UserFirstName + ' ' + u.UserLastName







GO


