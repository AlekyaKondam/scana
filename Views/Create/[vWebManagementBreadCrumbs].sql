USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vWebManagementBreadCrumbs]    Script Date: 4/23/2019 9:42:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create View [dbo].[vWebManagementBreadCrumbs]
AS

select
bc.BreadcrumbMapPlat [MapGrid]
,u.UserFirstName + ' ' + u.UserLastName + ' (' + u.UserName + ')' [User]
,bc.BreadcrumbLatitude [Latitude] 
,bc.BreadcrumbLongitude [Longitude]
From BreadcrumbTb bc
Join UserTb u on bc.BreadcrumbCreatedUserUID = u.UserName

GO


