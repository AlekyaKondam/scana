USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vAvailableWorkOrderBySection]    Script Date: 4/23/2019 9:37:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vAvailableWorkOrderBySection] 
AS 
  SELECT mapgrid 
         --  ---- new line added 12/1  
         , 
         inspectiontype, 
         billingcode 
         ------ new line added 12/1  
         , 
         Stuff ((SELECT DISTINCT ', ' + locationtype 
                 FROM   tworkorder wo2 
                 WHERE  wo2.mapgrid = mapgrid 
                        AND wo2.sectionnumber = sectionnumber 
                 FOR xml path('')), 1, 1, '') LocationType, 
         sectionnumber 
         -- SCC1425 removed distinct from count( WO.ID)  
         , 
         Count(id)                            AS AvailableWorkOrderCount, 
         officename 
  FROM 
  -- comment out if we need to display work order count for NULL sectionNumbers too 
  (SELECT * 
   FROM   [dbo].[tworkorder] 
   WHERE  completedflag = 0 
          AND Isnull(eventindicator, 0) <> 2 
          AND sectionnumber IS NOT NULL 
          AND id NOT IN (SELECT workorderid 
                         FROM   [dbo].[tworkqueue] 
                         WHERE  workqueuestatus <> 102))a 
  --SCC1425 rewritten below code to above two lines  
  --    (select * from [dbo].[tWorkOrder] where completedflag = 0 and ISNULL(EventIndicator, 0) <> 2) WO 
  --where  
  --  WO.sectionnumber is not null -- comment out if we need to display work order count for NULL sectionNumbers too 
  --  and WO.ID not in ( Select Workorderid from [dbo].[tWorkQueue] where WorkQueueStatus <> 102) 
  GROUP  BY mapgrid, 
            sectionnumber 
            ------ new line added 12/1  
            , 
            inspectiontype, 
            billingcode 
            ------ new line added 12/1  
            , 
            officename 
GO


