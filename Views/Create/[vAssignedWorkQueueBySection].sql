USE [vCAT_SCANA_GIS_DEV]
GO

/****** Object:  View [dbo].[vAssignedWorkQueueBySection]    Script Date: 4/23/2019 9:36:47 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vAssignedWorkQueueBySection] 
AS 
  SELECT s.MapGrid, 
         s.InspectionType, 
         s.BillingCode, 
         s.LocationType, 
         s.SectionNumber, 
         s.AssignedWorkQueueCount, 
         s.AssignedCount 
         -- SCC-1425  because of performance issues below code is commented 
         --, [dbo].[fnCreateSearchString](s.mapgrid, s.SectionNumber, 'ID', s.InspectionType, s.BillingCode) [UIDList] 
         --, [dbo].[fnCreateSearchString](s.mapgrid, s.SectionNumber, 'Name', s.InspectionType, s.BillingCode) [SearchString] 
         , 
         t.UidList [UIDList], 
         t.SearchString, 
         s.[Percent Completed], 
         s.Total, 
         s.Remaining, 
         s.InProgressFlag, 
         s.OfficeName 
  FROM   (SELECT AQ.mapgrid, 
                 AQ.inspectiontype, 
                 AQ.billingcode, 
                 AQ.locationtype, 
                 AQ.sectionnumber, 
                 Sum(AQ.assignedworkqueuecount) AS AssignedWorkQueueCount, 
                 CASE 
                   WHEN ( Count(DISTINCT assigneduserid) ) > 1 THEN 'MANY' 
                   ELSE '1' 
                 END                            AS AssignedCount, 
                 AQ.[percent completed], 
                 AQ.total, 
                 AQ.remaining, 
                 Sum(AQ.inprogressflag)         AS InProgressFlag, 
                 AQ.officename 
          FROM   vassignedworkqueuebysectiondetails AQ 
          GROUP  BY AQ.mapgrid, 
                    AQ.inspectiontype, 
                    AQ.billingcode, 
                    AQ.locationtype -- STUFF 
                    , 
                    AQ.sectionnumber, 
                    AQ.[percent completed], 
                    AQ.total, 
                    AQ.remaining, 
                    AQ.officename) s 
         -- SCC-1425  instead of using [dbo].[fnCreateSearchString] below code is used for getting columns UIDList and SearchString 
         INNER JOIN [dbo].[vcreateuidlistandsearchstring] t 
                 ON t.mapgrid = s.mapgrid 
                    AND t.sectionnumber = s.sectionnumber 
                    AND t.inspectiontype = s.inspectiontype 
                    AND t.billingcode = s.billingcode 
                    AND t.locationtype = s.locationtype 



GO


